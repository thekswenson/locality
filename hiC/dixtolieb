#!/usr/bin/env python
# Krister Swenson                                                Winter 2012-13
"""
Rename the given heatmaps according to their columns and rows.
"""
import sys

import os
import re
import argparse


### Constants:


### Flags:




#        _________________________
#_______/        Classes          \_____________________________________________





#        _________________________
#_______/        Functions        \_____________________________________________




#        _________________________
#_______/    Code Graveyard       \_____________________________________________






################################################################################
################################################################################

def main():

  desc = 'Convert a Dixon style file to a Lieberman-Aiden style file.'
  parser = argparse.ArgumentParser(description=desc)
  parser.add_argument('WINSIZE', type=int, help="The window size.")
  parser.add_argument('HEATMAP', nargs='+',
                      help='a Dixon heatmap')
  parser.add_argument('-c', '--chromosome',
                      help="The chromosome for this file.")
  parser.add_argument('-n', '--not-zero-ending',
                      help='If adding the headers, write 0-99 instead\
                      of 0-100 for the window location')

  args = parser.parse_args()

  files = args.HEATMAP
  winsize = args.WINSIZE
  chrom = args.chromosome

  if args.not_zero_ending:
    ending = 1
  else:
    ending = 0

#        ______________________
#_______/    Do Everything     \________________________________________________


  chromre = re.compile(r'.*chr(\w+)')

  for mfile in files:
    path, filename = os.path.split(mfile)
    with open(mfile) as f:
      matrix = []
      headings = []
      line = f.readline()

      locInFile = 'chr' in line

      f.seek(0)
      counter = 0

      m = chromre.match(mfile)
      if(m):
        chrom = 'chr'+str(m.group(1))

      for line in f:
        row = line.rstrip().split('\t')
        if locInFile:
          #h = ['Hi-C|hg00|', row[0], ':', row[1], '-', row[2]]
          #headings.append(''.join(h))
          h = 'Hi-C|hg00|{}:{}-{}'.format(row[0], row[1], row[2])
          headings.append(h)
          matrix.append(row[3:])
        else:
          if(not chrom):
            print('WARNING: null chromosome.')

          start = counter * winsize
          end = ((counter + 1) * winsize) - ending
          #h = ['Hi-C|hg00|', chrom, ':', str(start), '-', str(end)]
          #headings.append(''.join(h))
          h = 'Hi-C|hg00|{}:{}-{}'.format(chrom, start, end)
          headings.append(h)
          matrix.append(row)
          counter += 1

    if(not path):
      path = '.'
    writefile = path+'/'+filename+'.LA.txt'
    with open(writefile, 'w') as f:
      line1 = '#file converted to Lieberman-Aiden format from Dixon format\n'
      line2 = '\t'+'\t'.join(headings)+'\n'
      f.write(line1+line2)
      for head,row in zip(headings, matrix):
        f.write(head+'\t'.join(row)+'\n')



if __name__ == "__main__":
  main()
