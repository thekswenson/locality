#!/usr/bin/env python
# Krister Swenson                                                Summer 2014
"""
Fill the NULL diagonal of the matrices with averages from the neighboring cells.
"""
import sys
import os
import argparse
import numpy as np

from local_rearrangement.heatmapfuncs import readHeatmapAsMatrix, writeMatrix



### Constants:

FILL_HEAD = '(filled)'
NAME_SUFF = 'filled'

### Flags:



#        _________________________
#_______/        Classes          \_____________________________________________


class intraSymmetry(Exception):
  def __init__(self, x, y):
    self.x = x
    self.y = y


#        _________________________
#_______/        Functions        \_____________________________________________







def fillDiag(mat):
  """
  Given a matrix, fill the None diagonal using the maximum of neighboring
  values.  If the whole row or column is None then just leave it None.

  @param mat:   the numpy matrix
  """
  rows = mat.shape[0]
  cols = mat.shape[1]
  assert(rows == cols)

  c = -1   #Find the first non-NULL diagonal.
  while(emptyDiagonal(mat, c+1)):
    c += 1

          #Visit the upper diagonals until we find a non-NULL diagonal:
  assert(c != cols)
  nullrows = getNullRows(mat)
  while(c >= 0):
    assert(mat[c][0] == None)
    if(0 not in nullrows and c not in nullrows):
      mat[0][c] = mat[c][0] = mat[0][c+1]
    if(rows-c-1 not in nullrows and rows-1 not in nullrows):
      mat[rows-c-1][rows-1] = mat[rows-1][rows-c-1] = mat[rows-c-2][rows-1]

    for i in range(1,cols-c-1):
      j = i+c
      assert(mat[j][i] == None)
      if(i not in nullrows and j not in nullrows):
        mat[i][j] = mat[j][i] = max(mat[i-1][j],mat[i][j+1])
        #mat[i][j]  = mat[j][i] = 1.11111111
    c -= 1


def getNullRows(mat):
  """
  Return a set of indices marking the null rows of the given matrix.
  """
  return set([i for i in range(mat.shape[0]) if isNullRow(mat,i)])

def isNullRow(mat, i):
  """
  Return True if the row is all None.
  """
  return next((i for i in mat[i] if i != None), None) == None

 
def emptyDiagonal(mat, offset):
  """
  Return True if the diagonal at the given offset has no values.
  """
  return not next((i for i in np.diagonal(mat, offset) if i != None), False)


#        _________________________
#_______/    Code Graveyard       \_____________________________________________






################################################################################
################################################################################

def main():

  desc = 'Fill NULL diagonals with nearby values. Just pass interchromosomal'+\
         'matrices through without changing them.'
  parser = argparse.ArgumentParser(description=desc)
  parser.add_argument('MAT_FILE', nargs='+',
                      help='matrix file in lieberman-aiden format')
  args = parser.parse_args()

  filelist = args.MAT_FILE






#        ______________________
#_______/    Do Everything     \________________________________________________


  for mfile in filelist:
    mat, rowchr, colchr, head, rownames, colnames = readHeatmapAsMatrix(mfile)

    if(rowchr == colchr):
      try:
        fillDiag(mat)
      except intraSymmetry as e:
        print('ERROR: assymetric heatmap values for chromosome '+\
              '{} at {},{}: {} != {}'.format(rowchr, e.x, e.y, mat[e.x][e.y],
                                                               mat[e.y][e.x]))

    filename, extension = os.path.splitext(mfile)
    newfile = filename+'.'+NAME_SUFF+extension

    writeMatrix(mat, newfile, head+FILL_HEAD, colnames, rownames)


if __name__ == "__main__":
  main()

