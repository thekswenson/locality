Here is a description of the files and directories in the "output" folder:
(examples are given for the k562 cell-line the "k562.nn" is for non-normalized)


scenario.k562/k562.scenario.txt
	- averages over random scenarios
    - logscen.1000 - .pcl files containing scenarios with breakpoint coordinates
                     (see the text using scenlogstr)
    - random.1000  - the files associated with the randomized scenarios.
    - jitter.1000  - the files associated with the jittered scenarios.

__________________
THIS IS OLD STUFF:

old/jitter.k562/
	- files analyzing jittered data (place component randomly on chrosomes)

old/jitter.scenario.k562/
	- jittered data evaluated by scenario instead of all-pairs.
	- i've started just putting this data in the scenario.k562 dir instead.

old/slide.k562/
	- charts for the slide data

old/k562.scenario.1000.txt
	- average heat over 1000 scenarios

old/k562.slide.txt
	- slide each component along the chromosome and evaluate the all-pairs
	  heat on the edges for each point. 
	- each component is named by it's left-most reality edge (i think)

old/k562.slide.scenario.txt
    - same as k562.slide.txt but evaluation by scenarios instead of all-pairs.

k562.txt
	- each component has the following line:
"
Ave[26]M:P  -0.228658211787         0.720657138487[10] 0 [16]-0.821980305708
Ave[p]M:P  both         intra[intrapairs] 0 [interpairs]inter
"
	p- the number of pairs of edges
	M- this is a ?multichromosome? component
	P- ??
	both - average heat for both intra and inter chromosomal pairs
	intra - average heat for intra chromosomal pairs
	intrapairs - # of intrachromosomal pairs
	inter - average heat for inter chromosomal pairs
	interpairs - # of interchromosomal pairs


k562.sorted.txt
	- the version of k562.txt with only the value lines, sorted by value.
