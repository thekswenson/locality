#!/bin/bash

if [ -n "${LOCALITY_BASE_DIR:+1}" ]; then
  rootdir=$LOCALITY_BASE_DIR
else
  echo "Set the environment variable LOCALITY_BASE_DIR first!"
  echo "In BASH do something like: export LOCALITY_BASE_DIR=~/path/to/locality"
  exit
fi



mbfuzz="0"             #The amount of fuzz when scoring moves.
reps="1000"            #The number of scenarios.

obliterate=false        #Ignore already computed heatmaps (if true).
usenormper=false        #Normalize per chromosome.
usescrambled=false      #Use scrambled matrices.

scoreactual=true     #Rescore the actual scenarios.
scorerandom=true     #Rescore the random scenarios.
scorejitter=false     #Rescore the jitter scenarios.

userandom=true         #Use randomized moves rather than jittered moves.

createplots=false       #Create the perlength plots.



if [[ "$usenormper" = "true" ]]; then
  suffix=".normper"
  prefix="normper."
elif [[ "$usescrambled" = "true" ]]; then
  suffix=".scrambled"
  prefix="scrambled."
else
  suffix=""
  prefix=""
fi

function runanalysesfunc
  {
  directory=$1
  heatmap=$2
  fuzz=$3
  fuzzstr="fuzz$fuzz"

  rescoreflags="--fuzz $fuzz --rescore"
  plotflags="--intra --inter --plot plot.$fuzzstr.$reps"
  logscenloc="HeLa-G1.$reps.$fuzzstr"
  jitterloc="jitter.HeLa-G1.$reps.$fuzzstr"
  randomloc="random.HeLa-G1.$reps.$fuzzstr"
  basedir="$rootdir/output/scenario/HeLa-G1-all-HindIII-1000k"

  if [ ! -e $directory ]; then
    mkdir $directory
  fi
  cd $directory
  echo
  pwd
  echo $fuzzstr $reps
  
  if [[ "$scoreactual" = "true" ]]; then
            #Score the actual locations:
    rm -rf $logscenloc
    analyzescenarios $rescoreflags $logscenloc --heat $heatmap $basedir/logscen.$reps/
  fi
  if [[ "$scorejitter" = "true" ]]; then
            #Score the jittered locations:
    rm -rf $jitterloc
    analyzescenarios $rescoreflags $jitterloc --heat $heatmap $basedir/jitter.$reps/logscen.$reps/
  fi
  if [[ "$scorerandom" = "true" ]]; then
            #Score the randomized locations:
    rm -rf $randomloc
    analyzescenarios $rescoreflags $randomloc --heat $heatmap $basedir/random.$reps/
  fi

  if [[ "$userandom" = "true" ]]; then
           #Plot the actual vs. random distributions:
    analyzescenarios ${plotflags}.random $logscenloc/ -m $randomloc/

    if [[ "$createplots" = "true" ]]; then
           #Plot the heat on windows for rearrangement length:
      analyzescenarios --intra --suffix $fuzzstr.${reps}.random --vary 20 --vrange 1 --numsteps 20 $logscenloc/ -m $randomloc/
      analyzescenarios --intra --suffix $fuzzstr.${reps}.random --vary 200 --vrange 20 --numsteps 10 $logscenloc/ -m $randomloc/

             #Plot the actual distribution per move:
      #analyzescenarios --intra --permove permove.$fuzzstr --heat $heatmap $logscenloc/
      #       #Plot the randomized distribution per move:
      #analyzescenarios --intra --permove permove.random.$fuzzstr --heat $heatmap $randomloc/
    fi
  else
           #Plot the actual vs. jittered distributions:
    analyzescenarios $plotflags $logscenloc/ -m $jitterloc/

    if [[ "$createplots" = "true" ]]; then
           #Plot the heat on windows for rearrangement length:
      analyzescenarios --intra --suffix $fuzzstr.$reps --vary 20 --vrange 1 --numsteps 20 $logscenloc/ -m $jitterloc/
      analyzescenarios --intra --suffix $fuzzstr.$reps --vary 200 --vrange 20 --numsteps 10 $logscenloc/ -m $jitterloc/

             #Plot the actual distribution per move:
      #analyzescenarios --intra --permove permove.$fuzzstr --heat $heatmap $logscenloc/
      #       #Plot the randomized distribution per move:
      #analyzescenarios --intra --permove permove.jitter.$fuzzstr --heat $heatmap $jitterloc/
    fi
  fi
  }


if ! hash analyzescenarios 2>/dev/null
then
  echo "ERROR: add analyzescenarios to your path with 'pathadd $rootdir'"
  exit
fi

if ! hash intersectmats 2>/dev/null
then
  echo "ERROR: add intersectmats to your path with 'pathadd $rootdir/hiC'"
  exit
fi


function intermatsfunc
  {
  name=$1
  d1=$2
  d2=$3

  hicdir="$rootdir/hiC/"
  interdir="$hicdir/intersection/"
  symddir="$hicdir/symdiff/"

  if [ ! -e $interdir ]; then
    mkdir $interdir
  fi
  if [ ! -e $symddir ]; then
    mkdir $symddir
  fi

  echo "intersecting heatmaps..."
  idname=$interdir$prefix$name
  if [ ! -e $idname ]; then     #Make the directory if necessary.
    mkdir $idname
  fi
  if [[ ! "$(ls -A $idname)"  || "$obliterate" = "true" ]]; then
    cd $idname                 #Intersect the matrices if need be:
    pwd
    intersectmats $hicdir$d1 $hicdir$d2
  fi
  sdname=$symddir$prefix$name
  if [ ! -e $sdname ]; then     #Make the directory if necessary.
    mkdir $sdname
  fi
  if [[ ! "$(ls -A $sdname)"  || "$obliterate" = "true" ]]; then
    cd $sdname                  #Intersect the matrices if need be:
    pwd
    intersectmats --symm $hicdir$d1 $hicdir$d2
  fi

  #cd $rootdir
  #interpcl="${rootdir}pcl/heatmap.${prefix}intersect.${name}.pcl"
  #if [[ ! -e $interpcl  || "$obliterate" = "true" ]]; then
  #  echo "pickling ${interpcl}..."
  #  localdist -m $idname --pm $interpcl
  #fi
  #symdpcl="${rootdir}pcl/heatmap.${prefix}symdiff.${name}.pcl"
  #if [[ ! -e $symdpcl  || "$obliterate" = "true" ]]; then
  #  echo "pickling ${symdpcl}..."
  #  localdist -m $sdname --pm $symdpcl
  #fi

  if [[ ! -e $rootdir/output/intersection ]]; then
    mkdir $rootdir/output/intersection
  fi
  if [[ ! -e $rootdir/output/symdiff ]]; then
    mkdir $rootdir/output/symdiff
  fi
  echo "scoring scenarios..."
  interscendir="$rootdir/output/intersection/${prefix}intersect.$name"
  diffscendir="$rootdir/output/symdiff/${prefix}symdiff.$name"
  runanalysesfunc $interscendir $idname $mbfuzz
  runanalysesfunc $diffscendir $sdname $mbfuzz
  #runanalysesfunc $interscendir $interpcl $mbfuzz #USING PCL
  #runanalysesfunc $diffscendir $symdpcl $mbfuzz
  }



gm06690="lieberman/gm06690.heatmaps"
k562="lieberman/k562.heatmaps"

if [[ "$usenormper" = "true" ]]; then
  echo "using per chromosome normalization."
  gm06690n="lieberman/gm06690.heatmaps.normper"
  k562n="lieberman/k562.heatmaps.normper"
  hesc="dixon/hESC/1mb/normper"
  imr90="dixon/IMR90/1mb/normper"
  
  helaALL="mitosis/HeLa-G1-all-HindIII-1000k.normper"
  hela25FA="mitosis/G1-0.25FA-R1-HindIII-1000k.normper"
  hela1FA="mitosis/G1-1FA-R1-HindIII-1000k.normper"
  
  hffNS="mitosis/HFF-NS-R1-HindIII-1000k.normper"
  helaNS="mitosis/HeLa-NS-all-HindIII-1000k.normper"
  
  helaM="mitosis/HeLa-M-all-HindIII-1000k.normper"
  helaHSM="mitosis/HeLa-M-high-synchrony-HindIII-1000k.normper"
  k562M="mitosis/K562-M-all-HindIII-1000k.normper"
  hffM="mitosis/HFF-M-R2_highSynchrony-HindIII-1000k.normper"
elif [[ "$usescrambled" = "true" ]]; then
  echo "using scrambled matrices."
  gm06690n="lieberman/gm06690.heatmaps.scrambled"
  k562n="lieberman/k562.heatmaps.scrambled"
  hesc="dixon/hESC/1mb/scrambled"
  imr90="dixon/IMR90/1mb/scrambled"
  
  helaALL="mitosis/HeLa-G1-all-HindIII-1000k.scrambled"
  hela25FA="mitosis/G1-0.25FA-R1-HindIII-1000k.scrambled"
  hela1FA="mitosis/G1-1FA-R1-HindIII-1000k.scrambled"
  
  hffNS="mitosis/HFF-NS-R1-HindIII-1000k.scrambled"
  helaNS="mitosis/HeLa-NS-all-HindIII-1000k.scrambled"
  
  helaM="mitosis/HeLa-M-all-HindIII-1000k.scrambled"
  helaHSM="mitosis/HeLa-M-high-synchrony-HindIII-1000k.scrambled"
  k562M="mitosis/K562-M-all-HindIII-1000k.scrambled"
  hffM="mitosis/HFF-M-R2_highSynchrony-HindIII-1000k.scrambled"
else
  echo "using global normalization."
  gm06690n="lieberman/gm06690.heatmaps.normalized"
  k562n="lieberman/k562.heatmaps.normalized"
  hesc="dixon/hESC/1mb/normalized"
  imr90="dixon/IMR90/1mb/normalized"
  
  helaALL="mitosis/HeLa-G1-all-HindIII-1000k.normalized"
  hela25FA="mitosis/G1-0.25FA-R1-HindIII-1000k.normalized"
  hela1FA="mitosis/G1-1FA-R1-HindIII-1000k.normalized"
  
  hffNS="mitosis/HFF-NS-R1-HindIII-1000k.normalized"
  helaNS="mitosis/HeLa-NS-all-HindIII-1000k.normalized"
  
  helaM="mitosis/HeLa-M-all-HindIII-1000k.normalized"
  helaHSM="mitosis/HeLa-M-high-synchrony-HindIII-1000k.normalized"
  k562M="mitosis/K562-M-all-HindIII-1000k.normalized"
  hffM="mitosis/HFF-M-R2_highSynchrony-HindIII-1000k.normalized"
fi


### Dixon:
intermatsfunc hesc.imr90 $hesc $imr90

### 25FA:
intermatsfunc hela25FA.hesc $hela25FA $hesc
intermatsfunc hela25FA.imr90 $hela25FA $imr90

### HaLa vs. NS:
intermatsfunc helaALL.hffNS $helaALL $helaNS
intermatsfunc hela1FA.hffNS $hela1FA $helaNS
intermatsfunc hela25FA.hffNS $hela25FA $helaNS
intermatsfunc helaALL.hffNS $helaALL $hffNS
intermatsfunc hela1FA.hffNS $hela1FA $hffNS
intermatsfunc hela25FA.hffNS $hela25FA $hffNS

### HaLa vs. M:
intermatsfunc helaALL.helaM $helaALL $helaM
intermatsfunc hela1FA.helaM $hela1FA $helaM
intermatsfunc hela25FA.helaM $hela25FA $helaM
intermatsfunc helaALL.k562M $helaALL $k562M
intermatsfunc hela1FA.k562M $hela1FA $k562M
intermatsfunc hela25FA.k562M $hela25FA $k562M
intermatsfunc helaALL.hffM $helaALL $hffM
intermatsfunc hela1FA.hffM $hela1FA $hffM
intermatsfunc hela25FA.hffM $hela25FA $hffM

### Metaphase vs. dixon/lieb
intermatsfunc helaM.hesc $helaM $hesc
intermatsfunc helaM.imr90 $helaM $imr90
intermatsfunc k562M.imr90 $k562M $imr90
intermatsfunc hffM.gm06690n $hffM $gm06690n
intermatsfunc hffM.hesc $hffM $hesc
intermatsfunc hffM.imr90 $hffM $imr90


### NS vs. dixon/lieb
intermatsfunc helaNS.hesc $helaNS $hesc
intermatsfunc helaNS.imr90 $helaNS $imr90
intermatsfunc hffNS.hesc $hffNS $hesc
intermatsfunc hffNS.imr90 $hffNS $imr90

### M vs. NS
intermatsfunc helaM.helaNS $helaM $helaNS
intermatsfunc helaM.hffNS $helaM $hffNS
intermatsfunc k562M.helaNS $k562M $helaNS
intermatsfunc k562M.hffNS $k562M $hffNS
intermatsfunc hffM.helaNS $hffM $helaNS
intermatsfunc hffM.hffNS $hffM $hffNS

### HeLa vs. lieb/dix
intermatsfunc hesc.hela1FA $hesc $hela1FA
intermatsfunc imr90.hela1FA $imr90 $hela1FA
intermatsfunc helaALL.hesc $helaALL $hesc
intermatsfunc helaALL.imr90 $helaALL $imr90

### M vs. NS
intermatsfunc helaNS.k562M $helaNS $k562M
intermatsfunc helaM.k562M $helaM $k562M
intermatsfunc hffNS.k562M $hffNS $k562M
intermatsfunc hffM.helaNS $hffM $helaNS
intermatsfunc hffNS.helaM $hffNS $helaM
intermatsfunc hffM.helaM $hffM $helaM

### the rest
intermatsfunc helaNS.hela1FA $helaNS $hela1FA
intermatsfunc hela25FA.hela1FA $hela25FA $hela1FA
intermatsfunc helaALL.hela1FA $helaALL $hela1FA
intermatsfunc hffM.k562M $hffM $k562M
intermatsfunc hela25FA.helaNS $hela25FA $helaNS
intermatsfunc helaALL.helaNS $helaALL $helaNS
intermatsfunc hffNS.helaNS $hffNS $helaNS
intermatsfunc helaALL.hela25FA $helaALL $hela25FA


## the k562 and gm06690:
# using our normalization:
intermatsfunc k562M.k562n $k562M $k562n
intermatsfunc hela25FA.k562n $hela25FA $k562n
intermatsfunc helaALL.k562n $helaALL $k562n
intermatsfunc hela1FA.gm06690n $hela1FA $gm06690n
intermatsfunc hela1FA.k562n $hela1FA $k562n
intermatsfunc helaALL.gm06690n $helaALL $gm06690n
intermatsfunc helaNS.gm06690n $helaNS $gm06690n
intermatsfunc hffNS.k562n $hffNS $k562n
intermatsfunc hffNS.gm06690n $hffNS $gm06690n
intermatsfunc helaM.k562n $helaM $k562n
intermatsfunc helaM.gm06690n $helaM $gm06690n
intermatsfunc helaNS.k562n $helaNS $k562n
intermatsfunc k562M.gm06690n $k562M $gm06690n
intermatsfunc hffM.k562n $hffM $k562n
intermatsfunc k562M.hesc $k562M $hesc
intermatsfunc k562n.gm06690n $k562n $gm06690n
intermatsfunc hesc.gm06690n $hesc $gm06690n
intermatsfunc hesc.k562n $hesc $k562n
intermatsfunc imr90.gm06690n $imr90 $gm06690n
intermatsfunc imr90.k562n $imr90 $k562n
intermatsfunc hela25FA.gm06690n $hela25FA $gm06690n


#################################################################
## using the original normalization:
#intermatsfunc helaALL.k562 $helaALL $k562
#intermatsfunc hela25FA.k562 $hela25FA $k562
#intermatsfunc helaALL.k562 $helaALL $k562
#intermatsfunc hela1FA.gm06690 $hela1FA $gm06690
#intermatsfunc hela1FA.k562 $hela1FA $k562
#intermatsfunc helaALL.gm06690 $helaALL $gm06690
#intermatsfunc helaNS.k562 $helaNS $k562
#intermatsfunc helaNS.gm06690 $helaNS $gm06690
#intermatsfunc hffNS.k562 $hffNS $k562
#intermatsfunc hffNS.gm06690 $hffNS $gm06690
#intermatsfunc helaM.k562 $helaM $k562
#intermatsfunc helaM.gm06690 $helaM $gm06690
#intermatsfunc k562M.gm06690 $k562M $gm06690
#intermatsfunc hffM.k562 $hffM $k562
#intermatsfunc hffM.gm06690 $hffM $gm06690
#intermatsfunc k562.gm06690 $k562 $gm06690
#intermatsfunc hesc.gm06690 $hesc $gm06690
#intermatsfunc hesc.k562 $hesc $k562
#intermatsfunc imr90.gm06690 $imr90 $gm06690
#intermatsfunc imr90.k562 $imr90 $k562
#intermatsfunc hela25FA.gm06690 $hela25FA $gm06690
