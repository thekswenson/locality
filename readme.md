# Genome Rearrangements and Chromatin Structure

These are the tools developed in [my lab](http://www.lirmm.fr/~swenson) for relating rearrangements to 3D chromatin structure (using Hi-C data). They are writting in python and have been developed on an Ubuntu Linux system.

_________________
## Installation

To use this repository on Ubuntu 23.04:

1. Clone the repositories.
    `git clone --depth=1 https://thekswenson@bitbucket.org/thekswenson/locality` 
2. Install [mamba/conda](https://mamba.readthedocs.io/en/latest/installation/mamba-installation.html) if you don't have it.
3. From the new locality directory, create a new mamba environment.
    - `cd locality`
    - `mamba env create --file workflow/envs/global.yaml`
4. Activate the environment.
    `mamba activate locality`
3. Test the installation.
    `bin/localdist -s "[[1,2]]" "[[-2,-1]]"`
    

_________________
## Usage

Here we describe common uses for the tools found here. They all assume that you are calling them from the `locality/` directory.

Individual command are first described, before the Snakemake workflow is introduced.

### DCJ related functionality

#### Basics

- Build the adjacency graph for a specified input (see the `testcases` script)...
  `bin/localdist -s "[[1,2]]" "[[-1,-2]]"`
  See the `-s` option with `localdist -h` for an explanation of genome input syntax.
- Build the adjacency graph for the human/mouse data...
  `bin/localdist -f synteny/HomoSapiensMusMusculus_genes.txt`

#### Generating and scoring DCJ scenarios

These instructions assume you have either prepared your own Hi-C matrices, or dowloaded them as described in the "Dealing with Hi-C Heatmaps" section.

- Score by scenario and save the scenarios in directory `logdir/` (*print a scenario with `output/scenlogstr`, analyze it using `bin/analyzescenarios`*)...
  `bin/localdist --scenario logdir -m hiC/anopheles/AalbS2_V4.mcool -f synteny/HomoSapiensMusMusculus_genes.txt`

- Just create the scenario...
   `bin/localdist --noscore --scenario logdir -m hiC/anopheles/AalbS2_V4.mcool -f synteny/HomoSapiensMusMusculus_genes.txt`

- Do it 10 times...
   `bin/localdist -i 10 --noscore --scenario logdir -m hiC/anopheles/AalbS2_V4.mcool -f synteny/HomoSapiensMusMusculus_genes.txt`

- Rescore the scenarios you created...
  `bin/analyzescenarios --rescore outdir --heatmap hiC/anopheles/AalbS2_V4.mcool logdir/`

- Randomize and rescore the scenarios you created...
  `bin/analyzescenarios --randomize outdir --heatmap hiC/anopheles/AalbS2_V4.mcool logdir/`


### Analyzing the results

- Create a plot showing the distributions of scores for two sets of scenarios...
  `bin/analyzescenarios -p plotname -m randomized_scenarios_dir/ actual_scenarios_dir/`
- Do the same for inter and intrachromosomal moves seperately...
  `bin/analyzescenarios --intra --inter -p plotname -m randomized_scenarios_dir/ actual_scenarios_dir/`
- Plot the distribution of heat over the moves in the scenarios...
  `bin/analyzescenarios -permove plotname actual_scenarios_dir/`


### Dealing with Hi-C Heatmaps

#### File format
 
Heatmaps are stored in the [Cooler](https://cooler.readthedocs.io/en/latest/index.html) format.

#### Processing heatmaps

- Get stats about heatmaps...
  `bin/localdist -m hiC/anopheles/AalbS2_V4.mcool` 
- `hiC/normalizemats`: Normalize matrices based on distance (as in Lieberman-Aiden *et al.*).
- `hiC/scramblemats`: Permute matrices randomly.
- `hiC/verifyheatmaps`: Print out stats or confirm relationships between matrices.

### Pipelines for testinging the contact/rearrangement link with Snakemake

We give a [Snakemake](https://snakemake.readthedocs.io/en/stable/) workflow that produces a set of plots and statistical tests exploring the contact/rearrangement link. The Snakemake file exist in the top directory, along with an example configuration file comparing Anopheles _albimanus_ and _atroparvus_.
(change `4` to some larger number if you have more cores available)
```
snakemake -c 4 --configfile config/experiment_template.yaml
```

#### Human vs. Mouse

For evaluating the results in human/mouse, see instructions at [https://bitbucket.org/thekswenson/mammal_rearrange_interactions](https://bitbucket.org/thekswenson/mammal_rearrange_interactions). Note that this is used a previous version, with alternate installation instructions.

_________________
## Bibliography
