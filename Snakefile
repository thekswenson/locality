import sys
import os
#sys.path.append(workflow.srcdir('src/'))
sys.path.append(workflow.current_basedir)

import re
import glob
import shutil
from ast import literal_eval
from typing import Dict
from collections import defaultdict
from pprint import pprint
from itertools import combinations, chain
from pathlib import Path

configfile: 'config/defaults.yaml'

#### BASIC CONSTANTS ####

#PYTHONPATH = srcdir('src/')
PYTHONPATH = workflow.current_basedir.join('src/')

#Type of scenario sampling:
SPLIT = 'split'
MIX = 'mix'

#Parameters:
REPS = str(config.get('REPS', 1))
MAX_THREADS = min(config['MAX_THREADS'], int(REPS))
USE_MEDIAN = '--median' if config['USE_MEDIAN'] else ''
MEDIAN = '.median' if USE_MEDIAN else ''
PERSCEN_RANDREPS = int(config['PERSCEN_RANDREPS'])
PERSCEN_REPS = int(config['PERSCEN_REPS'])

HICDIR = f'{Path(config["HICDIR"])}/'
OUTDIR = f'{Path(config["OUTDIR"])}/'
SCENDIR = f'{Path(config["SCENDIR"])}/'

OUTSCENDIR = f'{OUTDIR}{SCENDIR}{REPS}/'

INTERSECHM = config['INTERSECDIR']
INTERSECOUT = '{}{}{}/'.format(OUTDIR, config['INTERSECDIR'], REPS)
SYMDIFFHM = config['SYMDIFFDIR']
SYMDIFFOUT = '{}{}{}/'.format(OUTDIR, config['SYMDIFFDIR'], REPS)
DIFFHM = config['DIFFDIR']
DIFFOUT = '{}{}{}/'.format(OUTDIR, config['DIFFDIR'], REPS)

SPECIES_A = config.get('SPECIES_A', '')
SPECIES_B = config.get('SPECIES_B', '')

CLOBBER = '' if config['NO_CLOBBER'] else ' --clobber-scenarios '

T_TESTS = config['T_TESTS']
T_TESTS_PER = config['T_TESTS_PER']
T_TESTS_SPLIT = config['T_TESTS_SPLIT'] if 'T_TESTS_SPLIT' in config else False
T_TESTS_MIX = config['T_TESTS_MIX'] if 'T_TESTS_MIX' in config else False
T_TESTS_SCRAM_V_SCRAM = config['T_TESTS_SCRAM_V_SCRAM'] if 'T_TESTS_SCRAM_V_SCRAM' in config else False
T_TESTS_SCRAM_V_NON = config['T_TESTS_SCRAM_V_NON'] if 'T_TESTS_SCRAM_V_NON' in config else False

SCRAM_PVAL = config['SCRAM_PVAL'] if 'SCRAM_PVAL' in config else False
SCRAM_PVAL_REPS = config['SCRAM_PVAL_REPS'] if 'SCRAM_PVAL_REPS' in config else 1


#### DATASET CONSTANTS and FLAGS ####

#Directory base names for scenarios:
ACTUAL_TEMPLATES = 'actualtemplates' #Templates for actual scenarios.
RANDOM_TEMPLATES = 'randomtemplates' #Used if RANDOM_DIR = 'fromrandomtemplates'
WANDER_TEMPLATES = 'wandertemplates' #Templates for wander scenarios.
MIX_TEMPLATES = 'mixtemplates'       #Templates for mix scenarios.
SPLIT_TEMPLATES = 'splittemplates'   #Templates for split scenarios.

ACTUAL_DIR = 'fromactualtemplates'     #Actual scenarios.
RANDOM_FROM_TEMPLATES = 'fromrandomtemplates'
WANDER_DIR = 'fromwandertemplates'     #Random wandering scenarios.
#RANDOM_DIR = RANDOM_FROM_TEMPLATES    #Randomize scenarios from templates.
RANDOM_DIR = 'randomscenarios'         #Randomized scenarios not from templates.
RANDOM_PER_DIR = 'randomperscenarios'  #Randomized scenarios not from templates.
SCRAM_ACT_DIR = 'scrambled/scrambledactualscenarios'  #Actual rescored with scrambled.
SCRAM_RAND_DIR = 'scrambled/scrambledrandomscenarios' #Random rescored with scrambled.
GREEDY_DIR = 'greedyscens'
REVGREEDY_DIR = 'greedyscensreversed'
REVRAND_DIR = 'randrev'
REV_DIR = 'reversed'

#Plots:
PLOT_DISTS = config['PLOT_DISTS']
PLOT_DISTS_STR = config['PLOT_DISTS_STR']
PLOT_DISTS_DESC = config['PLOT_DISTS_DESC']
PLOT_WINDOW = config['PLOT_WINDOW']
PLOT_BYWINDOW_DESC = config['PLOT_BYWINDOW_DESC']
PLOT_PERMOVE = config['PLOT_PERMOVE']
PLOT_PERMOVE_STR = config['PLOT_PERMOVE_STR']
PLOT_PERMOVE_DESC = config['PLOT_PERMOVE_DESC']
PLOT_WANDER = config['PLOT_WANDER']
PLOT_WANDER_STR = config['PLOT_WANDER_STR']
PLOT_WANDER_DESC = config['PLOT_WANDER_DESC']
PLOT_FREQ = config['PLOT_FREQ']
PLOT_FREQ_STR = config['PLOT_FREQ_STR']
PLOT_FREQ_DESC = config['PLOT_FREQ_DESC']
PLOT_IGNORE = config['PLOT_IGNORE']
PLOT_IGNORE_STR = config['PLOT_IGNORE_STR']
PLOT_IGNORE_DESC = config['PLOT_IGNORE_DESC']
PLOT_PERSCEN = config['PLOT_PERSCEN']
PLOT_PERSCEN_STR = config['PLOT_PERSCEN_STR']
PLOT_PERSCEN_DESC = config['PLOT_PERSCEN_DESC']
PLOT_SCRAM_V_NON = config['PLOT_SCRAM_V_NON']
PLOT_SCRAM_V_NON_STR = config['PLOT_SCRAM_V_NON_STR']
PLOT_SCRAM_V_NON_DESC = config['PLOT_SCRAM_V_NON_DESC']
PLOT_SCRAM_V_SCRAM = config['PLOT_SCRAM_V_SCRAM']
PLOT_SCRAM_V_SCRAM_STR = config['PLOT_SCRAM_V_SCRAM_STR']
PLOT_SCRAM_V_SCRAM_DESC = config['PLOT_SCRAM_V_SCRAM_DESC']

PLOT_GREEDYONACTUAL = config['PLOT_GREEDYONACTUAL']
PLOT_GREEDYONACTUAL_STR = config['PLOT_GREEDYONACTUAL_STR']
PLOT_GREEDYONACTUAL_DESC = config['PLOT_GREEDYONACTUAL_DESC']
PLOT_GREEDYREVONREV = config['PLOT_GREEDYREVONREV']
PLOT_GREEDYREVONREV_STR = config['PLOT_GREEDYREVONREV_STR']
PLOT_GREEDYREVONREV_DESC = config['PLOT_GREEDYREVONREV_DESC']
PLOT_REVERSED = config['PLOT_REVERSED']
PLOT_REVERSED_STR = config['PLOT_REVERSED_STR']
PLOT_REVERSED_DESC = config['PLOT_REVERSED_DESC']
PLOT_GREEDY_REVERSED = config['PLOT_GREEDY_REVERSED']
PLOT_GREEDY_REVERSED_STR = config['PLOT_GREEDY_REVERSED_STR']
PLOT_GREEDY_REVERSED_DESC = config['PLOT_GREEDY_REVERSED_DESC']
PLOT_RAND_REVERSED = config['PLOT_RAND_REVERSED']
PLOT_RAND_REVERSED_STR = config['PLOT_RAND_REVERSED_STR']
PLOT_RAND_REVERSED_DESC = config['PLOT_RAND_REVERSED_DESC']
PLOT_REV_REV = config['PLOT_REV_REV']
PLOT_REV_REV_STR = config['PLOT_REV_REV_STR']
PLOT_REV_REV_DESC = config['PLOT_REV_REV_DESC']
PLOT_REVONOTHER = config['PLOT_REVONOTHER']
PLOT_REVONOTHER_STR = config['PLOT_REVONOTHER_STR']
PLOT_REVONOTHER_DESC = config['PLOT_REVONOTHER_DESC']

GREEDY_COLOR = config['GREEDY_COLOR']
REV_GREEDY_COLOR = config['REV_GREEDY_COLOR']

#Ignoring moves:
DMIN = 0            #Ignore moves shorter than this (in Mbs).
DMIN_STR = '--dmin {}'.format(DMIN) if DMIN else ''
IGNORE_PROPORTION = 2.0 #Ignore moves this many standard deviations higher.

#Scenarios:
GREEDY = config['GREEDY']
GREEDY_REPS = str(config['GREEDY_REPS'])
REVERSE = config['REVERSE']
COMBINATIONS_FULL = config['COMBINATIONS_FULL']
INTERSECTION = config['INTERSECTION'] or COMBINATIONS_FULL
SCORE_FISSION = '-F' if config.get('score_fission', False) else ''

#Datasets:
SETS = [literal_eval(tstring) for tstring in config['DATASETS']]
if badset := list(filter(lambda l: len(l) != 8, SETS)):
  raise(Exception(f'Bad DATASETS format. Expected 8 values per line but got:'
                  f'\n{badset}'))

#For two sided computations:
OTHER_DATASET = {OUTSCENDIR+key: OUTSCENDIR+val
                 for key, val in config['PARTNER_DATASET'].items()}

#For intersection, symmdiff, and difference heatmaps:
COMBINATIONS_NAME = config['COMBINATIONS_NAME']
COMBINATIONS_INTRA = config['COMBINATIONS_INTRA']
COMBINATIONS_INTER = config['COMBINATIONS_INTER']

#ERROR Messages:

MISSING_OTHER = ('Missing species name specification for\n\t"{}" in\n{}.\n'+
                 'Set REVERSE to False, or provide Hi-C for partner species.')
MISSING_SPECIES = 'Missing species name specification for\n\t"{}"\nin\n\t{}.'
MISSING_SYNTENY = 'No DATASETS line mapping species "{}" to a synteny file.'

#Suffixes for heatmap directories according to normalization:
LANORM_HM = 'LA'
PROB_HM = 'PROB'
LOG_HM = 'LOG'
LANORMPROB_HM = 'LAPROB'
LANORMLOG_HM = 'LALOG'
LANORMPROBLOG_HM = 'LAPROBLOG'

SCRAMBLED_HM = 'scrambled'


################################################################################

def getComboDir(n1, n2, res, hmtype):
  """
  Return the name of an intersection/symdiff/diff directory, given two ids and a
  normalization type.
  """
  return f'{hmtype}/{res}/{n1}-{n2}/'


def getScendirTOhms():
  """
  Map scenarios directory to heatmaps directory.

  @note: This code is complicated by the fact that the .mcool format stores
         multiple resolutions, and that the DATASETS specification does not
         specify the exact heatmap to use (a trade off between complexity
         of the DATASETS spec and this code).
  """
  scendirTOhms = {f'{OUTSCENDIR+name+MEDIAN}/{norm}_{res}':
                  f'{hms}.norm{norm}_{res}{hext}'
                  for species,name,_,hms,norm,hext,res,_ in SETS}

  scendirTOhms = {}
  hicdir = Path(HICDIR)
  for _,name,_,hms,norm,hext,res,_ in SETS:
    basename = f'{hms}.norm{norm}'
    outdir = f'{OUTSCENDIR+name+MEDIAN}/{norm}_{res}'

    hm = Path(f'{basename}_{res}{hext}')
    if (hicdir/hm).exists():
      scendirTOhms[outdir] = str(hm)
      continue

    mcoolhm = Path(f'{basename}_0.mcool')
    if hext == '.mcool' and (hicdir/mcoolhm).exists():
      scendirTOhms[outdir] = str(mcoolhm)
      continue        #The resolution is stored in the .mcool file.

    scendirTOhms[outdir] = str(hm)


  if INTERSECTION:
    raise(NotImplementedError("Check this code. It hasn't been tested."))
    #TODO: update this to use the new SETS format:
    for (_,_,_,hms,hmtype,res1,n1),(_,_,_,hms,_,res2,n2) in combinations(SETS, 2):
      if n1 and n2 and res1 == res2:
        combodir = getComboDir(n1, n2, res1, hmtype)
        scendirTOhms[INTERSECOUT+combodir] = INTERSECHM+combodir
        scendirTOhms[SYMDIFFOUT+combodir] = SYMDIFFHM+combodir
        scendirTOhms[DIFFOUT+combodir+'/'+n1+'-'+n2+'/'] =\
            DIFFHM+combodir+n1+'-'+n2
        scendirTOhms[DIFFOUT+combodir+'/'+n2+'-'+n1+'/'] =\
            DIFFHM+combodir+n2+'-'+n1

  return scendirTOhms


def getTemplatedirTOhms() -> Dict[str, str]:
  """
  Map template directory to heatmaps with no normalization.

  @return: directory string without trailing slash.
  """
  templatedirTOhms = {}
  hicdir = Path(HICDIR)
  for _,name,_,hms,norm,hext,res,_ in SETS:
    if f'{OUTSCENDIR+name+MEDIAN}' in templatedirTOhms:
      continue          #the value is already set.

    hmname = Path(f'{hms}.norm{norm}_{res}{hext}')
    if (hicdir/hmname).exists():
      templatedirTOhms[f'{OUTSCENDIR+name+MEDIAN}'] = str(hmname)
      continue

    hmname = Path(f'{hms}.normNONE_0.mcool')
    if (hicdir/hmname).exists():
      templatedirTOhms[f'{OUTSCENDIR+name+MEDIAN}'] = str(hmname)
      continue

    hmname = Path(f'{hms}.normNONE_0.cool')
    if (hicdir/hmname).exists():
      templatedirTOhms[f'{OUTSCENDIR+name+MEDIAN}'] = str(hmname)
    elif not name == 'empty':
      raise(Exception(f'No heatmap found for {name} with basename '
                      f'"{hicdir/hms}".'))

  return templatedirTOhms


def getScendirTOspec():
  """
  Map scenarios directory to species character.
  """
  scendirTOspec = {f'{OUTSCENDIR+name+MEDIAN}': species 
                   for species,name,_,hms,norm,hext,res,_ in SETS}

  if INTERSECTION:
    for (sp1,_,_,_,nt1,res1,n1),(sp2,_,_,_,nt2,res2,n2) in combinations(SETS, 2):
      if n1 and n2 and res1 == res2:
        if sp1 != sp2:
          raise(Exception('Intersection of heatmaps from different species!'+
                          'Disable INTERSECTION or modify DATASETS.'))
        if nt1 != nt2:
          raise(Exception('Intersection of heatmaps with different '+
                          ' normalizations!\n\t{} != {}'.format(nt1, nt2)))

        combodir = getComboDir(n1, n2, res1, nt1)
        scendirTOspec[INTERSECOUT+combodir] = sp1
        scendirTOspec[SYMDIFFOUT+combodir] = sp1
        scendirTOspec[DIFFOUT+combodir+n1+'-'+n2+'/'] = sp1
        scendirTOspec[DIFFOUT+combodir+n2+'-'+n1+'/'] = sp1

  return scendirTOspec

scendirTOhms = getScendirTOhms()
scendirTOspec = getScendirTOspec()
templatedirTOhms = getTemplatedirTOhms()

specTOsynfile = {species: synfile for species,_,synfile,_,_,_,_,_ in SETS}
specTOoutdir = {species: f'{scendir}{MEDIAN}/'
                for species,scendir,_,_,_,_,_,_ in SETS}
idTOhms = {combid: f'{hms}.norm{norm}_{res}{hext}'
           for _,_,_,hms,norm,hext,res,combid in SETS if combid}
idTOnorm = {combid: norm for _,_,_,_,norm,hext,_,combid in SETS if combid}

#Datasets to use as Templates:
TEMPLATES_SCENS_AB = '{}{}'.format(OUTSCENDIR, specTOoutdir[SPECIES_A])
TEMPLATES_SCENS_BA = ('{}{}'.format(OUTSCENDIR, specTOoutdir[SPECIES_B])
                      if SPECIES_B in specTOoutdir else '')

################################################################################

def buildTargetList(wildcards):
  """
  Build the list of ultimate targets based on the current set of constants.
  """
  filelist = []
  for (species, name, synteny, heatmaps, norm, hext, resolution,
       intersectname) in SETS:
    name += MEDIAN
    path = f'{name}/{norm}_{resolution}'

    if PLOT_DISTS:
      filelist.append('{}{}/plot.reps{}.{}.intra.{}.0.pdf'.\
                      format(OUTSCENDIR, path, REPS,
                             PLOT_DISTS_STR, DMIN))
      filelist.append('{}{}/plot.reps{}.{}.inter.{}.0.pdf'.\
                      format(OUTSCENDIR, path, REPS,
                             PLOT_DISTS_STR, DMIN))

    if PLOT_WINDOW:
      filelist.append('{}{}/varDistPlot-None-20.1.20.reps{}.random.pdf'.\
                      format(OUTSCENDIR, path, REPS))
      filelist.append('{}{}/varDistPlot-None-200.20.10.reps{}.random.pdf'.\
                      format(OUTSCENDIR, path, REPS))

    if PLOT_PERMOVE:
      filelist.append('{}{}/plot.reps{}.{}.intra.0.0.pdf'.\
                      format(OUTSCENDIR, path, REPS, PLOT_PERMOVE_STR))
      filelist.append('{}{}/plot.reps{}.{}.inter.0.0.pdf'.\
                      format(OUTSCENDIR, path, REPS, PLOT_PERMOVE_STR))

    if PLOT_PERSCEN:
      perscendirname = f'{RANDOM_PER_DIR}.reps{REPS}'
      nametemp = '{}{}/{}/plot.reps{}.{}_{{num}}.{{ctype}}.0.0.pdf'.\
                 format(OUTSCENDIR, path, perscendirname,
                        PERSCEN_RANDREPS, PLOT_PERSCEN_STR)
      filelist.extend(expand(nametemp, ctype=['intra', 'inter'],
                             num=range(PERSCEN_REPS)))

    if PLOT_WANDER:
      filelist.append('{}{}/plot.reps{}.{}.intra.0.0.pdf'.\
                      format(OUTSCENDIR, path, REPS, PLOT_WANDER_STR))
      filelist.append('{}{}/plot.reps{}.{}.inter.0.0.pdf'.\
                      format(OUTSCENDIR, path, REPS, PLOT_WANDER_STR))

    if PLOT_FREQ:
      filelist.append('{}{}/plot.reps{}.{}.intra.0.pdf'.\
                      format(OUTSCENDIR, path, REPS, PLOT_FREQ_STR))
      filelist.append('{}{}/plot.reps{}.{}.inter.0.pdf'.\
                      format(OUTSCENDIR, path, REPS, PLOT_FREQ_STR))

    if PLOT_IGNORE:
      filelist.append('{}{}/plot.reps{}.{}.intra.{}.0.pdf'.\
                      format(OUTSCENDIR, path, REPS, PLOT_IGNORE_STR, DMIN))
      filelist.append('{}{}/plot.reps{}.{}.inter.{}.0.pdf'.\
                      format(OUTSCENDIR, path, REPS, PLOT_IGNORE_STR, DMIN))

    if PLOT_SCRAM_V_NON:
      filelist.append('{}{}/plot.reps{}.{}.intra.0.0.pdf'.\
                      format(OUTSCENDIR, path, REPS, PLOT_SCRAM_V_NON_STR))
      filelist.append('{}{}/plot.reps{}.{}.inter.0.0.pdf'.\
                      format(OUTSCENDIR, path, REPS, PLOT_SCRAM_V_NON_STR))

    if PLOT_SCRAM_V_SCRAM:
      filelist.append('{}{}/plot.reps{}.{}.intra.0.0.pdf'.\
                      format(OUTSCENDIR, path, REPS, PLOT_SCRAM_V_SCRAM_STR))
      filelist.append('{}{}/plot.reps{}.{}.inter.0.0.pdf'.\
                      format(OUTSCENDIR, path, REPS, PLOT_SCRAM_V_SCRAM_STR))

    if GREEDY:
      filelist.append('{}{}/{}.reps{}'.format(OUTSCENDIR, path, GREEDY_DIR,
                                              GREEDY_REPS))

    if REVERSE:
      if OUTSCENDIR+name in OTHER_DATASET:
        nametemp = (f'{OUTSCENDIR}{path}/{REV_DIR}.reps{REPS}/'
                    f'reversed.{{num}}.pgz')
        filelist.extend(expand(nametemp, num=range(int(REPS))))
      else:
        print(f'Warning: REVERSE is set, but {OUTSCENDIR+name} not found in '
              f'PARTNER_DATASET:')
        pprint(OTHER_DATASET)

    if PLOT_GREEDYONACTUAL:
      filelist.append('{}{}/plot.reps{}.{}.intra.0.0.pdf'.\
                      format(OUTSCENDIR, path, REPS, PLOT_GREEDYONACTUAL_STR))

    if REVERSE and PLOT_GREEDYREVONREV:
      if OUTSCENDIR+name in OTHER_DATASET:
        filelist.append('{}{}/plot.reps{}.{}.intra.0.0.pdf'.\
                        format(OUTSCENDIR, path, REPS, PLOT_GREEDYREVONREV_STR))
      else:
        print(f'Warning: PLOT_GREEDYREVONREV is set, but {OUTSCENDIR+name} not '
              f'found in PARTNER_DATASET:')
        pprint(OTHER_DATASET)

    if REVERSE and PLOT_REVERSED:
      if OUTSCENDIR+name in OTHER_DATASET:
        filelist.append('{}{}/plot.reps{}.{}.intra.0.0.pdf'.\
                        format(OUTSCENDIR, path, REPS, PLOT_REVERSED_STR))
        filelist.append('{}{}/plot.reps{}.{}.inter.0.0.pdf'.\
                        format(OUTSCENDIR, path, REPS, PLOT_REVERSED_STR))
      else:
        print(f'Warning: PLOT_REVERSED is set, but {OUTSCENDIR+name} not '
              f'found in PARTNER_DATASET:')
        pprint(OTHER_DATASET)

    if REVERSE and PLOT_GREEDY_REVERSED:
      if OUTSCENDIR+name in OTHER_DATASET:
        filelist.append('{}{}/plot.reps{}.{}.intra.0.0.pdf'.\
                        format(OUTSCENDIR, path, REPS,
                               PLOT_GREEDY_REVERSED_STR))
      else:
        print(f'Warning: PLOT_GREEDY_REVERSED is set, but {OUTSCENDIR+name} not '
              f'found in PARTNER_DATASET:')
        pprint(OTHER_DATASET)


    if REVERSE and PLOT_RAND_REVERSED:
      if OUTSCENDIR+name in OTHER_DATASET:
        filelist.append('{}{}/plot.reps{}.{}.intra.0.0.pdf'.\
                        format(OUTSCENDIR, path, REPS, PLOT_RAND_REVERSED_STR))
        filelist.append('{}{}/plot.reps{}.{}.inter.0.0.pdf'.\
                        format(OUTSCENDIR, path, REPS, PLOT_RAND_REVERSED_STR))
      else:
        print(f'Warning: PLOT_RAND_REVERSED is set, but {OUTSCENDIR+name} not '
              f'found in PARTNER_DATASET:')
        pprint(OTHER_DATASET)

    if REVERSE and PLOT_REV_REV:
      if OUTSCENDIR+name in OTHER_DATASET:
        filelist.append('{}{}/plot.reps{}.{}.intra.0.0.pdf'.\
                        format(OUTSCENDIR, path, REPS, PLOT_REV_REV_STR))
        filelist.append('{}{}/plot.reps{}.{}.inter.0.0.pdf'.\
                        format(OUTSCENDIR, path, REPS, PLOT_REV_REV_STR))
      else:
        print(f'Warning: PLOT_REV_REV is set, but {OUTSCENDIR+name} not '
              f'found in PARTNER_DATASET:')
        pprint(OTHER_DATASET)

    if REVERSE and PLOT_REVONOTHER:
      if OUTSCENDIR+name in OTHER_DATASET:
        filelist.append('{}{}/plot.reps{}.{}.intra.0.0.pdf'.\
                        format(OUTSCENDIR, path, REPS, PLOT_REVONOTHER_STR))
        filelist.append('{}{}/plot.reps{}.{}.inter.0.0.pdf'.\
                        format(OUTSCENDIR, path, REPS, PLOT_REVONOTHER_STR))
      else:
        print(f'Warning: PLOT_REVONOTHER is set, but {OUTSCENDIR+name} not '
              f'found in PARTNER_DATASET:')
        pprint(OTHER_DATASET)

    if T_TESTS:            #Paired T-tests actual VS rand (moves and scenarios)
       filelist.append('{}{}/t_tests.txt'.format(OUTSCENDIR, path))

    if T_TESTS_PER:        #Single value T-test for scenario vs. many.
       filelist.append('{}{}/t_tests_perscen.txt'.format(OUTSCENDIR, path))

    if T_TESTS_SPLIT:      #Paired T-tests for split versus normal sampling.
       filelist.append('{}{}/t_tests_pseudo_{}.txt'.format(OUTSCENDIR, path,
                                                           SPLIT))

    if T_TESTS_MIX:        #Paired T-tests for mix versus normal sampling.
       filelist.append('{}{}/t_tests_pseudo_{}.txt'.format(OUTSCENDIR, path,
                                                           MIX))

    if T_TESTS_SCRAM_V_SCRAM:  #Paired T-tests for scrambled moves and scens.
       filelist.append('{}{}/t_tests_scramVscram.txt'.format(OUTSCENDIR, path))

    if T_TESTS_SCRAM_V_NON:
       filelist.append('{}{}/t_tests_scramVnon.txt'.format(OUTSCENDIR, path))

    if SCRAM_PVAL:
       filelist.append('{}{}/scram_pvalue_{}.txt'.format(OUTSCENDIR, path,
                                                         SCRAM_PVAL_REPS))


  if INTERSECTION:      #All the computation necessary for intersection heatmaps.
     filelist.append('{}.intersection.intra.csv.pdf'.format(COMBINATIONS_NAME))
     filelist.append('{}.intersection.inter.csv.pdf'.format(COMBINATIONS_NAME))

  if COMBINATIONS_FULL: #Combo heatmaps with difference and symdiff included.
     #filelist += getIntersectNames()
     filelist.append('{}.combomatrixFULL.intra.pdf'.format(COMBINATIONS_NAME))
     filelist.append('{}.combomatrixFULL.inter.pdf'.format(COMBINATIONS_NAME))

  return filelist



rule all:
    input:
        buildTargetList,


####### Input Directories ##########################################

def scenariosASinput(wildcards, scentype, dsdir=None, filepref='rescore'):
    if dsdir is None:
        dsdir = wildcards.dsdir
    return expand('{dsdir}/{norm}_{res}/{scentype}.reps'+REPS+'/{filepref}.{rep}.pgz',
                  dsdir=dsdir, norm=wildcards.norm, res=wildcards.res,
                  scentype=scentype, filepref=filepref, rep=range(int(REPS)))

def actualscenariosASinput(wildcards):
    return scenariosASinput(wildcards, ACTUAL_DIR)

def randomscenariosASinput(wildcards):
    return scenariosASinput(wildcards, RANDOM_DIR)

def wanderscenariosASinput(wildcards):
    return scenariosASinput(wildcards, WANDER_DIR)

def actualPSEUDOscenariosASinput(wildcards):
    return scenariosASinput(wildcards, f'{wildcards.sampletype}{ACTUAL_DIR}')

def randomPSEUDOscenariosASinput(wildcards):
    return scenariosASinput(wildcards, f'{wildcards.sampletype}{RANDOM_DIR}')

def actualMIXscenariosASinput(wildcards):
    return scenariosASinput(wildcards, MIX+ACTUAL_DIR)

def randomMIXscenariosASinput(wildcards):
    return scenariosASinput(wildcards, MIX+RANDOM_DIR)

def actualSPLITscenariosASinput(wildcards):
    return scenariosASinput(wildcards, SPLIT+ACTUAL_DIR)

def randomSPLITscenariosASinput(wildcards):
    return scenariosASinput(wildcards, SPLIT+RANDOM_DIR)


def randomPERscenariosASinput(wildcards):
    retval = f'{wildcards.dsdir}/{wildcards.norm}_{wildcards.res}/{RANDOM_PER_DIR}.reps{REPS}/per_{wildcards.rep}'
    if(CLOBBER):
        return retval
    return ancient(retval)

def resolvePerScenarioTtestsASinput(wildcards):
    r = f'{wildcards.dsdir}/{wildcards.norm}_{wildcards.res}/{RANDOM_PER_DIR}.reps{REPS}/'+\
        f'ttest.reps{PERSCEN_RANDREPS}_{{rep}}.txt',
    retval = expand(r, rep=range(PERSCEN_REPS))
    if(CLOBBER):
        return retval
    return ancient(retval)


def reversedscenariosASinput(wildcards):
    return scenariosASinput(wildcards, REV_DIR, filepref='reversed')

def revrandomscenariosASinput(wildcards):
    return scenariosASinput(wildcards, REVRAND_DIR, filepref='reversed')

def revgreedyscenariosASinput(wildcards):
    return scenariosASinput(wildcards, REVGREEDY_DIR, filepref='reversed')

def greedyscenariosASinput(wildcards):
    retval = f'{wildcards.dsdir}/{wildcards.norm}_{wildcards.res}/{GREEDY_DIR}.reps{GREEDY_REPS}'
    if(CLOBBER):
        return retval
    return ancient(retval)


####### Heatmaps ##########################################


def resolveHeatmaps(wildcards):
    """
    Get the list of heatmaps used for the scenarios that are scored.
    """
    key = f'{wildcards.dsdir}/{wildcards.norm}_{wildcards.res}'

    if key not in scendirTOhms:
      raise(Exception(f'Unknown directory "{key}"! \nIn: {scendirTOhms}'))

    if CLOBBER:
      return HICDIR+scendirTOhms[key]
    return ancient(HICDIR+scendirTOhms[key])


def resolveTemplateHeatmaps(wildcards):
    """
    Get the list of heatmaps used for creating the templates, these are used
    only for getting the chromosome lengths, so there is no need for them to
    be normalized, etc.
    """
    key = wildcards.dsdir

    if key not in templatedirTOhms:
      raise(Exception(f'Unknown directory "{key}"! \nIn: {templatedirTOhms}'))

    if CLOBBER:
      return HICDIR+templatedirTOhms[key]
    return ancient(HICDIR+templatedirTOhms[key])


def nonscrambledASinput(ws):
    """
    Get the heatmap to be scrambled. If a .cool heatmap is requested that does
    not have a corresponding file, then return the .mcool file if it exists.
    """
    hm = Path(f'{ws.baseloc}/{ws.heatmap}.norm{ws.norm}_{ws.res}.{ws.hmtype}')
    mcoolhm = Path(f'{ws.baseloc}/{ws.heatmap}.norm{ws.norm}_0.mcool')

    if hm.exists() or not mcoolhm.exists():
        retval = str(hm)
    else:
        retval = str(mcoolhm)

    if CLOBBER:
        return retval
    return ancient(retval)


def nonscrambledASinputPVAL(ws):
    """
    Get the heatmap to be scrambled. If a .cool heatmap is requested that does
    not have a corresponding file, then return the .mcool file if it exists.
    """
    hm = Path(f'{ws.baseloc}/{ws.heatmap}.norm{ws.norm}_{ws.res}.{ws.hmtype}')
    mcoolhm = Path(f'{ws.baseloc}/{ws.heatmap}_0.mcool')

    if hm.exists() or not mcoolhm.exists():
        retval = str(hm)
    else:
        retval = str(mcoolhm)

    if CLOBBER:
        return retval
    return ancient(retval)




ruleorder: scramble_heatmap > normalize_heatmap

rule scramble_heatmap:
    """
    Create a scrambled heatmap in cool format.
    """
    input:
        nonscrambled = nonscrambledASinput

    output:
        '{baseloc}/'+SCRAMBLED_HM+'/{norm}_{res}/'+SCRAMBLED_HM+'.{heatmap}.norm{norm}_{res}.{hmtype}'

    log:
        '{baseloc}/log/{norm}_{res}/'+SCRAMBLED_HM+'.{heatmap}.norm{norm}_{res}.{hmtype}.log'

    params:
        '-w {res}'

    conda:    #Use with "--software-deployment-method conda" (--sdm)
        'workflow/envs/hic.yaml'

    shell:
        'PYTHONPATH={PYTHONPATH} '
        '{HICDIR}scramblemats {params} {input} {output} &> {log}'


ruleorder: scramble_heatmapPVAL > normalize_heatmap

rule scramble_heatmapPVAL:
    """
    Create a directory with normalized heatmaps.
    """
    input:
        nonscrambled = nonscrambledASinput

    output:
        '{baseloc}/'+SCRAMBLED_HM+'/{norm}_{res}/'+SCRAMBLED_HM+'_{scramrep}.{heatmap}.norm{norm}_{res2}.{hmtype}'

    log:
        '{baseloc}/log/{norm}_{res}/'+SCRAMBLED_HM+'_{scramrep}.{heatmap}.norm{norm}_{res2}.{hmtype}.log'

    params:
        '-w {res}'

    conda:    #Use with "--software-deployment-method conda" (--sdm)
        'workflow/envs/hic.yaml'

    shell:
        'PYTHONPATH={PYTHONPATH} '
        '{HICDIR}scramblemats {params} {input.nonscrambled} {output} &> {log}'


rule normalize_heatmap:
    """
    Create a directory with normalized heatmaps.
    NOTE: this rule only knows how to make .cool files, and not .mcool files.
    """
    input:
        HICDIR+'{baseloc}.normNONE_0.mcool'

    output:
        HICDIR+'{baseloc}.norm{normtype}_{res}.cool'

    log:
        HICDIR+'log/{baseloc}.norm{normtype}_{res}.cool'

    run:
        createNormalizedHeatmaps(str(input), str(output), wildcards.normtype,
                                 wildcards.res, str(log))


def createNormalizedHeatmaps(frommat, tomat, normtype, winsize, logfile):
    """
    Normalize heatmaps from frommat and put them in tomat.

    @param frommat:  use these heatmaps as input.
    @param tomat:    use these heatmaps as output.
    @param normtype: the type of normalization to use.
    """
    if normtype == LANORM_HM:
        flags = '--la'
        #suffix = 'la.txt'
    elif normtype == PROB_HM:
        flags = '-p'
        #suffix = 'prob.txt'
    elif normtype == LOG_HM:
        flags = '-l'
        #suffix = 'logratio.txt'
    elif normtype == LANORMPROB_HM:
        flags = '--la -p'
        #suffix = 'la.logratio.txt'
    elif normtype == LANORMPROBLOG_HM:
        flags = '--la -p -l'
        #suffix = 'la.prob.txt'
    elif normtype == LANORMLOG_HM:
        flags = '--la -l'
        #suffix = 'la.logratio.txt'
    else:
        raise(Exception('Unknown normalization type "{}"!'.format(normtype)))

    if CLOBBER:
        flags += ' --clobber'

        #Create the new files:
    shell('{HICDIR}normalizemats {flags} -w {winsize} {frommat} {tomat}'
          ' &> {logfile}')


####### Rescore Scenarios #################################


def chooseSyntenyfile(wildcards):
    """
    Use dsdir to determine which synteny file to use.
    """
    key = wildcards.dsdir

    if key not in scendirTOspec:
        raise(Exception(MISSING_SPECIES.format(key, scendirTOspec)))
    if scendirTOspec[key] not in specTOsynfile:
        raise(Exception(MISSING_SYNTENY.format(scendirTOspec[key])))

    synfile = specTOsynfile[scendirTOspec[key]]
    if CLOBBER:
        return synfile
    return ancient(synfile)


def chooseInScens(wildcards, templates):
    """
    Choose the input template scenarios based on dsdir.
    """
    if scendirTOspec[wildcards.dsdir] == SPECIES_A:
        retval = TEMPLATES_SCENS_AB+templates+'.reps'+REPS
    if scendirTOspec[wildcards.dsdir] == SPECIES_B:
        retval = TEMPLATES_SCENS_BA+templates+'.reps'+REPS

    if CLOBBER:
        return retval
    return ancient(retval)


def chooseInScen(wildcards):
    """
    Choose the input template scenario based on the wildcards.
    """
    if wildcards.scentype == ACTUAL_DIR:
        templates = ACTUAL_TEMPLATES
    elif wildcards.scentype == WANDER_DIR:
        templates = WANDER_TEMPLATES
    elif wildcards.scentype == RANDOM_FROM_TEMPLATES:
        templates = RANDOM_TEMPLATES
    elif wildcards.scentype == MIX+ACTUAL_DIR:
        templates = MIX_TEMPLATES
    elif wildcards.scentype == SPLIT+ACTUAL_DIR:
        templates = SPLIT_TEMPLATES
      #Include the following to avoid the error message, even though
      #rescore_scenario will not be used for these:
    else:
        if wildcards.scentype == REV_DIR:
            templates = ACTUAL_DIR
        elif wildcards.scentype == REVRAND_DIR:
            templates = RANDOM_DIR
        elif wildcards.scentype == REVGREEDY_DIR:
            templates = GREEDY_DIR
        else:
            raise(Exception('Unknown scentype "{wildcards.scentype}"!'))

    scendir = chooseInScens(wildcards, templates)
    retval = f'{scendir}/{templates}.reps{REPS}.{wildcards.rep}.pgz'

    if CLOBBER:
        return retval
    return ancient(retval)


rule rescore_scenario:
    """
    Use the unscored template scenarios to create scored scenarios with
    heatmap locations. This scores everything but the of the split and mix
    sampled scenarios.
    """
    input:
        heatmaps = resolveHeatmaps,
        inscen = chooseInScen,

    output:
        '{dsdir}/{norm}_{res}/{scentype}.reps'+REPS+'/rescore.{rep}.pgz'

    log:
        '{dsdir}/{norm}_{res}/log/{scentype}.reps'+REPS+'/rescore/{rep}.log'

    run:
        outdir = Path(str(output)).parent
        shell('PYTHONPATH={PYTHONPATH} '
              'bin/analyzescenarios --rescore {outdir} {CLOBBER}'
              ' -w {wildcards.res}'
              ' --heatmap {input.heatmaps} {input.inscen} &> {log}')


rule construct_random_split_scenarios:
    """
    Construct random split scenarios directly from the split templates.
    """
    input:
        heatmaps = resolveHeatmaps,
        actscen = '{dsdir}/'+SPLIT_TEMPLATES+'.reps'+REPS+'/'+SPLIT_TEMPLATES+'.reps'+REPS+'.{rep}.pgz',

    output:
        '{dsdir}/{norm}_{res}/'+SPLIT+RANDOM_DIR+'.reps'+REPS+'/rescore.{rep}.pgz'

    log:
        '{dsdir}/{norm}_{res}/log/'+SPLIT+RANDOM_DIR+'.reps'+REPS+'/rescore/{rep}.log'

    run:
        outdir = Path(str(output)).parent
        shell('PYTHONPATH={PYTHONPATH} '
              'bin/analyzescenarios --randomize {outdir} '
              ' --heat {input.heatmaps}'
              ' -w {wildcards.res} {CLOBBER} {input.actscen} &> {log}')


rule construct_random_mix_scenarios:
    """
    Construct random mix scenarios directly from the mix templates.
    """
    input:
        heatmaps = resolveHeatmaps,
        actscen = '{dsdir}/'+MIX_TEMPLATES+'.reps'+REPS+'/'+MIX_TEMPLATES+'.reps'+REPS+'.{rep}.pgz',

    output:
        '{dsdir}/{norm}_{res}/'+MIX+RANDOM_DIR+'.reps'+REPS+'/rescore.{rep}.pgz'

    log:
        '{dsdir}/{norm}_{res}/log/'+MIX+RANDOM_DIR+'.reps'+REPS+'/rescore/{rep}.log'

    run:
        outdir = Path(str(output)).parent
        shell('PYTHONPATH={PYTHONPATH} '
              'bin/analyzescenarios --randomize {outdir} '
              ' --heat {input.heatmaps}'
              ' -w {wildcards.res} {CLOBBER} {input.actscen} &> {log}')


####### Intersection/Symdiff/Difference #####################################



def getIntersectNames():
    """
    Return a plot for all pairs of intersection directories from SETS.
    """
    filelist = []
    for (_,_,_,_,hmtype,res1,n1),(_,_,_,_,_,res2,n2) in combinations(SETS, 2):
      if(n1 and n2 and res1 == res2):
        if(n1 == n2):
          raise(Exception('Two version of the same data set activated for'+
                          ' heatmap INTERSECTION. Modify DATASETS.'))

        name = getComboDir(n1, n2, res1, hmtype)
        filelist.append('{}{}plot.reps{}.{}.intra.0.0.pdf'.\
                        format(INTERSECOUT, name, REPS, PLOT_DISTS_STR))
        filelist.append('{}{}plot.reps{}.{}.inter.0.0.pdf'.\
                        format(INTERSECOUT, name, REPS, PLOT_DISTS_STR))

    return filelist


def getComboHeatmapDirs(wildcards):
    """
    Return a list of heatmap dirs that would be the input to intersectmats.
    """
    return [HICDIR+idTOhms[wildcards.id1], HICDIR+idTOhms[wildcards.id2]]



ruleorder: create_difference_heatmaps > create_combination_heatmaps

rule create_difference_heatmaps:
    """
    Create a directory with difference heatmaps.
    """
    input:
        sourceheatmaps = getComboHeatmapDirs
    output:
        directory(HICDIR+DIFFHM+'{hmtype}/{id1}-{id2}/{id1}-{id2}/'),
        directory(HICDIR+DIFFHM+'{hmtype}/{id1}-{id2}/{id2}-{id1}/')
        #hiC/difference/lognorm/gm06690n-k562n/gm06690n-k562n,
        #hiC/difference/lognorm/gm06690n-k562n/k562n-gm06690n

    log:
        HICDIR+DIFFHM+'{hmtype}/{id1}-{id2}.log'

    run:
        hms1, hms2 = getComboHeatmapDirs(wildcards)

        flags = '--diff --ids {} --ids {}'.format(wildcards.id1, wildcards.id2)
        todir = HICDIR+DIFFHM+'{}/{}-{}/'.format(wildcards.hmtype,
                                                 wildcards.id1, wildcards.id2)

        # TODO: Is this really needed?
        #if(os.path.exists(str(todir))):
        #  shell('touch {}'.format(todir))
        #else:
        #  os.mkdir(todir)

        cwd = Path.cwd()
        hicabs = cwd/HICDIR
        command = hicabs/Path('intersectmats')

        shell(f'{command} {flags} {todir} {cwd/hms1} {cwd/hms2} &> {log}')


rule create_combination_heatmaps:
    """
    Create a directory with intersection/symdiff heatmaps.
    """
    input:
        sourceheatmaps = getComboHeatmapDirs
    output:
        #Path of form: hiC/intersection/lognorm/gm06690n-k562n/
        directory(HICDIR+'{combodir}/{hmtype}/{id1}-{id2}/')
        #hiC/intersection/lognorm/gm06690n-k562n

    log:
        HICDIR+'{combodir}/{hmtype}/{id1}-{id2}.log'

    run:
        hms1, hms2 = getComboHeatmapDirs(wildcards)
        flags = '--ids {} --ids {}'.format(wildcards.id1, wildcards.id2)
        if(wildcards.combodir == 'symdiff'):
            flags += ' --sym'
        elif(wildcards.combodir == 'intersection'):
            pass
        else:
            raise(Exception(f'Unknown combodir id "{wildcards.combodir}".'))

        # TODO: Is this really needed?
        #if(os.path.exists(str(output[0]))):
        #  shell('touch {}'.format(output[0]))
        #else:
        #  os.mkdir(str(output[0]))

        #shell('cd {}; PYTHONPATH={}:$PYTHONPATH; '.format(output[0],
        #                                                  PYTHONPATH) +\
        #      '{}intersectmats {} {} {}'.format(hicabs, flags,
        #                                        cwd+hms1, cwd+hms2) +\
        #      ' &> {log}')

        cwd = Path.cwd()
        hicabs = cwd/HICDIR
        command = hicabs/Path('intersectmats')

        shell(f'{command} {flags} {output[0]} {cwd/hms1} {cwd/hms2} &> {log}')



def getComboDirs(wildcards):
    """
    Return a directory name for intrachromosomal pairs of
    intersection/symdiff/difference names from SETS.
    """
    if(not COMBINATIONS_INTRA):
      raise(Exception('Empty COMBINATIONS_INTRA!'))
    return getComboDirsForSet(COMBINATIONS_INTRA, wildcards.combo)


def getInterchromosomalComboDirs(wildcards):
    """
    Return a directory name for interchromosomal pairs of
    intersection/symdiff/difference names from SETS.
    """
    if(not COMBINATIONS_INTER):
      raise(Exception('Empty COMBINATIONS_INTER!'))
    return getComboDirsForSet(COMBINATIONS_INTER, wildcards.combo)


def getComboDirsForSet(includeset, combotype):
    """
    Return intersection directory names only for those that have both cids in
    the includeset.

    @param includeset: the combo ids to do combinations of
    @param combotype:  the type of combination to do (intersection/symdiff/etc.)
    """
    dirs = []
    for (_,_,_,_,hmtype,res1,n1),(_,_,_,_,_,res2,n2) in combinations(SETS, 2):
      if n1 and n2 and n1 in includeset and n2 in includeset and res1 == res2:
        if n1 == n2:
          raise(Exception('Two version of the same data set activated for'+
                          ' heatmap INTERSECTION. Modify DATASETS.'))

        name = getComboDir(n1, n2, res1, hmtype)
        if combotype == 'intersection':
          outdir = INTERSECOUT
        elif combotype == 'symdiff':
          outdir = SYMDIFFOUT
        elif combotype == 'difference' or combotype == 'balance':
          dirs.append('{}{}/{}-{}/'.format(DIFFOUT, name, n1, n2))
          dirs.append('{}{}/{}-{}/'.format(DIFFOUT, name, n2, n1))
          continue
        else:
          raise(Exception('Unknown combo ID "{}"'.format(combotype)))

        dirs.append('{}{}{}.reps{}'.format(outdir, name, ACTUAL_DIR, REPS))
        dirs.append('{}{}{}.reps{}'.format(outdir, name, RANDOM_FROM_TEMPLATES,
                                           REPS))

    if not dirs:
      raise(Exception('DATASETS must specify each of {}.'.format(includeset)+
                      '\nCurrently these are specified:\n{}'.format(SETS)))
    return dirs



def getNormFromScenPath(path):
    """
    From a path to an intersection directory, get the normalization string.
    """
    m = re.match('output/.+/'+REPS+r'/(\w+)/.*', path)
    if m:
      return m.group(1)

    raise(Exception('Cannot get normalization from path: "{}"'.format(path)))


def getDiffHeatmapDirs(wildcards):
    """
    Return a list of all heatmaps dirs needed.
    """
    tomake = set()
    for pair in chain(combinations(COMBINATIONS_INTRA, 2),
                      combinations(COMBINATIONS_INTER, 2)):
        tomake.add(pair)

    retlist = []
    for id1,id2 in tomake:
        if(idTOnorm[id1] != idTOnorm[id2]):
            raise(Exception('Norm mismatch for "{}" and "{}".'.format(id1,id2)))
        basedir = '{}{}{}/{}-{}/'.format(HICDIR, DIFFHM, idTOnorm[id1], id1,id2)
        retlist.append('{}{}-{}/'.format(basedir, id1, id2))
        retlist.append('{}{}-{}/'.format(basedir, id2, id1))

    return retlist



ruleorder: create_balance_matrices > create_combo_matrix
ruleorder: create_balance_matrices > create_interchromosomal_combo_matrix

rule create_balance_matrices:
    """
    Create the balance matrix from heatmaps.
    """
    input:
        hicdirs = getDiffHeatmapDirs

    output:
        '{prefix}.balance.intra.csv',
        '{prefix}.balance.inter.csv'

    run:
        norm = idTOnorm[COMBINATIONS_INTRA[0]]
        prefstr = '--out-prefix '+wildcards.prefix

        flags = prefstr+' -b'
        shell('PYTHONPATH={PYTHONPATH} ' +\
              'bin/makeintersecmat {} {} {} {}'.\
              format(flags, REPS, norm, ' '.join(COMBINATIONS_INTRA)))
        flags = prefstr+' -b --inter'
        shell('PYTHONPATH={PYTHONPATH} ' +\
              'bin/makeintersecmat {} {} {} {}'.\
              format(flags, REPS, norm, ' '.join(COMBINATIONS_INTER)))




rule create_combo_matrix:
    """
    Create the intersection/symdiff/difference matrix from scenarios.
    """
    input:
        scendirs = getComboDirs

    output:
        '{prefix}.{combo}.intra.csv'

    run:
        norm = getNormFromScenPath(input.scendirs[0])
        prefstr = '--out-prefix '+wildcards.prefix

        flags = prefstr+' '
        if wildcards.combo == 'difference':
            flags += '-d'
        elif wildcards.combo == 'balance':
            flags += '-b'
        elif wildcards.combo == 'symdiff':
            flags += '-s'
        else:
            if wildcards.combo != 'intersection':
                raise(Exception('Unknown combination string "{}".'.\
                                format(wildcards.combo)))

        #shell('PYTHONPATH={PYTHONPATH} '+\
        #      'bin/makeintersecmat {} {} {} {} {}'.\
        #      format(flags, FUZZ, REPS, norm, ' '.join(COMBINATIONS_INTRA)))
        combos = ' '.join(COMBINATIONS_INTRA)
        shell('PYTHONPATH={PYTHONPATH} '+\
              'bin/makeintersecmat {flags} {REPS} {norm} {combos}')


rule create_interchromosomal_combo_matrix:
    """
    Create the interchromosomal intersection/symdiff/etc. matrix from
    scenarios.
    """
    input:
        scendirs = getInterchromosomalComboDirs

    output:
        '{prefix}.{combo}.inter.csv'

    run:
        norm = getNormFromScenPath(input.scendirs[0])
        prefstr = '--out-prefix '+wildcards.prefix

        flags = prefstr+' --inter '
        if(wildcards.combo == 'difference'):
            flags += '-d'
        elif(wildcards.combo == 'symdiff'):
            flags += '-s'
        else:
            if(wildcards.combo != 'intersection'):
                raise(Exception('Unknown combination string "{}".'.\
                                format(wildcards.combo)))

        shell('PYTHONPATH={PYTHONPATH} ' +\
              'bin/makeintersecmat {} {} {} {}'.\
              format(flags, REPS, norm, ' '.join(COMBINATIONS_INTER)))





rule rescore_combination_scenarios:
    """
    Create scenarios for the the intersection/symdiff data sets.
    """
    input:
        '{interdir}/'+REPS+'/{hmtype}/{id1}-{id2}/'+
        'plot.reps{}.{}.intra.0.0.pdf'.format(REPS, PLOT_DISTS_STR)

    output:
        directory('{interdir}/'+REPS+'/{hmtype}/{id1}-{id2}/'+ACTUAL_TEMPLATES),
        directory('{interdir}/'+REPS+'/{hmtype}/{id1}-{id2}/'+RANDOM_FROM_TEMPLATES),

    run:
        pass



def resolveHeatmapsDiff(wildcards):
    key = '{}{}/{}/{}-{}/{}-{}'.format(DIFFOUT, wildcards.res, wildcards.norm,
                                       wildcards.n1, wildcards.n2, wildcards.m1,
                                       wildcards.m2)
    if(CLOBBER):
        return HICDIR+scendirTOhms[key]
    return ancient(HICDIR+scendirTOhms[key])


def chooseInScensDiff(wildcards):
    key = '{}{}/{}/{}-{}/{}-{}'.format(DIFFOUT, wildcards.res, wildcards.norm,
                                       wildcards.n1, wildcards.n2, wildcards.m1,
                                       wildcards.m2)
    if(scendirTOspec[key] == SPECIES_A):
        return TEMPLATES_SCENS_AB+'templates.reps'+REPS
    else:
        return TEMPLATES_SCENS_BA+'templates.reps'+REPS



rule rescore_difference_scenarios:
    input:
        heatmaps = resolveHeatmapsDiff,
        inscens = chooseInScensDiff,

    output:
        directory(DIFFOUT+'{norm}/{res}/{n1}-{n2}/{m1}-{m2}')

    shell:
        'PYTHONPATH={PYTHONPATH} '
        'bin/analyzescenarios --rescore {output} {CLOBBER}'
        ' -w {res} --heat {input.heatmaps} {input.inscens}'


rule create_heatmaps_of_intersections:
    input:
        '{COMBINATIONS_NAME}.intersection.intra.csv',
        '{COMBINATIONS_NAME}.intersection.inter.csv',

    output:
        '{COMBINATIONS_NAME}.intersection.intra.csv.pdf',
        '{COMBINATIONS_NAME}.intersection.inter.csv.pdf'

    shell:
        '{OUTDIR}intermattoheatmap {COMBINATIONS_NAME}.intersection.intra.csv; '
        '{OUTDIR}intermattoheatmap {COMBINATIONS_NAME}.intersection.inter.csv '



rule create_heatmaps_of_combinations_interleaved:
    input:
        '{COMBINATIONS_NAME}.intersection.intra.csv',
        '{COMBINATIONS_NAME}.balance.intra.csv',
        '{COMBINATIONS_NAME}.difference.intra.csv',
        '{COMBINATIONS_NAME}.symdiff.intra.csv',

        '{COMBINATIONS_NAME}.intersection.inter.csv',
        '{COMBINATIONS_NAME}.balance.inter.csv',
        '{COMBINATIONS_NAME}.difference.inter.csv',
        '{COMBINATIONS_NAME}.symdiff.inter.csv'

    output:
        '{COMBINATIONS_NAME}.combomatrixFULL.intra.pdf',
        '{COMBINATIONS_NAME}.combomatrixFULL.inter.pdf'

    shell:
        '{OUTDIR}intermattoheatmap -i -b {COMBINATIONS_NAME}.balance.intra.csv '
        '-d {COMBINATIONS_NAME}.difference.intra.csv '
        '{COMBINATIONS_NAME}.intersection.intra.csv '
        '{COMBINATIONS_NAME}.symdiff.intra.csv '
        '--out-name {COMBINATIONS_NAME}.combomatrixFULL.intra; '
        '{OUTDIR}intermattoheatmap -i -b {COMBINATIONS_NAME}.balance.inter.csv '
        '-d {COMBINATIONS_NAME}.difference.inter.csv '
        '{COMBINATIONS_NAME}.intersection.inter.csv '
        '{COMBINATIONS_NAME}.symdiff.inter.csv '
        '--out-name {COMBINATIONS_NAME}.combomatrixFULL.inter'



####### Greedy Scenarios #################################


rule greedy_scenarios:
    input:
        heatmaps = resolveHeatmaps,
        syntenyfile = chooseSyntenyfile,

    output:
        directory('{dsdir}/{norm}_{res}/'+GREEDY_DIR+'.reps'+GREEDY_REPS)

    log:
        '{dsdir}/{norm}_{res}/log/'+GREEDY_DIR+'.reps'+REPS+'.log'

    shell:
        'echo "WARNING: greedy scenario generation is EXTREMELY slow!";\n'
        'PYTHONPATH={PYTHONPATH} '
        'bin/localdist -i {GREEDY_REPS} --greedy --logscen {output} {CLOBBER}'
        ' {SCORE_FISSION} --heatmaps {input.heatmaps} -w {wildcards.res}'
        ' -f {input.syntenyfile} &> {log}'




####### Reverse Scenarios #################################


def oppositeHeatmaps(wildcards):
    """
    Return the heatmap location for the opposite species. wildcards.dsdir
    must be the path to the query data set.
    """
    if wildcards.dsdir not in OTHER_DATASET:
         sys.exit(f'ERROR: {wildcards.dsdir} has no partner:\n\t'
                  'reversing a scenario requires PARTNER_DATASET.')

    otherdataset = f'{OTHER_DATASET[wildcards.dsdir]}/{wildcards.norm}_{wildcards.res}'
    if otherdataset not in scendirTOhms:
        raise(Exception(MISSING_OTHER.format(otherdataset, scendirTOhms)))

    if CLOBBER:
      return HICDIR+scendirTOhms[otherdataset]

    return ancient(HICDIR+scendirTOhms[otherdataset])


ruleorder: reverse_scenario > rescore_scenario

rule reverse_scenario:
    input:
        scenario = '{dsdir}/{norm}_{res}/'+ACTUAL_DIR+'.reps'+REPS+'/rescore.{rep}.pgz',
        heatmaps = oppositeHeatmaps,
        syntenyfile = chooseSyntenyfile,

    output:
        '{dsdir}/{norm}_{res}/'+REV_DIR+'.reps'+REPS+'/reversed.{rep}.pgz'

    log:
        '{dsdir}/{norm}_{res}/log/'+REV_DIR+'.reps'+REPS+'/reversed.{rep}.pgz'

    params:
        CLOBBER+' -w {res}'

    run:
        outdir = Path(str(output)).parent
        shell('PYTHONPATH={PYTHONPATH} '
              'bin/reversescenarios {params} {input.syntenyfile} '
              '{input.heatmaps} {input.scenario} {outdir} &> {log}')


ruleorder: reverse_random_scenario > rescore_scenario

rule reverse_random_scenario:
    input:
        scenario = '{dsdir}/{norm}_{res}/'+RANDOM_DIR+'.reps'+REPS+'/rescore.{rep}.pgz',
        heatmaps = oppositeHeatmaps,
        syntenyfile = chooseSyntenyfile,

    output:
        '{dsdir}/{norm}_{res}/'+REVRAND_DIR+'.reps'+REPS+'/reversed.{rep}.pgz'

    log:
        '{dsdir}/{norm}_{res}/log/'+REVRAND_DIR+'.reps'+REPS+'/reversed.{rep}.pgz'

    params:
        CLOBBER+' -w {res}'

    run:
        outdir = Path(str(output)).parent
        shell('PYTHONPATH={PYTHONPATH} '
              'bin/reversescenarios {params} {input.syntenyfile} '
              '{input.heatmaps} {input.scenario} {outdir} &> {log}')


ruleorder: reverse_greedy_scenario > rescore_scenario

rule reverse_greedy_scenario:
    input:
        scenario = '{dsdir}/{norm}_{res}/'+GREEDY_DIR+'.reps'+GREEDY_REPS+'/rescore.{rep}.pgz',
        heatmaps = oppositeHeatmaps,
        syntenyfile = chooseSyntenyfile,

    output:
        '{dsdir}/{norm}_{res}/'+REVGREEDY_DIR+'.reps'+GREEDY_REPS+'/reversed.{rep}.pgz'

    log:
        '{dsdir}/{norm}_{res}/log/'+REVGREEDY_DIR+'.reps'+GREEDY_REPS+'/reversed.{rep}.pgz'

    params:
        CLOBBER+' -w {res}'

    run:
        outdir = Path(str(output)).parent
        shell('PYTHONPATH={PYTHONPATH} '
              'bin/reversescenarios {params} {input.syntenyfile} '
              '{input.heatmaps} {input.scenario} {outdir} &> {log}')





####### Make Plots #################################

rule plot_actualVSrandom:
    """
    Plot the actual scenarios against the randomized scenarios.
    """
    input:
        actscens = actualscenariosASinput,
        randscens = randomscenariosASinput,

    output:
        '{dsdir}/{norm}_{res}/plot.reps{reps}.'+PLOT_DISTS_STR+'.inter.{dmin}.0.pdf',
        '{dsdir}/{norm}_{res}/plot.reps{reps}.'+PLOT_DISTS_STR+'.intra.{dmin}.0.pdf',

    log:
        '{dsdir}/{norm}_{res}/log/plot.reps{reps}.'+PLOT_DISTS_STR+'{dmin}.0.log',

    run:
        actdir = Path(input.actscens[0]).parent
        randdir = Path(input.randscens[0]).parent
        dataset = Path(wildcards.dsdir).name
        shell('PYTHONPATH={PYTHONPATH} '
              'bin/analyzescenarios --intra --inter --plot '
              '{wildcards.dsdir}/{wildcards.norm}_{wildcards.res}/plot.'
              'reps{wildcards.reps}.{PLOT_DISTS_STR} '
              '{actdir} -m {randdir} {USE_MEDIAN} '
              '--caption "{PLOT_DISTS_DESC}" '
              '--title "{dataset}" '
              '{CLOBBER} {DMIN_STR} &> {log}')



rule plot_actualVSwander:
    """
    Plot the actual scenarios against the random wandering scenarios.
    """
    input:
        actscens = actualscenariosASinput,
        wandscens = wanderscenariosASinput,

    output:
        '{dsdir}/{norm}_{res}/plot.reps{reps}.'+PLOT_WANDER_STR+'.intra.{dmin}.0.pdf',
        '{dsdir}/{norm}_{res}/plot.reps{reps}.'+PLOT_WANDER_STR+'.inter.{dmin}.0.pdf',

    log:
        '{dsdir}/{norm}_{res}/log/plot.reps{reps}.'+PLOT_WANDER_STR+'{dmin}.0.log',

    run:
        actdir = Path(input.actscens[0]).parent
        wanddir = Path(input.wandscens[0]).parent
        dataset = Path(wildcards.dsdir).name
        shell('PYTHONPATH={PYTHONPATH} '
              'bin/analyzescenarios --intra --inter --plot '
              '{wildcards.dsdir}/{wildcards.norm}_{wildcards.res}/plot.reps{wildcards.reps}.'
              '{PLOT_WANDER_STR} {actdir} -m {wanddir} {USE_MEDIAN} '
              '--caption "{PLOT_WANDER_DESC}" '
              '--title "{dataset}" '
              '{DMIN_STR} &> {log}')


rule plot_per_move:
    input:
        actscens = actualscenariosASinput,
        randscens = randomscenariosASinput,

    output:
        '{dsdir}/{norm}_{res}/plot.reps{reps}.'+PLOT_PERMOVE_STR+'.{type}.0.0.pdf'

    log:
        '{dsdir}/{norm}_{res}/log/plot.reps{reps}.'+PLOT_PERMOVE_STR+'{type}.0.0.log',

    run:
        actdir = Path(input.actscens[0]).parent
        randdir = Path(input.randscens[0]).parent
        scendir = f'{wildcards.dsdir}/{wildcards.norm}_{wildcards.res}'
        dataset = Path(wildcards.dsdir).name
        shell(f'PYTHONPATH={PYTHONPATH} '
              f'bin/analyzescenarios '
              f'--permove {wildcards.dsdir}/{wildcards.norm}_{wildcards.res}/plot.'
              f'reps{wildcards.reps}.{PLOT_PERMOVE_STR} --{wildcards.type} '
              f'--heat {scendirTOhms[scendir]} '
              f'--caption "{PLOT_PERMOVE_DESC}" '
              f'--title "{dataset} {wildcards.type}" '
              f'--multiple {randdir} {actdir} {USE_MEDIAN} '
              f'&> {log}')



rule plot_per_scenario:
    input:
        actscenario = '{dsdir}/{norm}_{res}/'+ACTUAL_DIR+'.reps'+REPS+'/rescore.{rep}.pgz',
        randoms = randomPERscenariosASinput,

    output:
        '{dsdir}/{norm}_{res}/'+RANDOM_PER_DIR+'.reps'+REPS+\
        '/plot.reps{per_randreps}.'+PLOT_PERSCEN_STR+\
        '_{rep}.intra.0.0.pdf',
        '{dsdir}/{norm}_{res}/'+RANDOM_PER_DIR+'.reps'+REPS+\
        '/plot.reps{per_randreps}.'+PLOT_PERSCEN_STR+\
        '_{rep}.inter.0.0.pdf',

    log:
        '{dsdir}/{norm}_{res}/log/plot.reps{per_randreps}.'+PLOT_PERSCEN_STR+'_{rep}.0.0.log',

    run:
        dataset = Path(wildcards.dsdir).name
        shell('PYTHONPATH={PYTHONPATH} '
              'bin/analyzescenarios --inter --intra --plot '
              '{wildcards.dsdir}/{wildcards.norm}_{wildcards.res}/{RANDOM_PER_DIR}.reps{REPS}/'
              'plot.reps{wildcards.per_randreps}.'
              '{PLOT_PERSCEN_STR}_{wildcards.rep} {input.randoms} '
              '--caption "{PLOT_PERSCEN_DESC}" '
              '--title "{dataset}" '
              '--perscen {input.actscenario} &> {log}')



rule plot_by_window:
    input:
        actscens = actualscenariosASinput,
        randscens = randomscenariosASinput,

    output:
        '{dsdir}/{norm}_{res}/varDistPlot-None-20.1.20.reps'+REPS+'.random.pdf',
        '{dsdir}/{norm}_{res}/varDistPlot-None-200.20.10.reps'+REPS+'.random.pdf',

    log:
        log1 = '{dsdir}/{norm}_{res}/log/varDistPlot-None-20.1.20.reps'+REPS+'.random.log',
        log2 = '{dsdir}/{norm}_{res}/log/varDistPlot-None-200.20.10.reps'+REPS+'.random.log',


    run:
        actdir = Path(input.actscens[0]).parent
        randdir = Path(input.randscens[0]).parent
        dataset = Path(wildcards.dsdir).name
        shell('PYTHONPATH={PYTHONPATH} '
              "caption=$'{PLOT_BYWINDOW_DESC} (20 bins over 20 megabases)'; "
              'bin/analyzescenarios --suffix reps'+REPS+'.random '
              '--vary 20 --vrange 1 --numsteps 20 {actdir} -m {randdir} '
              '--directory {wildcards.dsdir}/{wildcards.norm}_{wildcards.res}/ '
              '--caption "$caption" '
              '--title "{dataset}" '
              '{CLOBBER} --intra &> {log.log1}; '
              "caption=$'{PLOT_BYWINDOW_DESC} (10 bins over 200 megabases)'; "
              'bin/analyzescenarios --suffix reps'+REPS+'.random '
              '--vary 200 --vrange 20 --numsteps 10 {actdir} -m {randdir} '
              '--directory {wildcards.dsdir}/{wildcards.norm}_{wildcards.res}/ {CLOBBER} '
              '--caption "$caption" '
              '--title "{dataset}" '
              '--intra &> {log.log2}') #--markstd



rule plot_greedy:
    """
    Plot the greedy average against the actual distribution.
    """
    input:
        scens = actualscenariosASinput,
        gscens = greedyscenariosASinput,

    output:
        intra = '{dsdir}/{norm}_{res}/plot.reps'+REPS+'.'+PLOT_GREEDYONACTUAL_STR+'.intra.0.0.pdf',
        inter = '{dsdir}/{norm}_{res}/plot.reps'+REPS+'.'+PLOT_GREEDYONACTUAL_STR+'.inter.0.0.pdf',

    log:
        '{dsdir}/{norm}_{res}/log/plot.reps'+REPS+'.'+PLOT_GREEDYONACTUAL_STR+'.0.0.log',

    run:
        m = re.match(r'(.+)\.intra\.0\.0\.pdf', output['intra'])
        if m:
          outprefix = m.group(1)
          intraave, interave = getAverageFromDist(input.gscens)

          actdir = Path(input.scens[0]).parent
          dataset = Path(wildcards.dsdir).name
          shell('PYTHONPATH={PYTHONPATH} '
                'bin/analyzescenarios --plot '+outprefix+CLOBBER+
                ' --intra -V '+intraave+' --caption "{PLOT_GREEDYONACTUAL_DESC}"'
                ' --title "{dataset} intra"'
                ' --color {GREEDY_COLOR} {actdir} &> {log}')
          shell('PYTHONPATH={PYTHONPATH} '
                'bin/analyzescenarios --plot '+outprefix+CLOBBER+
                ' --inter -V '+interave+' --caption "{PLOT_GREEDYONACTUAL_DESC}"'
                ' --title "{dataset} inter"'
                ' --color {GREEDY_COLOR} {actdir} >> {log} 2>&1')
        else:
          sys.exit('ERROR: unknown intra file format!')


rule plot_greedy_and_reversed:
    input:
        scens = actualscenariosASinput,
        gscens = greedyscenariosASinput,
        rgscens = revgreedyscenariosASinput,

    output:
        intra = '{dsdir}/{norm}_{res}/plot.reps'+REPS+'.'+PLOT_GREEDY_REVERSED_STR+'.intra.0.0.pdf',

    log:
        '{dsdir}/{norm}_{res}/log/plot.reps'+REPS+'.'+PLOT_GREEDY_REVERSED_STR+'.intra.0.0.log',

    run:
        m = re.match(r'(.+)\.intra\.0\.0\.pdf', output['intra'])
        if m:
          outprefix = m.group(1)
          actdir = Path(input.scens[0]).parent

                #Get intra average:
          intraave, interave = getAverageFromDist(input.gscens)
                #Get greedy intra average:
          revintraave, revinterave = getAverageFromDist(input.rgscens)

          dataset = Path(wildcards.dsdir).name
          shell('PYTHONPATH={PYTHONPATH} ' +
                'bin/analyzescenarios --plot '+outprefix+CLOBBER+
                ' --intra -V '+intraave+' -V '+revintraave+
                ' --color {GREEDY_COLOR} --color {REV_GREEDY_COLOR}'
                ' --caption "{PLOT_GREEDY_REVERSED_DESC}"'
                ' --title "{dataset} intra"'
                ' {actdir} &> {log}')
        else:
          sys.exit('ERROR: unknown intra file format!')


rule plot_actualVSreverse:
    """
    Plot actual distribution against the distribution of its reversed scenarios.
    """
    input:
        actscens = actualscenariosASinput,
        revscens = reversedscenariosASinput,

    output:
        '{dsdir}/{norm}_{res}/plot.reps'+REPS+'.'+PLOT_REVERSED_STR+'.inter.0.0.pdf',
        intra = '{dsdir}/{norm}_{res}/plot.reps'+REPS+'.'+PLOT_REVERSED_STR+'.intra.0.0.pdf',

    log:
        '{dsdir}/{norm}_{res}/log/plot.reps'+REPS+'.'+PLOT_REVERSED_STR+'.0.0.log',

    run:
        m = re.match(r'(.+)\.intra\.0\.0\.pdf', output['intra'])
        if m:
          outprefix = m.group(1)
          actdir = Path(input.actscens[0]).parent
          revdir = Path(input.revscens[0]).parent
          dataset = Path(wildcards.dsdir).name
          shell('PYTHONPATH={PYTHONPATH} ' +\
                'bin/analyzescenarios --intra --inter --plot '+outprefix+CLOBBER+\
                ' --title "{dataset}"'
                ' --caption "{PLOT_REVERSED_DESC}" {actdir} -m {revdir} &> {log}')
        else:
          sys.exit('ERROR: unknown intra file format!')


rule plot_randomVSreversedrand:
    """
    Plot random scenarios against reversed random scenarios.
    """
    input:
        random = randomscenariosASinput,
        revscens = revrandomscenariosASinput,

    output:
        '{dsdir}/{norm}_{res}/plot.reps'+REPS+'.'+PLOT_RAND_REVERSED_STR+'.inter.0.0.pdf',
        intra = '{dsdir}/{norm}_{res}/plot.reps'+REPS+'.'+PLOT_RAND_REVERSED_STR+'.intra.0.0.pdf',

    log:
        '{dsdir}/{norm}_{res}/log/plot.reps'+REPS+'.'+PLOT_RAND_REVERSED_STR+'.0.0.log',

    run:
        m = re.match(r'(.+)\.intra\.0\.0\.pdf', output['intra'])
        if m:
          randdir = Path(input.random[0]).parent
          revdir = Path(input.revscens[0]).parent
          outprefix = m.group(1)
          dataset = Path(wildcards.dsdir).name
          shell('PYTHONPATH={PYTHONPATH} '+
                'bin/analyzescenarios --intra --inter --plot {outprefix}'
                ' --title "{dataset}"'
                ' {CLOBBER} --caption "{PLOT_RAND_REVERSED_DESC}"'
                ' {randdir} -m {revdir} &> {log}')
        else:
          sys.exit('ERROR: unknown intra file format!')


rule plot_reversedVSreversedrand:
    """
    Plot reversed actual scenarios against reversed random.
    """
    input:
        rev = reversedscenariosASinput,
        randrev = revrandomscenariosASinput,

    output:
        '{dsdir}/{norm}_{res}/plot.reps'+REPS+'.'+PLOT_REV_REV_STR+'.inter.0.0.pdf',
        intra = '{dsdir}/{norm}_{res}/plot.reps'+REPS+'.'+PLOT_REV_REV_STR+'.intra.0.0.pdf',

    log:
        '{dsdir}/{norm}_{res}/log/plot.reps'+REPS+'.'+PLOT_REV_REV_STR+'.0.0.log',

    run:
        m = re.match(r'(.+)\.intra\.0\.0\.pdf', output['intra'])
        if m:
          outprefix = m.group(1)
          revdir = Path(input.rev[0]).parent
          randrevdir = Path(input.randrev[0]).parent
          dataset = Path(wildcards.dsdir).name
          shell('PYTHONPATH={PYTHONPATH} '
                'bin/analyzescenarios --intra --inter --plot {outprefix}'
                ' --title "{dataset}"'
                ' {CLOBBER} --caption "{PLOT_REV_REV_DESC}"'
                ' {revdir} -m {randrevdir} &> {log}')
        else:
          sys.exit('ERROR: unknown intra file format!')


rule plot_greedyrevave_on_reversed:
    """
    Plot the greedy reversed average on the reversed distribution.
    """
    input:
        reversed = reversedscenariosASinput,
        rgscens = revgreedyscenariosASinput,

    output:
        intra = '{dsdir}/{norm}_{res}/plot.reps'+REPS+'.'+PLOT_GREEDYREVONREV_STR+'.intra.0.0.pdf',

    log:
        '{dsdir}/{norm}_{res}/log/plot.reps'+REPS+'.'+PLOT_GREEDYREVONREV_STR+'.intra.0.0.log',

    run:
        m = re.match(r'(.+)\.intra\.0\.0\.pdf', output['intra'])
        if m:
          outprefix = m.group(1)
          revdir = Path(input.reversed[0]).parent
          rgdir = Path(input.rgscens[0]).parent
                #Get greedy intra average:
          revintraave, revinterave = getAverageFromDist(rgdir)

          dataset = Path(wildcards.dsdir).name
          shell('PYTHONPATH={PYTHONPATH} '
                'bin/analyzescenarios --plot {outprefix} --intra -V {CLOBBER}'
                ' --title "{dataset} intra"'
                ' {revintraave} --color {REV_GREEDY_COLOR}'
                ' --caption "{PLOT_GREEDYREVONREV_DESC}"'
                ' {revdir} &> {log}')
        else:
          sys.exit('ERROR: unknown intra file format!')


def oppositeScenarios(wildcards):
    """
    Return the actual scenarios location for the opposite species.
    wildcards.dsdir must be the path to the query data set.
    """
    if wildcards.dsdir not in OTHER_DATASET:
         sys.exit('ERROR: {} has no partner:\n\t'.format(wildcards.dsdir)+\
                  'reversing a scenario requires a specified PARTNER_HEATMAPS.')
    return scenariosASinput(wildcards, ACTUAL_DIR,
                            OTHER_DATASET[wildcards.dsdir])

rule plot_reverse_on_other:
    """
    Plot the reversed scenarios against the actual scenarios from the other
    data set.
    """
    input:
        reversedscenarios = reversedscenariosASinput,
        otherscenarios = oppositeScenarios,

    output:
        '{dsdir}/{norm}_{res}/plot.reps'+REPS+'.'+PLOT_REVONOTHER_STR+'.inter.0.0.pdf',
        intra = '{dsdir}/{norm}_{res}/plot.reps'+REPS+'.'+PLOT_REVONOTHER_STR+'.intra.0.0.pdf',

    log:
        '{dsdir}/{norm}_{res}/log/plot.reps'+REPS+'.'+PLOT_REVONOTHER_STR+'.0.0.log',

    run:
        m = re.match(r'(.+)\.intra\.0\.0\.pdf', output['intra'])
        if m:
          otherdir = Path(input.otherscenarios[0]).parent
          revdir = Path(input.reversedscenarios[0]).parent
          outprefix = m.group(1)
          dataset = Path(wildcards.dsdir).name
          shell('PYTHONPATH={PYTHONPATH} '
                'bin/analyzescenarios --intra --inter --plot {outprefix}'
                ' --title "{dataset}"'
                ' {CLOBBER} --caption "{PLOT_REVONOTHER_DESC}"'
                ' {revdir} -m {otherdir} &> {log}')
        else:
          sys.exit('ERROR: unknown intra file format!')



rule plot_heatBYfrequency:
    """
    Plot the moves from actual scenarios by frequency of occurrence.
    """
    input:
        actscens = actualscenariosASinput,

    output:
        '{dsdir}/{norm}_{res}/plot.reps{reps}.'+PLOT_FREQ_STR+'.{type}.{dmin}.pdf'

    log:
        '{dsdir}/{norm}_{res}/log/plot.reps{reps}.'+PLOT_FREQ_STR+'.{type}.{dmin}.log'

    run:
        actdir = Path(input.actscens[0]).parent
        dataset = Path(wildcards.dsdir).name
        shell('PYTHONPATH={PYTHONPATH} '
              'bin/analyzescenarios --{wildcards.type} --reps '
              '{wildcards.dsdir}/{wildcards.norm}_{wildcards.res}/plot.reps{wildcards.reps}.'
              '{PLOT_FREQ_STR} --caption "{PLOT_FREQ_DESC}" '
              '--title "{dataset} {wildcards.type}" '
              '{actdir} {DMIN_STR} &> {log}')



rule plot_actualVSrandom_ignore:
    """
    Plot the actual scenarios against the randomized scenarios while ignoring
    the most commmon moves.
    """
    input:
        actscens = actualscenariosASinput,
        random = randomscenariosASinput,

    output:
        '{dsdir}/{norm}_{res}/plot.reps{reps}.'+PLOT_IGNORE_STR+'.inter.{dmin}.0.pdf',
        '{dsdir}/{norm}_{res}/plot.reps{reps}.'+PLOT_IGNORE_STR+'.intra.{dmin}.0.pdf',

    log:
        '{dsdir}/{norm}_{res}/log/plot.reps{reps}.'+PLOT_IGNORE_STR+'{dmin}.0.log',

    run:
        randdir = Path(input.random[0]).parent
        actdir = Path(input.actscens[0]).parent
        dataset = Path(wildcards.dsdir).name
        shell('PYTHONPATH={PYTHONPATH} '
              'bin/analyzescenarios --intra --inter --plot '
              '{wildcards.dsdir}/{wildcards.norm}_{wildcards.res}/plot.reps{wildcards.reps}.'
              '{PLOT_IGNORE_STR} {actdir} -m {randdir} {USE_MEDIAN} '
              '--caption "{PLOT_IGNORE_DESC}" '
              '{CLOBBER} {DMIN_STR} '
              '--title "{dataset}" '
              '--ignorecommon {IGNORE_PROPORTION} &> {log}')



####### Scrambled #################################



def createScramActualScenList(ws):
    """
    Make a list of scrambled actual scenario directories.
    """
    scenlist = [f'{ws.dsdir}/{ws.norm}_{ws.res}/{SCRAM_ACT_DIR}_{i}.reps{REPS}'
                for i in range(SCRAM_PVAL_REPS)]
    return scenlist


def createScramRandomScenList(ws):
    """
    Make a list of scrambled random scenario directories.
    """
    scenlist = [f'{ws.dsdir}/{ws.norm}_{ws.res}/{SCRAM_RAND_DIR}_{i}.reps{REPS}'
                for i in range(SCRAM_PVAL_REPS)]
    return scenlist


rule pvalue_FromManyScrambled:
    """
    Make many scrambled heatmaps for actual and random scenarios.  Use the
    difference between the means in these to distributions to compute a p-value
    for the difference in the means of the actual and random scenarios.
    """
    input:
        scramactualscens = createScramActualScenList,
        scramrandomscens = createScramRandomScenList,
        actual = actualscenariosASinput,
        random = randomscenariosASinput,

    output:
        '{dsdir}/{norm}_{res}/scram_pvalue_'+str(SCRAM_PVAL_REPS)+'.txt'

    log:
        '{dsdir}/{norm}_{res}/log/scram_pvalue_'+str(SCRAM_PVAL_REPS)+'.log'

    run:
        actdir = Path(input.actual[0]).parent
        randdir = Path(input.random[0]).parent
        shell('PYTHONPATH={PYTHONPATH} '
              'bin/analyzescenarios {actdir} -m {randdir} '
              '--scrambled {wildcards.dsdir}/{wildcards.norm}_{wildcards.res}/'+SCRAM_ACT_DIR+\
              ' {wildcards.dsdir}/{wildcards.norm}_{wildcards.res}/'+SCRAM_RAND_DIR+' > {output}'
              ' 2> {log}')


def scramactualscenariosASinput(wildcards):
    retval = f'{wildcards.dsdir}/{wildcards.norm}_{wildcards.res}/{SCRAM_ACT_DIR}'+\
             f'.reps{REPS}'
    if CLOBBER:
        return retval
    return ancient(retval)

def scramrandomscenariosASinput(wildcards):
    retval = f'{wildcards.dsdir}/{wildcards.norm}_{wildcards.res}/{SCRAM_RAND_DIR}'+\
             f'.reps{REPS}'
    if CLOBBER:
        return retval
    return ancient(retval)


rule plot_scrambledactualVSrandom:
    """
	Plot the actual scenarios evaluated on scrambled matrices, against the
    randomized scenarios evauated on real matrices.

    @note: this could be simplified (now that I know snakemake better)
    """
    input:
        scrambled = scramactualscenariosASinput,
        random = randomscenariosASinput,

    output:
        '{dsdir}/{norm}_{res}/plot.reps'+REPS+'.'+PLOT_SCRAM_V_NON_STR+\
            '.inter.0.0.pdf',
        intra = '{dsdir}/{norm}_{res}/plot.reps'+REPS+'.'+PLOT_SCRAM_V_NON_STR+\
                '.intra.0.0.pdf',
    log:
        '{dsdir}/{norm}_{res}/log/plot.reps'+REPS+'.'+PLOT_SCRAM_V_NON_STR+\
            '.0.0.log',

    run:
        m = re.match(r'(.+)\.intra\.0\.0\.pdf', output['intra'])
        if m:
          outprefix = m.group(1)
          randdir = Path(input.random[0]).parent
          dataset = Path(wildcards.dsdir).name
          shell('PYTHONPATH={PYTHONPATH} ' +\
                'bin/analyzescenarios --intra --inter --plot '+outprefix+CLOBBER+\
                ' --title "{dataset}"'
                ' {input.scrambled} -m {randdir} &> {log}')
        else:
          sys.exit('ERROR: unknown intra file format!')


rule plot_scrambledactualVSscrambledrandom:
    """
	Plot the actual against the randomized scenarios, both evaluated on
    scrambled matrices.

    @note: this could be simplified (now that I know snakemake better)
    """
    input:
        scrambled = scramactualscenariosASinput,
        random = scramrandomscenariosASinput,

    output:
        '{dsdir}/{norm}_{res}/plot.reps'+REPS+'.'+PLOT_SCRAM_V_SCRAM_STR+\
            '.inter.0.0.pdf',
        intra = '{dsdir}/{norm}_{res}/plot.reps'+REPS+'.'+\
                PLOT_SCRAM_V_SCRAM_STR+'.intra.0.0.pdf',
        
    log:
        '{dsdir}/{norm}_{res}/log/plot.reps'+REPS+'.'+PLOT_SCRAM_V_SCRAM_STR+\
            '.0.0.log',

    run:
        m = re.match(r'(.+)\.intra\.0\.0\.pdf', output['intra'])
        if m:
          outprefix = m.group(1)
          dataset = Path(wildcards.dsdir).name
          shell('PYTHONPATH={PYTHONPATH} '+
                'bin/analyzescenarios --intra --inter --plot '+outprefix+
                ' --title "{dataset}"'
                ' {CLOBBER} {input.scrambled} -m {input.random} &> {log}')
        else:
          sys.exit('ERROR: unknown intra file format!')


heatmapre = re.compile(r'(\S+norm\S+)_(\d+)')
def resolveHeatmapScrambled(ws):
    """
    Compute the name of the scrambled heatmap.
    """
    key = f'{ws.dsdir}/{ws.norm}_{ws.res}'

    if key not in scendirTOhms:
      raise(Exception(f'Unknown directory "{key}"! \nIn: {scendirTOhms}'))

    p = Path(scendirTOhms[key])
    m = heatmapre.match(p.name)
    if not m:
      raise(Exception(f'Unexpected heatmap format "{scendirTOhms[key]}"!'))

    name = (f'{HICDIR}{p.parent}/{SCRAMBLED_HM}/{ws.norm}_{ws.res}/'
            f'{SCRAMBLED_HM}.{m.group(1)}_{ws.res}.cool')

    if CLOBBER:
      return name
    return ancient(name)


rule rescore_actual_scenarios_scrambled:
    """
    Rescore the actual scenarios using scrambled matrices.

    @note: this assumes scrambled matrices will have a SCRAMBLED_HM prefix
           appended to the standard matrix name
    """
    input:
        heatmaps = resolveHeatmapScrambled,
        inscens = actualscenariosASinput,

    output:
        outdir = directory('{dsdir}/{norm}_{res}/'+SCRAM_ACT_DIR+'.reps'+REPS)

    log:
        '{dsdir}/log/{norm}_{res}/'+SCRAM_ACT_DIR+'.reps'+REPS+'.log'

    params:
        '--noweight -w {res} '+CLOBBER

    run:
        actdir = Path(input.inscens[0]).parent
        shell('PYTHONPATH={PYTHONPATH} '
              'bin/analyzescenarios {params} --rescore {output.outdir}'
              ' --heat {input.heatmaps} {actdir} &> {log}')


rule rescore_random_scenarios_scrambled:
    """
    Rescore the randomized scenarios using scrambled matrices.

    @note: this assumes scrambled matrices will have a SCRAMBLED_HM prefix
           appended to the standard matrix name
    """
    input:
        heatmaps = resolveHeatmapScrambled,
        inscens = randomscenariosASinput,

    output:
        directory('{dsdir}/{norm}_{res}/'+SCRAM_RAND_DIR+'.reps'+REPS)

    log:
        '{dsdir}/log/{norm}_{res}/'+SCRAM_RAND_DIR+'.reps'+REPS+'.log'

    params:
        '--noweight -w {res} '+CLOBBER

    run:
        randdir = Path(input.inscens[0]).parent
        shell('PYTHONPATH={PYTHONPATH} '
              'bin/analyzescenarios {params} --rescore {output}'
              ' --heat {input.heatmaps} {randdir} &> {log}')


def resolveHeatmapsScrambledPVAL(ws):
    key = f'{ws.dsdir}/{ws.norm}_{ws.res}'

    if key not in scendirTOhms:
      raise(Exception(f'Unknown directory "{key}"! \nIn: {scendirTOhms}'))

    p = Path(scendirTOhms[key])
    m = heatmapre.match(p.name)
    if not m:
      raise(Exception(f'Unexpected heatmap format "{scendirTOhms[key]}"!'))

    name = (f'{HICDIR}{p.parent}/{SCRAMBLED_HM}/{ws.norm}_{ws.res}/'
            f'{SCRAMBLED_HM}_{ws.scramrep}.{m.group(1)}_{ws.res}.cool')

    if CLOBBER:
      return name
    return ancient(name)


rule rescore_actual_scenarios_scrambledPVAL:
    """
    Rescore the actual scenarios using scrambled matrices.

    @note: this assumes scrambled matrices will have a SCRAMBLED_HM prefix
           appended to the standard matrix name
    """
    input:
        heatmaps = resolveHeatmapsScrambledPVAL,
        inscens = actualscenariosASinput,

    output:
        directory('{dsdir}/{norm}_{res}/'+SCRAM_ACT_DIR+'_{scramrep}.reps'+REPS)

    log:
        '{dsdir}/log/{norm}_{res}/'+SCRAM_ACT_DIR+'_{scramrep}.reps'+REPS+'.log'
        
    params:
        '--noweight -w {res} '+CLOBBER

    run:
        actdir = Path(input.inscens[0]).parent
        shell('PYTHONPATH={PYTHONPATH} '
              'bin/analyzescenarios {params} --rescore {output}'
              ' --heat {input.heatmaps} {actdir} &> {log}')


rule rescore_random_scenarios_scrambledPVAL:
    """
    Rescore the randomized scenarios using scrambled matrices.

    @note: this assumes scrambled matrices will have a SCRAMBLED_HM suffix
           appended to the standard matrix name
    """
    input:
        heatmaps = resolveHeatmapsScrambledPVAL,
        inscens = actualscenariosASinput,

    output:
        directory('{dsdir}/{norm}_{res}/'+SCRAM_RAND_DIR+'_{scramrep}.reps'+REPS)

    log:
        '{dsdir}/{norm}_{res}/log/'+SCRAM_RAND_DIR+'_{scramrep}.reps'+REPS+'.log'

    params:
        '--noweight -w {res} '+CLOBBER

    run:
        actdir = Path(input.inscens[0]).parent
        shell('PYTHONPATH={PYTHONPATH} '
              'bin/analyzescenarios {params}'
              ' --rescore {output} --heat {input.heatmaps} '
              '{actdir} &> {log}')



####### Constructing Scenarios #################################


rule template_scenarios:
    """
    Create the template scenarios to be rescored and randomized in many ways.
    """
    input:
        heatmaps = resolveTemplateHeatmaps,
        synteny = chooseSyntenyfile,

    output:
        expand('{{dsdir}}/{{templatetype}}.reps'+REPS+'/{{templatetype}}.reps'+
               REPS+'.{rep}.pgz',
               rep=range(int(REPS)))

    log:
        '{dsdir}/log/{templatetype}.reps'+REPS+'.log'

    threads: MAX_THREADS
    run:
        if wildcards.templatetype == ACTUAL_TEMPLATES:
            typeswitch = '--logscen'
        elif wildcards.templatetype == WANDER_TEMPLATES:
            typeswitch = '--wander'
        elif wildcards.templatetype == MIX_TEMPLATES:
            typeswitch = '--pseudo-uniform-mix --logscen'
        elif wildcards.templatetype == SPLIT_TEMPLATES:
            typeswitch = '--pseudo-uniform-split --logscen'
        else:
            raise(ValueError('Unknown template type: '+wildcards.templatetype))

            #Check if the output files exist, if they do, skip this rule when
            #CLOBBER is False.
        if not all(Path(file).exists() for file in output) or CLOBBER:
            outdir = Path(str(output[0])).parent
            shell("echo 'creating scenarios with command: "
                  "bin/localdist --noscore -p {threads} -i {REPS} {CLOBBER} "
                  "{typeswitch} {outdir} --heatmaps {input.heatmaps} "
                  "-f {input.synteny}' > {log}; "
                  'PYTHONPATH={PYTHONPATH} '
                  'bin/localdist --noscore -p {threads} -i {REPS} {CLOBBER} '
                  '{typeswitch} {outdir} --heatmaps {input.heatmaps} '
                  '-f {input.synteny} >> {log}')
        else:
            message = (f'Skipping rule because output exists. '
                       f'Run `snakemake --touch.`')
            shell('echo "{message}" > {log}')
            print(message)


rule construct_random_scenarios:
    """
    Construct random scenarios directly from the rescored actual scenarios,
    not from a template.

    @note: to use a template you must comment this out and set
           RANDOM_DIR = RANDOM_FROM_TEMPLATES
    """
    input:
        heatmaps = resolveHeatmaps,
        actscen = '{dsdir}/{norm}_{res}/'+ACTUAL_DIR+'.reps'+REPS+'/rescore.{rep}.pgz',

    output:
        '{dsdir}/{norm}_{res}/'+RANDOM_DIR+'.reps'+REPS+'/rescore.{rep}.pgz'

    log:
        '{dsdir}/{norm}_{res}/log/'+RANDOM_DIR+'.reps'+REPS+'/rescore/{rep}.log'

    run:
        outdir = Path(str(output)).parent
        shell('PYTHONPATH={PYTHONPATH} '
              'bin/analyzescenarios --randomize {outdir} '
              ' --heat {input.heatmaps}'
              ' -w {wildcards.res} {CLOBBER} {input.actscen} &> {log}')


rule randomize_actual_many_times:
    """
    Randomize an actual scenario many times.
    """
    input:
        heatmaps = resolveHeatmaps,
        inscendir = actualscenariosASinput,

    output:
        directory('{dsdir}/{norm}_{res}/'+RANDOM_PER_DIR+'.reps'+REPS+'/per_{rep}')

    log:
        '{dsdir}/{norm}_{res}/log/'+RANDOM_PER_DIR+'.reps'+REPS+'/per_{rep}.log'

    run:
        actdir = Path(input.inscendir[0]).parent
        shell('PYTHONPATH={PYTHONPATH} ' +\
              'bin/analyzescenarios -w {wildcards.res} --randomize {output} '+
              '--heat {input.heatmaps} {CLOBBER} {actdir} '+
              '--random-reps rescore.{wildcards.rep}.pgz'+
              ' {PERSCEN_RANDREPS} &> {log}')




####### Helper Functions #################################

def getAverageFromDist(scenfile):
    """
    Return the average value of the distribution corresponding to the
    given scenario file wildcard.
    """
    intraavere = re.compile(r'intra: (\S+)')
    interavere = re.compile(r'inter: (\S+)')
    intraave = None
    interave = None
    for line in shell('PYTHONPATH={PYTHONPATH} ' +\
                      'bin/analyzescenarios '+scenfile, iterable=True):
        m = intraavere.match(line)
        if(m):
            intraave = m.group(1)

        m = interavere.match(line)
        if(m):
            interave = m.group(1)

    if(not intraave):
        sys.exit('ERROR: could not extract intra average.')

    return intraave, interave

 
####### Statistics ###################################

rule stats_ttests:
    """
    Compute student's T test between moves and between scenarios.
    """
    input:
        actual = actualscenariosASinput,
        random = randomscenariosASinput,

    output:
        '{dsdir}/{norm}_{res}/t_tests.txt'

    run:
        randdir = Path(input.random[0]).parent
        actdir = Path(input.actual[0]).parent
        shell('PYTHONPATH={PYTHONPATH} '
              'echo INTRA: >> {output}; '
              'bin/analyzescenarios --student-on-scen --intra {actdir}'
              ' -m {randdir} >> {output}; '
              'bin/analyzescenarios --student-on-move --intra {actdir}'
              ' -m {randdir} >> {output}; '
              'echo INTER: >> {output}; '
              'bin/analyzescenarios --student-on-scen --inter {actdir}'
              ' -m {randdir} >> {output}; '
              'bin/analyzescenarios --student-on-move --inter {actdir}'
              ' -m {randdir} >> {output};')


ttestre = re.compile(r'(Intra|Inter).* T = ([-e\.\d]+), p = ([-e\.\d]+)')
def getPvalues(tfile):
    """
    Extract the values from the given perscens ttest file.
    """
    intraT, intraP, interT, interP = None, None, None, None
    with open(tfile) as f:
      for line in f:
        m = ttestre.match(line)
        if(m and m.group(1) == 'Intra'):
          intraT = m.group(2)
          intraP = m.group(3)
        elif(m):
          interT = m.group(2)
          interP = m.group(3)
        else:
          raise(Exception(f'Unexpected format in "{tfile}":\n\t{line}'))

    if(intraT):
      intraT = float(intraT)
      intraP = float(intraP)
    if(interT):
      interT = float(interT)
      interP = float(interP)

    return intraT, intraP, interT, interP


def classifyPvalues(infiles):
    """
    Read in the pvalues from all the perscen trials and classify them as
    nonsignificant, or pass/fail with weak or strong p-values.

    @return: percentages of trial classified by outcome
    """
    intrapass = 0
    intrafail = 0
    intranonsig = 0
    intrakindofpass = 0
    intrakindoffail = 0

    interpass = 0
    interfail = 0
    internonsig = 0
    interkindofpass = 0
    interkindoffail = 0

    kindofsig = 0.02
    significant = 0.0001
    for tfile in infiles:
      Tintra, Pintra, Tinter, Pinter = getPvalues(tfile)
      if(Tintra):
        if(Pintra > kindofsig):
          intranonsig += 1
        elif(Pintra < significant):
          if(Tintra < 0):
            intrapass += 1
          else:
            intrafail += 1
        else:
          if(Tintra < 0):
            intrakindofpass += 1
          else:
            intrakindoffail += 1

      if(Tinter):
        if(Pinter > kindofsig):
          internonsig += 1
        elif(Pinter < significant):
          if(Tinter < 0):
            interpass += 1
          else:
            interfail += 1
        else:
          if(Tinter < 0):
            interkindofpass += 1
          else:
            interkindoffail += 1

    intrapass = 100*float(intrapass)/len(infiles)
    intrafail = 100*float(intrafail)/len(infiles)
    intranonsig = 100*float(intranonsig)/len(infiles)
    intrakindofpass = 100*float(intrakindofpass)/len(infiles)
    intrakindoffail = 100*float(intrakindoffail)/len(infiles)
    interpass = 100*float(interpass)/len(infiles)
    interfail = 100*float(interfail)/len(infiles)
    internonsig = 100*float(internonsig)/len(infiles)
    interkindofpass = 100*float(interkindofpass)/len(infiles)
    interkindoffail = 100*float(interkindoffail)/len(infiles)

    return intrapass, intrafail, intranonsig, intrakindofpass, intrakindoffail,\
           interpass, interfail, internonsig, interkindofpass, interkindoffail


rule stats_ttests_perscenario_summary:
    """
    Aggregate all one-sample T tests (between a scenario and the many random
    versions of it) into a summary.
    """
    input:
        randoms = resolvePerScenarioTtestsASinput,

    output:
        '{dsdir}/{norm}_{res}/t_tests_perscen.txt'

    run:
        intrapass, intrafail, intranonsig, intrakindofpass, intrakindoffail,\
        interpass, interfail, internonsig, interkindofpass,\
        interkindoffail = classifyPvalues(input.randoms)

        with open(str(output), 'w') as f:
          f.write('# pass, fail, kindofpass, kindoffail, nonsig\n'+
                  'Intra: %{:.2f}, %{:.2f}, %{:.2f}, %{:.2f}, %{:.2f}\n'
                  .format(intrapass, intrafail,
                          intrakindofpass, intrakindoffail, intranonsig)+
                  'Inter: %{:.2f}, %{:.2f}, %{:.2f}, %{:.2f}, %{:.2f}\n'
                  .format(interpass, interfail,
                          interkindofpass, interkindoffail, internonsig))



rule stats_ttests_perscenario:
    """
    Compute one-sample T test between a scenario and the many random
    versions of it.
    """
    input:
        inscendir = actualscenariosASinput,
        randoms = randomPERscenariosASinput,

    output:
        '{dsdir}/{norm}_{res}/'+RANDOM_PER_DIR+'.reps'+REPS+\
        '/ttest.reps{per_randreps}_{rep}.txt',

    params:
        perdir='{dsdir}/{norm}_{res}/'+RANDOM_PER_DIR+'.reps'+REPS+'/per_{rep}'

    run:
        actdir = Path(input.inscendir[0]).parent
        shell('PYTHONPATH={PYTHONPATH} '
              'bin/analyzescenarios --intra --inter '
              '--perscen {actdir}/rescore.{wildcards.rep}.pgz '
              '--one-sample {params.perdir} > {output}')




rule stats_ttests_scrambledVscrambled:
    """
    Compute student's T test between moves and between scenarios.
    """
    input:
        actual = scramactualscenariosASinput,
        random = scramrandomscenariosASinput,

    output:
        '{dsdir}/{norm}_{res}/t_tests_scramVscram.txt'

    shell:
        'PYTHONPATH={PYTHONPATH} '
        "echo 'INTRA\nscenarios:' >> {output}; "
        'bin/analyzescenarios --student-on-scen --intra {input.actual}'
        ' -m {input.random} >> {output}; '
        'echo moves: >> {output}; '
        'bin/analyzescenarios --student-on-move --intra {input.actual}'
        ' -m {input.random} >> {output}; '
        "echo 'INTER\nscenarios:' >> {output}; "
        'bin/analyzescenarios --student-on-scen --inter {input.actual}'
        ' -m {input.random} >> {output}; '
        'echo moves: >> {output}; '
        'bin/analyzescenarios --student-on-move --inter {input.actual}'
        ' -m {input.random} >> {output}; '


rule stats_ttests_scrambledVnonscrambled:
    """
    Compute student's T test between moves and between scenarios.
    """
    input:
        scram = scramactualscenariosASinput,
        actual = actualscenariosASinput,

    output:
        '{dsdir}/{norm}_{res}/t_tests_scramVnon.txt'

    log:
        '{dsdir}/{norm}_{res}/log/t_tests_scramVnon.log'

    run:
        actdir = Path(input.actual[0]).parent
        shell('PYTHONPATH={PYTHONPATH} '
              "echo 'INTRA\nscenarios:' >> {output}; "
              'bin/analyzescenarios --student-on-scen --intra {actdir}'
              ' -m {input.scram} >> {output} 2> {log}; '
              'echo moves: >> {output}; '
              'bin/analyzescenarios --student-on-move --intra {actdir}'
              ' -m {input.scram} >> {output} 2>> {log}; '
              "echo 'INTER\nscenarios:' >> {output}; "
              'bin/analyzescenarios --student-on-scen --inter {actdir}'
              ' -m {input.scram} >> {output} 2>> {log}; '
              'echo moves: >> {output}; '
              'bin/analyzescenarios --student-on-move --inter {actdir}'
              ' -m {input.scram} >> {output} 2>> {log}; ')


rule stats_ttests_mix:
    """
    Compute student's T test between scenarios created from drawing the
    next move uniformly at random, and drawing the scenario uniormly at
    random for each component seperately. Mpaths are mixed first so that
    the sampling is uniform after the mix moves.
    """
    input:
        actual = actualscenariosASinput,
        random = randomscenariosASinput,
        actpseudo = actualPSEUDOscenariosASinput,
        randpseudo = randomPSEUDOscenariosASinput,

    output:
        '{dsdir}/{norm}_{res}/t_tests_pseudo_{sampletype}.txt'

    run:
        actdir = Path(input.actual[0]).parent
        randdir = Path(input.random[0]).parent
        actpseudodir = Path(input.actpseudo[0]).parent
        randpseudodir = Path(input.randpseudo[0]).parent
        shell('PYTHONPATH={PYTHONPATH} '
              'echo "INTRA actual:" >> {output}; '
              'bin/analyzescenarios --student-on-scen --intra {actdir}'
              ' -m {actpseudodir} --notpaired >> {output}; ')
        shell('PYTHONPATH={PYTHONPATH} '
              'echo "INTER actual:" >> {output}; '
              'bin/analyzescenarios --student-on-scen --inter {actdir}'
              ' -m {actpseudodir} --notpaired >> {output}; ')
        shell('PYTHONPATH={PYTHONPATH} '
              'echo "INTRA random:" >> {output}; '
              'bin/analyzescenarios --student-on-scen --intra {randdir}'
              ' -m {randpseudodir} --notpaired >> {output}; ')
        shell('PYTHONPATH={PYTHONPATH} '
              'echo "INTER random:" >> {output}; '
              'bin/analyzescenarios --student-on-scen --inter {randdir}'
              ' -m {randpseudodir} --notpaired >> {output}; ')


#rule stats_ttests_split:
#    """
#    Compute student's T test between scenarios created from drawing the
#    next move uniformly at random, and drawing the scenario uniformly at
#    random for each component seperately. Mpaths are split first so that
#    the sampling is uniform after the split moves.
#    """
#    input:
#        actual = actualscenariosASinput,
#        random = randomscenariosASinput,
#        actmix = actualSPLITscenariosASinput,
#        randmix = randomSPLITscenariosASinput,
#
#    output:
#        '{dsdir}/{norm}_{res}/t_tests_{split}'+SPLIT+'.txt'
#
#    run:
#        actdir = Path(input.actual[0]).parent
#        randdir = Path(input.random[0]).parent
#        actsplitdir = Path(input.actmix[0]).parent
#        randsplitdir = Path(input.randmix[0]).parent
#        shell('PYTHONPATH={PYTHONPATH} '
#              'echo "INTRA actual:" >> {output}; '
#              'bin/analyzescenarios --student-on-scen --intra {actdir}'
#              ' -m {actsplitdir} --notpaired >> {output}; ')
#        shell('PYTHONPATH={PYTHONPATH} '
#              'echo "INTER actual:" >> {output}; '
#              'bin/analyzescenarios --student-on-scen --inter {actdir}'
#              ' -m {actsplitdir} --notpaired >> {output}; ')
#        shell('PYTHONPATH={PYTHONPATH} '
#              'echo "INTRA random:" >> {output}; '
#              'bin/analyzescenarios --student-on-scen --intra {randdir}'
#              ' -m {randsplitdir} --notpaired >> {output}; ')
#        shell('PYTHONPATH={PYTHONPATH} '
#              'echo "INTER random:" >> {output}; '
#              'bin/analyzescenarios --student-on-scen --inter {randdir}'
#              ' -m {randsplitdir} --notpaired >> {output}; ')


