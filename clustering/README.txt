The script ./experiments.sh takes three arguments as an input: 

1) The number of clusters k. It should be lower than the number of breakpoints, which is 70 for melanogaster/yakuba synteny data provided by the authors. 
2) The number of times the linear, k-medoids and random clusterings will be performed.  
3) An ILP solver. Either GUROBI or GLPK should be passed as an argument.

For the script to run either GLPK solver and pulp python package or GUROBI solver and gurobipy python package must be install on the machine.  

If Data/Breapoints directory does not exist, the script will create it and the breakpoint matrices using Hi-C and synteny data stored in Data/ directory.

Plots for the experiments will be stored in Plots/ directory. 
Parameters of these plots can be easily modified by changing the constants in the script experiments.py.
