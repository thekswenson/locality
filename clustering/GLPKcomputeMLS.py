#!/usr/bin/env python
#Pijus Simonaitis                                                                       Autumn 2017

'''
Construc junction graph given a clustering of the breakpoints of one of the genomes provided by makeclusters.py.
List simple cycles of a junction graph.
Solve maximum set packing problem for these cycles to obtain the cost of a minimum local scenario.

@warning: Requires GLPK solver and python pulp package.  

'''

import argparse
import cPickle as pickle
import os
import time

from pulp import * 
#from gurobipy import Model, GRB
import networkx as nx

from makeclusters import Clustering, MAX_SIZE_OF_CLUSTER
from lib.AdjacencyGraph import AdjacencyGraph
from collections import defaultdict


#MLSVALUES_DIRECTORY = 'MLSvalues/'

class Junctiongraph:
  '''
  Information on junction graph.

  @ivar clustering: clustering of the breakpoints.
  @type clustering: Clustering object imported from makeclusters.py.
  @ivar lengthONEcycles: number of length 1 cycles in junction graph.
  @type lengthONEcycles: integer.
  @ivar lengthTWOcycles: number of length 2 cycles in junction graph.
  @type lengthTWOcycles: integer.
  @ivar graph: junction graph with cycles of length 1 and 2 removed.
  @type graph: networkx graph.
  @ivar edges: number of edges in a junction graph once cycles length 1 and 2 are removed.
  @type edges: integer.
  @ivar mlps: cost of a minimum local parsimonious scenario.
  @type mlps: integer.
  @ivar mls: cost of a minimum local scenario.
  @type mls: integer.
  @ivar mcp: size of a maximum cycle packing of a junction graph.
  @type mcp: integer.
  @ivar simplecycles: number of simple cycles of length > 2 of a junction graph.
  @type simplecycles: integer.

  '''

  def __init__(self, clustering): 
    '''
    Initialization

    @param clustering: clustering of the breakpoints

    '''

    self.clustering = clustering
    self.lengthONEcycles = None
    self.lengthTWOcycles = 0  
    self.graph = None
    self.edges = None
    self.mlps = clustering.ag.getMLPSdist(clustering.breakpointTOcluster)
    self.mls = None
    self.mcp = None
    self.simplecycles = None
    self._constructjunctiongraph(clustering.ag, clustering.breakpointTOcluster)

  def _constructjunctiongraph(self, ag, breakpointTOcluster):
    '''
    Construct junction graph. Count its length one and two cycles and remove them to obtain a pruned junction graph.  

    @param breakpointTOcluster: each breakpoint is associated an integer ranging from 0 to k-1, these will be the vertices of a junction graph. 
    @param ag: adjacency graph.
    @return junction graph with length one and two cycles removed and the numbers of removed cycles

    '''

    #construct a multigraph 
    jg = nx.MultiGraph()
    for n1, n2 in AdjacencyGraph.getBreakpointsSide1(ag):
      #if an adjacency of geome B is internal (not a telomere) then it defines an edge of a junction graph.
      if n1.desire != None and n2.desire != None:
        u = breakpointTOcluster[(n1.desire.chromosome, n1.desire.physint)] 
        v = breakpointTOcluster[(n2.desire.chromosome, n2.desire.physint)]
        jg.add_edge(u,v)

    #length one and two cycles 
    self.lengthONEcycles = jg.number_of_selfloops()

    #construct a pruned junction graph
    jgpruned = nx.Graph()
 
    #iterate through the edges and remove all the cycles of length two from a junction graph
    for u,v in set(jg.edges()):
      if u!=v:
        if jg.number_of_edges(u,v) % 2 == 1:
          jgpruned.add_edge(u,v)
        self.lengthTWOcycles += jg.number_of_edges(u,v)/2

    self.graph = jgpruned  
    self.edges = len(jgpruned.edges())

  def simplecyclesANDmcp(self, simplecycles, mcp):
    '''
    Initialization of mcp, mls and simple cycles. 

    @param simplecycles: number of simple cycles of length > 2 of a junction graph.
    @param mcp: size of a maximum cycle packing

    '''

    self.mcp = int(mcp)
    self.mls = self.edges+self.lengthTWOcycles-int(mcp)
    self.simplecycles = simplecycles

  def dump(self, MLSVALUES_DIRECTORY):
    """
    Save junction graph into MLSVALUES_DIRECTORY

    """

    with open(MLSVALUES_DIRECTORY+'{}.pcl'.format(self.clustering.name), 'wb') as handle:
      pickle.dump(self, handle)


def generatesimplecycleswithduplications(graph):
  '''
  Returns a generator over the simple cycles of a graph where every cycle is repeated twice. 

  @param graph: networkx graph
 
  '''
  #every edge is associated its index.
  edgeTOindex = defaultdict(dict)
  t = 0
  for (u,v) in graph.edges():
    edgeTOindex[(u,v)] = t
    edgeTOindex[(v,u)] = t
    t+=1  		

  #graph is transformed into digraph.
  digraph = graph.to_directed()

  #we iterate through the simple cycles of a directed graph
  for cycle in nx.simple_cycles(digraph):  
    l = len(cycle)
    if l > 2:
      edges = []
      for i in xrange(-1, l-1):			
          edges.append(edgeTOindex[(cycle[i],cycle[i+1])])
      yield frozenset(edges)





def maximumsetpackingGUROBI(sets,numberofelements):
  '''
  Find a maximum set packing for sets. 
  Sets have at most numberofelements different elements indexed by (0, numberofelements-1).

  @warning: GUROBI must be installed and prepared for a work with python. 

  @param sets: tuple of sets.
  @param numberofelements: total number of different elements.  
  @return: size of a maximum set packing.

  '''
  if len(sets) == 0:
   #print "No simple cycles of length > 2"
    return 0 

  start_time = time.time()

  prob = Model('MSP')
  prob.setParam('OutputFlag', False)

  #constraint for every element
  constraints = []
  for element in xrange(numberofelements):
    constraints.append(0)

  variables = []
  objective = 0
  for i in xrange(len(sets)):
    #variable for every set
    variables.append(prob.addVar(vtype=GRB.BINARY, name = 'x'+str(i)))
    #we maximize the sum of variables
    objective += variables[i]
    #keeping at most one set with a given element
    for element in sets[i]:
      constraints[element]+=variables[i]

  prob.setObjective(objective, GRB.MAXIMIZE)
  for constraint in constraints:
    prob.addConstr(constraint <= 1)
	
  #print 'Constructing an instance took: ', time.time()-start_time
  start_time = time.time()

  prob.optimize()

  #print 'Solving ILP took: ', time.time()-start_time
  return prob.objVal


def maximumsetpackingGLPK(sets,numberofelements):
  '''
  If GUROBI is not installed we can use GLPK with a help of pulp. 

  @warning: GLPK and pulp must be installed. 

  ''' 
  if len(sets) == 0:
    return 0 	

  prob = LpProblem("MSP", LpMaximize)

  constraints = [] 
  for i in xrange(numberofelements):
    constraints.append(0)

  variables = []
  objective = 0
  for i in xrange(len(sets)):
    variables.append(LpVariable('x'+str(i), cat='Binary'))	
    objective+=variables[i]
    for element in sets[i]:
      constraints[element]+=variables[i]	
	
  prob += objective
  for constraint in constraints:
    prob += constraint <= 1

  GLPK(msg = 0).solve(prob)
  return value(prob.objective)

def main():
  desc = 'Construct junction graph, compute mls, mlps and other values and store them in the pickles in given output directory.'
  parser = argparse.ArgumentParser(description=desc)
  parser.add_argument('INPUT_DIR', help='Input directory where partitions provided by makeclusters.py are stored.')
  parser.add_argument('OUTPUT_DIR', help='Output directory where MLS cost and other information on junction graph and a corresponding clustering will be stored.')
  parser.add_argument('--del', dest='DELETE', action='store_true', help = 'Empties the output directory.')
  parser.set_defaults(DELETE=False)
  args = parser.parse_args()
  
  CLUSTERINGS_DIRECTORY = args.INPUT_DIR
  MLSVALUES_DIRECTORY = args.OUTPUT_DIR



  if not os.path.exists(MLSVALUES_DIRECTORY):
     os.makedirs(MLSVALUES_DIRECTORY)

  #empty the directory where MLSvalues will be stored

  if args.DELETE:
    for filename in os.listdir(MLSVALUES_DIRECTORY):
      os.remove(MLSVALUES_DIRECTORY+filename)

 
  #reads all the files in the CLUSTERINGS_DIRECTORY
  for filename in os.listdir(CLUSTERINGS_DIRECTORY):
    with open(CLUSTERINGS_DIRECTORY+filename, 'rb') as handle:
      clustering = pickle.loads(handle.read())

    jg = Junctiongraph(clustering)
    
    start_time = time.time()

    #we remove duplications from the simple cycles by applying set()
    simplecycles = set(generatesimplecycleswithduplications(jg.graph))
  
    #print 'Listing simple cycles took: ', time.time()-start_time, len(simplecycles)

    maximumcyclepacking = maximumsetpackingGLPK(tuple(simplecycles),jg.edges) 

    jg.simplecyclesANDmcp(simplecycles, maximumcyclepacking)

    #print jg.mlps, jg.mls


    jg.dump(MLSVALUES_DIRECTORY)


if __name__ == "__main__":
  main()	
