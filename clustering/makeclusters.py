#!/usr/bin/env python
#Pijus Simonaitis                                                                       Autumn 2017
"""
Partition breakpoints into clusters using various clustering algorithms.
Compute the weight of a clustering for a given Hi-C data and divergence from linearity of a clustering. 

@note: input must be provided by a script makebreakpointmatrices.

"""

import sys
import os
import argparse
import re
import random
import numpy as np
import cPickle as pickle
import itertools
from collections import defaultdict
from lib.AdjacencyGraph import AdjacencyGraph

### Constants:

INDEX_PHYSBREAKPOINT_MAP_FILE = 'indexTOchrTOphysbreakpoint.pcl'
PHYSBREAKPOINT_INDEX_MAP_FILE = 'physbreakpointTOindex.pcl'
COMPONENTSOFAG_FILE = 'componentsofag.pcl'
ADJACENCYGRAPH_FILE = 'adjacencygraph.pcl'
NAMERE = re.compile(r'chr(\w+)_chr(\w+)_breakpointcontacts')
#CLUSTERINGS_DIRECTORY = 'partitions/'
MAX_SIZE_OF_CLUSTER = 17


#        _________________________
#_______/        Classes          \________________________m_____________________


# physbreakpoint = (c, (start,end))
# breakpoint = (c, index)
# breakpoints 


class Inputforclustering(object):
  """
  Input of a clustering algorithm. 

  Physbreakpoint is a pair (chromosome name, interval on the chromosome). 
  Every physbreakpoint is assigned an index respecting the order of physbreakpoints on the chromosomes. 
  Breakpoint is a pair (chromosome name, index).
  For every pair of breakpoints hi-c values are stored in matdir. 

  @ivar hic: name of the hi-c data
  @type hic: string
  @ivar chrTOchrTOmatrix: hi-c values of the breakpoints.
  @type chrTOchrTOmatrix: map pair of chromosomes and pair of indexes to their hi-c values.  
  @ivar indexTOchrTOphysbreakpoint: physical location of a breakpoint
  @type indexTOchrTOphysbreakpoint: map breakpoin to physbreakpoint.
  @ivar physbreakpointToindex: index of a physbreakpoint.
  @type physbreakpointToindex: map physbreakpoint to index.
  @ivar k: number of clusters.
  @type k: integer.
  @ivar iterations: number of clusterings to be produced.
  @type iterations: integer.
  @ivar breakpoints: list of all the breakpoints. 
  @type breakpoints: list of pairs (chromosome, index).
  @ivar componentsofag: lists of breakpoints defined by the connected components of an adjacency graph.
  @type componentsofag: list of lists of breakpoints. 
  @ivar chromosomes: names of the chromosomes.
  @type chromosomes: list of strings. 

  """

  def __init__(self, k, iterations, matdir):
    """
    Innitialization

    @param k: number of clusters.
    @param itearations: number of clusterings to be produced.
    @param matdir: directory of hi-c matrices.
    
    """
    self.hic = matdir.split('/')[-2]
    self.chrTOchrTOmatrix = None
    self.indexTOchrTOphysbreakpoint = None
    self.physbreakpointToindex = None
    self.k = k
    self.iterations = iterations
    self.breakpoints = set()
    self.componentsofag = None
    self.ag = None
    self.chromosomes = []
    self._innitialize(matdir)  
   

  def _innitialize(self, matdir):
    """
    Read matrices storing hi-c values of the breakpoints from matdir.
    Read components of an adjacency graph from matdir.
    Compute the number of chromosomes and the list of all the breakpoints. 
   
    @param matdir: directory of hi-c matrices.

    """
    if(not os.path.isdir(matdir)):
      sys.exit('"{}" is not a directory.'.format(matdir))
    
    matrices = readBreakpointMatrices(matdir)
    self.indexTOchrTOphysbreakpoint = matrices[0]
    self.chrTOchrTOmatrix = matrices[1]
    self.physbreakpointTOindex  = matrices[2]

    self.componentsofag = readcomponents(matdir, self.physbreakpointTOindex)
    self.ag = readadjacencygraph(matdir)

    for c in self.chrTOchrTOmatrix.iterkeys():
      self.chromosomes.append(c) 
      for i in xrange(self.chrTOchrTOmatrix[c][c].shape[0]):
        self.breakpoints.add((c,i))

class Choiceofaclustering(object):

  """ 
  Information about a clustering function. 
  There are various types of clusterings: k-medoids, random, linear, one go k-medoids and merge of the connected components of an adjacency graph. 
   
  @ivar function: name of a clustering function to be used.
  @type function: string.
  @ivar type: type of a clustering.
  @type type: string.
  @ivar iterate: number of clusterings to be produced.
  @type iterate: integer.
  @ivar arguments: list of arguments to be passed for a clustering function.
  @type arguments: list.

  """
  def __init__(self, ci, clustertype):

    """
    Innitialization 

    @param ci: input for clustering.
    @param clustertype: type of a clustering.

    """
    self.function = None
    self.type = clustertype
    self.iterate = None
    self.arguments = None
    self._innitialize(ci, clustertype)

  def _innitialize(self, ci, clustertype):
    """
    Check if clustering is well defined.
    If it is, prepare the arguments for a clustering function.  

    """

    if clustertype in 'klor':
      self.arguments = (ci.k, ci.chrTOchrTOmatrix, ci.breakpoints)
      self.iterate = ci.iterations
      if clustertype == 'k':
        self.function = kmedoidscluster 
      if clustertype == 'r':
        self.function = randomcluster  
      if clustertype == 'o':
        self.function = onegokmedoidcluster
      if clustertype == 'l': 
        self.function = chromosomelinearcluster
        if ci.k < len(ci.chromosomes):
          sys.exit('ERROR: Chromosome number is smaller than given k')
           
    elif clustertype in 'AaZz':
      numofcomp = len(ci.componentsofag)
      if ci.k>numofcomp:
        sys.exit('ERROR: Number of connected components of AG is smaller than k')

      self.iterate = 1
      self.function = mergeclusters

      if clustertype == 'A':
        self.arguments = (ci.componentsofag, numofcomp-ci.k, -1)
      if clustertype == 'a':
        self.arguments = (ci.componentsofag, numofcomp-ci.k)
      if clustertype == 'Z':
        self.arguments = (ci.componentsofag, numofcomp-ci.k, -1, MAX_SIZE_OF_CLUSTER)
      if clustertype == 'z':
        self.arguments = (ci.componentsofag, numofcomp-ci.k, 1, MAX_SIZE_OF_CLUSTER)

    elif clustertype == 'R':
      self.arguments = (ci.k, ci.chrTOchrTOmatrix, ci.breakpoints) 
      self.iterate = ci.iterations
      self.function = fixeddivergence
    
    elif clustertype == 'h':
      if ci.k>19:
        sys.exit('ERROR: k is larger than 19')

      self.arguments = (melanogastercluster(), 19-ci.k)
      self.function = mergeclusters
      self.iterate = 1
      
    else:
      sys.exit('ERROR: Unknown clustertype')
      

class Clustering(object):
  
  """
  Clustering of breakpoints.
  
  @ivar breakpointTOcluster: clusters of the breakpoints where clusters are numbered from 0 to k-1.
  @type breakpointTOcluster: map breakpoint to cluster
  @ivar clusters: clusters of breakpoints.
  @type clusters: list of lists.
  @ivar type: type of a clustering.
  @type type: string.
  @ivar name: name of a clustering.
  @type name: string.
  @ivar k: number of clusters. 
  @type k: integer.
  @ivar weight: weight of a clustering.
  @type weight: float.
  @ivar divfromlin: divergence from linearity of a clustering.
  @type divfromlin: integer.
  """
  
  def __init__(self, clusters, clustertype, clusteringinput):
    """
    Innitialization 
 
    @param clusters: clusters.
    @param clustertype: type of a clustering.
    @param clusteringinput: input of a clustering. 

    """
    self.breakpointTOcluster = defaultdict(dict)
    self.clusters = clusters
    self.type = clustertype
    self.hic = clusteringinput.hic
    self.name = None
    self.k = len(clusters)
    self.weight = 0 
    self.divfromlin = 0
    self.ag = clusteringinput.ag
    self._weight(clusteringinput.indexTOchrTOphysbreakpoint, clusteringinput.chrTOchrTOmatrix)     
    self._divfromlin()

  def _weight(self, indexTOchrTOphysbreakpoint, chrTOchrTOmatrix):
    """
    Compute weight of a clustering and the map breakpointTOcluster.

    @param indexTOchrTOphysbreakpoint: physical locations of breakpoints. 
    @param chrTOchrTOmatrix: hi-c values of the breakpoints.

    """

    for i in xrange(self.k):
      self.weight += exhaustivekmedoids(1, chrTOchrTOmatrix, self.clusters[i])[0]
      for (c,index) in self.clusters[i]:
        breakpoint = indexTOchrTOphysbreakpoint[index][c]
        self.breakpointTOcluster[breakpoint] = i

  def _divfromlin(self):
    """
    Compute diveregece from linearity of clusters.

    """
    for cluster in self.clusters:
      self.divfromlin+=clusterdivfromlin(cluster)


  def dump(self, enumeration, CLUSTERINGS_DIRECTORY):
    """
    Define the name of a clustering.
    Save clustering as a pickle. 
 
    @param enumeration: iteration at which this clustering is generated. Integer.

    """

    self.name = str(self.k)+'-'+self.hic+'-'+str(self.type)+'-'+str(enumeration)
    with open(CLUSTERINGS_DIRECTORY+'{}.pcl'.format(self.name), 'wb') as handle:
      pickle.dump(self, handle)

#        _________________________
#_______/       Input Functions   \_____________________________________________

def readBreakpointMatrices(matrixdirectory):
  """
  Read in the output of "makebreakpointmatrices" consisting of:
  1) hi-c values of the breakpoints.
  2) physical locations of a breakpoints
  3) indexes of a physbreakpoints.
  4) components of an adjacency graph. 

  @return: (indexTOchrTOphysbreakpoint, chrTOchrTOmatrix, physbreakpointTOindex)
 
  """
  chrTOchrTOmatrix = defaultdict(dict) #Map chromosomes to numpy matrix.
  indexTOchrTOphysbreakpoint = None        #Map row/col index to breakpoint info.

  for filename in os.listdir(matrixdirectory):
    if filename == INDEX_PHYSBREAKPOINT_MAP_FILE:  #Read the map.
      with open(matrixdirectory+'/'+filename) as f:
        indexTOchrTOphysbreakpoint = pickle.load(f)

    elif filename == PHYSBREAKPOINT_INDEX_MAP_FILE:
      with open(matrixdirectory+'/'+filename) as f:
        physbreakpointTOindex = pickle.load(f)

    elif filename != ADJACENCYGRAPH_FILE and filename != COMPONENTSOFAG_FILE:                                       #Read a matrix.
      m = NAMERE.match(filename)
      if m:
        chr1, chr2 = m.group(1), m.group(2)

      if chr1 > chr2:
        chr1, chr2 = chr2, chr1

      chrTOchrTOmatrix[chr1][chr2] = np.loadtxt(matrixdirectory+'/'+filename)

    #else: sys.exit('ERROR: Unknown file in the directory')

  return indexTOchrTOphysbreakpoint, chrTOchrTOmatrix, physbreakpointTOindex 

def readcomponents(matrixdirectory,physbreakpointTOindex):
  """
  Read in the connected components of an adjacency graph. 
  Input is a list of pairs consisting of lists of physbreakpoints and a letter indicating if it is a path or a cycle.
  Physbreakpoints respect the order of an adjacency graph. 
  Physbreakpoints are transformed to breakpoints. 

  @return: Clusters in a form of a list of lists of breakpoints.

  """

  with open(matrixdirectory+'/'+COMPONENTSOFAG_FILE) as f:
    components = pickle.load(f)

  new_components = [] 
  for comp in components:
    new_component = []
    for breakpoint in comp[0]:
      new_component.append((breakpoint[0],physbreakpointTOindex[breakpoint])) 

    new_components.append(new_component)

  return new_components

def readadjacencygraph(matrixdirectory):
  with open(matrixdirectory+'/'+ADJACENCYGRAPH_FILE) as f:
    ag = pickle.load(f)
  return ag


def clusterdivfromlin(cluster):
  """
  Compute divergence from linearity of a cluster. 
 
  @param cluster: list of breakpoints.
  @return: divergence from linearity of a cluster. 
  @rtype: integer.

  """

  cluster.sort()
  divfromlin = 0
  for i in xrange(len(cluster)-1):
    if cluster[i][0] == cluster[i+1][0] and cluster[i][1]+1 != cluster[i+1][1]:
      divfromlin+=1
    if cluster[i][0] != cluster[i+1][0]:
      divfromlin+=1
    #IN THE ARTICLE WE HAVE USED THIS VARIANT:
    #if cluster[i][0] != cluster[i+1][0] and cluster[i][0] != '4' and cluster[i+1][0] != '4':
    #  divfromlin+=1
  return divfromlin


#        _______________________________________________________
#_______/           FUNCTIONS FOR COMPUTING CLUSTERING WEIGHT   \_____________________________________________

def closestcentroid(centroids, breakpoint, D):
  """
  chrTOchrTOmatrix is treated as a similarity matrix for the breakpoints. 
  For breakpoint we find the most similar centroid and return it and their similarity value.

  @note: What shall we do if breakpoint is in the centroids? Shall we return 0 or its similarity to itself? Now we return the latter.    

  @param centroids: list of special breakpoints that are denoted as centroids.
  @param breakpoint: breakpoint.
  @param D: chrTOchrTOmatrix.

  @return: (weight, closest) where closest is a centroid maximizing similarity (weight) to a breakpoint.  
  """

  if breakpoint in centroids:
    return D[breakpoint[0]][breakpoint[0]][breakpoint[1]][breakpoint[1]], centroids.index(breakpoint)  
  
  weight = float('-inf')
  for i in xrange(len(centroids)):
    if breakpoint[0]<centroids[i][0]: 
      newweight = D[breakpoint[0]][centroids[i][0]][breakpoint[1]][centroids[i][1]]
    else:
      newweight = D[centroids[i][0]][breakpoint[0]][centroids[i][1]][breakpoint[1]] 
    if newweight > weight:
      weight, centroid = newweight, i 
  return weight, centroid

def clusteringaroundcentroids(centroids,D,breakpoints):
  """
  Cluster the breakpoints around the most similar centroids. 
  
  @param centroids: list of special breakpoints that are denoted as centroids.
  @param D: chrTOchrTOmatrix.
  @param breakpoints: list of all the breakpoints. 

  """

  weight = 0
  clusters = []
  for i in xrange(len(centroids)):
    clusters.append([])

  for breakpoint in breakpoints:
    breakpointweight, centroid = closestcentroid(centroids, breakpoint, D)
    clusters[centroid].append(breakpoint)
    weight += breakpointweight
  return weight, clusters

def exhaustivekmedoids(k, D, breakpoints):
  """
  Check all the possible list of centroids of size k and outputs the one maximizing the weight.
  It is only used with k=1 to find the medoid of a cluster, an element maximizing the sum of the similarities to the rest of the cluster.
  
  @param k: number of clusters.
  @param D: chrTOchrTOmatrix.
  @param breakpoints: list of all the breakpoints. 
 
  @return: (weight, centroids) where clustering around centroids maximize the weight. 

  """
  weight = float('-inf')
  for newcentroids in itertools.combinations(breakpoints, k):
    newweight, newclusters = clusteringaroundcentroids(newcentroids, D, breakpoints)
    if newweight > weight:
      weight, clusters = newweight, newclusters
      centroids = newcentroids
  return weight, centroids

#        _________________________
#_______/        Clusterings      \_____________________________________________

def kmedoidscluster(k, D, breakpoints):
  """
  k centroids are initialized at random and breakpoints are clustered around these centroids.
  iterate for tmax times the following:
    medoids of the clusters are computed. these become the new centroids.
    breakpoints are clustered around the new centroids.
    if weight increases then we continue otherwise we abort. 
  if after tmax iterations we did not converge, then we print 'Did not Converge!' (this was never observed to happen)

  @note: k-medoids clustering algorithm with a random initialization
  @param k: number of clusters.
  @param D: chrTOchrTOmatrix.
  @param breakpoints: list of all the breakpoints.  

  """
  tmax = 100
  centroids = random.sample(breakpoints, k)
  weight, clusters = clusteringaroundcentroids(centroids,D,breakpoints)
  
  for t in xrange(tmax):
    updatedcentroids = []
    for i in xrange(k):
        medoid = exhaustivekmedoids(1, D, clusters[i])[1][0]
        updatedcentroids.append(medoid) 

    newweight, newclusters = clusteringaroundcentroids(updatedcentroids,D,breakpoints)
 
    if newweight <= weight:
      return newclusters
    else:   
      centroids = updatedcentroids 
      weight, clusters = newweight, newclusters
  print('Did not Converge!')
  return clusters

def onegokmedoidcluster(k, D, breakpoints):
  """
  k centroids are initialized at random and breakpoints are clustered around these centroids.

  @note: clustering around randomly initialized centroids using hi-c values.  

  """
  centroids = random.sample(breakpoints,k)
  return clusteringaroundcentroids(centroids,D,breakpoints)[1]



def randomcluster(k, D, breakpoints_imported):
  """
  @note: clustering algorithm that outputs k random-non-empty clusters 
 
  """ 
  breakpoints = set(breakpoints_imported)
  clusters = []
 
  for i in xrange(k):
    breakpoint = random.sample(breakpoints,1)[0]
    breakpoints.remove(breakpoint) 
    clusters.append([breakpoint])

  for breakpoint in breakpoints:
    clusters[random.randint(0, k-1)].append(breakpoint)  

  return clusters


def chromosomelinearcluster(k, D, breakpoints_imported):
  """
  k-#chromosomes syntenic blocks are chosen at random and chromosomes are cut in those places. 
  This provides us with k linear clusters. 

  @note: works for k>#numberofchromosomes
 
  """
  breakpoints = list(breakpoints_imported)
  clusters = []
  chromosomeTocuts = defaultdict(dict)
  chromosomes = set()
 
  for c in D.iterkeys():
    chromosomes.add(c)
    chromosomeTocuts[c]=[]
    breakpoints.remove((c,0))
 
  cuts = random.sample(breakpoints, k-len(chromosomes))
  # chromosome c will be cut in between breakpoints i-1 and i
  for (c,i) in cuts:
    chromosomeTocuts[c].append(i)

  for c in chromosomes:
    chromosomeTocuts[c].sort()
    start = 0
    for finish in chromosomeTocuts[c]:
      cluster = []
      for j in range(start,finish):
        cluster.append((c,j))          
      clusters.append(cluster)
      start = finish
    cluster = [] 
    for j in range(start,D[c][c].shape[0]):
      cluster.append((c,j))
    clusters.append(cluster)

  return clusters


def fixeddivergence(k, D, breakpoints_imported, div):
  """
  div-#chromosomes syntenic blocks are chosen at random and chromosomes are cut in those places. 
  These pieces then are partitioned into k clusterst at random. 

  @note: works for div>#numberofchromosomes
 
  """
  breakpoints = list(breakpoints_imported)
  clusters = []
  chromosomeTocuts = defaultdict(dict)
  chromosomes = set()
 
  for c in D.iterkeys():
    chromosomes.add(c)
    chromosomeTocuts[c]=[]
    breakpoints.remove((c,0))
 
  cuts = random.sample(breakpoints, div-len(chromosomes))
  # chromosome c will be cut in between breakpoints i-1 and i
  for (c,i) in cuts:
    chromosomeTocuts[c].append(i)

  for c in chromosomes:
    chromosomeTocuts[c].sort()
    start = 0
    for finish in chromosomeTocuts[c]:
      cluster = []
      for j in range(start,finish):
        cluster.append((c,j))          
      clusters.append(cluster)
      start = finish
    cluster = [] 
    for j in range(start,D[c][c].shape[0]):
      cluster.append((c,j))
    clusters.append(cluster)
    
  for merge in xrange(div-k):
    (i,j) = random.sample(range(div-merge), 2)
    clusters[i] = clusters[i]+clusters[j]
    clusters.pop(j)

  return clusters



  
def melanogastercluster():
  """
  A single linear clustering of melanogaster's breakpoints into 19 clusters having mls = 8. This is the minimum possible mls for a linear clustering. 

  @note: clustering of melanogaster's adjacencies, constructed by hand.

  """
  handmaid = []
  handmaid.append([('2L',0)])
  handmaid.append([('4',0)])
  handmaid.append([('4',1)])
  handmaid.append([('X',0)])
  handmaid.append([('X',17)])
  handmaid.append([('3R',10)])
  handmaid.append([('3L',0)])
  handmaid.append([('3L',8)])
  handmaid.append([('2R',0)])
  handmaid.append([('2R',1)])
  handmaid.append([('2R',2)])
  handmaid.append([('2R',14)])
  handmaid.append([('2R',3),('2R',4),('2R',5)])
  handmaid.append([('2R',6),('2R',7),('2R',8),('2R',9),('2R',10),('2R',11),('2R',12),('2R',13)])
  handmaid.append([('3L',1),('3L',2),('3L',3),('3L',4),('3L',5),('3L',6),('3L',7)])
  handmaid.append([('2L',1),('2L',2),('2L',3),('2L',4),('2L',5),('2L',6),('2L',7),('2L',8),('2L',9)])
  handmaid.append([('2L',10),('2L',11),('2L',12),('2L',13),('2L',14)])
  handmaid.append([('3R',0),('3R',1),('3R',2),('3R',3),('3R',4),('3R',5),('3R',6),('3R',7),('3R',8),('3R',9)])
  handmaid.append([('X',1),('X',2),('X',3),('X',4),('X',5),('X',6),('X',7),('X',8),('X',9),('X',10),('X',11),('X',12),('X',13),('X',14),('X',15),('X',16)])
 
  return handmaid

def mergeclusters(clusters, merges, minmax = 1, treshold = float('inf')):

  """
  Perform merges of clusters by either greedily minimizing or maximizing the divergence from linearity.

  @note: Works for merges<len(clusters)
  @param merges: number of merges to perform
  @param minmax: 1 to minimize divergence from linearity and -1 to maximize it.
  @param treshold: maximum allowed size of a cluster.
  @return: clusters. 

  """
  clusters = list(clusters)  
  for _ in xrange(merges):  
    bestchange = float('-inf')*minmax
    (cluster1,cluster2) = (-1,-1)
    l = len(clusters)

    #we try merging every pair of clusters and choose the one maximizing\minimizing the change of divergence from linearity 

    for i in xrange(l-1):
      for j in range(i+1,l):
        olddivfromlin = clusterdivfromlin(clusters[i])+clusterdivfromlin(clusters[j])
        changeindivfromlin = olddivfromlin - clusterdivfromlin(clusters[i]+clusters[j])

        if changeindivfromlin*minmax>=bestchange*minmax and len(clusters[i]+clusters[j])<treshold:
          bestchange = changeindivfromlin
          (cluster1,cluster2) = (i,j)

    clusters[cluster1] = clusters[cluster1]+clusters[cluster2]
    clusters[cluster1].sort()
    clusters.pop(cluster2)
  
  return clusters

def main():
  desc = 'Reads in the output of a script makebreakpointmatrices form and outputs clusterings of the breakpoints'
  parser = argparse.ArgumentParser(description=desc)
  parser.add_argument('MATRIX_DIR', help='Directory storing the files created by makebreakpointmatrices. These files store the contact maps of the'+\
                      ' breakpoints and the connected components of an adjacency graph.')
  parser.add_argument('OUTPUT_DIR', help='Output directory where partitions will be stored')
  parser.add_argument('TYPES', help='String of characters consisting of letters "aAklorzZh" indicating the types of clusterings to be generated: ' 
                      'k for k-medoids, r for random clusters, o for one go clusters, l for linear clusters, '+\
                      'a/A for clusters obtained merging connected components of an adjacency graph minimizing/maximizing divergence from linearity, '+\
                       'z/Z for such clusters of size at most {},'.format(MAX_SIZE_OF_CLUSTER)+\
                       ' h for hand maid linear clustering of melanogaster minimizing the MLS cost')
  parser.add_argument('K', help='Number of clusters.')
  parser.add_argument('ITERATIONS', help='Number of clusterings to be generated.')
  parser.add_argument('--del', dest='DELETE', action='store_true', help = 'Empties the output directory')
  parser.set_defaults(DELETE=False)

  args = parser.parse_args()
  clustertypes = args.TYPES

  CLUSTERINGS_DIRECTORY = args.OUTPUT_DIR

  if not os.path.exists(CLUSTERINGS_DIRECTORY):
     os.makedirs(CLUSTERINGS_DIRECTORY)

  #empty the directory where clusters will be stored if required.
  if args.DELETE:
    for filename in os.listdir(CLUSTERINGS_DIRECTORY):
      os.remove(CLUSTERINGS_DIRECTORY+filename)

  #construct input of a clustering.
  ci = Inputforclustering(int(args.K), int(args.ITERATIONS), args.MATRIX_DIR)

  #iterate through different types of clusters that were required.
  for clustertype in clustertypes:
    #define clustering parameters.
    cc = Choiceofaclustering(ci, clustertype)
    #construct a given number of different clusterings and dump them.
    for i in xrange(cc.iterate):
      medoidToindex = cc.function(*cc.arguments)
      cluster = Clustering(medoidToindex, clustertype, ci)
      cluster.dump(i, CLUSTERINGS_DIRECTORY)



if __name__ == "__main__":
  main()
