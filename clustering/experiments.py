#!/usr/bin/env python
#Pijus Simonaitis                                                                      Spring 2018
"""
Given the output of computeMLS.py or GLPKcomputeMLS.py produces plots for the weight, 
MLS cost and divergence from linearity of the clusters.

This script is indented to be used with the clusterings of types k, l, r, A, a and h having a fixed number of clusters k.

Constants were manually set in the script to be used with the clusterings of melanogaster obtained using the syntenic blocks and Hi-C matrices
provided by the authors.  



"""


import argparse
import cPickle as pickle
import sys
import os
import numpy as np 

from lib.AdjacencyGraph import AdjacencyGraph
from makeclusters import Clustering, MAX_SIZE_OF_CLUSTER
from computeMLS import Junctiongraph

from collections import defaultdict
from scipy.stats import pearsonr

import matplotlib.pyplot as plt
import matplotlib.mlab as mlab

import networkx as nx

### Constant

#To obtain consistent plots we fix the maximum values of clustering weight, MLS and divergence from linearity  
MAX_NORMALIZED_WEIGHT = 180
MAX_NON_NORMALIZED_WEIGHT = 400000
MAX_DIVERGENCE_FROM_LINEARITY = 60
MAX_MLS = 60
 
ALPHA = 0.15
SIZE = 15
OUTLIERALPHA = 1
OUTLIERSIZE = 15

#PLOTS_DIRECTORY = 'plots/'



def definecolor(clustering):
  """
  Sets the color of a clustering in a scatter plot depending on the type of a clustering. 

  """

  if clustering.type == 'k':
    if clustering.weight > MAX_NORMALIZED_WEIGHT:
      return 'blue'
    else: 
      return 'red'
  elif clustering.type == 'r':
    return 'black'
  elif clustering.type == 'l':
    return 'green'
  elif clustering.type == 'A':
    return 'orange'
  elif clustering.type == 'a':
    return 'brown' 
  elif clustering.type == 'h':
    return 'olive'
  else:
    sys.exit('ERROR: Only clusterings of types k, r, l, A, a and h are allowed for this experiment.')   
  


def main():
  desc = 'Plots divergence from linearity, MLS cost and clustering weight given the output of computeMLS.py or GLPKcomputeMLS.py.'
  parser = argparse.ArgumentParser(description=desc)
  parser.add_argument('INPUT_DIRECTORY',
                      help='Directory containing output of computeMLS.py or GLPKcomputeMLS.py.')
  parser.add_argument('OUTPUT_DIRECTORY',
                      help='Directory where plots will be stored.')

  parser.add_argument('PLOT',
                      help='w for clustering weight/MLS plot, d for divergence from linearity/MLS plot and dw for both.')


  args = parser.parse_args()
  directory = args.INPUT_DIRECTORY
  PLOTS_DIRECTORY = args.OUTPUT_DIRECTORY
  
  plot = args.PLOT

  if not 'd' in plot and not 'w' in plot:
    sys.exit('d, w of dw should be passed as the second parameter depending on what plots are required.')   


  mls = []
  color = [] 
  divfromlin = []
  weight = []

  outliermls = []
  outliercolor = [] 
  outlierdivfromlin = []
  outlierweight = []
  
  k = 0
  types = ""

  for filename in os.listdir(directory):
    with open(directory+filename, 'rb') as handle:
      values = pickle.loads(handle.read())
    
    if k == 0:
      k = values.clustering.k
    elif not k == values.clustering.k:
      sys.exit('ERROR: More than one value of k was found in the directory.')   


    if not values.clustering.type in types:
      types += values.clustering.type  


    if definecolor(values.clustering) in ['olive', 'orange', 'brown']:
      outliercolor.append(definecolor(values.clustering))
      outliermls.append(values.mls)
      outlierdivfromlin.append(values.clustering.divfromlin)
      outlierweight.append(values.clustering.weight)
    else:
      color.append(definecolor(values.clustering))
      mls.append(values.mls)
      divfromlin.append(values.clustering.divfromlin)
      weight.append(values.clustering.weight)

  #check if the data is normalized or not



  if max(weight)>MAX_NORMALIZED_WEIGHT:
    maxweight = MAX_NON_NORMALIZED_WEIGHT
    w = 'Non-normalized'
  else: 
    maxweight = MAX_NORMALIZED_WEIGHT
    w = 'Normalized'
    
  if not os.path.exists(PLOTS_DIRECTORY):
     os.makedirs(PLOTS_DIRECTORY)


  if 'w' in plot:
    plt.scatter(weight, mls, c = color, s = SIZE, alpha = ALPHA)
    plt.scatter(outlierweight, outliermls, c = outliercolor, s = OUTLIERSIZE, alpha = OUTLIERALPHA)
    plt.xlabel("{} clustering weight".format(w))
    plt.ylabel("MLS") 
    plt.ylim((-1,MAX_MLS))
    plt.xlim((0,maxweight))   

    if  maxweight == MAX_NON_NORMALIZED_WEIGHT:
      tick_val = [0,40000,80000,120000,160000,200000,240000,280000,320000,360000, 400000]
      tick_lab = ['0','40k','80k','120k','160k','200k','240k','280k','320k','360k', '400k']
      plt.xticks(tick_val, tick_lab)

    plt.savefig(PLOTS_DIRECTORY+str(k)+'-'+str(len(mls)/3)+'-{}-weight-mls.pdf'.format(w), format = 'pdf')
    plt.clf() 
    plt.cla()


    

 
# Adapt the ticks on the x-axis
 



  if 'd' in plot:
    plt.scatter(divfromlin, mls, c = color, s = SIZE, alpha = ALPHA)
    plt.scatter(outlierdivfromlin, outliermls, c = outliercolor, s = OUTLIERSIZE, alpha = OUTLIERALPHA)
    plt.xlabel("Divergence from linearity")
    plt.ylabel("MLS") 
    plt.ylim((-1,MAX_MLS))
    plt.xlim((-1,MAX_DIVERGENCE_FROM_LINEARITY))   
    plt.savefig(PLOTS_DIRECTORY+str(k)+'-'+str(len(mls)/4)+'-divfromlin-mls.pdf', format = 'pdf')
    plt.clf() 
    plt.cla()
  
if __name__ == "__main__":
  main()
