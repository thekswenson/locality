#!/bin/bash

#Constants

nbofbreakpoints=70
nbofAGcomponents=19
nbofchromosomes=6

#Creates Breakpoint matrices given Hi-C and synteny data in Data/ directory

if ! [ -d "Data/Breakpoints/" ]; then
    echo "Creating Breakpoint matrices"  
    python2 makebreakpointmatrices Data/Synteny/dmel-dyak-orthocluster-oma.tsv Data/Heatmaps/mel-yak-40kb.nonnormalized/ Data/Breakpoints/mel-yak/
    python2 makebreakpointmatrices Data/Synteny/dmel-dyak-orthocluster-oma.tsv Data/Heatmaps/mel-yak-40kb.normalized/ Data/Breakpoints/mel-yak-normalized/
fi

#Chooses a ILP solver

if [ "$3" = "GLPK" ]
then 
    mls=GLPKcomputeMLS.py
elif [ "$3" = "GUROBI" ]
then 
    mls=computeMLS.py
else 
  echo GLPK or GUROBI should be passed as the third argument
  exit 
fi

#Chooses the clustering algorithms 

if [ "$1" -gt $nbofbreakpoints ]
then 
  echo "Number of clusters k is larger than the number of breakpoints" $nbofbreakpoints
  exit
elif [ "$1" -lt $nbofchromosomes ]
then 
    types=rkaA
    echo "k is smaller than the number of chromosomes. Algorithm linear is not implemented for such values of k." 
elif [ "$1" -lt $nbofAGcomponents ]
then 
    types=rlkaAh
else 
    types=rlk
    echo "Only random, k-medoids and linear clusterings are implemented for the number of clusters greater than" $nbofAGcomponents
fi

#Performs the experiments

python2 makeclusters.py Data/Breakpoints/mel-yak/ tmppartitions/ $types $1 $2 --del
echo "Computing mls for non-normalized HiC"
python2 $mls tmppartitions/ tmpmls/ --del | egrep -v "Academic license - for non-commercial use only"
python2 experiments.py tmpmls/ Plots/ w

python2 makeclusters.py Data/Breakpoints/mel-yak-normalized/ tmppartitions/ $types $1 $2 --del
echo "Computing mls for normalized HiC"
python2 $mls tmppartitions/ tmpmls/ --del | egrep -v "Academic license - for non-commercial use only"

python2 experiments.py tmpmls/ Plots/ w

python2 makeclusters.py Data/Breakpoints/mel-yak/ tmppartitions/ k $1 $2 --del
echo "Computing mls for non-normalized HiC"
python2 $mls tmppartitions/ tmpmls/ | egrep -v "Academic license - for non-commercial use only"
python2 experiments.py tmpmls/ Plots/ d

#Removes temporary folders

rm -rf tmppartitions
rm -rf tmpmls


echo "Plots were saved in Plots/ directory"
