#!/usr/bin/env python
# Krister Swenson                                                Spring 2017
#
"""
Create matrices that have a row/column for each breakpoint.
Create adjacency graph with additional information on the lengths of the chromosomes of the genome whose hi-c values are used. 
Retrieve the connected components of an adjacency graph 

@note: we refrain from using the numpy array format for writing to files for
       the sake of simplicity.  The matrices created by this script will
       be human readable and parsable by any language.
"""
import sys
import os
import argparse
import itertools
import shutil
import numpy as np
import cPickle as pickle

from collections import defaultdict

from lib import heatmapfuncs
from lib.Genomes import GenomePair, readGenomeMap, removeOverlappingGenes
from lib.heatmapfuncs import getWinSize
from lib.scenariofuncs import scorePhysIntList
from lib.pyinclude import usefulfuncs
from lib.AdjacencyGraph import AdjacencyGraph


### Constants:

INDEX_BREAKPOINT_MAP_FILE = 'indexTOchrTOphysbreakpoint.pcl'
BREAKPOINT_INDEX_MAP_FILE = 'physbreakpointTOindex.pcl'
ADJACENCYGRAPH_FILE = 'adjacencygraph.pcl'
COMPONENTSOFAG_FILE = 'componentsofag.pcl'


#        _________________________
#_______/        Classes          \_____________________________________________





#        _________________________
#_______/        Functions        \_____________________________________________



def hasInterMats(hm):
  """
  Return True if there is at least one interchromosomal heatmap.

  @param hm: map chromosome pair to Heatmap object
  """
  for c1, c2 in hm.iterkeys():
    if c1 != c2:
      return True

  return False


def invertPhysintTOindex(physintTOindex):
  """
  Return the inverse of the given dictionary.
  """
  indexTOchrTObreakpoint = defaultdict(dict)
  for physint in physintTOindex.iterkeys():
    indexTOchrTObreakpoint[physintTOindex[physint]][physint[0]] = physint

  return indexTOchrTObreakpoint


def components(ag): 
  """
  Retrieve the breakpoints of one of the genomes belonging to the connected components of an adjacency graph.
  These connected components are either paths or cycles.

  @return: list of pairs consisting of lists of breakpoints and a letter 'p' or 'c' for path or cycle.  

  """
  visitedset = set()
  components = []
  
  #We first retrieve paths by starting at the telomeres and update visited set by visited elementes
  telomeres = ag.telomeres2 
  for t in telomeres:
    path = [(t.chromosome, t.physint)]
    visitedset.add(t)
    visitedset.add(t.reality)
    while t.reality.desire.reality.desire!=None:
      t=t.reality.desire.reality.desire
      visitedset.add(t)
      visitedset.add(t.reality)
      path.append((t.chromosome, t.physint))
    components.append((path,'p'))

  #We retrieve cycles by checking all unvisited elements
  for n, n2 in AdjacencyGraph.getBreakpointsSide2(ag):
    cycle = []
    while n not in visitedset:
      cycle.append((n.chromosome, n.physint))
      visitedset.add(n)
      visitedset.add(n.reality)
      n = n.reality.desire.reality.desire
    if len(cycle)>0:
      components.append((cycle,'c'))  

  return components 


def main():
  desc = 'Create a contact matrix where rows/columns represent breakpoint'+\
         ' regions.'
  parser = argparse.ArgumentParser(description=desc)
  parser.add_argument('SYNTENY_FILE',
                      help='the file specifying the synteny information for two genomes')
  parser.add_argument('HEATMAPS_LOC',
                      help='the location for heatmap files')
  parser.add_argument('OUT_LOC',
                      help='the location to write the new heatmaps to')
  parser.add_argument('-f', '--force', action='store_true',
                      help='overwrite existing directory')
  parser.add_argument('-z', '--zip', action='store_true',
                      help='gzip the matrices')

  args = parser.parse_args()

  genomefile = args.SYNTENY_FILE
  heatmaps = args.HEATMAPS_LOC
  outdir = args.OUT_LOC
  overwrite_dir = args.force
  zip_mats = args.zip

  if(zip_mats):
    extension = 'gz'
  else:
    extension = 'txt'

  if not os.path.exists(outdir):
    os.makedirs(outdir)

  


  #if(os.path.exists(outdir)):
  #  if(overwrite_dir):
  #    shutil.rmtree(outdir)
  #  else:
  #    sys.exit('The given OUT_LOC already exists!')

#        ______________________
#_______/    Do Everything     \________________________________________________

  hm = heatmapfuncs.getHeatmap(heatmaps)
  useinter = hasInterMats(hm)
  mapping = readGenomeMap(genomefile)                  #Read the genomes.
  mapping = removeOverlappingGenes(mapping, swap=True) #Remove dups.

  #The left column (e.g. human with human->mouse) is now genome 2.
  gp = GenomePair(mapping)                             #Get the genome pair.

  #Chromosome lengths.
  chrTOlen = heatmapfuncs.getChromLengths(hm)         
  gp.g2.setChromLengths(chrTOlen)

  #Conctruct and adjacency graph 

  ag = AdjacencyGraph(gp)



     #Map chromosome to physical interval (chromosome, interval pair):
  chTOphysicalintervals = defaultdict(list)
  for n1,n2 in ag.getBreakpointsSide2():
    chTOphysicalintervals[n1.chromosome].append((n1.chromosome, n1.physint))

     #Innitialize intrachromosomal maps and map from physint to matrix index:
  chTOchTOmatrix = defaultdict(dict)
  physintTOindex = {}
  for chrom, physints in chTOphysicalintervals.iteritems():
    chTOchTOmatrix[chrom][chrom] = np.empty([len(physints), len(physints)])

    physints.sort()   #NOTE: these are probably already sorted.
    for i, physint in enumerate(physints):
      physintTOindex[physint] = i

    #Innitialize interchromosomal maps:
  if(useinter):
    for c1, c2 in itertools.combinations(chTOphysicalintervals.iterkeys(), 2):
      if c1 > c2:
        c1, c2 = c2, c1

      shape = [len(chTOphysicalintervals[c1]), len(chTOphysicalintervals[c2])]
      chTOchTOmatrix[c1][c2] = np.empty(shape)

    #Output the key mapping index to physical interval:
  filename = '{}/{}'.format(outdir, BREAKPOINT_INDEX_MAP_FILE)
  with open(filename, 'w') as f:
    pickle.dump(physintTOindex, f)
  
  filename = '{}/{}'.format(outdir, INDEX_BREAKPOINT_MAP_FILE)
  with open(filename, 'w') as f:
    pickle.dump(invertPhysintTOindex(physintTOindex), f)

  
  filename = '{}/{}'.format(outdir, ADJACENCYGRAPH_FILE)
  with open(filename, 'w') as f:
    pickle.dump(ag, f)


  filename = '{}/{}'.format(outdir, COMPONENTSOFAG_FILE)
  with open(filename, 'w') as f:
    pickle.dump(components(ag), f)



    #Compute the score for each pair of intervals:
  print 'creating matrices...'
  chromiter = sorted(chTOphysicalintervals.iterkeys())
  if(useinter):
    chromcombos = itertools.combinations_with_replacement(chromiter, 2)
  else:
    chromcombos = ((chrom, chrom) for chrom in chromiter)

  for c1, c2 in chromcombos:
    for physintpair in itertools.product(chTOphysicalintervals[c1],
                                         chTOphysicalintervals[c2]):
      pi1, pi2 = physintpair
      if pi1 > pi2:
        pi1, pi2 = pi2, pi1

      i1 = physintTOindex[pi1]
      i2 = physintTOindex[pi2]
      _, heatlist, _ = scorePhysIntList(hm, [physintpair])
      chTOchTOmatrix[c1][c2][i1][i2] = heatlist[0]
      if(c1 == c2):                     #Make matrix symmetric.
        chTOchTOmatrix[c1][c2][i2][i1] = heatlist[0]

                                        #Output the matrices:
    filename = '{}/chr{}_chr{}_breakpointcontacts.{}'.format(outdir, c1, c2,
                                                             extension)
    np.savetxt(filename, chTOchTOmatrix[c1][c2])


if __name__ == "__main__":
  main()
