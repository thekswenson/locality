------------------------------------------------------------------------------

## dmel-dyak-genes-oma-2016-06-21.tsv

Syntenic blocks type:           orthologous genes
Database for orthologous genes: OMA
Version of database:            OMA groups - 2016/06/21

### Left species (species 1)

_Drosophila melanogaster_

Genome release: FlyBase 5.09

### Right species (species 2)

_Drosophila yakuba_

Genome release: FlyBase 1.04

-----------------------------------------------------------------------------
