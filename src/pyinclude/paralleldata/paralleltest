#!/usr/bin/python
"""
Test different ways of mapping a function to a processor pool.

This could have been implemented in the unittest framework.
"""

import sys, time
from multiprocessing import Pool
from itertools import combinations, chain, islice
from timeit import timeit
import progressbar



## CONSTANTS:
LIST_SIZE = 500

REPETITIONS = 1
POOL_SIZE = 6
GROUP_SIZE = 1000


def testfunc(pair):
  """
  Given a pair of integers, do something that wastes valuable computation
  power.
  """
  pair = sorted(pair)
  return binomial(pair[1], pair[0])
FUNC = testfunc



def test1(l, poolsize=2):
  """
  This is the simple version which may have race condition with the return
  values (it will probably run OK with your machine but if the function
  is more complicated, then you will have trouble):
  """
  print "Test 1 [just map]..."
  sys.stdout.flush()
  
  p = Pool(poolsize)
  r = p.map(FUNC, combinations(l,2))
  p.close()
  p.join()
  #print r


def test2(l, poolsize=2):
  """
  This fixes the problem by giving a callback and waiting until everything
  has finished.  You can also put a timeout, but I prefer not having one.
  """
  print "Test 2 [map_async]..."
  sys.stdout.flush()
  
  p = Pool(poolsize)
  retval = []
  r = p.map_async(FUNC, combinations(l,2), callback=retval.append)
  r.wait()
  p.close()
  p.join()
  retval = iter(*retval)      #Remove the outer list.
  #print list(retval)


def test2chunk(l, poolsize=2):
  """
  This is a variant of test2 that uses a chunksize.
  """
  print "Test 2 [map_async chunk]..."
  sys.stdout.flush()
  
  p = Pool(poolsize)
  retval = []
  cs = (binomial(len(l),2)/poolsize)/4   #/4 to compensate for uneven workload.
  if(not cs):
    cs = 1
  r = p.map_async(FUNC, combinations(l,2), callback=retval.append, chunksize=cs)
  r.wait()
  p.close()
  p.join()
  retval = iter(*retval)      #Remove the outer list.
  #print list(retval)



def opGroup(pairlist):
  """
  Operate on the given list.

  Keyword Arguments:
  pairlist -- a list of pairs of numbers, e.g.  [(2,4),...,(5,3)]
  """
  return map(FUNC, pairlist)


def grouper(n, iterable):
    """
    Cut the iterable up into groups of n elements.  The last group may have
    less than n.

    grouper(3, 'ABCDEFG', 'x') --> ABC DEF G
    http://stackoverflow.com/questions/8991506/iterate-an-iterator-by-chunks-of-n-in-python
    """
    while True:
      chunk = tuple(islice(iter(iterable), n))
      if not chunk:
        return
      yield chunk



def test3(l, poolsize=2, groupsize=5000):
  """
  Note that CPU utilization is fairly low for the other example:
  each CPU is around 30%.  This is because the work being done for each
  map call of FUNC is very little, so most of the time the machine is just
  sending information between the threads instead of computing something.
  You can break up the data into chunks to make it more efficient.
  """
  print "Test 3 [map_async] (group size "+str(groupsize)+')...'
  sys.stdout.flush()
  
  p = Pool(poolsize)
  retval = []
  groups = grouper(groupsize, combinations(l,2))
  r = p.map_async(opGroup, groups, callback=retval.append)
  r.wait()
  p.close()
  p.join()
  
  retval = chain(*iter(*retval))  #Remove the outer list and then combine lists.
  #print list(retval)



def test3chunk(l, poolsize=2, groupsize=5000):
  """
  This is the version of test3 that uses chunks.
  """
  print "Test 3 [map_async chunk] (group size "+str(groupsize)+')...'
  sys.stdout.flush()
  
  p = Pool(poolsize)
  retval = []
  groups = grouper(groupsize, combinations(l,2))
  numgroups = binomial(len(l), 2)/groupsize
  cs = (numgroups/poolsize)/4   #/4 to compensate for uneven workload.
  if(not cs):
    cs = 1
  r = p.map_async(opGroup, groups, callback=retval.append, chunksize=cs)
  r.wait()
  p.close()
  p.join()
  
  retval = chain(*iter(*retval))  #Remove the outer list and then combine lists.
  #print list(retval)


def test4(l, poolsize=2):
  """
  This is a modification of Arash's code.
  Chunking but no grouping
  """
  print "Test 4 [imap_unordered]..."
  sys.stdout.flush()
  
  result = []
  p = Pool(poolsize)
  for i, value in enumerate(p.imap_unordered(FUNC, combinations(l,2), 1)):
    result.append(value)
  p.close()
  p.join()

  #print list(result)




def test4chunk(l, poolsize=2):
  """
  This is a modification of Arash's code.
  Chunking but no grouping
  """
  print "Test 4 [imap_unordered chunk]..."
  sys.stdout.flush()
  
  result = []
  p = Pool(poolsize)
  cs = (binomial(len(l),2)/poolsize)/4   #/4 to compensate for uneven workload.
  if(not cs):
    cs = 1
  for i, value in enumerate(p.imap_unordered(FUNC, combinations(l,2), cs)):
    result.append(value)
  p.close()
  p.join()

  #print list(result)



def test4group(l, poolsize=2, groupsize=5000):
  """
  This is a modification of Arash's code.
  """
  print "Test 4 [imap_unordered] (group size "+str(groupsize)+')...'
  sys.stdout.flush()
  
  result = []
  p = Pool(poolsize)
  groups = grouper(groupsize, combinations(l,2))
  for i, value in enumerate(p.imap_unordered(opGroup, groups, 1)):
    result.append(value)
  p.close()
  p.join()

  #print list(*result)


def test4gc(l, poolsize=2, groupsize=5000):
  """
  This is a modification of Arash's code.
  """
  print "Test 4 [imap_unordered chunk] (group size "+str(groupsize)+')...'
  sys.stdout.flush()
  
  result = []
  p = Pool(poolsize)
  groups = grouper(groupsize, combinations(l,2))
  numgroups = binomial(len(l), 2)/groupsize
  cs = (numgroups/poolsize)/4   #/4 to compensate for uneven workload.
  if(not cs):
    cs = 1
  for i, value in enumerate(p.imap_unordered(opGroup, groups, chunksize=cs)):
    result.append(value)
  p.close()
  p.join()

  #print list(*result)


def getYMatrix(l, poolsize=2):
  """
  Arash's code.
  Essentialy Test 4 chunk with a progressbar.
  """

  print "Test 5 [imap_unordered chunk progressbar]..."
  sys.stdout.flush()

  totalJobs = binomial(len(l), 2)
  widgets = ['finding distances: ', progressbar.Percentage(), ' ',
             progressbar.Bar(marker='-'), ' ',
             progressbar.ETA()]

  result = []
  p = Pool(poolsize)
  cs = (binomial(len(l),2)/poolsize)/4   #/4 to compensate for uneven workload.
  if(not cs):
    cs = 1
  pbar = progressbar.ProgressBar(widgets=widgets, maxval=totalJobs).start()
  for i, value in enumerate(p.imap_unordered(FUNC, combinations(l,2)), cs):
    if(i < totalJobs):
      pbar.update(i)
    result.append(value)
  pbar.finish()
  p.close()
  p.join()

  #print result

def getYMatrixgroup(l, poolsize=2, groupsize=5000): 
  """
  Arash's code.
  Essentialy Test 4 chunk group with a progressbar.
  """

  print "Test 5 [imap_unordered chunk progressbar] (group size "+str(groupsize)+')...'
  sys.stdout.flush()

  groups = grouper(groupsize, combinations(l,2))
  numgroups = binomial(len(l), 2)/groupsize
  widgets = ['finding distances: ', progressbar.Percentage(), ' ',
             progressbar.Bar(marker='-'), ' ',
             progressbar.ETA()]

  result = []
  p = Pool(poolsize)
  cs = (numgroups/poolsize)/4   #/4 to compensate for uneven workload.
  if(not cs):
    cs = 1
  pbar = progressbar.ProgressBar(widgets=widgets, maxval=numgroups).start()
  for i, value in enumerate(p.imap_unordered(opGroup, groups, cs)):
    pbar.update(i)
    result.append(value)
  pbar.finish()
  p.close()
  p.join()

  #print list(*result)


def binomial(n,k):
  """
  Compute n factorial by a direct multiplicative method.
  (http://userpages.umbc.edu/~rcampbel/Computers/Python/probstat.html)
  """
  if k > n-k: k = n-k # Use symmetry of Pascal's triangle
  accum = 1
  for i in range(1,k+1):
    accum *= (n - (k - i))
    accum /= i
  return accum



def main():
  global LIST_SIZE, POOL_SIZE, REPETITIONS

  
  print binomial(LIST_SIZE, 2),'pairs'
  print 'pool size',POOL_SIZE
  print '_________'

  #map(testfunc, combinations(xrange(LIST_SIZE),2))
  test1(xrange(LIST_SIZE))
  test3(xrange(LIST_SIZE), poolsize=POOL_SIZE, groupsize=GROUP_SIZE)

  print timeit("test1(xrange(LS), poolsize=PS)",
               setup="from __main__ import test1; LS="+str(LIST_SIZE)+\
                     "; PS="+str(POOL_SIZE),
               number=REPETITIONS)
  print timeit("test2(xrange(LS), poolsize=PS)",
               setup="from __main__ import test2; LS="+str(LIST_SIZE)+\
                     "; PS="+str(POOL_SIZE),
               number=REPETITIONS)
  print timeit("test2chunk(xrange(LS), poolsize=PS)",
               setup="from __main__ import test2chunk; LS="+str(LIST_SIZE)+\
                     "; PS="+str(POOL_SIZE),
               number=REPETITIONS)
  print timeit("test3(xrange(LS), poolsize=PS, groupsize=GS)",
               setup="from __main__ import test3; LS="+str(LIST_SIZE)+\
                     "; PS="+str(POOL_SIZE)+"; GS="+str(GROUP_SIZE),
               number=REPETITIONS)
  print timeit("test3chunk(xrange(LS), poolsize=PS, groupsize=GS)",
               setup="from __main__ import test3chunk; LS="+str(LIST_SIZE)+\
                     "; PS="+str(POOL_SIZE)+"; GS="+str(GROUP_SIZE),
               number=REPETITIONS)
  print timeit("test4(xrange(LS), poolsize=PS)",
               setup="from __main__ import test4; LS="+str(LIST_SIZE)+\
                     "; PS="+str(POOL_SIZE),
               number=REPETITIONS)
  print timeit("test4chunk(xrange(LS), poolsize=PS)",
               setup="from __main__ import test4chunk; LS="+str(LIST_SIZE)+\
                     "; PS="+str(POOL_SIZE),
               number=REPETITIONS)
  print timeit("test4group(xrange(LS), poolsize=PS, groupsize=GS)",
               setup="from __main__ import test4group; LS="+str(LIST_SIZE)+\
                     "; PS="+str(POOL_SIZE)+"; GS="+str(GROUP_SIZE),
               number=REPETITIONS)
  print timeit("test4gc(xrange(LS), poolsize=PS, groupsize=GS)",
               setup="from __main__ import test4gc; LS="+str(LIST_SIZE)+\
                     "; PS="+str(POOL_SIZE)+"; GS="+str(GROUP_SIZE),
               number=REPETITIONS)

  print timeit("getYMatrix(xrange(LS), poolsize=PS)",
               setup="from __main__ import getYMatrix; LS="+str(LIST_SIZE)+\
                     "; PS="+str(POOL_SIZE)+"; GS="+str(GROUP_SIZE),
               number=REPETITIONS)
  print timeit("getYMatrixgroup(xrange(LS), poolsize=PS, groupsize=GS)",
               setup="from __main__ import getYMatrixgroup; LS="+str(LIST_SIZE)+\
                     "; PS="+str(POOL_SIZE)+"; GS="+str(GROUP_SIZE),
               number=REPETITIONS)
  #getYMatrix(xrange(LIST_SIZE), POOL_SIZE)
  #getYMatrixgroup(xrange(LIST_SIZE), POOL_SIZE, GROUP_SIZE)


if __name__ == "__main__":
  main()
