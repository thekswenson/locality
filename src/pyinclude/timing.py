# Tests some efficiency questions.
#
# Question: 2D dict instead of 1D dict of pairs.
# Conclusion: using 2D dict is WAY faster than using a tuple.
#
# Question: set usage soas to avoid reassigning to dictionary location.
# Conclusion: reassign is faster.
#
from itertools import combinations
from collections import defaultdict


def test2D(l):
  """
  Make and reference a 2D dict.
  """
  print("test 2d")
  d = defaultdict(dict)
  for i,j in combinations(l,2):
    d[i][j] = 'it'
  
  for i,j in combinations(l,2):
    a = d[i][j]


def testTuple(l):
  """
  Make and reference a 1D dict indexed by a tuple.
  """
  print("test tuple")
  d = {}
  for i,j in combinations(l,2):
    d[(i,j)] = 'it'
  
  for i,j in combinations(l,2):
    a = d[(i,j)]


  #Testing 2D dict instead of 1D dict of pairs:
#l = xrange(8000)
#testTuple(l)
#test2D(l)


def testSet(l):
  """
  Use a set to make sure we don't reassign multiple times.
  """
  d = {}
  for i in set(l):
    d[i] = 'here'

def testReassign(l):
  """
  Reassign to a dictionary location rather than using a set to filter dups.
  """
  d = {}
  for i in l:
    d[i] = 'here'



  #Testing set usage soas to avoid reassigning to dictionary location:
l = list(range(30000000))
l = l*5
#testSet(l)
testReassign(l)
