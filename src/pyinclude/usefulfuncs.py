"""
 Various useful functions.
"""

import os
import itertools
import sys
import decimal
import time
from decimal import Decimal
from math import exp, log, pi



#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|   OBJECTS   |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#


#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|  FUNCTIONS  |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#




def dump(obj):
  """
  Print the attributes for a given object.
  """
  for attr in dir(obj):
    print("obj.{} = {}".format(attr, getattr(obj, attr)))


def invdict(d):
  """
  Return the inverse dictionary.

  @note: the given dictionary must have values that are hashable.
  """
  if d:
    return {v:k for k,v in d.items()}
  else:
    return d


def printnow(message='', end='\n', stream=sys.stdout):
  """
  Print and then flush.

  @param message: the thing to print
  @param end:     put this at the end of the line
  @param stream:  output to this stream
  """
  stream.write(str(message)+end)
  stream.flush()

def printerr(message='', end='\n'):
  """
  Print to stderr and then flush.
  """
  printnow(message, end, stream=sys.stderr)


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, flush=True, **kwargs)


def statusprint(message, leader='  '):
    eprint(f'{leader}{time.ctime(time.time())} {message}')


def binomial(n,k):
  """
  Compute n choose k by a direct multiplicative method.
   - U{http://userpages.umbc.edu/~rcampbel/Computers/Python/probstat.html}
  """
  if n < k:
    return 0

  if k > n-k: k = n-k # Use symmetry of Pascal's triangle
  accum = 1
  for i in range(1,k+1):
    accum *= (n - (k - i))
    accum /= i
  return accum

def binomial_approx(n,k):
  """
  Compute n choose k with stirling's approximation.
  """
  return exp(k*log((float(n)/k)-0.5) + k - 0.5*log(2*pi*k))


def binomial_fast(n,k):
  """
  Compute n choose k.
  """
  accum = Decimal(1)
  for i in range(0, k):
    accum *= Decimal(n-i)/(k-i)
  return int(accum)


def nChoose2(n) -> int:
  """
  Compute n choose 2.
  """
  return n*(n-1)/2


def grouper(iterable, n, *, incomplete='fill', fillvalue=None):
  """
  Collect data into non-overlapping fixed-length chunks or blocks
   - U{https://docs.python.org/3/library/itertools.html#itertools-recipes}
  """
  # grouper('ABCDEFG', 3, fillvalue='x') --> ABC DEF Gxx
  # grouper('ABCDEFG', 3, incomplete='strict') --> ABC DEF ValueError
  # grouper('ABCDEFG', 3, incomplete='ignore') --> ABC DEF
  args = [iter(iterable)] * n
  if incomplete == 'fill':
      return itertools.zip_longest(*args, fillvalue=fillvalue)
  if incomplete == 'strict':
      return zip(*args, strict=True)
  if incomplete == 'ignore':
      return zip(*args)
  else:
      raise ValueError('Expected fill, strict, or ignore')


def binomialCDF_nonR(trials, successes, prob):
  """
  Compute the binomial cumulative distribution function (without R).
  """
  decimal.getcontext().prec = 32
  total = Decimal(0.0)
  prob = Decimal(prob)
  for i in range(successes+1):
    total += binomial(trials, i) * pow(prob, i) * pow(1-prob, trials-i)
  return total


#def binomialCDF(trials, successes, prob):
#  """
#  This uses R to compute the binomial cumulative distribution function.
#  """
#  return ro.r.pbinom(successes, size=trials, prob=prob)[0]


def pairwise(iterable):
  """
  From the itertools webpage.
  s -> (s0,s1), (s1,s2), (s2, s3), ...
  """
  a, b = itertools.tee(iterable)
  next(b, None)
  return zip(a, b)


def touch(fname, times=None):
  """
  Touch the given file.
  """
  os.utime(fname, times)


def cmp_to_key(mycmp):
  """
  Convert a cmp= function into a key= function

  Notes
  -----
      From https://docs.python.org/3.10/howto/sorting.html
  """
  class K:
    def __init__(self, obj, *args):
      self.obj = obj
    def __lt__(self, other):
      return mycmp(self.obj, other.obj) < 0
    def __gt__(self, other):
      return mycmp(self.obj, other.obj) > 0
    def __eq__(self, other):
      return mycmp(self.obj, other.obj) == 0
    def __le__(self, other):
      return mycmp(self.obj, other.obj) <= 0
    def __ge__(self, other):
      return mycmp(self.obj, other.obj) >= 0
    def __ne__(self, other):
      return mycmp(self.obj, other.obj) != 0
  return K
