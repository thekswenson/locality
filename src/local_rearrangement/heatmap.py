"""
These classes are deprecated since we've moved to Cooler!!!
"""
from typing import Dict, Tuple
import numpy as np

from .types import T_Chrom


class Heatmap:
  """
  This class represents a heatmap of Hi-C.

  It holds the values in a Numpy matrix, and the basic info
  needed to work with it (the chromosomes, the window size, etc).

  Usage:::
      # Initialize the heatmap, chromosome 7, intra-contact, resolution 1Mb
      h = Heatmap(chroms=('7', '7'), window_size=1000000)

      # Load the data
      h.load_from_lieb('chr7.txt')

      # Get the Hi-C value for these two positions
      h[123456, 654321]

  .. For now, even if the matrix is symmetric, the values are
  duplicated. That is because I don't know how to automagically
  handle that with Numpy, and I don't want to do it by hand.
  """

  def __init__(self, chroms, window_size):
    ''' Initialize a heatmap.
    Args:
        chroms (str, str): The two chromosomes for this heatmap.
        window_size (int): The size for the window (i.e. the resolution)
    '''
    self.chroms = chroms           #:Pair (str,str) represeting the chromosomes.
    self.window_size = window_size #:The window size.
    self.matrix: np.ndarray = None             #:The numpy matrix.

  def __getitem__(self, indices):
    ''' Access heatmap entries with genomic coordinates.
    ''' 
    #assert(self.matrix)
    i = indices[0] / self.window_size
    j = indices[1] / self.window_size

    # If a coordinate is bigger than the heatmap, we take the last one.
    if i >= self.matrix.shape[0]:
      i = self.matrix.shape[0] - 1
    if j >= self.matrix.shape[1]:
      j = self.matrix.shape[1] - 1

    return self.matrix[i, j]
  
  @property
  def chrom_len(self):
    '''
    Returns a dict with the chromosomes as keys and their size as values.
    '''
    #assert(self.matrix)
    return {self.chroms[0]: self.matrix.shape[0] * self.window_size,
            self.chroms[1]: self.matrix.shape[1] * self.window_size}

  def load_from_lieb(self, filename):
    ''' Load the heatmap from the given file.
    Basically, just drop off the first row and first column and
    load the remaining data as a Numpy matrix.

    .. warning:: May not be really space efficient...
    '''
    # Remove the first column and rows
    with open(filename, 'r') as f:
        file_content = f.read()
    file_content = file_content.split('\n')
    i = 0
    while('#' in file_content[i]):
        i += 1

    data = []
    for r in file_content[i+1:]:
        data.append('\t'.join(r.split('\t')[1:]))

    # Create the Numpy matrix
    self.matrix = np.asmatrix(np.genfromtxt(data), dtype=np.float64)



# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#Types:

T_PairHeatmaps = Dict[Tuple[T_Chrom, T_Chrom], Heatmap]
T_NPHeatmaps = Dict[T_Chrom, Dict[T_Chrom, np.ndarray]]
