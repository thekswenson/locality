#!/usr/bin/env python
# Krister Swenson
"""
Functions for creating matrices that summarize the intersection/symdiff/diff
of a set of datasets.
"""
import sys
import os

from itertools import combinations
import numpy as np

from .scenariofuncs import getDistributionDistances
from .heatmapfuncs import getInDictOfMatricesForm
import  local_rearrangement.heatmapfuncs as hf

from pyinclude.usefulfuncs import printnow, printerr

#CONSTANTS:


DELIM = '-'  #Delimeter between the two intersectionids in the directory names.


# ____________________________________________________________________________ #
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|  FUNCTIONS  |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#


def createCombinationMatrix(path, datasets, scenariodir, randdir,
                            makeinter=False, makediff=False, makebalance=False,
                            justcheck=False, outprefix='', usejitter=''):
  """
  Given a list of datasets, create the combination matrix with the given prefix.
  By default create the intersection matrix.

  @param path:        path to input files
  @param datasets:    list of datasets to use
  @param scenariodir: directory that holds actual scenarios
  @param randdir:     directory that holds randomized scenarios
  @param makeinter:   make interchromosomal matrix
  @param makediff:    make difference matrix
  @param makebalance: make balance matrix
  @param justcheck:   don't make changes, just check if files are there
  @param outprefix:   output filename prefix
  @param usejitter:   use jittered data instead of randomized if this is set to
                      a directory name
  """
  nTp = {}   #name TO position.
  for i,n in enumerate(datasets):
    nTp[n] = i
  print(' '.join(datasets))

    #Compute the pair-wise distribution distances:
  missing = False
  intradistmat = np.zeros([len(datasets), len(datasets)])
  interdistmat = np.zeros([len(datasets), len(datasets)])
  numfilesindirs = None           #The number of files in the scenario directories.
  for n,m in combinations(datasets, 2):
    if(os.path.isdir(path+n+DELIM+m)):
      dname = path+n+DELIM+m
    elif(os.path.isdir(path+m+DELIM+n)):
      dname = path+m+DELIM+n
    else:
      print('WARNING: missing dir for '+m+' and '+n+'.')
      print( path+m+DELIM+n)
      missing = True
      continue

    dnm = dname+scenariodir
    if(usejitter):
      dmn = dname+usejitter
    else:
      dmn = dname+randdir
    if(makediff or makebalance):
      dnm = dname+'/'+n+DELIM+m
      dmn = dname+'/'+m+DELIM+n
      if(not os.path.isdir(dnm) or not os.path.isdir(dmn)):
        printnow('\nWARNING: expected subdirectories not found in '+dname,
                 stream=sys.stderr)
        missing = True
      else:
        if(numfilesindirs == None):
          numfilesindirs = len(os.listdir(dnm))
        if(numfilesindirs != len(os.listdir(dnm)) or
           numfilesindirs != len(os.listdir(dmn))):
          printerr('\nWARNING: mismatch between number of heatmaps in dirs:\n'+
                   '{}\n{}\n        expected {}'.format(dnm, dmn,
                                                        numfilesindirs))

    if(not justcheck):
      if(makediff or makebalance):
        if(makediff):
          intradist,interdist = getDistributionDistances(dnm, dmn, False)
        else:
          intradist,interdist = getCellBalance(dnm, dmn)
        i = nTp[n]
        j = nTp[m]
        if(i < j):
          intradistmat[i][j] = intradist
          intradistmat[j][i] = -intradist
          if(makeinter):
            interdistmat[i][j] = interdist
            interdistmat[j][i] = -interdist
        else:
          intradistmat[i][j] = -intradist
          intradistmat[j][i] = intradist
          if(makeinter):
            interdistmat[i][j] = -interdist
            interdistmat[j][i] = interdist
      else:
        intradist,interdist = getDistributionDistances(dnm, dmn, False)
        i = nTp[n]
        j = nTp[m]
        intradistmat[i][j] = intradistmat[j][i] = intradist
        if(makeinter):
          interdistmat[i][j] = interdistmat[j][i] = interdist
      printnow('.', end='')

  if(justcheck):
    if(not missing):
      print('nothing missing.')
    else:
      raise(Exception('Failed check: missing files (see above).'))
    return

  print('')

    #Write the files:
  hdr = ' '.join(datasets)
  if(makediff):
    hdr += '\nThese are percentages of all nonnull matrix entries.'+\
           '\nValues are row-oriented, so M[i][j] has the sign from the'+\
           '\nperspective of the dataset in row i.'
  if(makebalance):
    hdr += '\nValues are row-oriented, so M[i][j] has the sign from the'+\
           '\nperspective of the dataset in row i.'

  if(makeinter):
    interfile = outprefix+'inter.csv'
    print(' '+interfile)
    np.savetxt(interfile, interdistmat, delimiter=',', fmt='%6.2f', header=hdr)
  else:
    intrafile = outprefix+'intra.csv'
    printnow('creating: '+intrafile)
    np.savetxt(intrafile, intradistmat, delimiter=',', fmt='%6.2f', header=hdr)



def getCellBalance(d1, d2):
  """
  Count only non-zero values in both matrices.  Return the difference between
  the two counts over the total non-zero values (should be sum of matrix sizes).

  @param d1: directory with heatmaps
  @param d2: directory with heatmaps
  """
  ms1 = getInDictOfMatricesForm(hf.readHeatmaps(d1))
  ms2 = getInDictOfMatricesForm(hf.readHeatmaps(d2))
  inter1 = 0
  inter2 = 0
  intra1 = 0
  intra2 = 0
  for c in ms1.keys():
    for d in ms1[c].keys():
      m1 = ms1[c][d]
      m2 = ms2[c][d]

      if(c == d):
        intra1 += np.count_nonzero(~np.isnan(m1))
        intra2 += np.count_nonzero(~np.isnan(m2))
      else:
        inter1 += np.count_nonzero(~np.isnan(m1))
        inter2 += np.count_nonzero(~np.isnan(m2))

  intraval = float(intra1-intra2)/(intra1+intra2)
  interval = 0
  if(inter1):
    interval = float(inter1-inter2)/(inter1+inter2)

  return intraval, interval






# ____________________________________________________________________________ #
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|  GRAVEYARD  |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#


