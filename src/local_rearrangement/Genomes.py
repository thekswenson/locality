#!/usr/bin/env python
# Krister Swenson                                                Winter 2012-13
#
# Classes to handle genomes.
#

import sys

from collections import  defaultdict
from typing import Dict, List, Optional, Tuple
from functools import cmp_to_key
from pathlib import Path
from .types import T_Chrom


class Gene:
  """
  A gene has a number, start and end coordinates, along with a
  stableID (for Ensembl).
  It also has a pointer to another gene that may be its sole ortholog.
  """
  genenum = 0     #The unique gene number represented by a signed integer.
  begin = 0       #Begin coordinates.
  end = 0         #End coordinates.
  stableId = ''   #StableID.
  chromosome = '' #The chromosome the gene is on.
  strand = 0      #1 if read on the forward strand, otherwise 0.
                  #NOTE: this does not necessarily correspond to the sign!!!
  ortholog = None #An orthologous gene to this one.

  def __init__(self, genenum=0, begin=0, end=0, stableId='', chrom='',
                     strand=0, ortho=None):
    self.genenum = genenum
    self.begin = begin
    self.end = end
    self.stableId = stableId
    self.chromosome = chrom
    if(strand > 0):
      self.strand = strand
    else:
      self.strand = 0
    self.ortholog = ortho

  def setOrtholog(self, g):
    """
    Pair the given gene with this one.
    """
    self.ortholog = g

  def memberStr(self):
    """
    Return a string representing
    """
    retstr = 'genenum:'+str(self.genenum)+'\n'
    retstr += 'begin:'+str(self.begin)+'\n'
    retstr += 'end:'+str(self.end)+'\n'
    retstr += 'stableId:'+str(self.stableId)+'\n'
    retstr += 'chromosome:'+str(self.chromosome)+'\n'
    retstr += 'strand:'+str(self.strand)+'\n'
    retstr += 'ortholog:'+str(self.ortholog)
    return retstr

  def __str__(self):
    return str(self.genenum)

  def __repr__(self):
    return str(self)



# _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _
class Genome:
  """
  A genome.
  """
  def __init__(self):
    self.gnm: Dict[str, List[Gene]] =\
      defaultdict(list) #: The genome index by chrom, then order.
    self.chrmTOlen = None        #: Map chromosome to chromosome length.

  def __getitem__(self, key):
    return self.gnm[key]

  def keys(self):
    return self.gnm.keys()


  def __str__(self):
    """
    Return a str representation of the given genome.
    """
    retstr = ''
    for chrm in sorted(self.gnm.keys(), key=cmp_to_key(cmpChromosomes)):
      retstr += 'Chromosome '+chrm+':\n'
      for gene in self.gnm[chrm]:
        retstr += str(gene)+' '
      retstr += '\n'

    return retstr

  def __repr__(self):
    return str(self)


  def setChromLengths(self, lengths):
    """
    Set the chromosom to length dict.

    @param lengths: dictionary mapping chromosome to chromosome length
    """
    self.chrmTOlen = lengths
      #Confirm that no gene has indices greater than the chromosome length:
    for chrm in self.gnm.keys():
      for gene in self.gnm[chrm]:
        if(gene.end > lengths[chrm]):
          raise(Exception(f'ERROR: gene {gene.stableId} has index'
                          f' {chrm}:{gene.end}, which is greater than the'
                          f' chromosome length {lengths[chrm]}.'))



# _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _
class GenomePair:
  """
  Represents two genomes on the same set of genes.
  """

  def __init__(self,
               mapping: Optional[Dict[T_Chrom,
                                      List[Tuple[Tuple[int, int, int, str],
                                                 Tuple[T_Chrom, int, int, int, str]]]]]=None,
               genomes: Optional[Tuple[List[str], List[str]]]=None):
    """
    Build the genomes from the mapping.

    @param mapping: a dictionary keyed on chrom1.
    @type mapping: lists of pairs of the form
                   I{((beg1,end1,sign1,sid1), (chrm2,beg2,end2,sign2,sid2))}
    @param genomes: a pair of genomes where each genome is either a list of
                    lists or a string represention of such.
    """
    self.g1 = Genome()            #:The first genome.
    self.g2 = Genome()            #:The second genome.

                           #Map chrom and beginning to gene:
    self.locationTOgene1 = defaultdict(dict)    #:Map (chr, beg) to gene.
    self.locationTOgene2 = defaultdict(dict)    #:Map (chr, beg) to gene.

                           #Map gene number to gene:
    self.genenumTOgene1 = dict()  #:Map gene number to gene
    self.genenumTOgene2 = dict()  #:Map gene number to gene

    if mapping:
      self._innitializeMap(mapping)
    elif genomes:
      self._innitializeLists(*genomes)


  def __str__(self):
    return 'Genome1: '+str(self.g1)+'\n'+'Genome2: '+str(self.g1)


  def getChromosomes1(self):
    "Generator for iterating over the chromosomes."
    for chrm in sorted(self.g1.keys(), key=cmp_to_key(cmpChromosomes)):
      yield chrm

  def getChromosomes2(self):
    "Generator for iterating over the chromosomes."
    for chrm in sorted(self.g2.keys(), key=cmp_to_key(cmpChromosomes)):
      yield chrm



  def _innitializeLists(self, l1, l2):
    """
    Innitialize the genomes given two strings representing lists of lists.
    """
    count = 0
    for i,c in enumerate(l1):                  #For each chromosome,
      for j in c:                              #and each element,
        begin = count*2                        #make a fake beginning
        end = begin+1                          #and a face ending,
        count += 1
        newgene = Gene(j, begin, end)          #create the gene,
        self.g1[str(i)].append(newgene)        #and add it to the list.
        #self.locationTOgene1[begin] = newgene
        #self.genenumTOgene1[j] = newgene

    count = 0
    for i,c in enumerate(l2):                  #For each chromosome,
      for j in c:                              #and each element,
        begin = count*2                        #make a fake beginning
        end = begin+1                          #and a face ending,
        count += 1
        newgene = Gene(j, begin, end)          #create the gene,
        self.g2[str(i)].append(newgene)        #and add it to the list.




  def _innitializeMap(self,
                      mapping: Dict[T_Chrom,
                                    List[Tuple[Tuple[int, int, int, str],
                                               Tuple[T_Chrom, int, int, int, str]]]]):
    """
    Innitialize the genomes given a map.

    @param mapping: a dictionary keyed on chrom1.
    @type  mapping: lists of pairs of the form
                    I{((beg1,end1,sign1,sid1), (chrm2,beg2,end2,sign2,sid2))}
    """
    chrmbegTOgene = defaultdict(dict)   #Map chroms and beginnings to gene.
    current = 1
    for chr1 in sorted(mapping.keys(), key=cmp_to_key(cmpChromosomes)):
      for e in sorted(mapping[chr1], key=lambda x: int(x[0][0])):
        #gene = current*e[0][2]
        if(e[0][0] in self.locationTOgene1[chr1]):
          sys.exit('ERROR: multiple genes with start '+str(e[0][0])+' in'+
                   ' genome 1.')
        if(e[1][1] in chrmbegTOgene[e[1][0]]):
          sys.exit('ERROR: multiple genes with start '+str(e[1][1])+' in'+
                   ' genome 2.')

        genenum1 = current
        newgene1 = Gene(genenum1, e[0][0], e[0][1], e[0][3], chr1, e[0][2])
        genenum2 = genenum1*e[0][2]*e[1][3]
        newgene2 = Gene(genenum2, e[1][1], e[1][2], e[1][4], e[1][0],
                        e[1][3], newgene1)
        newgene1.setOrtholog(newgene2)

        self.g1[chr1].append(newgene1)
        self.locationTOgene1[chr1][e[0][0]] = newgene1
        self.genenumTOgene1[abs(genenum1)] = newgene1

        chrmbegTOgene[e[1][0]][e[1][1]] = newgene2
        current += 1

    for chr2 in sorted(chrmbegTOgene.keys(), key=cmp_to_key(cmpChromosomes)):
      for beg2 in sorted(chrmbegTOgene[chr2].keys()):
        currgene = chrmbegTOgene[chr2][beg2]
        self.g2[chr2].append(currgene)
        self.locationTOgene2[chr2][beg2] = currgene
        self.genenumTOgene2[abs(currgene.genenum)] = currgene




def chromosomesToKey(c):
  """
  Return a value for the given chromosome that is suitable for comparisions.

  @note: we make no attempt to sort chromosomes numerically.
  """
  #if(c.isdigit()):
  #  return int(c)

  return c


def cmpChromosomes(c1, c2):
  """
  Return a postive, zero, or negative number depending on whether the first
  chromosome is considered larger than, equal to, or less than the second.
  """
  if(c1.isdigit()):
    c1 = int(c1)
    if(not c2.isdigit()):
      return -1
  elif(c2.isdigit()):
    return 1

  if(c2.isdigit()):
    c2 = int(c2)

  if(c1 < c2):
    return -1

  return c1 > c2


def readGenomeMap(genomefile: Path)\
  -> Dict[T_Chrom,
          List[Tuple[Tuple[int, int, int, str],
                     Tuple[T_Chrom, int, int, int, str]]]]:
  """
  Read in the genome file.
  Return a dictionary keyed on chromosome of Genome 1, where each entry is
  a list of pairs of corresponding genes. Each pair has an interval of Genome 1
  paired with an interval of Genome 2.
  Genome 1 is the left column of the input file, while genome 2 is the right.
  """
  try:
    f = genomefile.open()
  except IOError as e:
    sys.exit(f'Error opening file "{genomefile}": {e}')

  lines = f.read().split('\n')
  f.close()
  lines = [l for l in lines if l]  # Removes empty lines

  mapping = defaultdict(list)
  for line in lines:
      id1, chr1, beg1, end1, sign1, id2, chr2, beg2, end2, sign2 = line.split('\t')
      mapping[chr1].append(( (int(beg1), int(end1), int(sign1), id1),
                             (chr2, int(beg2), int(end2), int(sign2), id2) ))
      
  f.close()

  return mapping


def removeOverlappingGenes(mapping:Dict[T_Chrom,
                                        List[Tuple[Tuple[int, int, int, str],
                                                   Tuple[T_Chrom, int, int, int, str]]]],
                           swap=False):
  """
  Remove genes that overlap.
    - I{this modifies -mapping-}

  @param mapping: dict of pairs of gene location information keyed by
                  chromosome 1.
  @param swap: if True, then reverse the roles of genome 1 and 2.
  """
  while(removeOverlapsFromFirst(mapping)): pass
  new = swapRoles(mapping)
  while(removeOverlapsFromFirst(new)): pass
  if swap:                     #If we need to swap then
    mapping = new              #leave things in swapped state.
  else:
    mapping = swapRoles(new)   #Otherwise, swap things back to original state.

  return mapping


def removeOverlapsFromFirst(mapping):
  """
  Take a dictionary of lists of pairs, keyed on the chromosome of the first of
  the pair, and return a version without any of the overlapping genes specified
  by the first of the pair.
  Return True if something was removed.
  The left (upstream) gene in an overlapping pair is the one that is kept.
  """
  wasremoved = False
  for pairs in mapping.values():
    pairs.sort(key=lambda x: x[0])
    previous = None
    toremove = []       #Indices to be removed.
    for i in range(1, len(pairs)):
      if(pairs[i][0][0] < pairs[i-1][0][1]):
        toremove.append(i)
    for i in reversed(toremove):
      wasremoved = True
      pairs.pop(i)

  return wasremoved


def swapRoles(mapping):
  """
  Take a dictionary of lists of pairs keyed on the chromosome of the first of
  the pair, and return a version with the roles of the first and second pair
  swaped.
  """
  newmapping = defaultdict(list)
  for k in mapping.keys():
    for entry in mapping[k]:
      second = [k]+list(entry[0])
      newentry = (entry[1][1:], second)
      newmapping[entry[1][0]].append(newentry)
  return newmapping


