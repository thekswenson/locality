#!/usr/bin/env python2
# Krister Swenson                                                Winter 2012-13
"""
The DCJ adjacency graph augmented to support localities.

Human is assumed to be genome 2.
"""
from __future__ import annotations
from decimal import Decimal

import matplotlib
matplotlib.use('Agg')

import sys
import os
import pydot
import subprocess
import numpy as np
import copy
import pickle
import random
import re
import scipy
import progressbar as pb
import networkx as nx

from math import ceil
from collections import defaultdict, deque
from itertools import chain, combinations, product, repeat
from multiprocessing import Pool, Manager
from typing import Any, Collection, List, NewType, Optional, Tuple
from cooler import Cooler


import pyinclude.coloredpartition as coloredpartition
from pyinclude.usefulfuncs import nChoose2, grouper, printnow

from .Genomes import Genome, GenomePair, Gene
from .heatmapfuncs import NULLV, CoolerHeatmap
from .heatmapfuncs import getNonNullIntsOnDiag
from .scenariofuncs import readScenario, writeScenario, scorePhysIntList
from .types import T_IntervalPair, T_GeneEnd, T_NodeID, T_Move


DOT_FILE = '.tempdotfile.dot'
X_SEP = 100
X_REALITY = 120
Y_SEP = 200


SCENARIO_REPS = 1  #The number of scenarios to average over.
GROUPSIZE  = 3000  #The number of pairs to send to each processor (for repeats).
MAX_JITTER_TRIES  = 1000         #Number of tries to place a jittered component.
COMPONENT_WARN_THRESH=0#10000000 #Warn if less than this much space to jitter.


edgeTOrpts = dict() #Map edges to repeats on that edge (used in AnalyzeEdges).


CLASS  = 0
FAMILY = 1
NAME   = 2


# _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _
# TYPES:

T_GreedyMove = Tuple[float, Tuple[Tuple[str, int], Tuple[str, int]], Tuple]
T_MoveType = NewType('T_MoveType', int)

NULL_MOVE: T_GreedyMove = (float('-inf'), (('', 0), ('', 0)), ())

# _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _
class AdjacencyGraph(object):
  """
  The DCJ adjacency graph.

  Typical sorting functions (e.g. getMLPS(), sampleRandomScenarios()) sort from
  genome 2 to genome 1.  The nodes for each genome are indexed by the gene name
  and end.

    - Example Entries: 1: {AGNode._HEAD: 1h, AGNode._TAIL: 1t}

  @ivar nodes1: AGNodes for genome1
  @type nodes1: map gene name to end to AGNode
  @ivar nodes2: AGNodes for genome2
  @type nodes2: map gene name to end to AGNode
  @ivar telomeres1: telomeres for genome1
  @type telomeres1: list
  @ivar telomeres2: telomeres for genome2
  @type telomeres2: list
  @ivar cycles: the cycles in the graph.
  @type cycles: list
  @ivar opaths: the odd-length paths in the graph.
  @type opaths: list
  @ivar wpaths: the W paths in the graph.
  @type wpaths: list
  @ivar mpaths: the M paths in the graph.
  @type mpaths: list
  """

  _PSEUDO_RAND_SPLIT = 1 #:Sample scenarios, first splitting Mpaths.
  _PSEUDO_RAND_MIX = 2   #:Sample scenarios, first mixing Mpaths.

#       .       .       .       .       .       .       .       .       .
#{ Innitialization

  def __init__(self, pair: GenomePair):
    """
    Build the graph for the given pair.

    @param pair: a GenomePair
    """
    self.genome1 = pair.g1
    self.genome2 = pair.g2
    self.nodes1 = defaultdict(dict)
    self.nodes2 = defaultdict(dict)
    self.telomeres1 = list()
    self.telomeres2 = list()

    self.cycles1: List[T_Comp] = None
    self.opaths1: List[T_Comp] = None
    self.wpaths1: List[T_Comp] = None
    self.mpaths1: List[T_Comp] = None
    self.cycles2: List[T_Comp] = None
    self.opaths2: List[T_Comp] = None
    self.wpaths2: List[T_Comp] = None
    self.mpaths2: List[T_Comp] = None

    self._initialize(pair)



  def _addReality(self, n1, n2, chromlen=None):
    """
    Add a reality edge between n1 and n2. Set the physical interval too.

    @param n1:       the left-hand node
    @param n2:       the right-hand node
    @param chromlen: the length of the chromosome
                     (used if adding a right-hand telomere)
    """
    n1.reality = n2
    n2.reality = n1
    if(not n1.gene):
      n1.physint = n2.physint = (0,n2.gene.begin)
    elif(not n2.gene):  #If it's a right-hand telomere then we set the
      if(chromlen):     #chromosome length as the right side of the interval.
        n1.physint = n2.physint = (n1.gene.end, max(chromlen-1, n1.gene.end))
      else:
        #sys.stderr.write('WARNING: chrmsm length not given for telomere!\n')
        n1.physint = n2.physint = (n1.gene.end,0)
    else:
      if(n1.gene.begin < n2.gene.begin):
        n1.physint = n2.physint = (n1.gene.end, n2.gene.begin)
      else:
        n1.physint = n2.physint = (n2.gene.end, n1.gene.begin)


  def _addDesire(self, n1, n2):
    "Add a desire edge between n1 and n2."
    n1.desire = n2
    n2.desire = n1



  def _initialize(self, pair: GenomePair):
    """
    Innitialize the graph.
    A circular chromosome should have the number at the start and end.
    """
    self._constructGraphSide(pair.g1, pair.getChromosomes1(),
                             self.telomeres1, self.nodes1,
                             chromlens=pair.g1.chrmTOlen)

    largest = max(self.nodes1.keys())
    #n1s = sorted(self.nodes1.keys())
    #if(n1s != list(range(1, n1s[-1]+1))):
    if(len(set(self.nodes1.keys())) != largest):
      sys.exit('ERROR: missing genes in genome 1.')

    self._constructGraphSide(pair.g2, pair.getChromosomes2(), self.telomeres2,
                             self.nodes2, 2, self.nodes1, pair.g2.chrmTOlen)

    #if(sorted(self.nodes2.keys()) != range(1, n1s[-1]+1)):
    if(len(set(self.nodes2.keys())) != largest):
      sys.exit('ERROR: missing genes in genome 2.')





  def _constructGraphSide(self, genome: Genome, chromosomes, telomeres, nodes,
                          genomenum=1, otherside=None, chromlens=None):
    """
    Add nodes for one side (genome) of the graph, along with reality edges.
    Also create desire edges if the other side is given.

    @param genome:      A Genome.
    @param chromosomes: The chromosomes from the given Genome -genome-.
    @param telomeres:   The telomeres to fill.
    @param nodes:       The nodes to fill.
    @param genomenum:   The number {1,2} of the genome that being built.
    @param otherside:   This is set to the other node dictionary if desire
                        edges are to be made.
    @param chromlens:   The lengths of the chromosomes.
    @type  chromlens:   Dict of integers indexed by chromosome.
    """
    for chr1 in chromosomes:                        #For each chromosome:
                                                    #Create potential telomere
      lastnode = AGNode(chromosome=chr1, genome=genomenum)
      if(len(genome[chr1]) > 1 and                  #and detect circular chroms.
         genome[chr1][0].genenum == genome[chr1][-1].genenum):
        circular = True
      else:
        circular = False
        telomeres.append(lastnode)

      secondone = firstone = True
      firstnode = None
      for g in genome[chr1]:                        #Visit each gene:
        if not circular or not firstone:            #skip first gene in circular
          if g.genenum > 0:                         #figure out the endpoints
            end1 = AGNode._TAIL
            end2 = AGNode._HEAD
          else:
            end1 = AGNode._HEAD
            end2 = AGNode._TAIL

          if abs(g.genenum) in nodes:
            sys.exit("ERROR: multiple occurrences of "+str(abs(g.genenum))+\
                     " in genome "+str(genomenum)+".")
                                                    #Create the first node
          newnode = AGNode(g.genenum, end1, g, genome=genomenum)
          nodes[abs(g.genenum)][end1] = newnode
          self._addReality(lastnode, newnode)       #and link it to the last.

          if otherside:                             #If adding desire edges:
            if abs(g.genenum) not in otherside:
              sys.exit("ERROR: gene "+str(abs(g.genenum))+" not in genome 1.")
            self._addDesire(newnode, otherside[abs(g.genenum)][end1])
                                                    #Create the second node,
          lastnode = AGNode(g.genenum, end2, g, genome=genomenum)
          nodes[abs(g.genenum)][end2] = lastnode
          if otherside:                             #and add the desire edge.
            self._addDesire(lastnode, otherside[abs(g.genenum)][end2])

          if secondone:                             #Done to keep track of
            firstnode = newnode                     #the first node added, when
            secondone = False                       #dealing with circulars.
        firstone = False

      if circular:                                  #Add the final reality edge:
        assert firstnode
        self._addReality(firstnode, lastnode)
      else:                                         #Link last node to telomere:
        telomeres.append(AGNode(chromosome=chr1, genome=genomenum))
        if chromlens:
          self._addReality(lastnode, telomeres[-1], chromlens[chr1])
        else:
          self._addReality(lastnode, telomeres[-1])

      #for n in self._getNodes(telomeres, nodes):
      #  print str(n),id(n)
      #print '_________'




  def findThreshold(self, heatmap: CoolerHeatmap):
    """
    Given a heatmap, find a threshold that corresponds to the connected
    components of this graph.

    @param heatmap: the heatmap
    @type heatmap:  2D dict heatmap keyed on begin index
    """
    (cycles, opaths, (wpaths,mpaths)) =\
      self._classifyComps(self.telomeres2, self.nodes2)

    allaveboth, allaveintra, allaveinter = heatmap.getCoolerStats()

    intranegative = 0
    intrapositive = 0
    internegative = 0
    interpositive = 0
    for cycle in cycles:           #Score a cycle:
      physints = []
      if len(cycle) > 1:
        G = nx.Graph()
        for n,m in combinations(cycle, 2):
          edgesizen = n.getEdgeLen()
          edgesizem = m.getEdgeLen()
          if edgesizen != None and edgesizem != None:
            physints.append(((n.chromosome, n.physint),
                             (m.chromosome, m.physint)))

            (val, intraval, interval), heat, pos =\
              scorePhysIntList(heatmap, physints[-1:])
            if val:
              weight = -1/val if val > 0 else -1*val
              G.add_edge(n,m, weight=weight, val=val, intra=(intraval != None))
          else:                         #If there's a telomere.
            print('telomere:', n, n.physint, n.physint,
                               (n.gene.chromosome if n.gene else ''))

        (aveboth, aveintra, aveinter), heatl, posl =\
          scorePhysIntList(heatmap, physints)

        assert(aveboth)
        diffboth = aveboth - allaveboth
        if allaveintra and aveintra:
          diffintra = aveintra - allaveintra
        else:
          diffintra = None
        if allaveinter and aveinter:
          diffinter = aveinter - allaveinter
        else:
          diffinter = None

        spantotal = 0
        for u,v in nx.minimum_spanning_tree(G).edges_iter():
          spantotal += G[u][v]['val']
        spanave = spantotal/G.number_of_edges()
        diffspan = spanave - allaveboth


        #print "LEN: {} MAX: {} MIN: {}\n\tBOTH: {} INTRA: {} INTER: {}".\
        #      format(len(cycle), max(heatl), min(heatl), diffboth, diffintra, diffinter)
        signstr = '+' if diffboth >= 0 else '-'
        signstr += '+' if (diffintra is None or diffintra >= 0) else '-'
        signstr += '+' if (diffinter is None or diffinter >= 0) else '-'
        if diffintra != None:
          if diffintra >= 0:
            intrapositive += 1
          else:
            intranegative += 1

        if diffinter != None:
          if diffinter >= 0:
            interpositive += 1
          else:
            internegative += 1

        print("{} BOTH: {} INTRA: {} INTER: {} SPAN: {}".\
              format(signstr, diffboth, diffintra, diffinter, diffspan))

    print("{} {}  {} {}".format(intrapositive, intranegative,
                                interpositive, internegative))


  def _getRandomSpanningTreeOnWeights(self, numedges):
    """
    Pick randomly the given number of edges.
    Compute heatmap values for all pairs of these edges.
    Create a complete graph whose nodes are the chosen edges, and edges are
    weighted based on heatmap values.
    Choose a minimum spannning tree on this graph.
    """
    raise(NotImplementedError)


#} Innitialization



#       .       .       .       .       .       .       .       .       .
#{ DCJ functions

  def getDist(self) -> int:
    """
    Return the DCJ distance for this graph.
    """
    evenpaths = 0
    oddpaths = 0
    cycles = 0
                #Get the paths:
    for t in chain(self.telomeres2, self.telomeres1):
      if(not t.visited):
        n = t.reality
        t.visited = True
        n.visited = True
        odd = False
        while(n.notTelomere()):
          n.desire.visited = True
          n.desire.reality.visited = True
          n = n.desire.reality
          odd = not odd
        if(odd):
          oddpaths += 1
        else:
          evenpaths += 1

                #Get the cycles:
    for e in self.nodes2.values():
      for n in e.values():
        if(not n.visited):
          while(not n.visited):
            n.visited = True
            n.reality.visited = True
            n = n.reality.desire
          cycles += 1

    self._resetVisited()

    return len(self.nodes2) - cycles - int(oddpaths/2)





  def getMLPSdist(self, breakpointTOcolor):
    """
    Return the number of non-local (i.e. costly) moves in the Maximum Local
    Parsimonious Scenario on this graph.
    A pair of physical intervals are co-located if they map to the same value
    in breakpointTOcolor.
    Sorts genome 2 to genome 1.

    @note: use this and L{getDist()} to get the total distance
           (with rare moves weighted as you please).

    @warn: this only works when there are no even-lengt paths to be merged.

    @param breakpointTOcolor: map breakpoint to color (breakpoints with
                                similar color are more likely to be in a DCJ).

    @return: the MLPS distance
    @rtype:  int
    """
    (cycles, oddpaths, (Wpaths,Mpaths)) = self._classifyComps(self.telomeres2,
                                                              self.nodes2,
                                                              compsaslist=True)

    if(Wpaths and Mpaths):
      raise(NotImplementedError('Both AA and BB paths exist!'))

    allcomps = cycles + oddpaths + Wpaths + Mpaths
    chrTOnode = defaultdict(list)
    distance = 0
    for comp in allcomps:
      partitionmodel,_ = self._buildColoredOrderedSet(comp, breakpointTOcolor)
      partition = coloredpartition.mncp(partitionmodel)
      distance += len(partition)-1

    return distance


  def getMLPS(self, breakpointTOcolor, heatmaps: CoolerHeatmap, greedy=True,
              scenariofile=''):
    """
    Return a sceanario that minimized the number of non-local moves in the
    Maximum Local Parsimonious Scenario on this this graph.
    A pair of physical intervals are co-located if they map to the same value
    in breakpointTOcolor.  Sorts genome 2 to genome 1.  


    @note: use this and L{getDist()} to get the total distance
           (with rare moves weighted as you please).

    @warn: this only works when there are no even-length paths to be merged.

    @param breakpointTOcolor: map breakpoint to color.
                                (Those with the same integer are more likely to
                                participate in a DCJ).
    @param heatmaps: the CoolerHeatmap object.
    @param greedy: if True, do the local moves greedily choosing the best
                   next move using the Heatmap.
                   if False, do the local moves randomly.
    @param scenariofile: write the scenario to this file.

    @return: not sure yet
    """
    AG = copy.deepcopy(self)
    (cycles, oddpaths, (Wpaths,Mpaths)) = AG._classifyComps(AG.telomeres2,
                                                            AG.nodes2,
                                                            compsaslist=True)

    if(Wpaths and Mpaths):
      raise(NotImplementedError('Both AA and BB paths exist!'))

    distbefore = AG.getDist()

    allcomps = cycles + oddpaths + Wpaths + Mpaths
    moves = []
    physints: List[T_IntervalPair] = []
    for comp in allcomps:
      node = next(iter(comp))
      if(AG._isSorted((node, node.reality))):
        continue

      partitionmodel, indexTOnode =\
        AG._buildColoredOrderedSet(comp, breakpointTOcolor)
      partition = coloredpartition.mncp(partitionmodel)

      tphysints, tmoves = AG._getMovesFromPartition(partition, indexTOnode)
      physints += tphysints
      moves += tmoves

    distafter = AG.getDist()       #Sanity check on the distance.
    assert(len(moves) == distbefore-distafter)

         #Get the heatmap values for the non-local moves:
    _, heatlist, poslist = scorePhysIntList(heatmaps, physints)

         #Sort out the local moves:
    if(greedy):
      lphysints, lposlist, lheatlist, lmoves, species =\
        AG.getGreedyScenario(heatmaps)
    else:
      lphysints, lposlist, lheatlist, lmoves, species =\
        AG.getRandomScenarioWithScore(heatmaps)


    physints += lphysints
    poslist += lposlist
    heatlist += lheatlist
    moves += lmoves

    if(scenariofile):
      speclist = [2]*len(moves)    # We only made moves in species 2
      writeScenario(scenariofile, physints, poslist, heatlist, moves, speclist)

    return getAverages(physints, heatlist)





  def sampleRandomMLPSScenarios(self, breakpointTOcolor,
                                heatmaps: CoolerHeatmap,
                                reps=1, logto=''):
    """
    Sample MLPS scenarios with random local moves.

    @param breakpointTOcolor: map breakpoint to integer.
                                (Those with the same integer are more likely to
                                participate in a DCJ).
    @param heatmaps: the heatmap object.
    @param logto:    log the scenarios to this directory if it isn't empty
    """
    averages =[]
    for i in range(reps):
      logtobegin, logtoend = os.path.split(logto)
      logtofile = logtoend+'.'+str(i)

      averages.append(self.getMLPS(breakpointTOcolor, heatmaps, False,
                                   logto+'/'+logtofile))

    return map(np.average, zip(*averages))



  def _getMovesFromPartition(self, partition, indexTOnode):
    """
    Return a list of DCJs based on the given partition.

    @param partition:   a set of sets of indices into the partitionmodel
    @param indexTOnode: map partitionmodel index to node
    """
      #NOTE: this code depends on reality edges in the partition being
      #      represented by nodes
      #      with the same parity (e.g. the first node of each edge as
      #      visited on the cycle is in the list).
    moves = []
    for part in partition:
      partnodes = [indexTOnode[index] for index in part]

      ispath = self._isOnPath((partnodes[0], partnodes[0].reality))
      if(ispath):
        if(len(partnodes) == 1 and not partnodes[0].desire):
          other = partnodes[0]  #If the first edge is a loner in the partition:
          while(other.reality.desire.reality.desire):
            other = other.reality.desire.reality.desire
          moves.append(self._DCJOnNodes(partnodes[0], other.reality,
                                        self._DCJ1))

        else:    #Extract the cycle around extremities of parition:
          left = partnodes[0]
          right = partnodes[-1]
          if(right.reality.desire and right.reality.desire.reality.desire):
            moves.append(self._DCJOnNodes(left.reality,
                                          right.reality.desire.reality.desire,
                                          self._DCJ1))
          elif(left.desire.reality.desire):
            moves.append(self._DCJOnNodes(left.desire.reality.desire,
                                          right, self._DCJ2))
          else:
            raise(Exception('Problem innitializing path!'))

      for n1 in partnodes:      #Clean up the cycle.
              #Go way one:
        n2 = n1
        if(n2.reality.desire and n2.reality.desire.reality.desire and
           n2.reality.desire.reality.desire not in partnodes):
          while(n2.reality.desire and n2.reality.desire.reality.desire and
                n2.reality.desire.reality.desire not in partnodes and
                n2.reality.desire.reality.desire != n1):
            n2 = n2.reality.desire.reality.desire

          if(n2.reality.desire and n2.reality.desire.reality.desire):
            moves.append(self._DCJOnNodes(n1, n2.reality, self._DCJ1))

              #Go the other:
        n2 = n1
        if(n2.desire and n2.desire.reality.desire and
           n2.desire.reality.desire.reality not in partnodes):
          while(n2.desire and n2.desire.reality.desire and
                n2.desire.reality.desire.reality not in partnodes and
                n2.desire.reality.desire.reality != n1):
            n2 = n2.desire.reality.desire.reality

          if(n2.desire and n2.desire.reality.desire):
            moves.append(self._DCJOnNodes(n1.desire.reality.desire,
                                          n2.desire.reality.desire.reality,
                                          self._DCJ2))

        #------- Sanity check -------:
      compedges =\
        self._getEdgesInCompSingleGenomeOrdered((partnodes[0],
                                                 partnodes[0].reality))
      visited = set()
      for edge in compedges:
        if(edge[0] in partnodes):
          visited.add(edge[0])
        elif(edge[1] in partnodes):
          visited.add(edge[1])
        else:
          raise(Exception('Partition {} not properly split:\n{}'.
                          format(partnodes, compedges)))
      if(visited != set(partnodes)):
        raise(Exception('visted: {}\n !=\npartnodes: {}'.format(visited,
                                                                partnodes)))
     
        #------- Sanity check -------:
    if(not moves):
      return [(), ()]
      
    return zip(*moves)  #(physints, moves)





  def _buildColoredOrderedSet(self, comp, breakpointTOcolor):
    """
    Given a connected component, visit each edge and build the colored ordered
    set which is an instance to Maximum Colored Noncrossing Partition (MCNP).

    @warn: assumes that the edges are ordered according to how they
           appear on the component.

    @param comp:              an ordered list of nodes.
    @param breakpointTOcolor: map (chromosome name, physical interval) to integer.

    @return: colormodel, indexTOnode
             where colormodel is a list of integers and indexTOnode[i] is the
             node corresponding to the value in position i of colormodel.
    """
    colormodel = []  #The colored ordered set.
    indexTOnode = [] #Map color model index to node from edge it represents.
    for node in comp:
      indexTOnode.append(node)
      color = breakpointTOcolor[(node.chromosome, node.physint)]
      colormodel.append(color)

    return colormodel, indexTOnode


  @staticmethod
  def _findGoodNodeForEdges(start, end):
    """
    Find the node, on the edge with the end node, that will close a cycle
    when start is adjacent to it. Othewise, return None.

    @warn: this function hasn't been tested!
    """
    goodone = AdjacencyGraph._traverseTillEnd(start, end, False)
    if(not goodone):
      goodone = AdjacencyGraph._traverseTillEnd(start, end, True)

    assert(goodone == None or end == goodone or end == goodone.reality)

    return goodone


  @staticmethod
  def _traverseTillEnd(start, end, otherdirection=False):
    """
    Traverse the connected component starting on the edge with start, and
    ending on the edge with end.

    @param start: node to start with
    @param end:   node from target edge
    @param otherdirection: if False then return the closer of the two nodes,
                           on the edge with end vertex, when traversing the
                           component to the side of the start.
                           Otherwise, return the farther of the
                           two when traversing the component in the opposite
                           direction.
    """
    if(start.isTelomere()):
      #raise(Exception('Cannot give a telomere to _findGoodNodeForEdges().'))
      return None

    if(otherdirection):
      farther = start.reality
    else:
      farther = start

    #path = set()
    closer = farther.reality
    while(closer != end and farther != end and farther.notTelomere()):
      #path.add(farther)
      if(farther.desire.reality.desire):
        closer = farther.desire.reality.desire
        farther = closer.reality
        #path.add(closer)
      else:
        return None


    if(end != closer and end != closer.reality):
      return None

    if(otherdirection):
      assert(end == farther or end == farther.reality)
      return farther

    assert(end == closer or end == closer.reality)
    return closer




  _RAND = 0
  _DCJ1 = 1
  _DCJ2 = 2
  def _DCJOnNodes(self, n1, n2, deterministic=_RAND)\
    -> Tuple[T_IntervalPair, T_Move]:
    """
    Do a DCJ so that the given nodes become neighbors
    (connected by reality edge).

    @note: this does a fusion if the neighbors of n1 and n2 are both telomeres,
           in this case the neighbors are removed from the appropriate
           telomeres list.

    @note: If deterministic is _RAND, randomly assign the physical intervals to
           the edges (of 2 ways).
           There are two ways to do the same DCJ... we can reattach the
           edge of n1 to n2 or we can reattach the edge of n2 to n1.
           I{This shouldn't matter if the breakpoint regions are small.}


    @param n1:            AGNode to move next to n2
    @param n2:            AGNode to move next to n1
    @param deterministic: If _DCJ1, update physical intervals so that n1 keeps
                          hers, and consequently n2.reality keeps hers. If
                          _DCJ2, n2 and n1.reality keep their places.
                          (see note above about two ways to do the same DCJ)

    @return: (physints, move) where physints is the pair of physical intervals
             acted upon,
             and move is (((i,j),(k,l)),((i,l),(k,j))) implying the DCJ
             swapping the adjacencies i,j and k,l while disconnecting l and j
             (note the same DCJ disconnecting i and k would result in
             ((k,j),(i,l))).  In the prior the physint stays the same for
             i and k, and in the latter the physint stays the same for l and j.
             To summarize, nodes where the edge is disconnected from change
             pairs (i.e. they lose their physints).
    """
    #print("DCJ on:",(n1,n2))
    n1other = n1.reality
    n2other = n2.reality

    if(n1.chromosome and n1other.chromosome and
       n2.chromosome and n2other.chromosome):
      assert((n1.chromosome == n1other.chromosome) and
             (n2.chromosome == n2other.chromosome))
           #Record the physical intervals that are acted upon:
    if(n1.notTelomere()):
      chr1 = n1.chromosome
      int1 = n1.physint
    else:
      chr1 = n1other.chromosome
      int1 = n1other.physint
    if(n2.notTelomere()):
      chr2 = n2.chromosome
      int2 = n2.physint
    else:
      chr2 = n2other.chromosome
      int2 = n2other.physint

           #Reconnect the reality edges:
    (n1.reality, n1other.reality,  n2.reality, n2other.reality) =\
      (n2, n2other,  n1, n1other)

           #Update the physical intervals on the edges:
    n1p = self.pairFromNode(n1)              #Pairs to build the move from.
    n2p = self.pairFromNode(n2)
    n1otherp = self.pairFromNode(n1other)
    n2otherp = self.pairFromNode(n2other)

    if(deterministic == self._DCJ1 or
       (deterministic == self._RAND and random.getrandbits(1))):
      n2.physint = n1.physint           #Randomly choose interval assignment.
      n2.chromosome = n1.chromosome     # ((n1other,n1), (n2,n2other))
      n1other.physint = n2other.physint #  => ((n2,n1), (n1other,n2other))
      n1other.chromosome = n2other.chromosome
      move = (((n1otherp,n1p), (n2p,n2otherp)),
              ((n2p,n1p), (n1otherp,n2otherp)))
    else:
      n1.physint = n2.physint           # ((n1other,n1), (n2,n2other))
      n1.chromosome = n2.chromosome     #  => ((n1other,n2other), (n2,n1))
      n2other.physint = n1other.physint
      n2other.chromosome = n1other.chromosome
      move = (((n1otherp,n1p), (n2p,n2otherp)),
              ((n1otherp,n2otherp), (n2p,n1p)))

           #Remove telomeres if they are connected to each other (Fusion).
    if(n1other.isTelomere() and n2other.isTelomere()):
      self._removeTelomere(n1other)
      self._removeTelomere(n2other)
      #move = (((n1p,t1p), (n2p,t2p)), ((n1p,n2p), (n1pi,n2p)))
      move = (((n1p,n1otherp), (n2p,n2otherp)), ((n1p,n2p), (n1p,n2p)))

    return (((chr1, int1), (chr2, int2))), move


  def _DCJOnEdges(self, e1, e2):
    """
    Do a DCJ on the given edges.

    @note: this picks one of the two DCJs to do at random.

    @note: randomly assign the physical intervals to the new edges (of 2 ways).
           There are two ways to do the same DCJ... we can reattach the
           edge of n1 to n2 or we can reattach the edge of n2 to n1.

    @param e1:            edge 1
    @param e2:            edge 2

    @return: (physints, move) where physints is the pair of physical intervals
             acted upon,
             and move is (((i,j),(k,l)),((i,l),(k,j))) implying the DCJ
             swapping the adjacencies i,j and k,l while disconnecting l and j
             (note the same DCJ disconnecting i and k would result in
             ((k,j),(i,l))).  In the prior the physint stays the same for
             i and k, and in the latter the physint stays the same for l and j.
             To summarize, nodes where the edge is disconnected from change
             pairs (i.e. they lose their physints).
    """
    return self._DCJOnNodes(random.choice(e1), random.choice(e2))


  #def _doFusion(self, n1,n2, telomeres):
  #  """
  #  Do a fusion so that the given nodes are adjacent.

  #  @param n1: node 1
  #  @param n2: node 2
  #  @param telomeres: the telomeres containing neighbors of n1 and n2

  #  @return: (physints, move) where physints is a pair ((i,j), (i,j)) and
  #           move is (((i,j), (i,j)), ((i,teli), (j,telj))) where telx is
  #           a newly created telomere.
  #  """
  #  if(n1.reality.notTelomere() or n2.reality.notTelomere()):
  #    raise(Exception('cannot do fission on non-telomeric adjacencies'))

  #  assert(n1.physint == n2.physint)

  #  n2.physint = n1.physint
  #  n2.chromosome = n1.physint
  #  telomeres.remove(n1.reality)
  #  telomeres.remove(n2.reality)
  #  n1.reality, n2.reality = n2, n1

  #  n1p = self.pairFromNode(n1)
  #  n2p = self.pairFromNode(n2)
  #  t1p = self.pairFromNode(n1.reality)
  #  t2p = self.pairFromNode(n2.reality)
  #  move = (((n1p,t1p), (n2p,t2p)), ((n1p,n2p), (n1pi,n2p)))

  #  locpair = (n1.chromosome, n1.physint)
  #  return (locpair, locpair), move



  def scoreScenarioReverse(self, scenfiles, hms:CoolerHeatmap, newscenloc,
                           side=1, showprogress=True):
    """
    Apply the given scenario to the given side of the graph.

    @param scenfiles:  the scenarios to reverse
    @param hms:        the heatmaps
    @param newscenloc: location to create scenarios
    @param side:  the side of the graph to apply the moves to usually side
                  2 is the one that is sorted.
    @param showprogress: show a progressbar
    """
    retintervals = []
    retlocations = []
    retheats = []
    retmoves = []
    retspecies = []
    dist = self.getDist()
    seqre = re.compile(r'.+\.(\d+)\.(?:pcl|pgz|pbz2)')

    if showprogress:
      widgets = ['reversing scenarios: ', pb.Percentage(), ' ',
                 pb.Bar(marker='-'), ' ', pb.ETA()]
      pbar = pb.ProgressBar(widgets=widgets, maxval=len(scenfiles)).start()

    for i, scenfile in enumerate(sorted(scenfiles)):
      m = seqre.match(scenfile)
      if m:
        j = m.group(1)
      else:
        j = i
        print('Cannot extract seq number from filename "{}".'.format(scenfile))

      try:
        intlist, loclist, heatlist, movelist, speclist = readScenario(scenfile)
      except Exception as e:
        print(str(e))
        continue

      if not movelist:
        print('WARNING: no moves in "{}".'.format(scenfile))
        continue

      if side == 1:
        telomeres = self.telomeres1
        nodes = self.nodes1
        speclist = [1]*len(intlist)
      else:
        telomeres = self.telomeres2
        nodes = self.nodes2
        speclist = [1]*len(intlist)

      intervals, moves = self.undoScenario(movelist, telomeres, nodes)
      self.undoScenario(moves, telomeres, nodes)  #Undo moves we just did.
      assert(self.getDist() == dist)              #TODO: remove this.

      (_,nheatlist,nloclist) = scorePhysIntList(hms, intervals,
                                                f'{newscenloc}/reversed.{j}',
                                                moves, speclist)
      retintervals.append(intervals)
      retlocations.append(nloclist)
      retheats.append(nheatlist)
      retmoves.append(moves)
      retspecies.append(speclist)

      if showprogress:
        pbar.update(i)


    if showprogress:
      pbar.finish()

    return retintervals, retlocations, retheats, retmoves, retspecies



  def undoScenario(self, moves: List[T_Move], telomeres,
                   nodes) -> Tuple[List[T_IntervalPair], List[T_Move]]:
    """
    Apply the given scenario to the given side of the graph, in reverse
    order.

    @param moves: A move (((i,j),(k,l)),((i,l),(k,j))) implies the DCJ
                  swapping the adjacencies i,j and k,l while disconnecting l
                  and j (note the same DCJ disconnecting i and k would result
                  in ((k,j),(i,l))).
    @param telomeres: the telomeres to act on
    @param nodes:     the nodes to act on (must be the same side as telomeres)
    """
    intervals: List[T_IntervalPair] = []
    outmoves: List[T_Move] = []
    for (((i,j),(k,l)),((m,n),(o,p))) in reversed(moves):
      #print((((i,j),(k,l)),((m,n),(o,p))))
      #print('d:',self.getDist())
      if i == o and j == n:                # ((n1other,n1), (n2,n2other))
        #print('A')
        #print(n)
        #print(o)
        n1 = None                          #  => ((n2,n1), (n1other,n2other))
        if n[1] != AGNode._TEL:               #Get n1 from n:
          n1 = self.nodeFromPair(n, nodes)
        elif m[1] != AGNode._TEL:
          n1 = self.nodeFromPair(m, nodes).reality

        n1other = None
        if o[1] != AGNode._TEL:               #Get n1other from o:
          n1other = self.nodeFromPair(o, nodes)
        elif p[1] != AGNode._TEL:
          n1other = self.nodeFromPair(p, nodes).reality

        if n1 and n1other:         #If we have them both:
          interval, move = self._DCJOnNodes(n1,n1other, self._DCJ1)
        elif n1:
          interval, move = self._doFission(n1, telomeres)
        elif n1other:
          interval, move = self._doFission(n1other, telomeres)
        else:
          raise(Exception("Overwhelmed by telomeres (shoudn't happen)!"))

      elif k == o and l == n:                 # ((n1other,n1), (n2,n2other))
        #print('B')
        n2 = None                             #  => ((n1other,n2other), (n2,n1))
        if o[1] != AGNode._TEL:               #Get n2 from o:
          n2 = self.nodeFromPair(o, nodes)
        elif p[1] != AGNode._TEL:
          n2 = self.nodeFromPair(p, nodes).reality

        n2other = None
        if n[1] != AGNode._TEL:               #Get n2other from n:
          n2other = self.nodeFromPair(n, nodes)
        elif m[1] != AGNode._TEL:
          n2other = self.nodeFromPair(m, nodes).reality

        if n2 and n2other:         #If we have them both:
          #print(n2other, n2)
          interval, move = self._DCJOnNodes(n2other,n2, self._DCJ2)
        elif n2:
          interval, move = self._doFission(n2, telomeres)
        elif n2other:
          interval, move = self._doFission(n2other, telomeres)
        else:
          raise(Exception("Overwhelmed by telomeres (shoudn't happen)!"))

      elif i == k and j == l:            # ((n1,n2), (n1,n2))    (fission)
        #print('C')
        n1 = self.nodeFromPair(m, nodes) #  => ((n1,tel), (n2,tel))
        n2 = self.nodeFromPair(o, nodes)
        interval, move = self._DCJOnNodes(n1,n2)  #Do a fusion.
        #interval, move = self._doFusion(n1,n2, telomeres)

      elif i == m and k == n:            # ((n1,tel), (n2,tel))    (fusion)
        #print('D')
        n1 = self.nodeFromPair(m, nodes) #  => ((n1,n2), (n1,n2))
        interval, move = self._doFission(n1, telomeres)

      else:
        raise(Exception("unclassifyable move"+\
                        str((((i,j),(k,l)),((m,n),(o,p))))))
        #interval, move = None, None

      intervals.append(interval)
      outmoves.append(move)
      #print('rev:',(((m,n),(o,p)), ((i,j),(k,l))))
      #print('    ',move)
      #if(move != (((m,n),(o,p)), ((i,j),(k,l)))):
      #  print('PROBLEM!')
      #print('___')
      #assert(dist+1 == self.getDist())   #TODO: comment this out!
      #dist += 1

    return intervals, outmoves



  def applyScenario(self, moves, telomeres, nodes):
    """
    Apply the given scenario to the given side of the graph. The order
    is unchanged.

    @param moves: (physints, move) where physints is the pair of physical
                  intervals acted upon,
                  and move is (((i,j),(k,l)),((i,l),(k,j))) implying the DCJ
                  swapping the adjacencies i,j and k,l while disconnecting l
                  and j (note the same DCJ disconnecting i and k would result
                  in ((k,j),(i,l))).
    @param telomeres: the telomeres of the genome we will undo from
    @param nodes:     the nodes to undo from (must be the same side as telomeres)
    """
    moves = [(m[1],m[0]) for m in moves]
    moves.reverse()
    return self.undoScenario(moves, telomeres, nodes)


  def getScenarioForOneGenome(self, scenfile, genome):
    """
    Read the scenario in scenfile then return the SAME scenario (not reversed)
    with the corresponding intervals in the given genome.
    So, if genome is the genome of all interval in the scenario, the returned
    scenario is equal to the given one.
    Useful when the given scenario is the result of a 2-way greedy computation
    and so contains moves from two sides (and two genomes) of the AG.
    """

    pass


  def pairFromNode(self, node: AGNode) -> Tuple[T_NodeID, T_GeneEnd]:
    """
    Create the pair associated with this node.
    """
    return (node.nid, node.end)

  def nodeFromPair(self, pair, nodes):
    """
    Find the node associated with this pair.
    """
    return nodes[pair[0]][pair[1]]

  def _classifyComps(self, telomeres, nodes, compsaslist=False)\
    -> Tuple[List[T_Comp], List[T_Comp], Tuple[List[T_Comp], List[T_Comp]]]:
    """
    Classify the components of the graph.  Partition the reality edges in the
    given list according to its components, in the following format::

        (cycles, oddpaths, (Wpath, Mpaths)),

    where each is a list of lists of nodes, one node from each reality edge
    in the genome corresponding to the given nodes and telomeres.
    All lists together form a partition of the reality edges of the given
    genome, where one node per edge is in a set.
    W and M paths are even length paths that have endpoints in these telomeres,
    or not these telomeres, respectively.

    @warn: telomeres and nodes must come from the same side of the graph.
    @note: each connected component is a (ordered) list (i.e. the edges come
           in the order they appear on a path or cycle).

    @param telomeres:   the telomeres from the side of interest.
    @param nodes:       the nodes from the side of interest.
    @param compsaslist: return connected components as lists rather than sets,
                        in which case they are ordered according to the
                        component.

    @return: lists of node lists (see above) such that one node comes from each
             reality edge in the given side of the graph
    """
              #Classify the paths:
    oddpaths = []
    Wpaths = []
    self._getPathsFromTelomeres(telomeres, oddpaths, Wpaths, False, compsaslist)
    Mpaths = []
    self._getPathsFromTelomeres(self._getOtherTelomeres(telomeres),
                                oddpaths, Mpaths, True, compsaslist)

              #Classify the cycles:
    cycles = self._getCyclesFromNodes(nodes, compsaslist)

    self._resetVisited()

    return(cycles, oddpaths, (Wpaths,Mpaths))


  def _getNumDCJsEPaths(self, wpaths, mpaths):
    """
    Get the number of DCJ moves that can be done on the given even length paths.
    """
    wpath = 0
    mpath = 0
    fission = 0
    if(len(wpaths)):
      wpath = self._getNumDCJsCycleExtracts(wpaths)
      #fission += sum(map(len, wpaths))-(2*len(wpaths))

    if(len(mpaths)):
      mpath = self._getNumDCJsCycleExtracts(mpaths)
      fission +=  sum(map(len, mpaths))

    #print(list(product(map(len, wpaths), map(len, mpaths))))

    combo = 2*sum(map(lambda x: x[0]*x[1],
                      product(map(len, wpaths), map(len, mpaths))))

    #print('wpaths:',wpath)
    #print('mpaths:',mpath)
    #print('com:',combo)
    return (mpath, wpath, combo, fission)


  def _getNumDCJsCycleExtracts(self, components):
    """
    Get the number of DCJ moves that can be done on the given cycles or
    odd length paths.
    """
    if(len(components)):
      return sum(map(nChoose2, map(len, components)))
    return 0

  def _getCutsCycle(self, cycles):
    """
    Generate the list of possible sorting cuts on this cycle.
    """
    for c in cycles:
      for a,b in combinations(c,2):
        yield (a,b)



#} DCJ functions

#       .       .       .       .       .       .       .       .       .
#{ DCJ functions - randomization

  def getRandomScenario(self, undoscenario=True, pseudouniform=False):
    """
    Get random sorting scenario on the given graph.
    Sorts genome 2 to genome 1.

    @param undoscenario: leave the graph in the same state by backing out
                         all of the operations done to find the scenario.
    @param pseudouniform: create scenarios by sampling each individual component
                          uniformly at random (should be either False, or one of
                          _PSEUDO_RAND_MIX or _PSEUDO_RAND_SPLIT)

    @return: a list of physical interval pairs that were acted upon.
    @rtype:  list of pairs ((chrm1, (start, end)), (chrm1, (start, end)))
    """
    dist = self.getDist()
    intervals = []
    moves = []
    for _ in range(dist):
      if pseudouniform:
        interval, move = self._nextPseudoUniform(self.telomeres2, self.nodes2,
                                                 pseudouniform)
      else:
        interval, move = self._randomSortingMove(self.telomeres2, self.nodes2)
      intervals.append(interval)
      moves.append(move)
    specieslist = [2]*dist    # We only made moves in species 2

    if self.getDist():
      sys.exit('ERROR: not sorted! Dist={}'.format(self.getDist()))

    if undoscenario:
      _, bms = self.undoScenario(moves, self.telomeres2, self.nodes2)
      self._invalidateComponents()  #next _randomSortingMove will re-classify

      for m1,(p1,p2) in zip(reversed(moves), bms):
        assert m1 == (p2,p1)  #TODO: remove this sanity check

    if self.getDist() != dist:
      sys.exit('ERROR: not unsorted! Dist={}'.format(self.getDist()))

    return intervals, moves, specieslist



  def sampleRandomScenario(self, heatmap: Optional[CoolerHeatmap],
                           logto='', sid='',
                           pseudouniform=False)\
    -> Tuple[float, float, float]:
    """
    Return the average heat (and std dev) over a set of random scenarios from
    Genome 2 to Genome 1.

    @param heatmap: the heatmap. if this is None, then only log the scenarios
    @param reps:    the number of random scenarios to average over
    @param logto:   log the scenarios to this directory if it isn't empty
    @type logto:    string
    @param sid:     a unique identifier for these scenarios
    @type sid:      string
    @param pseudouniform:  create scenarios by sampling each individual component
                           uniformly at random 

    @return: pair (average, stddev) for all, intra, and inter
    """
    heatave = (0,0,0)

    physintlist, movelist, specieslist =\
      self.getRandomScenario(pseudouniform=pseudouniform)

    _, logtoend = os.path.split(logto)
    logtofile = logtoend+'.'+sid
    if not heatmap:  #Only log the scenarios:
      empty = [None]*len(physintlist)
      writeScenario(logto+'/'+logtofile, physintlist, empty, empty,
                    movelist, specieslist)

    else:             #Evaluate the scenarios with logging:
      heatave, _, _ = scorePhysIntList(heatmap, physintlist,
                                       f'{logto}/{logtofile}',
                                       movelist=movelist,
                                       speclist=specieslist)

    return heatave


  def sampleMultipleRandomScenarios(self, heatmap: Optional[CoolerHeatmap],
                                    reps=10, logto='', sid='',
                                    pseudouniform=False)\
    -> Tuple[Tuple[float, float], Tuple[float, float], Tuple[float, float]]:
    """
    Return the average heat (and std dev) over a set of random scenarios from
    Genome 2 to Genome 1.

    @param heatmap: the heatmap. if this is None, then only log the scenarios
    @param reps:    the number of random scenarios to average over
    @param logto:   log the scenarios to this directory if it isn't empty
    @type logto:    string
    @param sid:     a unique identifier for these scenarios
    @type sid:      string
    @param pseudouniform:  create scenarios by sampling each individual component
                           uniformly at random 

    @return: pair (average, stddev) for all, intra, and inter
    """
    allheat = []
    intraheat = []
    interheat = []
    heatave = (0,0,0)
    for i in range(reps):
      #g = copy.deepcopy(self)
      #print(g == self)

      heatave = self.sampleRandomScenario(heatmap, logto, f'{i}.{sid}',
                                          pseudouniform)

      if heatave[0]:
        allheat.append(heatave[0])
      if heatave[1]:
        intraheat.append(heatave[1])
      if heatave[2]:
        interheat.append(heatave[2])

    allpair = (None, None)
    intrapair = (None, None)
    interpair = (None, None)
    if len(allheat):
      allpair = (np.average(allheat), np.std(allheat))
    if len(intraheat):
      intrapair = (np.average(intraheat), np.std(intraheat))
    if len(interheat):
      interpair = (np.average(interheat), np.std(interheat))

    return allpair, intrapair, interpair


  def getRandomScenarioWithScore(self, heatmap: CoolerHeatmap):
    """
    Compute a random scenario and also score it.

    @param heatmap: the heatmap

    @return: physintlist, poslist, heatlist, movelist, specieslist
    """
    physintlist, movelist, specieslist = self.getRandomScenario()
    _, heatl, posl = scorePhysIntList(heatmap, physintlist)

    return physintlist, posl, heatl, movelist, specieslist
    


  _CYCEX = T_MoveType(0)
  _OPEX = T_MoveType(1)
  _MPM = T_MoveType(2)
  _WPM = T_MoveType(3)
  _COMBO = T_MoveType(4)
  _FISSION = T_MoveType(5)
  def _randomSortingMove(self, telomeres, nodes):
    """
    Do a random sorting move on the given genome.

    @return: (physints, move) where physints is the pair of physical intervals
             acted upon,
             and move is (((i,j),(k,l)),((i,l),(k,j))) implying the DCJ
             swapping the adjacencies i,j and k,l while disconnecting l and j
             (note the same DCJ disconnecting i and k would result in
             ((k,j),(i,l))).
    """
    cycles, opaths, wpaths, mpaths = self._getComponentsFor(telomeres, nodes)


    #print 'cycles:',self.cycles
    #print 'opaths:',self.opaths
    #print 'wpaths:',self.wpaths
    #print 'mpaths:',self.mpaths
    #self.printGraphCL("tmp1.pdf")
    #self.printGraphCL('order'+str(self.getDist())+'.pdf')

    mtype = self._chooseMoveTypeUniform(cycles, opaths, wpaths, mpaths)
    if(mtype == self._CYCEX):
      #print 'extract cycle from cycle'
      intervals, move = self._extractRandomCycle(cycles, cycles)
    elif(mtype == self._OPEX):
      #print 'extract cycle from opath'
      intervals, move = self._extractRandomCycle(opaths, cycles)
    elif(mtype == self._WPM):
      #print 'extract cycle from wpath'
      intervals, move = self._extractRandomCycle(wpaths, cycles)
    elif(mtype == self._MPM):
      #print 'extract cycle from mpath'
      intervals, move = self._extractRandomCycle(mpaths, cycles)
    elif(mtype == self._COMBO):
      #print 'mix paths'
      intervals, move = self._mixPathsRand(wpaths, mpaths, opaths)
    elif(mtype == self._FISSION):
      #print 'do fission'
      intervals, move = self._doRandFission(mpaths, telomeres, opaths)
      #print 'fission done at:',intervals
    else:
      sys.exit(f'ERROR: unknown move code: {mtype}.')
    #self.printGraphCL("tmp2.pdf")
    #raw_input()
    #print self.getDist()

    return intervals, move


  def _getComponentsFor(self, telomeres, nodes)\
    -> Tuple[List[T_Comp], List[T_Comp], List[T_Comp], List[T_Comp]]:
    """
    Get the components for the given set of telomeres and nodes.
    """
    if(self.nodes1 == nodes and self.telomeres1 == telomeres):
      genome = 1
      cycles = self.cycles1
    elif(self.nodes2 == nodes and self.telomeres2 == telomeres):
      genome = 2
      cycles = self.cycles2
    else:
      raise(Exception('telomeres and nodes must be from the same genome.'))
      
    if(cycles == None):
      (cycles, opaths, (wpaths, mpaths)) = self._classifyComps(telomeres, nodes)
      self.cycles2, self.opaths2, self.wpaths2, self.mpaths2 =\
        (cycles, opaths, wpaths, mpaths)
      if(genome == 1):
        self.cycles1, self.opaths1, self.wpaths1, self.mpaths1 =\
          (cycles, opaths, wpaths, mpaths)
    else:
      cycles, opaths, wpaths, mpaths =\
        (self.cycles2, self.opaths2, self.wpaths2, self.mpaths2)
      if(genome == 1):
        cycles, opaths, wpaths, mpaths =\
          (self.cycles1, self.opaths1, self.wpaths1, self.mpaths1)

    return cycles, opaths, wpaths, mpaths


  def _nextPseudoUniform(self, telomeres, nodes, treatmpaths):
    """
    Do a sorting move on the given genome that is psuedo-uniform, in the
    sense that it is uniform for scenarios limited to the particular cycle it
    is a part of, but not uniform over the entire scenario on all cycles.

    @param treatmpaths: Either split all mpaths first, or fission them all first
    @type treatmpaths:  False, or one of _PSEUDO_RAND_MIX or _PSEUDO_RAND_SPLIT

    @return: (physints, move) where physints is the pair of physical intervals
             acted upon,
             and move is (((i,j),(k,l)),((i,l),(k,j))) implying the DCJ
             swapping the adjacencies i,j and k,l while disconnecting l and j
             (note the same DCJ disconnecting i and k would result in
             ((k,j),(i,l))).
    """
    cycles, opaths, wpaths, mpaths = self._getComponentsFor(telomeres, nodes)

    if(mpaths):      #Always deal with an mpath if it exists.
      if(treatmpaths == AdjacencyGraph._PSEUDO_RAND_SPLIT or not wpaths):
        return self._doRandFission(mpaths, telomeres, opaths)
      elif(treatmpaths == AdjacencyGraph._PSEUDO_RAND_MIX):
        return self._mixPathsRand(wpaths, mpaths, opaths)
      else:
        raise(Exception('Unknown Mpath specifier: {}'.format(treatmpaths)))

      #There are only cycle extractions left to do.
    comp, mtype = self._chooseCompPseudoUniform(cycles, opaths, wpaths, mpaths)
    n1, n2 = self._chooseMoveOnCycleUniform(comp)

    before = self.getDist()
    if(mtype == self._CYCEX):
      #print 'extract cycle from cycle'
      intervals, move = self._extractCycle(n1, n2, comp, cycles, cycles)
    elif(mtype == self._OPEX):
      #print 'extract cycle from opath'
      intervals, move = self._extractCycle(n1, n2, comp, opaths, cycles)
    elif(mtype == self._WPM):
      #print 'extract cycle from wpath'
      intervals, move = self._extractCycle(n1, n2, comp, wpaths, cycles)
    elif(mtype == self._MPM):
      #print 'extract cycle from mpath'
      raise(Exception('All Mpaths should have been transformed through fission.'))
    else:
      sys.exit('ERROR: unsupported move code: {mtype}.')
    assert(before == self.getDist()+1)

    return intervals, move



  def _invalidateComponents(self):
    """
    Invalidate the components so that the next call to _classifyComps will
    do it's work.
    """
    self.cycles1 = None
    self.cycles2 = None




  def _chooseNodesForExtract(self, components: List[T_Comp]):
    """
    Choose a component such that a cycle extraction on that component will
    be uniformly chosen over all extractions on all components.
    Then choose two endpoints from that component.
    Return the endpoints (nodes).

    @return: ((n,m), i) where n and m are nodes on component i
    """
    if(not components):
      raise(Exception('no component to extract a cycle from in '+
                      str(components)+'!'))

    complens = [len(c) if len(c) > 1 else 0 for c in components]
    total = float(sum(complens))
    compindex = self._chooseIndexByProb([l/total for l in complens])

    return (random.sample(sorted(components[compindex]), 2), compindex)


  def _chooseNodesForExtractSLOW(self, components):
    """
    Choose a component such that a cycle extraction on that component will
    be uniformly chosen over all extractions on all components.
    Then choose two endpoints from that component.
    Return the endpoints (nodes).

      - I{sample from the multinomial using a list (ugly but simple).}

    @return: ((n,m), i) where n and m are nodes on component i
    """
    l = []
    for i,c in enumerate(components):
      numoves = nChoose2(len(c))
      l += [i] * numoves

    if(not l):
      sys.exit('ERROR: no component to extract a cycle from in '+\
               str(components)+'!')

    compindex = random.choice(l)
    return (random.sample(components[compindex], 2), compindex)


  def _chooseMoveOnCycleUniform(self, comp):
    """
    Choose a pair of nodes to do a DCJ on with probability proportional to
    the number of scenarios on the resulting components.
    """
    if(len(comp) == 1): raise(Exception('component already sorted.'))

    movelenTOscens = self._getNumScensByMoveLength(len(comp))

    total = sum(movelenTOscens)
    length = self._chooseIndexByProb([count/total for count in movelenTOscens])
    return self._getNodesAtDist(comp, length)


  def _getNodesAtDist(self, comp, distance):
    """
    Return two nodes on the given component at the given distance from each
    other (distance wraps around).
    """
    n1 = random.choice(list(comp))

    n2 = n1
    count = 0
    while(count != distance and n2.reality.notTelomere()):     #Go one way:
      if(n2.reality.desire.reality.isTelomere()):
        break               #Reached telomere too soon in Odd path or M path.
      n2 = n2.reality.desire.reality.desire
      count += 1

    if(count != distance):  #Search for compsize - distance in other direction.
      n2 = n1 
      count = 0
      while(count != len(comp)-distance and n1.notTelomere()): #Go other way:
        if(n1.desire.reality.isTelomere()):
          break             #Reached telomere too soon in Odd path or M path.
        n1 = n1.desire.reality.desire.reality
        count += 1

    if(count != distance):  #Can't go far enough in either direction!
      Exception('distance ({}) more than half size ({}) of component.'.
                format(distance, len(comp)))

    return n1, n2

  def _chooseIndexByProb(self, p):
    """
    Return index i from the given list with probability p[i].
    """
    return list(np.random.multinomial(1, p)).index(1)



  def _chooseCompPseudoUniform(self, cycles, opaths, wpaths, mpaths):
    """
    Choose a component to act on uniformly with respect to scenarios, while
    ignoring orderings of moves between component. In other words, each
    component is considered independently from the others.

    If c_i is the ith component, and the number of scenarios restricted to the
    graph on this component is S(c_i), then a move that creates two new
    components c_i1 and c_i2, will be chosen with probability::
      (s(c_i1)+s(c_i2)) / (sum_i S(c_i)).

    @return: (component, mtype) return the component that will have
             the random extraction as well as the type of extraction.
    """
    allcomps = list(chain(cycles, opaths, wpaths, mpaths))

      #Decide which component to act on (proportional to number of scenarios):
    compiTOnonzerocount = []
    nonzerocompiTOcompi = []
    for i, c in enumerate(allcomps):
      if(len(c) != 1):
        compiTOnonzerocount.append(self._getNumScenariosByCompSize(len(c)))
        nonzerocompiTOcompi.append(i)

    total = Decimal(sum(compiTOnonzerocount))
    i = self._chooseIndexByProb([count/total for count in compiTOnonzerocount])

    mtype = self._CYCEX
    if(nonzerocompiTOcompi[i] >= len(cycles) and
       nonzerocompiTOcompi[i] < len(cycles)+len(opaths)):
      mtype = self._OPEX
    elif(nonzerocompiTOcompi[i] >= len(cycles)+len(opaths) and
         nonzerocompiTOcompi[i] < len(cycles)+len(opaths)+len(wpaths)):
      mtype = self._WPM
    elif(nonzerocompiTOcompi[i] >= len(cycles)+len(opaths)+len(wpaths)):
      mtype = self._MPM

    return allcomps[nonzerocompiTOcompi[i]], mtype



  def _getNumScensByMoveLength(self, compsize):
    """
    Return a dictionary mapping the length of a move (minimum distance on the
    component between the nodes operated on) to the number of scenarios after
    applying that move.
    There are k different DCJs of each length on a component with k
    edges in genome 2. Compute the number of scenarios after applying a DCJ
    of each length in [1, k/2].
    """
    lengthTOcount = [0]
    if compsize == 1:
      return lengthTOcount

      #k is the size of the resulting component
    for k in range(1, int(ceil(float(compsize)/2))):
      #print "new size {}:\n\t{}\n\t{}".format(k, self._getNumScenariosByCompSize(k), self._getNumScenariosByCompSize(compsize-k))
      km1 = k-1
      lm1 = compsize-k-1
      numk = self._getNumScenariosByCompSize(k) 
      numl = self._getNumScenariosByCompSize(compsize-k)
      numscens = numk * numl * scipy.special.comb(km1+lm1, km1, exact=True)
      
      #print 'new distances', km1, lm1
      #print '{}*{} * {}'.format(numk,numl, scipy.special.comb(km1+lm1, km1, exact=True))
      #print 'numscens', numscens
      lengthTOcount.append(numscens*compsize)


    if compsize%2 == 0:      #Handle the even case.
      k = compsize/2
      numk = self._getNumScenariosByCompSize(k) 
      numscens = Decimal(numk * numk * scipy.special.comb(2*(k-1), k-1, exact=True))
      lengthTOcount.append(numscens*compsize/2)
      #print 'even split', lengthTOcount[-1]

    total = int(sum(lengthTOcount))

    realtotal = self._getNumScenariosByCompSize(compsize)
    #assert total == realtotal, f'{total} != {realtotal}'

    return lengthTOcount


  def _getNumScenariosOnComponent(self, comp):
    """
    Return the number of scenarios on the given component.
    """
    return self._getNumScenariosByCompSize(len(comp))


  def _getNumScenariosByCompSize(self, size):
    """
    Return the number of scenarios on a component of this size.
    """
    return int((size)**(size-2))


  def _chooseMoveTypeUniform(self, cycles, opaths, wpaths, mpaths):
    """
    Choose a next move uniformly out of all possible next moves.
    """
    cycext = self._getNumDCJsCycleExtracts(cycles)
    opext = self._getNumDCJsCycleExtracts(opaths)
    emoves = self._getNumDCJsEPaths(wpaths, mpaths)

    #l = [self._CYCEX]*cycext+[self._OPEX]*opext+[self._MPM]*emoves[0]+\
    #    [self._WPM]*emoves[1]+[self._COMBO]*emoves[2]+[self._FISSION]*emoves[3]
    #return random.choice(l)

    t = float(cycext+opext+emoves[0]+emoves[1]+emoves[2]+emoves[3])
    #print 'TOTAL number of moves:', t
    probabilities = [cycext/t, opext/t, emoves[0]/t, emoves[1]/t,
                     emoves[2]/t, emoves[3]/t]
    i = self._chooseIndexByProb(probabilities)
    if(i == 0):
      return self._CYCEX
    elif(i == 1):
      return self._OPEX
    elif(i == 2):
      return self._MPM
    elif(i == 3):
      return self._WPM
    elif(i == 4):
      return self._COMBO
    elif(i == 5):
      return self._FISSION
    else:
      sys.exit("ERROR: BIG problem!")


  def _extractRandomCycle(self, comps: List[T_Comp], cycles):
    """
    Extract a cycle from the given list of components.

    @warning: adds the new cycle into the given cycle list

    @param comps:  the components to extract a cycle from
    @param cycles: the list of cycles to add the new cycle to
    @return: (physints, move) where physints is the pair of physical intervals
             acted upon,
             and move is (((i,j),(k,l)),((i,l),(k,j))) implying the DCJ
             swapping the adjacencies i,j and k,l while disconnecting l and j
             (note the same DCJ disconnecting i and k would result in
             ((k,j),(i,l))).
    """
                         #Choose a pair of nodes from one of the components:
    ((n1,n2), compi) = self._chooseNodesForExtract(comps)
    return self._extractCycle(n1, n2, comps[compi], comps, cycles)

  def _extractCycle(self, n1, n2, comp, comps, cycles):
    """
    Extract the cycle from the given component, using the given nodes.

    @note: adds the new cycle into the given list of cycles
    @note: removes the old component from comps

    @param comps:  the components to extract a cycle from
    @param cycles: the list of cycles to add the new cycle to
    @return: (physints, move) where physints is the pair of physical intervals
             acted upon,
             and move is (((i,j),(k,l)),((i,l),(k,j))) implying the DCJ
             swapping the adjacencies i,j and k,l while disconnecting l and j
             (note the same DCJ disconnecting i and k would result in
             ((k,j),(i,l))).
    """
                         #Find closer of n2 or n2.reality to n1 on the cycle,
    node = self._traverseTillEnd(n1, n2)
    if(node):            #and make them adjacent (attached by reality edge).
      assert(node == n2 or node == n2.reality)
      o = node.reality
      physints, move = self._DCJOnNodes(n1, node)
    else:                #If we have a path and went the wrong way:
      node = self._traverseTillEnd(n1.reality, n2)
      if(node):
        assert(node == n2 or node == n2.reality)
        o = node.reality
        physints, move = self._DCJOnNodes(n1.reality, node)
      else:
        raise(Exception("couldn't extract a cycle!"))

                         #Update the components:
    comps.remove(comp)                      #Remove the old component.

    if(o.end != AGNode._TEL or o.reality.end != AGNode._TEL):
      s1 = set((x for (x,y) in self._getEdgesInCompSingleGenome((o,o.reality))))
      comps.append(s1)
    s2 = set((x for (x,y) in self._getEdgesInCompSingleGenome((node, node.reality))))
    cycles.append(s2)

    return physints, move




  def _doRandFission(self, mpaths, telomeres, opaths):
    """
    Choose an Mpath and do a fission on it.
    The split path is removed from -mpaths-.

    @note: adds the new paths to opaths.
    @note: adds new telomeres to telomeres.

    @return: (physints, move) where physints is a pair ((i,j), (i,j)) and
             move is (((i,j), (i,j)), ((i,teli), (j,telj))) where telx is
             a newly created telomere.
    """
    n1 = self._chooseSingleNode(mpaths)
    return self._doFissionUpdate(n1, mpaths, telomeres, opaths)



  def _doFissionUpdate(self, n1, mpaths: List[T_Comp], telomeres,
                       opaths):
    """
    Choose an Mpath and do a fission on it.
    The split path is removed from -mpaths-.

    @note: adds the new paths to opaths.
    @note: adds new telomeres to telomeres.

    @return: (physints, move) where physints is a pair ((i,j), (i,j)) and
             move is (((i,j), (i,j)), ((i,teli), (j,telj))) where telx is
             a newly created telomere.
    """
    n2 = n1.reality

    #mi = filter(lambda (i,S): n1 in S, enumerate(mpaths))[0][0]
    mi = next(i for i, S in enumerate(mpaths) if n1 in S)
    mpaths.pop(mi)                  #Remove the component from mpaths.
                                    #Create a telomere,
    new = AGNode(chromosome=n1.chromosome, physint=n1.physint, genome=n1.genome)
    n1.reality = new                #link it to the graph,
    new.reality = n1
    telomeres.append(new)           #and add it to the list of telomeres.

    s1 = set((x for (x,y) in self._getEdgesInCompSingleGenome((n1,n1.reality))))
    opaths.append(s1)

                                    #Create a telomere,
    new = AGNode(chromosome=n2.chromosome, physint=n2.physint, genome=n2.genome)
    n2.reality = new                #link it to the graph,
    new.reality = n2
    telomeres.append(new)           #and add it to the list of telomeres.

    s2 = set((x for (x,y) in self._getEdgesInCompSingleGenome((n2,n2.reality))))
    opaths.append(s2)

    n1p = self.pairFromNode(n1)
    new1p = self.pairFromNode(n1.reality)
    n2p = self.pairFromNode(n2)
    new2p = self.pairFromNode(n2.reality)
    move = (((n1p,n2p), (n1p,n2p)), ((n1p,new1p), (n2p,new2p)))

    return ((n1.chromosome, n1.physint), (n1.chromosome, n1.physint)), move



  def _doFission(self, n1, telomeres) -> Tuple[T_IntervalPair, T_Move]:
    """
    Do a fission on a given reality edge.

    @param n1: a node connected to the reality edge to fission.
    @param telomeres: the telomeres from the same side of the graph as n1.

    @return: (physints, move) where physints is a pair ((i,j), (i,j)) and
             move is (((i,j), (i,j)), ((i,teli), (j,telj))) where telx is
             a newly created telomere.
    """
    n2 = n1.reality
                                    #Create a telomere,
    new = AGNode(chromosome=n1.chromosome, physint=n1.physint, genome=n1.genome)
    n1.reality = new                #link it to the graph,
    new.reality = n1
    telomeres.append(new)           #and add it to the list of telomeres.

                                    #Create a telomere,
    new = AGNode(chromosome=n2.chromosome, physint=n2.physint, genome=n2.genome)
    n2.reality = new                #link it to the graph,
    new.reality = n2
    telomeres.append(new)           #and add it to the list of telomeres.

    n1p = self.pairFromNode(n1)
    new1p = self.pairFromNode(n1.reality)
    n2p = self.pairFromNode(n2)
    new2p = self.pairFromNode(n2.reality)
    move = (((n1p,n2p), (n1p,n2p)), ((n1p,new1p), (n2p,new2p)))

    return ((n1.chromosome, n1.physint), (n1.chromosome, n1.physint)), move





  def _mixPathsRand(self, wpaths, mpaths, opaths):
    """
    Choose one node from an Mpath and one from a Wpath, and mix the paths.

    @note: the new odd length paths are added to opaths

    @return: (physints, move) where physints is the pair of physical intervals
             acted upon,
             and move is (((i,j),(k,l)),((i,l),(k,j))) implying the DCJ
             swapping the adjacencies i,j and k,l while disconnecting l and j
             (note the same DCJ disconnecting i and k would result in
             ((k,j),(i,l))).
    """
    n1 = self._chooseSingleNode(mpaths)
    n2 = self._chooseSingleNode(wpaths)

        #Get the components that n1 and n2 are in:
    #wpath = filter(lambda S: n2 in S, wpaths)[0]
    wpath = next(S for S in wpaths if n2 in S)
    #mpath = filter(lambda S: n1 in S, mpaths)[0]
    mpath = next(S for S in mpaths if n1 in S)

        #Do the DCJ:
    return self._mixPaths(n1, n2, wpaths, wpath, mpaths, mpath, opaths)


  def _mixPaths(self, n1, n2, wpaths, wpath, mpaths, mpath, opaths):
    """
    Mix the wpath and mpath using nodes n1 and n2.

    n1 is from wpath, n2 is from mpath.

    @return: (physints, move) where physints is the pair of physical intervals
             acted upon,
             and move is (((i,j),(k,l)),((i,l),(k,j))) implying the DCJ
             swapping the adjacencies i,j and k,l while disconnecting l and j
             (note the same DCJ disconnecting i and k would result in
             ((k,j),(i,l))).
    """
    if(random.getrandbits(1)):
      o = n2.reality
      physints, move = self._DCJOnNodes(n1, n2)
    else:
      o = n2
      physints, move = self._DCJOnNodes(n1, n2.reality)

        #Fix the sets of components:
    wpaths.remove(wpath)
    mpaths.remove(mpath)
    s1 = set((x for (x,y) in self._getEdgesInCompSingleGenome((n1,n1.reality))))
    opaths.append(s1)
    s2 = set((x for (x,y) in self._getEdgesInCompSingleGenome((o,o.reality))))
    opaths.append(s2)

    return physints, move



  def _chooseSingleNode(self, paths):
    """
    Choose a node unifomly at random from the given paths.
    """
    return random.choice(list(chain(*paths)))


  def getWanderScenarios(self, location, nummoves, reps=1,
                         hm: Optional[CoolerHeatmap]=None,
                         poolsize=None):
    """
    Create scenarios that wander from genome 2 through a sequence of
    random DCJs (genome 1 is completely ignored). The number of moves done
    is the same as the DCJ distance between genomes 1 and 2.

    @param location: store generated scenarios here
    @param nummoves: do this many random moves
    @param hm:       heatmaps as dict (keyed by pairs of pairs (chrom, index))
    @param reps:     make this many scenarios
    @param poolsize: use this many processors to parallelize
    """
    sys.setrecursionlimit(25000)     #(avoid "max recursion depth exceeded")
    identity = copy.deepcopy(self)   #Make a deep copy of this
    identity._eraseGenome1()         #and transform it into the identity.
    
    if poolsize:
      pool = Pool(processes=poolsize)

      hmstr = ''
      if hm:
        hmstr = hm.getCoolerStore()

      chunksize = min(poolsize, ceil(reps/poolsize))
      widgets = ['generating: ', pb.Percentage(), ' ', pb.Bar(marker='-'), ' ',
                 pb.ETA()]
      pbar = pb.ProgressBar(widgets=widgets, maxval=reps).start()
      for i,value in\
        enumerate(pool.imap_unordered(saveWanderScenario_unpack,
                                      zip([self]*reps, [nummoves]*reps,
                                          range(reps), [location]*reps,
                                          [hmstr]*reps),
                                      chunksize=chunksize)):
        pbar.update(i)
      pbar.finish()
    else:
      for i in range(reps):
        identity.saveWanderScenario(nummoves, i, location, hm)


  def saveWanderScenario(self, nummoves, sid, location,
                         heatmaps: Optional[CoolerHeatmap]=None):
    """
    Construct a wandering random scenario with nummoves moves, and write
    it to a file in the given location.

    @note: does not modify the object

    @param nummoves: do this many random moves
    @param sid:      string id to represent this scenario with
    @param location: store generated scenarios here
    @param heatmaps: heatmaps as 4d dict
    """
    ag = copy.deepcopy(self)
    scenario = ag.constructWanderScenario(nummoves)
    physints, moves = zip(*scenario)
    _, logtoend = os.path.split(location)
    if heatmaps:
      scorePhysIntList(heatmaps, physints, f'{location}/{logtoend}.{sid}',
                       movelist=moves, speclist=[2]*len(moves))

    else:
      poslist = [None]*len(moves)
      heatlist = [None]*len(moves)
      specieslist = [2]*len(moves)      # We only made moves in species 2

      logtofile = f'{location}/{logtoend}.{sid}'
      writeScenario(logtofile, physints, poslist, heatlist, moves, specieslist)

    

  def constructWanderScenario(self, nummoves):
    """
    Create a scenario that wanders from genome 2 through a sequence of
    random DCJs.

    @param nummoves: do this many DCJs

    @return: list of pairs (physints, move)
    """
    allrealityedges = list(self._getRealityEdges(self.telomeres2, self.nodes2))
    scenario = []
    for _ in range(nummoves):
      scenario.append(self._DCJOnEdges(*random.sample(allrealityedges, 2)))

    return scenario
  
  
  
  def _eraseGenome1(self):
    """
    Erase the information of genome 1 so that the graph is sorted.
    """
    self.nodes1 = copy.deepcopy(self.nodes2)
    self.telomeres1 = copy.deepcopy(self.telomeres2)

    for genename, nodesdict in self.nodes2.items():
      n2tail = nodesdict[AGNode._TAIL]
      n1tail = self.nodes1[genename][AGNode._TAIL]
      #beforeD = str(n2tail.desire)
      #beforeR = str(n2tail.reality)
      n2tail.desire = n1tail
      n1tail.desire = n2tail
      if(n2tail.reality.isTelomere()):
        n1tail.reality =\
          next(t for t in self.telomeres1 if t.reality.nid == n2tail.nid)
      else:
        n1tail.reality = self.nodes1[n2tail.reality.nid][n2tail.reality.end]

      n1tail.reality.reality = n1tail
      n1tail.setUID()

      #if(beforeD != str(n2tail.desire)):
      #  print 'n2d {} != {}'.format(beforeD, n2tail.desire)
      #if(beforeR != str(n2tail.reality)):
      #  print 'n2r {} != {}'.format(beforeR, n2tail.reality)
      #if(beforeR != str(n1tail.reality)):
      #  print 'n1r {} != {}'.format(beforeR, n1tail.reality)

      #if(str(n2tail) != str(n1tail.desire)):
      #  print 'n1d {} != {}'.format(n2tail, n1tail.desire)

      n2head = nodesdict[AGNode._HEAD]
      n1head = self.nodes1[genename][AGNode._HEAD]
      #beforeD = str(n2head.desire)
      #beforeR = str(n2head.reality)
      n2head.desire = n1head
      n1head.desire = n2head
      if(n2head.reality.isTelomere()):
        n1head.reality =\
          next(t for t in self.telomeres1 if t.reality.nid == n2head.nid)
      else:
        n1head.reality = self.nodes1[n2head.reality.nid][n2head.reality.end]

      n1head.reality.reality = n1head
      n1head.setUID()

      #if(beforeD != str(n2head.desire)):
      #  print 'h n2d {} != {}'.format(beforeD, n2head.desire)
      #if(beforeR != str(n2head.reality)):
      #  print 'h n2r {} != {}'.format(beforeR, n2head.reality)
      #if(beforeR != str(n1head.reality)):
      #  print 'h n1r {} != {}'.format(beforeR, n1head.reality)

      #if(str(n2head) != str(n1head.desire)):
      #  print 'h n1d {} != {}'.format(n2head, n1head.desire)
    


#} DCJ functions - randomization



#       .       .       .       .       .       .       .       .       .
#{ DCJ functions - Greedy!

  def getGreedyScenario(self, heatmap: CoolerHeatmap, score_fission=False):
    """
    Get sorting scenario on the given graph in a greedy way
    according to heatmap genome 2 heatmap.
    Sort genome 2 to genome 1.

    @param heatmap: the heatmap

    @param score_fission: If True, then score the fission like if they were
                          normal moves. Else, perform them when all other
                          types of move are unavailable.

    @return: (physints, pos, heats, moves, species) where
             physints is the list of pairs of physical intervals acted upon,
             and pos is the list of physints position on the heatmap,
             and heats is the list of corresponding heatmap values,
             and move is (((i,j),(k,l)),((i,l),(k,j))) implying the DCJ
             swapping the adjacencies i,j and k,l while disconnecting l and j
             (note the same DCJ disconnecting i and k would result in
             ((k,j),(i,l)))
             and species is the list of corresponding species for each move.
    """
    dist = self.getDist()
    intervals = []
    poslist = []
    moves = []
    heats = []
    for _ in range(dist):
      printnow(f'plunk {self.getDist()}')
      interval, pos, heat, move = self._greedySortingMove(self.telomeres2,
                                                          self.nodes2,
                                                          heatmap,
                                                          score_fission)
      intervals.append(interval)
      poslist.append(pos)
      moves.append(move)
      heats.append(heat)
    specieslist = [2]*dist    # We only made moves in species 2

    if self.getDist():
      sys.exit('ERROR: not sorted!')
    return intervals, poslist, heats, moves, specieslist


  def getGreedyScenario2Way(self, hm1: CoolerHeatmap, hm2: CoolerHeatmap,
                            showprogress=False):
    """
    Get a sorting scenario on the given graph in a greedy way.
    For each step, look in the heatmap of each species and apply
    the best scored move.

    @return: (physints, move) where physints is the pair of physical intervals
             acted upon in the form (physint, species, heat),
             and move is (((i,j),(k,l)),((i,l),(k,j))) implying the DCJ
             swapping the adjacencies i,j and k,l while disconnecting l and j
             (note the same DCJ disconnecting i and k would result in
             ((k,j),(i,l))).
    """
    dist = self.getDist()
    intvl1 = []
    intvl2 = []
    poslt1 = []
    poslt2 = []
    ht1 = []
    ht2 = []
    mv1 = []
    mv2 = []
    spl1 = []
    spl2 = []
    counter = 0
    if showprogress:
      widget = ['Computing scenario: ', pb.Percentage(), ' ',
                pb.Bar(marker='-'), ' ', pb.ETA()]
      pbar = pb.ProgressBar(widgets=widget, maxval=dist).start()


    for _ in range(dist):
      intvl,pos,ht,mv,sp = self._greedySortingMove2Way(self.telomeres1,
                                                       self.nodes1, hm1,
                                                       self.telomeres2,
                                                       self.nodes2, hm2)
      #print mv
      #print "self.getDist = ", self.getDist()

      if sp == 1:
        intvl1.append(intvl)
        poslt1.append(pos)
        ht1.append(ht)
        mv1.append((mv[1], mv[0]))
        spl1.append(sp)
      elif sp == 2:
        intvl2.append(intvl)
        poslt2.append(pos)
        ht2.append(ht)
        #mv2.append((mv[1], mv[0]))
        mv2.append(mv)
        spl2.append(sp)
      else:
        raise Exception("Unknown genome %s" % sp)

      #print "===> Move", counter
      #counter += 1

      if showprogress:
        counter += 1
        pbar.update(counter)

    if showprogress:
      pbar.finish()

    if(self.getDist()):
      sys.exit('ERROR: not sorted!')

    # We "sort" the scenario
    intervals = intvl2 + list(reversed(intvl1))
    poslist = poslt2 + list(reversed(poslt1))
    heats = ht2 + list(reversed(ht1))
    moves = mv2 + list(reversed(mv1))
    specieslist = spl2 + spl1

    return intervals, poslist, heats, moves, specieslist


  def sampleGreedyScenarios(self, heatmap: CoolerHeatmap, score_fission, reps=1,
                            logto=''):
    """
    Return the average heat (and std dev) over a set of "greedy scenarios"".

    @param heatmap:       the heatmap
    @param score_fission: If True, then score the fission like if they were
                          normal moves. Else, perform them when all other
                          types of move are unavailable.
    @param reps:          the number of random scenarios to average over
    @param logto:         log the scenarios to this directory
                          if it isn't empty
    @type logto:          string

    @return: pair (average, stddev) for all, intra, and inter
    """
    allheat = []
    intraheat = []
    interheat = []
    for i in range(reps):
      sys.setrecursionlimit(25000)     #(avoid "max recursion depth exceeded")
      g = copy.deepcopy(self)

      #try:

      physints, pos, heats, moves, species = g.getGreedyScenario(heatmap,
                                                                 score_fission)

      logtobegin, logtoend = os.path.split(logto)
      logtofile = logtoend+'.'+str(i)
      writeScenario(logto+'/'+logtofile, physints, pos, heats, moves, species)

      #########################################################################
      # We don't need that, we already have the necessary but we need to look
      # into physintlist to know the intra and inter moves and I'm too tired
      # to do that now...
      heatave, heatl, posl = scorePhysIntList(heatmap, physints)
      #except Exception as inst:
      #  print "WARNING: exception for a random scenario:"+str(inst)+\
      #        "\n\tmoving on..."


      if(heatave[0]):
        allheat.append(heatave[0])
      if(heatave[1]):
        intraheat.append(heatave[1])
      if(heatave[2]):
        interheat.append(heatave[2])

    allpair = (None, None)
    intrapair = (None, None)
    interpair = (None, None)
    if(len(allheat)):
      allpair = (np.average(allheat), np.std(allheat))
    if(len(intraheat)):
      intrapair = (np.average(intraheat), np.std(intraheat))
    if(len(interheat)):
      interpair = (np.average(interheat), np.std(interheat))

    return (allpair, intrapair, interpair)


  def sampleGreedyScenarios2Way(self, hm1: CoolerHeatmap, hm2: CoolerHeatmap,
                                reps=1, logto='', sid='', perGenome=False):
    """
    Return the average heat (and std dev) over a set of "greedy scenarios"
    in a 2-Way fashion.


    @param hm1:     the heatmap for genome 1
    @param hm2:     the heatmap for genome 2
    @param winsize: the window size of the heatmap
    @param reps:    the number of random scenarios to average over
    @param logto:   log the scenarios to this directory if it isn't empty
    @type logto:    string
    @param sid:     a unique identifier for these scenarios
    @type sid:      string
    @param perGenome: If perGenome is True, then apply the computed scenario on
                      each genome independently of the other, and write this
                      two scenarios in the same directory.  Note that the
                      returned stats don't take into account this perGenome
                      scenarios.

    @return: triplet of pair (average, stddev) for each species and for both.
    """
    raise(Exception('This code needs to be checked for correctness.  '+
                    'See e.g. the heats and scores1.'))
    allheat = []
    intraheat = []
    interheat = []
    for i in range(reps):
      g = copy.deepcopy(self)

      #try:
      physints, pos, heats, moves, species = g.getGreedyScenario2Way(hm1, hm2,
                                                                     True)

      logtobegin, logtoend = os.path.split(logto)
      logtofile = logtoend+'.'+str(i)+'.'+sid
      writeScenario(logto+'/'+logtofile, physints, pos, heats, moves, species)

      # We're going "from genome 2 to genome 1"
      if perGenome:
        ints2, moves2 = self.applyScenario(moves, self.telomeres2, self.nodes2)
        ints1, moves1 = self.undoScenario(moves, self.telomeres1, self.nodes1)

        l = len(physints)
        writeScenario(logto+'/'+logtofile+'_1to2', ints1, pos,
                      list(repeat(None, l)), moves1, list(repeat(1, l)))

        writeScenario(logto+'/'+logtofile+'_2to1', ints2, pos,
                      list(repeat(None, l)), moves2, list(repeat(2, l)))

    scores1 = [h for h, s in zip(heats, species) if s == 1]
    scores1 = [h for h in scores1 if h != float('-inf') and type(h) != str]
    scores2 = [h for h, s in zip(heats, species) if s == 2]
    scores2 = [h for h in scores2 if h != float('-inf') and type(h) != str]

    mean1 = np.mean(scores1)
    stddev1 = np.std(scores1)

    mean2 = np.mean(scores2)
    stddev2 = np.std(scores2)

    mean_all = np.mean(heats)
    stddev_all = np.std(heats)

    print("Species 1")
    print("\tAverage:", mean1, "\tStd dev:", stddev1)
    print("Species 2")
    print("\tAverage:", mean2, "\tStd dev:", stddev2)
    print("Full scenario")
    print("\tAverage:", mean_all, "\tStd dev:", stddev_all)

    return ((mean1, stddev1), (mean2, stddev2), (mean_all, stddev_all))


  def printGreedyScenariosStats(self, heatmap, winsize, reps=1):
    """
    Compute and print stats for the greedily-found local scenarios.

    @param heatmap: the heatmap
    @param winsize: the window size in the heatmap
    @param reps: the number of random scenarios to average over
    """
    allmoves, intra, inter = self.sampleGreedyScenarios(heatmap, winsize, reps)

    print("All moves")
    print("\tAverage:", allmoves[0], "\tStd deviation:", allmoves[1])

    print("Intra moves")
    print("\tAverage:", intra[0], "\tStd deviation:", intra[1])

    print("Inter moves")
    print("\tAverage:", inter[0], "\tStd deviation:", inter[1])


  #def writeGreedyScenarios(self, heatmap, winsize, reps=1, logto='', sid=''):
  #  """
  #  A hack...
  #  """
  #  allheat = []
  #  intraheat = []
  #  interheat = []
  #  for i in range(reps):
  #    g = copy.deepcopy(self)

  #    physintlist, movelist, specieslist = g.getGreedyScenario(heatmap)
  #    heatave, heatl, posl = scorePhysIntList(heatmap, physintlist,
  #                                            logto, logto+'.'+str(i)+'.'+sid)

  #    if(heatave[0]):
  #      allheat.append(heatave[0])
  #    if(heatave[1]):
  #      intraheat.append(heatave[1])
  #    if(heatave[2]):
  #      interheat.append(heatave[2])

  #  allpair = (None, None)
  #  intrapair = (None, None)
  #  interpair = (None, None)
  #  if(len(allheat)):
  #    allpair = (np.average(allheat), np.std(allheat))
  #  if(len(intraheat)):
  #    intrapair = (np.average(intraheat), np.std(intraheat))
  #  if(len(interheat)):
  #    interpair = (np.average(interheat), np.std(interheat))

  #  print("All moves")
  #  print("\tAverage:", allpair[0], "\tStd deviation:", allpair[1])

  #  print("Intra moves")
  #  print("\tAverage:", intrapair[0], "\tStd deviation:", intrapair[1])

  #  print("Inter moves")
  #  print("\tAverage:", interpair[0], "\tStd deviation:", interpair[1])



  def _greedySortingMove(self, telomeres, nodes, heatmap: CoolerHeatmap,
                         score_fission=False):
    """
    Do a sorting move on the given nodes and telomeres.
    This move is chosen using the heatmap.

    If score_fission is True, then score the fission like if they are
    normal moves. Else, perform them when all other type of move are
    unavailable.

    @return: (physints, pos, heat, move) where
             physints is the pair of physical intervals acted upon,
             and pos is the heatmap position of this physints,
             and heat is the heatmap score for this physints
             and move is (((i,j),(k,l)),((i,l),(k,j))) implying the DCJ
             swapping the adjacencies i,j and k,l while disconnecting l and j
             (note the same DCJ disconnecting i and k would result in
             ((k,j),(i,l)))
    """
    cycles, opaths, wpaths, mpaths = self._getComponentsFor(telomeres, nodes)


    # print 'cycles:',self.cycles
    # print 'opaths:',self.opaths
    # print 'wpaths:',self.wpaths
    # print:',self.mpaths
    #self.printGraphCL("tmp1.pdf")
    #self.printGraphCL('order'+str(self.getDist())+'.pdf')

    moveType, moveToDo = self._chooseMoveGreedy(cycles, opaths, wpaths, mpaths,
                                                heatmap, score_fission)

    if moveType != self._FISSION or score_fission:
      heat = moveToDo[0]
      pos = moveToDo[1]

    #beforedist = self.getDist()  #NOTE: debug!
    if(moveType == self._CYCEX or moveType == self._OPEX or
       moveType == self._WPM or moveType == self._MPM):
      if(moveType == self._CYCEX):
        comps = cycles
      elif(moveType == self._OPEX):
        comps = opaths
      elif(moveType == self._WPM):
        comps = wpaths
      else:
        assert moveType == self._MPM
        comps = mpaths
      #print('extract cycle from component')
      n1, n2, c = moveToDo[2]
      intervals, move = self._extractCycle(n1, n2, c, comps, cycles)
    elif moveType == self._COMBO:
      #print('mix paths')
      n1, n2, w, m = moveToDo[2]
      intervals, move = self._mixPaths(n1, n2, wpaths, w, mpaths, m, opaths)
    elif moveType == self._FISSION:
      #print('do fission')
      #print("Cycles: ", self.cycles, '\n')
      #print("Odd paths: ", self.opaths, '\n')
      #print("W-paths: ", self.wpaths, '\n')
      #print("M-paths: ", self.mpaths, '\n')
      if score_fission:
        n1, n2, c = moveToDo[2]
        intervals, move = self._doFissionUpdate(n1, mpaths, telomeres, opaths)
      else:
        #print('rand fission: {}'.format(mpaths))
        intervals, move = self._doRandFission(mpaths, telomeres, opaths)

        # We should find a more efficient way to do that
        _, heatl, posl = scorePhysIntList(heatmap, [intervals])
        heat = heatl[0]
        pos = posl[0]
        #print('fission done at:',intervals)
    else:
      sys.exit(f'ERROR: unknown moveType code: {moveType}.')
    #self.printGraphCL("tmp2.pdf")
    #raw_input()
    #print(self.getDist())
    #assert(beforedist-1 == self.getDist()) #NOTE: temporary

    return intervals, pos, heat, move




  def _greedySortingMove2Way(self, t1, n1, hm1: CoolerHeatmap,
                             t2, n2, hm2: CoolerHeatmap):
    """
    Do a sorting move on one of the two genomes.
    This move is chosen using the heatmap of each species.

    @return: (physints, pos, heat, move, sp) where
             physints is the pair of physical intervals acted upon,
             and pos is the corresponding heatmap position,
             and heat is the heatmap value for this move,
             and move is (((i,j),(k,l)),((i,l),(k,j))) implying the DCJ
             swapping the adjacencies i,j and k,l while disconnecting l and j
             (note the same DCJ disconnecting i and k would result in
             ((k,j),(i,l)))
             and sp is the species acted upon.
    """
    self.onattr = False
    #if(self.cycles1 == None):
    (cycles, opaths, (wpaths, mpaths)) = self._classifyComps(t1, n1)
    self.cycles1 = cycles
    self.opaths1 = opaths
    self.wpaths1 = wpaths
    self.mpaths1 = mpaths

    #if(self.cycles2 == None):
    (cycles, opaths, (wpaths, mpaths)) = self._classifyComps(t2, n2)
    self.cycles2 = cycles
    self.opaths2 = opaths
    self.wpaths2 = wpaths
    self.mpaths2 = mpaths

    # print 'cycles:',self.cycles
    # print 'opaths:',self.opaths
    # print 'wpaths:',self.wpaths
    # print:',self.mpaths
    #self.printGraphCL("tmp1.pdf")
    #self.printGraphCL('order'+str(self.getDist())+'.pdf')


    # We're doing each choice in a different process
    # pool = Pool(processes=2)
    # r1 = pool.apply_async(self._chooseMoveGreedy, (self.cycles1, self.opaths1,
    #                                                self.wpaths1, self.mpaths1,
    #                                                hm1, winsize))
    # r2 = pool.apply_async(self._chooseMoveGreedy, (self.cycles2, self.opaths2,
    #                                                self.wpaths2, self.mpaths2,
    #                                                hm2, winsize))
    # moveType1, move1 = r1.get()
    # moveType2, move2 = r2.get()
    # pool.close()
    moveType1, move1 = self._chooseMoveGreedy(self.cycles1, self.opaths1,
                                              self.wpaths1, self.mpaths1,
                                              hm1)
    moveType2, move2 = self._chooseMoveGreedy(self.cycles2, self.opaths2,
                                              self.wpaths2, self.mpaths2,
                                              hm2)


    if move1[0] > move2[0]:
      moveType = moveType1
      moveToDo = move1
      species = 1
      hm = hm1
      cycles, opaths, wpaths, mpaths, telomeres = (self.cycles1, self.opaths1, 
                                                   self.wpaths1, self.mpaths1,
                                                   self.telomeres1)
    else:
      moveType = moveType2
      moveToDo = move2
      species = 2
      hm = hm2
      cycles, opaths, wpaths, mpaths, telomeres = (self.cycles2, self.opaths2,
                                                   self.wpaths2, self.mpaths2,
                                                   self.telomeres2)


    # print "Choose species", species
    # print "  heats:\t", move1[0], "\t", move2[0], '\t', moveToDo[0]
    # print "  moves:"
    # print "    move1:", move1
    # print "    move2:", move2
    # print "  moveType1:", moveType1, "\tmoveType2:", moveType2
    # print "  moveType done:", moveType

    if moveType != self._FISSION:
      heat = moveToDo[0]
      pos = moveToDo[1]

    if(moveType == self._CYCEX or moveType == self._OPEX or
       moveType == self._WPM or moveType == self._MPM):
      #print 'extract cycle from component'
      #print ''
      #print "Extract a cycle!\tmoveType = ", moveType, '\n'
      if(moveType == self._CYCEX):
        comps = cycles
      elif(moveType == self._OPEX):
        comps = opaths
      elif(moveType == self._WPM):
        comps = wpaths
      else:
        assert moveType == self._MPM
        comps = mpaths
      n1, n2, c = moveToDo[2]
      intervals, move = self._extractCycle(n1, n2, c, comps, cycles)
    elif(moveType == self._COMBO):
      #print 'mix paths'
      #print ""
      #print "Mix two paths\n"
      n1, n2, w, m = moveToDo[2]
      intervals, move = self._mixPaths(n1, n2, wpaths, w, mpaths, m, opaths)
    elif(moveType == self._FISSION):
      #print 'do fission'
      #print ""
      #print "Do a random fission\n"
      #print "Cycles: ", self.cycles, '\n'
      #print "Odd paths: ", self.opaths, '\n'
      #print "W-paths: ", self.wpaths, '\n'
      #print "M-paths: ", self.mpaths, '\n'
      intervals, move = self._doRandFission(mpaths, telomeres, opaths)
      _, heatl, posl = scorePhysIntList(hm, [intervals])

      heat = heatl[0]
      pos = posl[0]
      #print 'fission done at:',intervals
    else:
      sys.exit(f'ERROR: unknown moveType code: {moveType}.')
    #self.printGraphCL("tmp2.pdf")
    #raw_input()
    #print self.getDist()

    return intervals, pos, heat, move, species


  def _chooseMoveGreedy(self, cycles, opaths, wpaths, mpaths,
                        heatmap: CoolerHeatmap,
                        fissions_are_normal_moves=False)\
    -> Tuple[T_MoveType, T_GreedyMove]:
    """
    Choose a move based on the distribution given by the arguments
    and on the heatmap.

    If fissions_are_normal_moves is True, then consider the fissions as
    normal moves (...) by looking for the heat of the uniq breakpoint and
    comparing it with other moves. Else, do a fission only if the other moves
    can not be done.

    @return: (movetype, move) where movetype is one of the move-type constants
             and move is a tuple (heatscore, heatpos, (node1, node2, component))
    """
    if not cycles and not opaths and not wpaths:
      if filter(lambda x: len(x) == 1, mpaths):
        return self._FISSION, NULL_MOVE

    else:
      # pool = Pool()
      # r1 = pool.apply_async(self._getExtractCycleWithBestScore,
      #                       (cycles, heatmap, winsize))
      # r2 = pool.apply_async(self._getExtractCycleWithBestScore,
      #                       (opaths, heatmap, winsize))
      # r3 = pool.apply_async(self._getExtractCycleWithBestScore,
      #                       (mpaths, heatmap, winsize))
      # r4 = pool.apply_async(self._getExtractCycleWithBestScore,
      #                       (wpaths, heatmap, winsize))
      # r5 = pool.apply_async(self._getMixPathWithBestScore,
      #                       (wpaths, mpaths, heatmap, winsize))
      # scores = dict()
      # scores[self._CYCEX] = r1.get()
      # scores[self._OPEX] = r2.get()
      # scores[self._MPM] = r3.get()
      # scores[self._WPM] = r4.get()
      # scores[self._COMBO] = r5.get()
      # pool.close()

      # else:
      typeTOmove = dict()  #(heat, (positions), (nodes, connected component))
      typeTOmove[self._CYCEX] = self._getExtractCycleWithBestScore(cycles,
                                                                   heatmap)
      typeTOmove[self._OPEX] = self._getExtractCycleWithBestScore(opaths,
                                                                  heatmap)
      typeTOmove[self._MPM] = self._getExtractCycleWithBestScore(mpaths,
                                                                 heatmap)
      typeTOmove[self._WPM] = self._getExtractCycleWithBestScore(wpaths,
                                                                 heatmap)
      typeTOmove[self._COMBO] = self._getMixPathWithBestScore(wpaths, mpaths,
                                                              heatmap)
      if fissions_are_normal_moves:
        typeTOmove[self._FISSION] = self._getFissionWithBestScore(mpaths,
                                                                  heatmap)

      # We remove None scores
      typeTOmove = {k: v for k, v in typeTOmove.items()
                    if k != None and v != None}

      if(not mpaths or
         any(v[0] != float('-inf') for v in typeTOmove.values())):
        moveType = max(typeTOmove, key=lambda x: typeTOmove[x][0])
        return (moveType, typeTOmove[moveType])
      else:
        return self._FISSION, NULL_MOVE

    raise(Exception('Should not reach here.'))


  def _getExtractCycleWithBestScore(self, components, heatmap: CoolerHeatmap):
    """
    Use heatmap to score each possible move from each components.

    The move is meant to be done between 2 nodes from the same component.

    @return: heatscore, heatpos, (node1, node2, component)
    """
    best_score = float('-inf')
    result = None

    for c in components:
      if len(c) == 1:
        continue

      temp_list = list(combinations(c, 2))
      intervals = [((n.chromosome, n.physint), (m.chromosome, m.physint))
                   for n, m in temp_list]
      _, heatlist, posl = scorePhysIntList(heatmap, intervals)
      # We take care of the NULLV
      for i in range(len(heatlist)):
        if heatlist[i] == NULLV or np.isnan(heatlist[i]):
          heatlist[i] = float('-inf')

      best_temp = max(heatlist)

      if  best_temp >= best_score:
        i = heatlist.index(best_temp)
        best_score = best_temp
        result = (best_score, posl[i], (temp_list[i][0], temp_list[i][1], c))

    return result

  def _getMixPathWithBestScore(self, wpaths, mpaths, heatmap: CoolerHeatmap):
    """
    Use heatmap to score each possible mix between each wpaths and mpaths.

    @return: heatscore, heatpos, (n1, n2, wpath, mpath)
             where n1 is from wpath and n2 from mpath
    """
    best_score = float('-inf')
    result = None

    for w, m in product(wpaths, mpaths):
      temp_list = list(product(w, m))
      intervals = [((x.chromosome, x.physint), (y.chromosome, y.physint))
                   for x, y in temp_list]
      _, heatlist, posl = scorePhysIntList(heatmap, intervals)
      # We take care of the NULLV
      for i in range(len(heatlist)):
        if heatlist[i] == NULLV or np.isnan(heatlist[i]):
          heatlist[i] = float('-inf')

      best_temp = max(heatlist)

      if best_temp >= best_score:
        i = heatlist.index(best_temp)
        best_score = best_temp
        result = (best_score, posl[i],
                  (temp_list[i][0], temp_list[i][1], w, m))

    return result
  
  
  def _getFissionWithBestScore(self, mpaths, heatmap: CoolerHeatmap)\
      -> Tuple[float, Tuple[Tuple[str, int], Tuple[str, int]], Tuple]:
    """ Score all possible fissions using the heatmap and return the best one.

    :param mpaths: 
    :param heatmap: 
    :return: heatscore, heatpos, (node1, node2, component)
    """
    best_score = float('-inf')
    result = None

    for path in mpaths:
      temp_list = [(n, n.reality) for n in path]
      intervals = [((x.chromosome, x.physint), (y.chromosome, y.physint))
                   for x, y in temp_list]
      _, heatlist, posl = scorePhysIntList(heatmap, intervals)
      # We take care of the NULLV
      for i in range(len(heatlist)):
        if heatlist[i] == NULLV or np.isnan(heatlist[i]):
          heatlist[i] = float('-inf')

      best_temp = max(heatlist)

      if best_temp >= best_score:
        i = heatlist.index(best_temp)
        best_score = best_temp
        result = (best_score, posl[i],
                  (temp_list[i][0], temp_list[i][1], path))

    assert result
    return result

#} DCJ Functions - Greedy!





#       .       .       .       .       .       .       .       .       .
#{ Scoring


  def _aveAllPairs(self, edges, heatmaps: CoolerHeatmap, bychrom=False):
    """
    Return a string with the average of the given heatmaps over the given edges.
    The maximum heatmap value occurring on the interval for an edge is taken.
      - I{rounds down left endpoint of the breakpoint to it's nearest window.
          (unless it's a telomere, then the only known coordinate is used).}
      - I{rounds down even further if the heatmap index is too high for the
          given heatmap (which seems to happen rarely in our case).}

    @param edges:      the iterable we average over.
    @param heatmaps:   the heatmaps
    @param windowsize: size of the window we round to for location coordinates.
    @param bychrom:    separate the information per chromosome.

    @return: a pair (stringdescription, (totave, intraave, interave))
    """
    interv = []
    intrav = []
    numzeros = 0
    numskipped = 0
    chromosome = None
    multichromosomal = False
    chrTOval = defaultdict(list)

            #Get the score for each pair of edges:
    for (e1,e2) in combinations(edges,2):
      i=0                           #The node we will be using.
      if(e1[0].isTelomere()):       #If we are at a telomere
        i = 1                       #then use the other node.
      j=0                           #The node we will be using.
      if(e2[0].isTelomere()):       #If we are at a telomere
        j = 1                       #then use the other node.

      c1 = e1[i].chromosome         #Get the chromosomes.
      c2 = e2[j].chromosome

      if(chromosome):               #Mark things as multichromosomal (with M).
        if(chromosome != c1 or chromosome != c2):
          multichromosomal = True
      else:
        chromosome = c1

      heat, _ = heatmaps.getCoordinate(c1, e1[i].physint, c2, e2[j].physint)

      if heat == None:
        numskipped += 1
      else:
        if heat == 0 or heat == np.nan:
          numzeros += 1
        if c1 == c2:
          intrav.append(heat)
          if(bychrom):
            chrTOval[c1].append(heat)
        else:
          interv.append(heat)


            #Output the results:
    rs = ''
    totalave = None
    intraa = None
    intera = None
    if(len(intrav) or len(interv)):
      intras = '_____________'
      if(len(intrav)):
        intraa = np.average(intrav)
        intras = str(intraa)
      inters = '_____________'
      if(len(interv)):
        intera = np.average(interv)
        inters = str(intera)

      multistring = ''
      pathstring = ''
      if(self._isOnPath(edges[0])):
        pathstring = 'P'
      if(multichromosomal):
        multistring = "M"
      totallen = len(intrav)+len(interv)
      totalave = np.average(intrav+interv)
      #rs += ("Ave["+str(totallen)+"]"+multistring+":"+pathstring).ljust(12)+\
      #      str(totalave).ljust(19)+' '+\
      #      (intras+'['+str(len(intrav))+']').rjust(22)+\
      #      str(numzeros).center(3)+'['+str(len(interv))+']'+inters
      rs += self._getAveLine(totallen, totalave, intras, inters, multistring,
                             pathstring, len(intrav), len(interv), numzeros)
      rs += "\n\tdev:"+str(np.std(intrav+interv))+'\n'

      for c in chrTOval.keys():   #Add chromosome-specific stats.
        v = chrTOval[c]
        rs += '\tchr '+c+'['+str(len(v))+']'+': '+str(np.average(v))+'\n'

    rs += "\tnumskipped: "+str(numskipped)+"\n"
    return (rs, (totalave, intraa, intera))


  def _heatOnComponentScen(self, redge, heatmap: CoolerHeatmap):
    """
    Evaluate the heat for a component with the given reality edge by sorting it.
    Do this independent of all other components, I{even for even-length
    components}.
    The maximum heatmap value occurring on the interval for an edge is taken.
      - I{rounds down left endpoint of the breakpoint to it's nearest window.
          (unless it's a telomere, then the only known coordinate is used).}
      - I{rounds down even further if the heatmap index is too high for the
          given heatmap (which seems to happen rarely in our case).}

    @param redge:      the edge with the connected component of interest
    @param heatmap:    the heatmap indexed by pairs (chrome,location).

    @return: a string with the heat values
    """
    g = self._extractComponent(redge)
    (both, intra, inter) = g.sampleMultipleRandomScenarios(heatmap,
                                                           reps=SCENARIO_REPS)

    multistring = ''
    if(intra[0] and inter[0]):
      multistring = 'M'

    rs = self._getAveLine(g.getDist(), both[0], intra[0], inter[0], multistring)
    return (both[0], rs+'\n')





  def _analyzeEdgesRP(self, edges, rps, poolsize=0, showprogress=False,
                      long_results=False):
    """
    Print information about the given repeats for the give edge set.
    """
    global edgeTOrpts, GROUPSIZE

    repeatLists = []    #The list of repeats for the chromosomes for each edge.
    for e in edges:     #For each edge, find the gene
      g = e[0].gene     #associated with the edge soas to know the chromosome.
      if(not g):
        g = e[1].gene   #Save the list of repeats for this edge.
      repeatLists.append(rps['chr'+str(g.chromosome)])

    edgerptpairs = []
    if(poolsize and len(edges) > GROUPSIZE):      #If we're using a pool then
      pool = Pool(processes=int(poolsize))        #start the worker processes,

      r = pool.map_async(_getRepeatsForEdgeRP, zip(edges, repeatLists),
                         callback=edgerptpairs.append)
      r.wait()                                    #and crank on them.
      pool.close()
      edgerptpairs = chain(*edgerptpairs)
      #edgerptpairs = reduce(lambda x,y: x+y, edgerptpairs)
    else:
      edgerptpairs = map(_getRepeatsForEdgeRP, zip(edges, repeatLists))

    for (e, rpts) in edgerptpairs:
      edgeTOrpts[str(e)] = rpts

                        #The argument list for the map command.
    numpairs = nChoose2(len(edges))
    if(numpairs > 100000):
      print("computing "+str(numpairs)+" pairs...")
      sys.stdout.flush()
    result = []
    if(poolsize and (numpairs > 2*GROUPSIZE)):
      chunks = grouper(GROUPSIZE, combinations(edges,2))
      pool = Pool(int(poolsize), maxtasksperchild=10) #start worker processes,

      if(long_results):
        r = pool.map_async(_scoreEdgePairsLong, chunks, callback=result.append)
      else:
        r = pool.map_async(_scoreEdgePairs, chunks, callback=result.append)

      print("waiting...")
      sys.stdout.flush()
      r.wait()                                    #and crank on them.
      pool.close()
      result = chain(*result)
      result = chain(*result)
    else:
      if(showprogress and numpairs > 100):
        widgets = ['scoring edge pairs: ', pb.Percentage(), ' ',
                   pb.Bar(marker='-'), ' ', pb.ETA()]
        pbar = pb.ProgressBar(widgets=widgets, maxval=numpairs).start()
      counter = 0
      for pair in combinations(edges, 2):
        if(long_results):
          result.append(_scoreEdgePairLong(pair))
        else:
          result.append(_scoreEdgePair(pair))
        counter += 1
        if(showprogress and numpairs > 100):
          pbar.update(counter)
      if(showprogress and numpairs > 100):
        pbar.finish()

    print('compiling results...') # for '+str(len(result))+' entries...'
    sys.stdout.flush()

    if(long_results):
      cctot = 0
      fctot = 0
      nctot = 0
      lenctot = 0
      lenftot = 0
      lenntot = 0
      count = 0
      ccount = 0
      fcount = 0
      ncount = 0
      for ((cc,lenc), (fc,lenf), (nc,lenn)) in result:
        count += 1
        cctot += cc
        fctot += fc
        nctot += nc
        if cc: ccount += 1
        if fc: fcount += 1
        if nc: ncount += 1
        lenctot += lenc
        lenftot += lenf
        lenntot += lenn

      cave = 0
      fave = 0
      nave = 0
      if(lenctot):
        cave = float(lenctot)/float(cctot)
      if(lenftot):
        fave = float(lenftot)/float(fctot)
      if(lenntot):
        nave = float(lenntot)/float(nctot)

      print(str(ccount)+'/'+str(count)+' : '+str(cave)+','+str(lenctot))
      print(str(fcount)+'/'+str(count)+' : '+str(fave)+','+str(lenftot))
      print(str(ncount)+'/'+str(count)+' : '+str(nave)+','+str(lenntot))
    else:
      count = 0
      ccount = 0
      fcount = 0
      ncount = 0
      for (cc,fc,nc) in result:
        count += 1
        ccount += cc
        fcount += fc
        ncount += nc
      print(str(ccount)+'/'+str(count))
      print(str(fcount)+'/'+str(count))
      print(str(ncount)+'/'+str(count))
    sys.stdout.flush()

    return (count, (ccount,fcount,ncount))





#} Scoring




#       .       .       .       .       .       .       .       .       .
#{   Retrieve graph parts

  def getBreakpointsSide1(self):
    """
    Return a list of all the breakpoints from side 1 (the typical target
    -- identity permutation -- when sorting) of the graph.

    @return: list of pairs of AGNode objects.
    """
    return list(self._getRealityEdges(self.telomeres1, self.nodes1))

  def getBreakpointsSide2(self):
    """
    Return a list of all the breakpoints from side 2 (the subject target when
    sorting) of the graph.

    @return: list of pairs of AGNode objects.
    """
    return list(self._getRealityEdges(self.telomeres2, self.nodes2))


  def _getDesireEdges(self):
    """
    A generator to iterate through the desire edges.
    """
    for e in self.nodes1.values():
      for n in e.values():
        yield (n, n.desire)


  def _getRealityEdges(self, telomeres, nodes):
    """
    Generate the reality edges in a sensible order.  Each reality edge is
    visited in chromosome order.

    @warn: we use the -visited- variable from the AGNode.

    @return: a list of pairs of AGNodes, each one representing a reality edge.
    """
      #Get the edges from the linear chromosomes:
    for t in telomeres:
      if(not t.visited):
        for e in self._visitEdgesInChromosome(t, nodes):
          yield e

      #Get the edges from the circular chromosomes:
    for node in self._nodes(nodes):
      if(not node.visited):
        for e in self._visitEdgesInChromosome(node, nodes):
          yield e

    self._resetVisited()


  def _getEdgesInComp(self, e):
    """
    Generator for the reality edges in a path/cycle with the given edge.

    @note: the edges are visited in no particular order!  Actually, if this is
           a path then they are visited from this edge out one side, and then
           the other.
    """
    current = e[0]
    start = current
    while current.desire:   #Explore the path to the left:
      yield (current, current.reality)
      current = current.desire.reality
      if(current == start): #This is a cycle,
        return              #so return.
    yield (current, current.reality)

    current = e[1]
    while current.desire:   #Explore the path to the right:
      current = current.desire.reality
      yield (current, current.reality)



  def _getEdgesInCompOrdered(self, e):
    """
    Return the edges of the component with reality edge e.
    They are given in order from one telomere to the other if this is a path.

    @param e: pair of MNodes
    """
    assert(e[0].reality == e[1])

    current = e[0]
    start = current
    edges = deque()
    while current.desire:   #Explore the path to the left:
      edges.appendleft((current, current.reality))
      current = current.desire.reality
      if(current == start): #This is a cycle,
        return edges        #so return.
    edges.appendleft((current, current.reality))

    current = e[1]
    while current.desire:   #Explore the path to the right:
      current = current.desire.reality
      edges.append((current.reality, current))

    return edges



  def _getEdgesInCompSingleGenome(self, e):
    """
    Generator for the reality edges in a path/cycle from the genome of the
    given edge only.
    @note: the edges are visited in no particular order!  Actually, if this is
           a path then they are visited from this edge out one side, and then
           the other.
    """
    current = e[0]
    start = current                        #Explore the path to the left:
    while current.desire and current.desire.reality.desire:
      yield (current, current.reality)
      current = current.desire.reality.desire.reality
      if(current == start):                #If this is a cycle,
        return                             #then return.
    yield (current, current.reality)

    current = e[1]                         #Explore the path to the right:
    while current.desire and current.desire.reality.desire:
      current = current.desire.reality.desire.reality
      yield (current, current.reality)



  def _getEdgesInCompSingleGenomeOrdered(self, e):
    """
    Reaturn a list of reality edges in a path/cycle from the genome of the
    given edge only.  If it's a path, then return an ordered list from one 
    telomere to the other.
    """
    edges = deque()
    current = e[0]
    start = current                        #Explore the path to the left:
    while current.desire and current.desire.reality.desire:
      edges.appendleft((current, current.reality))
      current = current.desire.reality.desire.reality
      if(current == start):                #If this is a cycle,
        return edges                       #then return.
    edges.appendleft((current, current.reality))

    current = e[1]                         #Explore the path to the right:
    while current.desire and current.desire.reality.desire:
      current = current.desire.reality.desire.reality
      edges.append((current, current.reality))

    return edges


  def _getOneEdgeFromEachCC(self, genome1=False):
    """
    Generator that yields a list of edges, where there is one edge per connected
    component.

    @note: Only yields the edges from components with more than one edge in one
           of the genomes.

    @param genome1: True if the edges are to come from genome 1. Otherwise, they
                    come from genome 2.
    """
    visitedset = set()
    if(genome1):
      telomeres = self.telomeres1
      nodes = self.nodes1
    else:
      telomeres = self.telomeres2
      nodes = self.nodes2

    for redge in self._getRealityEdges(telomeres, nodes):
      pathlen = self._getCompLen(redge)
      if(redge[0] not in visitedset and pathlen > 2):
        for e in self._getEdgesInCompSingleGenome(redge): #Mark the nodes in
          visitedset.add(e[0])                            #this path as
          visitedset.add(e[1])                            #being visited.
        yield redge


  def _visitChromosome(self, node, nodes):
    """
    A generator to iterate through the nodes of the chromosome of the given
    node.
    """
    node.visited = True
    yield node
    current = node.reality
    current.visited = True
    yield current
    while(current.otherEnd() and
          not nodes[current.nid][current.otherEnd()].visited and
          current.notTelomere()):
      current = nodes[current.nid][current.otherEnd()]
      current.visited = True
      yield current
      current = current.reality
      current.visited = True
      yield current


  def _visitEdgesInChromosome(self, node: AGNode, nodes):
    """
    A generator to iterate through the edges of the chromosome for the given
    node.
    """
    node.visited = True
    current = node.reality
    current.visited = True
    yield (node, current)
    while(current.notTelomere() and
          not nodes[current.nid][current.otherEnd()].visited):
      nextone = nodes[current.nid][current.otherEnd()]
      nextone.visited = True
      current = nextone.reality
      current.visited = True
      yield (nextone, current)


  def _getCyclesFromNodes(self, nodes, retlist=False):
    """
    Return the cycles that these nodes are included in.
    Each cycle is a set of nodes, one node from each desire edge in -nodes-.

      - I{Assume that nodes in paths have already been marked as visited!
          (i.e. _getPathsFromTelomeres() has already been called on all paths)}

    @param retlist: return list of lists rather than list of sets

    @return: list of cycles.
    @rtype:  list of sets, or list of lists.
    """
    cycles = []
    for node in AdjacencyGraph._nodes(nodes):
      nodegroup = [] if retlist else set()

      if(not node.visited):
        c = node
        c.visited = True
        even = True
        nodegroup.append(c) if retlist else nodegroup.add(c)
        while(not c.reality.desire.visited):
          c.reality.visited = True
          c = c.reality.desire
          c.visited = True
          even = not even
          if(even):
            nodegroup.append(c) if retlist else nodegroup.add(c)
        cycles.append(nodegroup)
    return cycles


  def _getNodes(self, telomeres, nodes):
    """
    A generator to iterate through the given nodes in the order they appear
    in the chromosome.
    """
      #Get the nodes from the linear chromosomes:
    for t in telomeres:
      if(not t.visited):
        for n in self._visitChromosome(t, nodes):
          yield n

      #Get the nodes from the circular chromosomes:
    for node in self._nodes(nodes):
      if(not node.visited):
        for n in self._visitChromosome(node, nodes):
          yield n

    self._resetVisited()


  @staticmethod
  def _nodes(nodes):
    """
    A generator to iterate through the given dictionary of nodes.
    (this iterates through the nodes buried inside the dictionary)
    """
    return chain(*(p.values() for p in nodes.values()))


  def _getRepresentativeEdge(self, e):
    """
    Get the representative edge for the component with edge e.
    (i.e. the left-most edge)
    """
    repe = sorted(self._getEdgesInComp(e),
                  key=lambda x: x[0].nid if x[0].nid else x[1].nid)[0]
    if(repe[0].nid > repe[1].nid):
      return (repe[1], repe[0])
    return repe


  def _getPathsFromTelomeres(self, telomeres, oddpaths, evenpaths,
                             fromother=False, pathaslist=False):
    """
    Fill oddpaths and evenpaths from the given telomeres.
    Each path is a list of nodes, where every other node
    (odd or even depending on fromother) is saved.

    @note: for each path nodes are ordered by how they appear in the path.

    @param telomeres: telomeres to start searching from.
    @param oddpaths:  the list to put odd paths into.
    @param evenpaths: list to put even paths into.
    @param fromother: save nodes from the other genome (with other telomeres).
    @param pathaslist:save the path as a list rather than a set.
    """
    for t in telomeres:
      if(not t.visited):
        c = t
        c.visited = True
        oddlen = False
        nodegroup = [] if pathaslist else set()
        if(not fromother):
          nodegroup.append(c) if pathaslist else nodegroup.add(c)

        while(c.reality.notTelomere()):
          c.reality.visited = True
          c = c.reality.desire
          c.visited = True
          oddlen = not oddlen
          if((fromother and oddlen) or (not fromother and not oddlen)):
            nodegroup.append(c) if pathaslist else nodegroup.add(c)

        c.reality.visited = True
        if(oddlen):
          oddpaths.append(nodegroup)
        else:
          evenpaths.append(nodegroup)


  def _getOtherTelomeres(self, telomeres):
    """
    Get the other telemere list.
    """
    if(telomeres == self.telomeres1):
      return self.telomeres2
    else:
      return self.telomeres1



  def _extractComponent(self, redge) -> AdjacencyGraph:
    """
    Return a graph that has a single nontrivial (more than 2 reality edges)
    connected component corresponding to the component with -redge-.
    @note: Physical interval and chromosome information is copied onto reality
           edges.
    @note: An empty gene is initialized for the non-Telomere nodes.

    @param redge: the reality edge from the component to extract
    """
    edges = list(self._getEdgesInComp(redge))
    for (n,m) in edges:            #Mark the edges in this component:
      n.mark = m.mark = True


          #Build the first genome and mark nodes appropriately:
    label = 1
    previousm = AGNode()
    previousm.nid = AGNode._NULLNODE
    genome1 = []
    chrm = []
    for (n,m) in self._getRealityEdges(self.telomeres1,self.nodes1):
      if(n.mark):
        if(n.notTelomere()):         #If it's not a telomere,
          if(previousm.nid == n.nid):#and we're at the next redge,
            n.mark = label           #then add the current label.
          else:                      #Otherwise,
            if(previousm.nid >= 0):  #if we're not the first gene seen then
              genome1.append(chrm)   #start a new chromosome,
              if(previousm.notTelomere()):
                label += 1
            chrm = [label]
            n.mark = label           #and set the new label.
          label += 1                 #Update the label.
        elif(previousm.nid >= 0):    #It is a telomere and it's not the first
          genome1.append(chrm)       #redge so create a new chromosome.
          chrm = []
          if(previousm.notTelomere()):
            label += 1

        if(m.notTelomere()):         #If the right node is not a telomere:
          m.mark = -label            #Add the label (- indicates tail).
          chrm.append(label)

        previousm = m

    if(chrm):
      genome1.append(chrm)

    #print redge
    #print genome1

          #Copy the marks to the second genome:
    for (n,m) in self._getRealityEdges(self.telomeres2, self.nodes2):
      if(n.mark):
        if(n.desire):
          n.mark = n.desire.mark
        if(m.desire):
          m.mark = m.desire.mark

    #self.printGraphCL('graph'+str(redge)+'.pdf')

          #Now build the second genome:
    genome2 = []
    chrm = []
    lastnode = None
    for (n,m) in self._getRealityEdges(self.telomeres2,self.nodes2):
      if(n.mark):
        if(not lastnode or not lastnode.mark or
           abs(lastnode.mark) != abs(n.mark)):
          if(chrm):
            genome2.append(chrm)
          if(n.isTelomere()):
            chrm = []
          else:
            chrm = [n.mark]
        if(m.notTelomere()):
          chrm.append(-m.mark)

      lastnode = m
    genome2.append(chrm)

    #print genome2

          #Now save the physical intervals:
    nodeTOinfo = {}      #Map node to physical interval and chromosome.
    for (n,m) in edges:
      nm = n.mark
      if(n.isTelomere()):
        nm = 0
      mm = m.mark
      if(m.isTelomere()):
        mm = 0
      nodeTOinfo[(n.genome,nm)] = (n.physint, n.chromosome)
      nodeTOinfo[(m.genome,mm)] = (m.physint, n.chromosome)

          #Create the graph with the component:
    g = AdjacencyGraph(GenomePair(genomes=(genome1, genome2)))

          #Copy the physical intervals:
    for e in g._getOneEdgeFromEachCC():
      for (n,m) in g._getEdgesInComp(e):
        if(n.notTelomere()):
          index = n.nid
          if(n.end == AGNode._TAIL):
            index = -n.nid
          n.gene = Gene()
        else:
          index = m.nid
          if(m.end == AGNode._TAIL):
            index = -m.nid

        n.physint = m.physint = nodeTOinfo[(n.genome,index)][0]
        n.chromosome = m.chromosome = nodeTOinfo[(n.genome,index)][1]

        if(m.notTelomere()):
          m.gene = Gene()

          #Unmark the edges:
    for (n,m) in edges:
      n.mark = m.mark = False

    #g.printGraphCL('graph'+str(redge)+'.pdf')
    return g



#}   Retrieve graph parts







#       .       .       .       .       .       .       .       .       .
#{ Output functions



  def printGraphCL(self, filename, cleanup=True):
    """
    Use DOT from the command line to print the graph to the given filename.
      - I{currently prints the circular chromosomes in either direction.}
      - I{this is the preferred way (over printGraph).}
    """
    self.makeDotFile(DOT_FILE)
    command = ['dot', '-Kneato', '-n', '-Tpdf', '-o'+filename, DOT_FILE]
    subprocess.call(command)
    if cleanup:
      os.remove(DOT_FILE)


  def makeDotFile(self, filename, splines='line'):
    """
    Make a DOT file of the graph with the given filename.
      - I{currently prints the circular chromosomes in arbitrary direction.}

    @param splines: one of {line, curved, polyline, spline}
    """
    try:
      f = open(filename, 'w')
    except IOError as e:
      sys.exit("Error opening file "+filename+': '+e)

    f.write("graph G\n\t{\n")
    f.write(f"\tsplines={splines};\n")

    x=0
    y=0
    f.write('\tsubgraph genome1\n\t\t{\n\t\trank=same;\n')
    even = True
    for n in self._getNodes(self.telomeres1, self.nodes1):
      f.write('\t\t"'+str(n)+'" [pos="'+str(x)+','+str(y)+'!"];\n')
      if(even):
        x += X_REALITY
      else:
        x += X_SEP
      even = not even
    for edge in self._getRealityEdges(self.telomeres1, self.nodes1):
      f.write('\t\t"'+str(edge[0])+'" -- "'+str(edge[1])+
              '" [ label="'+str(edge[0].physint)+'" ]\n')
    f.write('\t\t}\n')

    x=0
    y = Y_SEP
    f.write('\tsubgraph genome2\n\t\t{\n\t\trank=same;\n')
    even = True
    for n in self._getNodes(self.telomeres2, self.nodes2):
      f.write('\t\t"'+str(n)+'." [pos="'+str(x)+','+str(y)+'!"];\n')
      if(even):
        x += X_REALITY
      else:
        x += X_SEP
      even = not even
    for edge in self._getRealityEdges(self.telomeres2, self.nodes2):
      f.write('\t\t"'+str(edge[0])+'." -- "'+str(edge[1])+'." [ label="'+
              str(edge[0].physint)+'" ]\n')
    f.write('\t\t}\n')

    for edge in self._getDesireEdges():
      f.write('\t"'+str(edge[0])+'":n -- "'+str(edge[1])+'.":s\n')

    f.write('\t}')
    f.close()





  def printGraph(self, filename):
    """
    Use DOT to print the graph to the given filename.

      - I{nodes aren't places the way we want for some reason!?}
    """

    subgraph1 = pydot.Subgraph('genome1', rank='same')
    #for n in self._getNodes(self.telomeres1, self.nodes1):
    #  node = pydot.Node(str(n))
    #  node.set('pos', '1,1!')
    #  #subgraph1.add_node(pydot.Node(str(n), pos="0,0"))
    #  subgraph1.add_node(node)#and then add the edge.
    for e in self._getRealityEdges(self.telomeres1, self.nodes1):
      subgraph1.add_edge(pydot.Edge(str(e[0]), str(e[1])))

    subgraph2 = pydot.Subgraph('genome2', rank='same')
    for e in self._getRealityEdges(self.telomeres2, self.nodes2):
      subgraph2.add_edge(pydot.Edge(str(e[0])+'.', str(e[1])+'.'))

    #for n in nodes1.keys():
    #  subgraph1.add_node(pydot.Node(n, style="",fillcolor=''))
    graph = pydot.Dot(graph_type='graph')
    graph.add_subgraph(subgraph1)
    graph.add_subgraph(subgraph2)

    #for e in self._getDesireEdges():
    #  subgraph2.add_edge(pydot.Edge(str(e[0]), str(e[1])+'.'))
    #print graph.to_string()
    graph.write_pdf(filename, prog='neato')
    graph.write_dot('test.dot')






  def evalGraph(self, heatmap: Optional[CoolerHeatmap], poolsize=0,
                bychrom=False, byscenario=False, reps=1,
                slide=False, scencomp=False, logscen='',
                pseudouniform=False):
    """
    Given the heatmap in Cooler format, find stats relating the heatmap values
    to the connected components.

    @param heatmap:      the heatmap
    @param poolsize:     calculate the solution on a pool of this size
    @param bychrom:      partition the intra values by chromosome
    @param byscenario:   score the graph by scenario rather than all pairs
    @param reps:         repeat the computation this many times
    @param slide:        slide the component (per chromosome) to see where the
                         max values are
    @param scencomp:     score each component by scenario instead of all pairs
    @param logscen:      log the scenarios here
    @type  logscen:      string
    @param pseudouniform: create scenarios by sampling each individual component
                          uniformly at random 
    """
    heatmapstore = ''
    if heatmap:
      heatmapstore = heatmap.getCoolerStore()

    if(poolsize > 1):
      self._evalGraph_parG(reps, heatmapstore,
                           poolsize, bychrom, byscenario,
                           slide, scencomp, logscen, pseudouniform)
    else:
      args = zip([self]*reps, range(reps), [heatmapstore]*reps,
                 [bychrom]*reps, [byscenario]*reps, [slide]*reps,
                 [scencomp]*reps, [logscen]*reps, [pseudouniform]*reps)
      for argset in args:
        _evalGraph(argset)





  #def _printLocalityStatsHM_par(self, reps, heatmap, windowsize, jitter,
  #                              poolsize, bychrom):
  #  """
  #  Do the parallel computation of the locality stats.
  #  """
  #  widgets = ['jittering: ', progressbar.Percentage(), ' ',
  #             progressbar.Bar(marker='-'), ' ',
  #             progressbar.ETA()]
  #
  #  result = []
  #  p = Pool(poolsize)
  #  cs = (reps/poolsize)/4   #/4 to compensate for uneven workload.
  #  if(not cs):
  #    cs = 1

  #  print 'chunk size:', cs  #THERE IS SOMETHING WRONG (only 4 processors used)!
  #  print
  #  pbar = progressbar.ProgressBar(widgets=widgets, maxval=reps).start()
  #  args = zip([self]*reps, range(reps), [heatmap]*reps, [windowsize]*reps,
  #             [jitter]*reps, [bychrom]*reps)
  #  for i, value in enumerate(p.imap_unordered(_printLocalityStatHM, args), cs):
  #    if(i < reps):
  #      pbar.update(i)
  #  pbar.finish()
  #  p.close()
  #  p.join()



  def _evalGraph_parG(self, reps, heatmapstore: str,
                      poolsize, bychrom, byscenario, slide,
                      scencomp, logscen, pseudouniform, groupcomputation=False):
    """
    Do the parallel computation of the locality stats in groups.
    """
    # This was useful when the heatmaps took too long to open:
    if groupcomputation:
      mingsize = 8
      if(reps/poolsize/4 > mingsize):     #If we have a large reps to poolsize ratio,
        groupsize = ceil(reps/poolsize/4) #then make a lot of big groups.
      elif(reps/poolsize > mingsize):     #Otherwise, if there's still enough to do,
        groupsize = ceil(reps/poolsize)   #then make less groups.
      else:                               #If there's not enough to do:
        poolsize = int(ceil(float(reps)/mingsize))
        groupsize = ceil(reps/poolsize)
      numgroups = ceil(reps/groupsize)
    else:
      groupsize = 1
      numgroups = reps

    widgets = ['scenarios: ', pb.Percentage(), ' ', pb.Bar(marker='-'), ' ',
               pb.ETA()]

    sys.stderr.write('pool size:'+str(poolsize)+'\n')
    sys.stderr.write('num groups:'+str(numgroups)+'\n')
    sys.stderr.write('groups size:'+str(groupsize)+'\n\n')
    sys.stderr.flush()

    p = Pool(poolsize)

    sys.setrecursionlimit(25000)  #(unclear why max recursion depth exceeded)
    args = zip([self]*reps, range(reps), [heatmapstore]*reps, [bychrom]*reps,
               [byscenario]*reps, [slide]*reps, [scencomp]*reps,
               [logscen]*reps, [pseudouniform]*reps)
    groups = grouper(args, groupsize)

    pbar = pb.ProgressBar(widgets=widgets, maxval=numgroups).start()
    for i,_ in enumerate(p.imap_unordered(_evalGraphG, groups, 1)):
      if(i < numgroups):
        pbar.update(i)
    pbar.finish()
    p.close()
    p.join()


  #def printLocalityStatsSA(self, sa, genome1=False):
  #  """
  #  Given the self alignment dict keyed on chromosome, and valued on
  #  pairs (t1,t2) where t_ is a tuple (chrom, interval),
  #  find stats relating the existance of pairs to the paths in the graph.

  #  sa      -- the self alignment
  #  genome1 -- True if the given self alignment is for genome 1.
  #  """
  #  for redge in self._getOneEdgeFromEachCC(genome1):
  #    print
  #    print self._getRepresentativeEdge(redge)
  #    self.analyzeEdgesSA(list(self._getEdgesInCompSingleGenome(redge)), sa)



  #def printFullGraphStatsSA(self, sa, genome1=False):
  #  """
  #  Given the self alignment dict keyed on chromosome, and valued on
  #  pairs (t1,t2) were t_ is a tuple (chrom, interval),
  #  find stats for self alignments on all pairs of edges
  #  (regardless of graph structure).

  #  sa      -- the self alignment
  #  genome1 -- True if the given self alignment is for genome 1.
  #  """
  #  if(genome1):
  #    telomeres = self.telomeres1
  #    nodes = self.nodes1
  #  else:
  #    telomeres = self.telomeres2
  #    nodes = self.nodes2

  #  self.analyzeEdgesSA(list(self._getRealityEdges(telomeres, nodes)), sa)



  def printFullGraphStatsHM(self, heatmap):
    """
    Given the 2D dict heatmap keyed on begin location (chrom/begin pair),
    find stats relating the heatmap values to all pairs of edges.
    """
    rs,_ = self._aveAllPairs(list(self._getRealityEdges(self.telomeres2,
                                                        self.nodes2)),
                             heatmap)
    print(rs)








  def _getVisitedStr(self):
    "Return a string of the visited state for the nodes."
    retstr = 'Genome1:\n'
    for n in self.telomeres1:
      retstr += '\t'+str(n)+': \t'+str(n.visited)+'\n'
    for e in self.nodes1.values():
      for n in e.values():
        retstr += '\t'+str(n)+': \t'+str(n.visited)+'\n'

    retstr += '\nGenome2:\n'
    for n in self.telomeres2:
      retstr += '\t'+str(n)+': \t'+str(n.visited)+'\n'
    for e in self.nodes2.values():
      for n in e.values():
        retstr += '\t'+str(n)+': \t'+str(n.visited)+'\n'
    return retstr



  def printCoordinates(self, minlength=3, maxlength=1000):
    """
    Print the coordinates of all components with the given lengths (>= 2).
    Also print the average length and std deviation of the two.

    @param minlength: minimum length of the path we will consider.
    @param maxlength: maximum length of the path we will consider.
    """
    intervals = []
    visitedset = set()

    for redge in self._getRealityEdges(self.telomeres2, self.nodes2):
      pathlen = self._getCompLen(redge)
      if(redge[0] not in visitedset and pathlen > 1):
        for e in self._getEdgesInCompSingleGenome(redge): #Mark the nodes in
          visitedset.add(e[0])                            #this path as
          visitedset.add(e[1])                            #being visited.
          if(pathlen >= minlength and pathlen <= maxlength):
            if(e[0].gene):
              print(e[0].chromosome, e[0].physint)
              #if(e[1].gene and not self._spansNULLs(mat,e,wsize)):
              if(e[1].gene):
                intervals.append(e[0].physint)   #If not a telomere or
            else:                                #interval with NULLs, save it.
              print(e[1].chromosome, e[1].physint)
              #if(e[0].gene and not self._spansNULLs(mat,e,wsize)):
              if(e[0].gene):
                intervals.append(e[1].physint)
        if(pathlen >= minlength and pathlen <= maxlength):
          print()

    #lens = map(lambda x: abs(x[1]-x[0]), intervals)
    lens = [abs(x[1]-x[0]) for x in intervals]
    print("Average Length: "+str(np.average(lens)))
    print("Std Deviation:  "+str(np.std(lens)))



  def pickleCompToChromosomes(self, filename):
    """
    Build a dictionary mapping each nontrivial component's representative
    to a set of chromosomes.
    Pickle the dictionary to filename.
    """
    if(os.path.splitext(filename)[1] != '.pcl'):
      sys.exit('ERROR: '+filename+' does not have extension ".pcl".')

    compTOchrm = defaultdict(set)
    for redge in self._getOneEdgeFromEachCC():
      rep = str(self._getRepresentativeEdge(redge))
      for e in self._getEdgesInCompSingleGenome(redge):
        if(e[0].gene):
          compTOchrm[rep].add(e[0].chromosome)
        else:
          compTOchrm[rep].add(e[1].chromosome)

    with open(filename, 'wb') as f:
      pickle.dump(compTOchrm, f, pickle.HIGHEST_PROTOCOL)



  def getStatsStr(self, printpathlens=False):
    """
    Return a string with statistics about the graph.
    """
    (clist, oplist, eplist) = self._classifyComps(self.telomeres2, self.nodes2)

    numgenes = len(self.nodes2)
    cycles = len(clist)
    oddpaths = len(oplist)
    evenpaths = len(eplist[0])+len(eplist[1])
    retstr = (f'n = {numgenes}\nop = {oddpaths}\n'
              f'ep = {evenpaths}\nc = {cycles}\n'
              f'n - (c + op/2) = {int(numgenes-cycles-(oddpaths/2))}')

    if(printpathlens):
      Wpaths, Mpaths = eplist
      evenedges = sum(len(p) for p in chain(Wpaths,Mpaths))
      print('{} edges on this side of even length paths.'.format(evenedges))
      totaledges = sum(len(p) for p in chain(Wpaths,Mpaths,oplist,clist))
      print('{} total edges.'.format(totaledges))

    return retstr


  def getCompInfoStr(self):
    """
    Return a string with information about the chromosomes on each component.
    """
    (c, op, (wp, mp)) = self._classifyComps(self.telomeres2, self.nodes2)
    allcomps = c+op+wp+mp

          #Gather chromosome information:
    retval = '# of components per length (reality edge count in human):\n'
    compCountByLen = defaultdict(int)
    for comp in allcomps:
      compCountByLen[len(comp)] += 1

    for key in sorted(compCountByLen.keys()):
      retval += str(key)+':\t'+str(compCountByLen[key])+'\n'

          #Gather info about # of moves of each type:
    cycext = self._getNumDCJsCycleExtracts(c)
    opext = self._getNumDCJsCycleExtracts(op)
    emoves = self._getNumDCJsEPaths(wp, mp)
    retval += '\ntotal # of opening moves: '+str(cycext+opext+sum(emoves))

          #Gather chromosome information:
    retval += '\n\nchromosome info per component:\n'

    retval += 'cycles:\n'
    retval += self._getCompInfoFromSet(c)
    retval += '\nodd paths:\n'
    retval += self._getCompInfoFromSet(op)
    retval += '\nW paths:\n'
    retval += self._getCompInfoFromSet(wp)
    retval += '\nM paths:\n'
    retval += self._getCompInfoFromSet(mp)

    return retval


  def _getCompInfoFromSet(self, comps):
    """
    Return a string with info about each of the given components.
    """
    retval = ''
    for comp in comps:
      chromosomes = set()
      clist = []
      node = None
      for n in comp:
        node = n
        chromosomes.add(n.chromosome)
        clist.append(n.chromosome)

      assert node, 'Empty component encountered!'
      rep = self._getRepresentativeEdge((node, node.reality))
      clen = self._getCompLen((node, node.reality))
      if(clen > 2):
        retval += str(rep)+'['+str(clen)+']: '+','.join(chromosomes)+'\n'+\
                  '\t'+','.join(clist)+'\n'

    return retval


  def _getAveLine(self, size, totave, intrav, interv, multi='', path='',
                  intrac=0, interc=0, numzeros=0):
    """
    Return a string nicely displaying all the information.

    @param size:   the size of the data
    @param totave: the average value for both intra and inter
    @param intrav: the intra species value
    @param interv: the inter species value
    @param multi:  a string signifying a mutli chromosomal component
    @param path:   a string signifying a path
    @param intrac: a string signifying a path
    """
    rs = ("Ave["+str(size)+"]"+multi+":"+path).ljust(12)+\
         str(totave).ljust(19)+' '+\
         (str(intrav)+'['+str(intrac)+']').rjust(22)+\
         str(numzeros).center(3)+'['+str(interc)+']'+str(interv)
    return rs



  def printLocalityStatsRP(self, rps, genome1=False, poolsize=0,
                           showprogress=False, longresults=False):
    """
    Given the repeat dict keyed on chromosome.
    It is valued on I{((star,end)(class,family,name))}, find stats relating the
    existance of pairs to the connected components in the graph.

    @param rps:          the repeats
    @param genome1:      if the given repeats are for genome 1.
    @param poolsize:     if nonzero, compute on this many processors
    @param showprogress: if True, show a progressbar
    @param longresults:  if True, show a progressbar
    """
    count = 0
    cctot = fctot = nctot = 0
    for redge in self._getOneEdgeFromEachCC(genome1):
      print()
      print(self._getRepresentativeEdge(redge))

      (c, (cc,fc,nc)) = self._analyzeEdgesRP(list(self._getEdgesInCompSingleGenome(redge)), rps, poolsize, showprogress, longresults)

      count += c
      cctot += cc
      fctot += fc
      nctot += nc

    print("Totals:")
    print(str(cctot)+'/'+str(count))
    print(str(fctot)+'/'+str(count))
    print(str(nctot)+'/'+str(count))




  def printFullGraphStatsRP(self, rps, genome1=False, poolsize=0,
                            showprogress=False, longresults=False):
    """
    Given the repeat dict keyed on chromosome, and valued on
    I{((star,end)(class,family,name))},
    find stats for self alignments on all pairs of edges
    (regardless of graph structure).

    @param rps: the repeats
    @param genome1: if the given repeats are for genome 1.
    """
    if(genome1):
      telomeres = self.telomeres1
      nodes = self.nodes1
    else:
      telomeres = self.telomeres2
      nodes = self.nodes2

    self._analyzeEdgesRP(list(self._getRealityEdges(telomeres, nodes)),
                         rps, poolsize, showprogress, longresults)


  def _getCompStr(self, node):
    """
    Print the component (starting at telomere for path) for the given node.
    """
    edgeformat = '{}__{}'
    if(node.reality.isTelomere()):
      node = node.reality

      #Traverse the cycle (or search for telomere):
    n = node.reality.desire
    nodes = deque([edgeformat.format(node, node.reality)])
    while(n != node and n.reality.notTelomere()):
      nodes.append(edgeformat.format(n, n.reality))
      n = n.reality.desire
    
    if(n != node):
      nodes.append(edgeformat.format(n, n.reality))
      if(node.notTelomere()): #We're missing an end of a path.
        n = node
        while(n.notTelomere()):
          n = n.desire.reality
          nodes.appendleft(edgeformat.format(n, n.reality))

    return ' '.join(map(str, nodes))


#}   Output


#       .       .       .       .       .       .       .       .       .
#{   Helper Functions


  def _containment(self, edge, t, i):
    """
    Returns true if the first interval of the given tuple is contained in
    the interval corresponding to this edge.
    """
    return edge[0].physint[0] < t[i][1][0] and edge[0].physint[1] > t[i][1][1]

  def _spansNULLs(self, hmat, e, wsize):
    """
    Return True if the given edge spans many nulls in the heatmap matrix.
    This should correspond to missing data around the centromeres.

    @warn: NOT DONE!!!
    """
    raise(NotImplementedError)
    nonnullints = getNonNullIntsOnDiag(hmat, e[0].chromosome, wsize)
    return False

    #if(len(self._getOverlaps(e[0].physint, nonnullints)) > 1):
    #  return True

    #return False


  def _getOverlaps(self, interval, ilist):
    """
    Return the amount of overlap between the given interval and the given
    list of intervals.
    """
    overlaps = []
    for i in ilist:
      overlap = self._getOverlapInt(interval, i)
      if(overlap):
        overlaps.append(overlap)
    return overlaps
    #if(overlaps):
    #  return sum(map(lambda x: abs(x[0]-x[1]), overlaps))
    #return 0



  def _getOverlapInt(self,i,j):
    """
    Return the overlapping part of the give intervals.
    """
    if(j[0] < i[0]):      #Ensure that i is the leftmost.
      (i,j) = (j,i)

    if(j[0] < i[1]):      #If there is some overlap,
      if(i[1] < j[1]):    #and j is not nested in i:
        return (j[0],i[1])
      else:
        return (j[0],j[1])
    return None



  def _isOnPath(self, edge):
    """
    Return True if the given edge is on a path.
    """
    start = edge[0]
    if(not start.reality.desire):
      return True

    current = start.reality.desire
    while(current and start != current):
      if(not current.reality.desire):
        return True
      current = current.reality.desire
    return False




  def _resetVisited(self):
    """
    Return the visited state of all the nodes to False.
    """
    for e in chain(self.nodes1.values(), self.nodes2.values()):
      for n in e.values():
        n.visited = False

    for n in chain(self.telomeres1, self.telomeres2):
      n.visited = False


  def _getMaxLength(self, i1, i2):
    """
    Return the self alignment tuple with the max length.
    """
    if(i1[0][1][1]-i1[0][1][0] > i2[0][1][1]-i2[0][1][0]):
      return i1
    else:
      return i2

  def _getMaxScore(self, i1, i2):
    """
    Return the self alignment tuple with the max score.
    """
    if(i1[2] > i2[2]):
      return i1
    else:
      return i2


  def _filterTelomereEdges(self, wpaths):
    """
    Remove nodes from the given wpaths that are attached to a telomere edge.
    """
    return map(lambda l: filter(lambda x: x.notTelomere() and
                                          x.reality.notTelomere(), l), wpaths)


  def _isSorted(self, redge):
    """
    Return True if this reality edge is part of a length 2 cycle or
    trivial path.
    Unused.
    """
    return self._getCompLen(redge) == 2


  def _getCompLen(self, e):
    """
    Get the number of reality edges in the path/cycle with the given reality
    edge.
    """
    length = 0
    for edge in self._getEdgesInComp(e):
      length += 1
    return length



  def _getCompLenSingleGenome(self, e):
    """
    Get the number of reality edges of the path/cycle with the given reality
    edge, in the genome with the given edge.
    """
    length = 0
    for edge in self._getEdgesInCompSingleGenome(e):
      length += 1
    return length


  def _removeTelomere(self, t):
    """
    Remove the given telomere, no matter which genome it's in.
    """
    try:
      self.telomeres2.remove(t)
    except:
      try:
        self.telomeres1.remove(t)
      except:
        sys.exit('ERROR: telomere '+str(t)+' does not exist!')




#}   Helper Functions


#       .       .       .       .       .       .       .       .       .
#{ Chromosome coordinates randomization

  #def _jitterEdges(self, heatmap: Cooler):
  #  """
  #  Jitter the edges from every nontrivial connected component so that the
  #  relative distance and absolute lengths remain the same, all that changes
  #  is their positions.

  #  @warning: the indices of the physint for a node and the Gene coordinates
  #            will no longer correspond to each other
  #            (i.e. physint is updated only)!

  #  @param heatmap:    the heatmat in Cooler form
  #  """
  #  for redge in self._getOneEdgeFromEachCC():
  #    #rep = self._getRepresentativeEdge(redge)

  #               #Group the edges in the cc by chromosome:
  #    chrmTOedges = defaultdict(list)
  #    for e in self._getEdgesInCompSingleGenome(redge):
  #      if(e[0].gene):
  #        chrm = e[0].chromosome
  #      else:
  #        chrm = e[1].chromosome
  #      chrmTOedges[chrm].append(e)

  #               #Now move all of the edges one way or another for each chrom:
  #    for c in chrmTOedges.keys():
  #      elist = chrmTOedges[c]                #The edge list.
  #      elist.sort(key=lambda node: node[0].physint[0])

  #      #chromlen = (heatmat[c][c].shape[0]-1)*windowsize
  #      #chromlen = max(heatmat[c][c].keys())
  #      chromlen = heatmap.chromsizes[c]
  #      compmin = elist[0][0].physint[0]
  #      compmax = elist[-1][0].physint[1]
  #      lendiff = max(chromlen - (compmax-compmin), 0)
  #      if(lendiff < COMPONENT_WARN_THRESH):
  #        sys.stdout.write('WARNING: component spans most of chromosome '+c+\
  #                         '\n         the difference is: '+str(lendiff)+'\n'+\
  #                         '         component size:'+str(len(elist))+'\n')
  #        sys.stdout.flush()

  #      endspace = max(chromlen - compmax, 0)
  #      df=0
  #      if(endspace != 0):          #If we have space to move the edges:
  #        newloc = getRandomLocation(heatmat[c][c], windowsize)
  #        df = newloc - compmin
  #        count = 0
  #        while(compmax + df > chromlen):
  #          newloc = getRandomLocation(heatmat[c][c], windowsize)
  #          df = newloc - compmin
  #          count += 1
  #          if(count > MAX_JITTER_TRIES):
  #            raise JitterPlacement(c, chromlen, df, elist[0][0].physint,
  #                                  elist[-1][0].physint)

  #      for e in elist:
  #        newdf = df
  #        if(df == 0 and e[0].physint[0]+df > chromlen):#If interval is off the
  #          newdf = chromlen - e[0].physint[0]          #heatmap, then fix it.
  #        assert(e[0].physint[0]+newdf <= chromlen)
  #        e[0].physint=e[1].physint = (e[0].physint[0]+newdf,
  #                                     e[0].physint[1]+newdf)







  def _slideEdges(self, edges, heatmap: CoolerHeatmap, scenario):
    """
    Return a string with the average of the given heatmap over the given edges,
    for each position as the reality edges are slid across the chromosome.

    @note: the positions near the centromere won't show up.
    @note: only single chromosomal components are considered at the moment.

    @param edges:      the reality edges of the component from one genome
    @param heatmap:    the heatmap in Cooler format
    @param heatmat:    the heatmap indexed by [chrm1][chrm2][ind1][ind2]
    @param windowsize: size of the window we round to for location coordinates
    @param scenario:   score each component by scenario instead of by all pairs
    """
    windowsize = heatmap.binsize()
    shift = 1*windowsize   #The increment value for each slide.

               #Group the edges in the cc by chromosome:
    chrmTOedges = defaultdict(list)
    for e in edges:
      if e[0].gene:
        chrm = e[0].chromosome
      else:
        chrm = e[1].chromosome
      chrmTOedges[chrm].append(e)

    if len(chrmTOedges) > 1:
      return ''

               #Now sweep the edges from beginning to end:
    retval = ''
    for c in chrmTOedges.keys():
      retval += 'chromosome: '+c+'\n'
      elist = chrmTOedges[c]
      elist.sort(key=lambda node: node[0].physint[0])
      retval += 'numedges: '+str(len(elist))+'\n'

      #chrommax = max(heatmat[c][c].keys())
      chrommax = heatmap.maxbin(c)

      if scenario:
        (ave,_) = self._heatOnComponentScen(elist[0], heatmap)
      else:
        _,(ave,_,_) = self._aveAllPairs(elist, heatmap)
      retval += str(elist[0][0].physint[0])+': '+str(ave)+'\n'

      #offsets = []        #The physints stored as offsets from the left-most.
      first = elist[0][0].physint[0]
      for (n,m) in elist:
        #offsets.append((n.physint[0] - first, n.physint[1] - first))
        n.physint = m.physint = (n.physint[0]-first, n.physint[1]-first)

      while(elist[-1][0].physint[1] + shift <= chrommax+windowsize):
        if scenario:
          (ave,_) = self._heatOnComponentScen(elist[0], heatmap)
        else:
          _,(ave,_,_) = self._aveAllPairs(elist, heatmap)
        if ave:
          retval += str((elist[0][0].physint[0], ave))
        for (n,m) in elist:   #Shift all the edges:
          n.physint = m.physint = (n.physint[0]+shift, n.physint[1]+shift)

    return retval


#} Chromosome coordinates randomization









  #def analyzeEdgesSAparallel(self, edges, sa):
  #  """
  #  Print information about the given self alignment for the give edge set.
  #  Parallelize it.
  #  """
  #  edgeints = dict()
  #  edgeints = self._computeEntriesForEdgesSA(edges, sa)

  #  maxscores = []
  #  maxlengths = []
  #  count = 0
  #  for (e1,e2) in combinations(edges,2):
  #    count += 1
  #    if(edgeints[e1]):
  #      entries = self.getEntriesForEdge(e2, edgeints[e1], 1)
  #      if(entries):
  #        maxscores.append(reduce(self._getMaxScore, entries))
  #        maxlengths.append(reduce(self._getMaxLength, entries))

  #  avescore = 0
  #  if(maxscores):
  #    avescore = np.average(map(lambda x: x[2], maxscores))
  #  avelen = 0
  #  if(maxlengths):
  #    avelen = np.average(map(lambda x: x[0][1][1]-x[0][1][0],maxlengths))
  #  print str(len(maxscores))+' for '+str(count)
  #  print 'ave score:',avescore
  #  print 'ave length:',avelen



  #def _computeEntriesForEdgesSA(self, edges, sa):
  #  """ Return a dictionary of self alignment entries for the given edges."""
  #  edgeints = dict()
  #  for e in edges:
  #    g = e[0].gene
  #    if(not g):
  #      g = e[1].gene
  #    entries = self.getEntriesForEdge(e, sa['chr'+str(g.chromosome)])
  #    edgeints[e] = entries

  #  return edgeints



  #def analyzeEdgesSA(self, edges, sa):
  #  """
  #  Print information about the given self alignment for the give edge set.
  #  """
  #  edgeints = dict()
  #  for e in edges:
  #    g = e[0].gene
  #    if(not g):
  #      g = e[1].gene
  #    entries = self.getEntriesForEdge(e, sa['chr'+str(g.chromosome)])
  #    edgeints[e] = entries

  #  maxscores = []
  #  maxlengths = []
  #  count = 0
  #  for (e1,e2) in combinations(edges,2):
  #    count += 1
  #    if(edgeints[e1]):
  #      entries = self.getEntriesForEdge(e2, edgeints[e1], 1)
  #      if(entries):
  #        maxscores.append(reduce(self._getMaxScore, entries))
  #        maxlengths.append(reduce(self._getMaxLength, entries))

  #  avescore = 0
  #  if(maxscores):
  #    avescore = np.average(map(lambda x: x[2], maxscores))
  #  avelen = 0
  #  if(maxlengths):
  #    avelen = np.average(map(lambda x: x[0][1][1]-x[0][1][0],maxlengths))
  #  print str(len(maxscores))+' for '+str(count)
  #  print 'ave score:',avescore
  #  print 'ave length:',avelen





  #def getEntriesForEdgeSA(self, edge, l, interval=0):
  #  """
  #  Return a list of entries in the self alignment that are contained in
  #  this edge.

  #  edge     -- the edge
  #  l        -- the list of tuples ((chr0, (b0,e0)), (chr1, (b1,e1)), score)
  #  interval -- the interval we will check against (must be 0 or 1)
  #  """
  #  return filter(lambda t: self._containment(edge,t,interval), l)















# _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _
#{ AdjacencyGraph comparators:

  def __eq__(self, other):
    """
    Equality between graphs.
    """
    for e,f in zip(self._getRealityEdges(self.telomeres2, self.nodes2),
                   other._getRealityEdges(other.telomeres2, other.nodes2)):
      if(e[0].notRoughlyEQ(f[0]) or e[1].notRoughlyEQ(f[1])):
        print(e[0], '!=', f[0])
        return False

      if(e[0].reality.notRoughlyEQ(f[0].reality)):
        print(e[0].reality, " r!= ",  f[0].reality)
        return False

    return True




#} AdjacencyGraph comparators:





#
# _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _
# Helper functions:
#    (put here so that I could figure out to use them as a callback for
#     parallelization)
#
#{ AdjacencyGraph functions here for parallelization (is there a better way?)


def saveWanderScenario_unpack(args):
  """
  Just unpack the arguments and call saveWanderScenario using them.
  """
  ag, nummoves, sid, location, heatmat = args

  if(heatmat):
    heatmat = CoolerHeatmap(heatmat)

  ag.saveWanderScenario(nummoves, sid, location, heatmat)


def _evalGraph(arguments):
  """
  Do one group of computation for printLocalityStatsHM.
  """
  if not arguments:
    return

      #Get arguments this way because of Cython:
  (self, rep, heatmapstore, bychrom,
   byscenario, slide, scencomp, logscen, pseudouniform) = arguments
  
  #assert heatmapstore, 'ERROR: heatmapstore is undefined!'
  #heatmap = CoolerHeatmap(heatmapstore)
  heatmap = CoolerHeatmap(heatmapstore) if heatmapstore else None


      #Now do the analysis:
  out = ''
  ag = self

  if slide:                                 #Slide the edges along the chrom:
    sys.setrecursionlimit(25000)     #(avoid "max recursion depth exceeded")
    ag: AdjacencyGraph = copy.deepcopy(self)#Make a deep copy of this.
    for redge in ag._getOneEdgeFromEachCC():
      out += '\n'+str(ag._getRepresentativeEdge(redge))+'\n'
      out += ag._slideEdges(list(ag._getEdgesInCompSingleGenome(redge)),
                            heatmap, byscenario)+'\n'

  elif byscenario:                          #Score scenarios:
    (both, intra, inter) = ag.sampleRandomScenario(heatmap,
                                                   logto=logscen, sid=str(rep),
                                                   pseudouniform=pseudouniform)
    if both:
      out += '(scenarios)\n'
      out +=  self._getAveLine(0, both, intra, inter)

  elif scencomp:                            #Compute scenarios on components:
    for redge in ag._getOneEdgeFromEachCC():
      out += '\n'+str(ag._getRepresentativeEdge(redge))+'\n'
      (_,st) = ag._heatOnComponentScen(redge, heatmap)
      out += st

  else:                                     #Compute all pairs of edges:
    for redge in ag._getOneEdgeFromEachCC():
      out += '\n'+str(ag._getRepresentativeEdge(redge))+'\n'
      rs,_ = ag._aveAllPairs(list(ag._getEdgesInCompSingleGenome(redge)),
                             heatmap, bychrom)
      out += rs


      #And output the result:
  if out:
    printnow(out)




def _evalGraphG(groups):
  """
  Do one round of computation for printLocalityStatsHM by groups.
  """
  #map(_printLocalityStatHM, groups)
  for argset in groups:
    _evalGraph(argset)
  return True




def _getRepeatsForEdgeRP(arguments):
  """
  Return a list of entries in the repeats that are contained in this edge.

    - edge: the edge
    - l: the list of tuples ((start, end), (class, family, name))
  """
  (edge, l) = arguments   #For Cython.
  return (edge, filter(lambda t: _containmentRP(edge,t), l))

def _containmentRP(edge, t):
  """
  Returns true if the first interval of the given tuple is contained in
  the interval corresponding to this edge.
  """
  return edge[0].physint[0] < t[0][0] and edge[0].physint[1] > t[0][1]



def _getMatchingPairs(rpts1, rpts2, label=CLASS):
  """
  Return a list of pairs that have the same label (e.g. class, family, name).
  """
  labelTOrpt = defaultdict(list)  #Maps labels to repeats.
  for r in rpts1:
    (interval, labels) = r
    labelTOrpt[labels[label]].append(r)

  pairs = []                      #The list of repeat pairs.
  for r in rpts2:
    (interval, labels) = r
    if(labels[label] in labelTOrpt):
      for pair in product([r], labelTOrpt[labels[label]]):
        pairs.append(pair)

  return pairs





def _hasMatch(rpts1, rpts2, label=CLASS):
  """
  Return 1 if there is a pair of matching repeat that have the same label
  (e.g. class, family, name), otherwise 0.
  """
  rpts = set((r[1][label] for r in rpts1))

  for r in rpts2:
    (interval, labels) = r
    if(labels[label] in rpts):
      return 1

  return 0



def _scoreEdgePairs(pairs):
  """
  For the given list of pairs of edges, return a list of tuples of the form
  (classmatch, familymatch, namematch), where match is 1 if there is a matching
  repeat on the pair of edges.
  """
  result = []
  for pair in pairs:
    if(pair):
      result.append(_scoreEdgePair(pair))
  return result


def _scoreEdgePair(pair):
  """
  For the given pair of edges, return a tuple
  (classmatch, familymatch, namematch),
  that represents the matching repeats shared on the edges.
  """
  global edgeTOrpts

  (e1, e2) = pair
  if(edgeTOrpts[str(e1)] and edgeTOrpts[str(e2)]):
    cc = _hasMatch(edgeTOrpts[str(e1)], edgeTOrpts[str(e2)], CLASS)
    fc = _hasMatch(edgeTOrpts[str(e1)], edgeTOrpts[str(e2)], FAMILY)
    nc = _hasMatch(edgeTOrpts[str(e1)], edgeTOrpts[str(e2)], NAME)
  else:
    return (0,0,0)

  return (cc,fc,nc)


def _scoreEdgePairsLong(pairs):
  """
  For the given list of pairs of edges, return a list of tuples
  ((classcount, classlen), (familycount, famlen), (namecount,nlen)), where
  count is the number of matching repeat pairs that are found on the pair of
  edges, and len is the total length of those repeats.
  """
  result = []
  for pair in pairs:
    if(pair):
      result.append(_scoreEdgePairLong(pair))
  return result


def _scoreEdgePairLong(pair):
  """
  For the given pair of edges, return a tuple
  ((classcount, classlen), (familycount, famlen), (namecount,nlen))
  that represents the matching repeats shared on the edges.
  """
  global edgeTOrpts

  cc = 0
  fc = 0
  nc = 0
  lenc = 0
  lenf = 0
  lenn = 0
  (e1, e2) = pair
  if(edgeTOrpts[str(e1)] and edgeTOrpts[str(e2)]):
    classes = _getMatchingPairs(edgeTOrpts[str(e1)],edgeTOrpts[str(e2)], CLASS)
    families = _getMatchingPairs(edgeTOrpts[str(e1)],edgeTOrpts[str(e2)],FAMILY)
    names = _getMatchingPairs(edgeTOrpts[str(e1)], edgeTOrpts[str(e2)], NAME)
    if(len(classes)):
      cc = 1
      lenc = len(classes)
    if(len(families)):
      fc = 1
      lenf = len(families)
    if(len(names)):
      nc = 1
      lenn = len(names)

  return ((cc, lenc), (fc, lenf), (nc, lenn))


#} AdjacencyGraph function here for parallelization (is there a better way?)


# _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _
# _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _
class AGNode(object):
  """
  Adjacency graph node.
  It keeps the nodes connected to it by a reality and desire edge, the
  corresponding gene information, and the chromosome physical location
  interval information for the adjacency represented by the reality edge.
  """
  _TAIL = T_GeneEnd(-1)         #: Indicates a tail.
  _TEL = T_GeneEnd(0)           #: Indicates a telomere.
  _HEAD = T_GeneEnd(1)          #: Indicates a head.
  _NULLNODE = T_NodeID(-1)
  _COUNTER = 0                  #: Counter for unique node naming.


  def __init__(self, nid=0, end=_TEL, gene=None, chromosome=None,
               physint: Tuple[int, int]=None, genome=0):
    self.nid = T_NodeID(abs(nid)) #:Shared id of the node (absolute value of gene id, telomeres have none).
    self.end = end         #:The end of the node (tail or heads or telomere).
    self.gene = gene       #:The gene the node represents an extremity of.
    self.genome = genome   #:The genome the node belongs to.

    self.reality: AGNode = None #:The node connected to this by a reality edge.
    self.desire: AGNode = None  #:The node connected to this by the desire edge.

    self.physint: Tuple[int, int] = physint #:The interval represented by the reality edge.
    if(gene):
      self.chromosome = gene.chromosome #:Chromosome (maintained through DCJs).
    else:
      self.chromosome = chromosome

    self.visited = False        #:Marks whether this has been visited or not.
    self.mark: Any = None       #:Used to mark this node with some information.

    self.setUID()


  def setUID(self):
    "Assign this node a new unique ID."
    self.uniqueid = AGNode._COUNTER
    AGNode._COUNTER += 1

  def otherEnd(self):
    "Return the opposite of -end-.  If -end- is _TEL then return _TEL."
    return self.end*-1

  def getEdgeLen(self):
    "Return the physical length of the reality edge attached to this node."
    if(self.isTelomere() or self.reality.isTelomere()):
      return None
    return self.physint[1] - self.physint[0]


  def isTelomere(self):
    return self.end == self._TEL

  def notTelomere(self):
    return self.end != self._TEL


  def notRoughlyEQ(self, other):
    return (hash(repr(self)+str(self.physint)) !=
            hash(repr(other)+str(self.physint)))

  def __hash__(self):
    return hash(repr(self))

  def __repr__(self):
    markstr = ''
    if(self.mark):
      markstr = ' '+str(self.mark)

    if(self.end == AGNode._TAIL):
      endstr = 't'
    elif(self.end == AGNode._HEAD):
      endstr = 'h'
    else:           #telomere
      endstr = 't'
      if(not self.reality):
        nodestr = 'NULL'
      else:
        nodestr = str(self.reality.nid)

      if(self.reality and self.reality.end == AGNode._HEAD):
        endstr = 'h'
      return 'o'+nodestr+str(endstr)+'o'+markstr

    return str(self.nid)+endstr+markstr

  def __eq__(self, other):
    if isinstance(other, AGNode):
      return repr(self) == repr(other)

    return NotImplemented

  def __le__(self, other):
    if isinstance(other, AGNode):
      selfr, otherr = repr(self), repr(other)
      return selfr <= otherr

    return NotImplemented

  def __gt__(self, other):
    return not self <= other

  def __ge__(self, other):
    return other <= self

  def __lt__(self, other):
    return not self >= other


# _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _
# _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _
class JitterPlacement(Exception):
  def __init__(self, chrm, chrmlen, df, begint, endint):
    self.chrm = chrm
    self.chrmlen = chrmlen
    self.df = df
    self.begint = begint
    self.endint = endint

  def __str__(self):
    return 'Cannot place the jittered component!\n'+\
           'chromosome: '+str(self.chrm)+'\n'+\
           'chromlen: '+str(self.chrmlen)+'\n'+\
           'df: '+str(self.df)+'\n'+\
           'extremities: '+str(self.begint)+','+str(self.endint)


# _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _
# _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _
def getAverages(physints, heatlist):
  """
  Return the average value for both, intra, and inter moves.

  @param physints: list of chromosome/index pairs.

  """
  assert(len(physints) == len(heatlist))
  intra = []
  inter = []
  for i, ((c1, i1), (c2, i2)) in enumerate(physints):
    if(c1 == c2):
      intra.append(heatlist[i])
    else:
      inter.append(heatlist[i])

  return np.average(heatlist), np.average(intra), np.average(inter)


# _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _
# TYPES:

T_Comp = Collection[AGNode]
