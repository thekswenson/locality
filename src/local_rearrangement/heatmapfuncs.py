#!/usr/bin/env python3
# Krister Swenson
"""
Functions to handle heatmaps.
"""
from pathlib import Path
import sys
import os
os.environ['NUMEXPR_MAX_THREADS'] = '4'   #Prevent numexpr.utils message.

import pickle
import re
import random
import gzip
import progressbar
import gc
import functools
import seaborn as sns
import pandas as pd
import numpy as np
import cooler.fileops as cfileops
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

from matplotlib.colors import colorConverter
from pandas import Series
from typing import Iterator, List, Optional, Tuple
from glob import iglob, glob
from collections import defaultdict as defdict
from itertools import combinations, product, combinations_with_replacement
from scipy.sparse import coo_matrix
import cooler.util as cutil
from cooler import Cooler
from cooler.create import ArrayLoader
from cooler import create_cooler


from .heatmap import Heatmap, T_PairHeatmaps, T_NPHeatmaps
from .types import T_IntervalPair, T_HMCoord, T_Pair, T_Chrom

from pyinclude.usefulfuncs import printnow




NONNULL_DIST = 10          #Search this many windows for a non-NULL value
COMMENT_CHAR = '#'         #Comment character for the matrix file.
NULLV = 'NULL'



# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Types:



# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#{   Cooler (the new mirnylab way to store heatmaps)

class CoolerHeatmap:
  """
  A simple wrapper to store an opened Cooler heatmap, along with some flags for
  choosing which weighting scheme to use when accessing the Hi-C matrix.
  """
  def __init__(self, heatmaploc: 'Path|str', windowsize=0, usebalanced=True,
               verbose=True):
    """
    Construct a CoolerHeatmap object. If usebalanced is true, and there is
    a weight column in the cooler bins, then they will be used when accessing
    the matrix.
    """
    self.cooler = getCooler(str(heatmaploc), windowsize, verbose)
    self.usebalanced = usebalanced   #:Use balancing (doubly-stochastic) weights

    if usebalanced and 'weight' not in self.cooler.bins():
      if verbose:
        printnow('WARNING: No balancing weights in the cooler while '
                 'usebalace=True.')
      self.usebalanced = False


  def getMatrix(self, selector: Optional['Tuple|str']=None,
                allownoweights=False,
                sparse=False) -> np.ndarray:
    """
    Return the matrix of the given cooler. If usebalanced is True, then use the
    weights from the matrix, unless there are no weights in the cooler.

    @param selector: the string or tuple to select the part of the matrix
                     (https://cooler.readthedocs.io/en/latest/concepts.html#data-selection)
    @param allownoweights: if True, open the matrix even if usebalanced==True,
                           and there are no balancing weights in the cooler.
    @param sparse: if True, return a scipy.sparse.coo_matrix instead of ndarray
    """
    usebalanced = self.usebalanced
    if usebalanced and 'weight' not in self.cooler.bins():
      if allownoweights:
        usebalanced = False
      else:
        raise(Exception('No balancing weights in the cooler.'))
  
    if selector:
      if isinstance(selector, str) or len(selector) == 1:
        return self.cooler.matrix(balance=usebalanced, sparse=sparse).fetch(selector)
      else:
        assert len(selector) == 2, f'Bad selector: {selector}'
        return self.cooler.matrix(balance=usebalanced, sparse=sparse).fetch(*selector)

    return self.cooler.matrix(balance=usebalanced, sparse=sparse)[:]


  def getMatrices(self) -> T_NPHeatmaps:
    """
    Return a 2D dictionary mapping chromsome to chromosome to the matrix.
    The interchromosomal matrices are indexed by lexicographical order.
    """
    cTOcTOm: T_NPHeatmaps = defdict(dict)
    for c1, c2 in combinations_with_replacement(self.chromnames(), 2):
      if c1 > c2:       #Ensure lexicographical order on chromosome names.
        c1, c2 = c2, c1
      cTOcTOm[c1][c2] = self.getMatrix((c1, c2))
      #cTOcTOm[c2][c1] = cTOcTOm[c1][c2].T
  
    return cTOcTOm


  def shape(self):
    return self.cooler.shape


  def chromsizes(self):
    return self.cooler.chromsizes


  def chromnames(self) -> List[T_Chrom]:
    return self.cooler.chromnames


  def binsize(self) -> int:
    """ Get the window size. """
    assert self.cooler.binsize, 'No binsize found in the cooler.'
    return self.cooler.binsize


  def maxbin(self, chrom: str) -> int:
    """
    Return the maximum bin index for the given chromosome.
    """
    return self.cooler.bins()['start'].fetch(chrom).max() #type: ignore


  def nnz(self, selector: 'Tuple|str'):
    """
    Get the number of non-zero elements in the given part of the matrix.
    The selector can be any string or tuple as described here:
      https://cooler.readthedocs.io/en/latest/concepts.html#matrix-selector
    """
    if isinstance(selector, str) or len(selector) == 1:
      return self.cooler.matrix(sparse=True, balance=False).fetch(selector).nnz
    else:
      assert len(selector) == 2, f'Bad selector: {selector}'
      return self.cooler.matrix(sparse=True, balance=False).fetch(*selector).nnz


  def getCoolerStats(self, printstats=False):
    """
    Given a Cooler, return the pair of averages (intra, inter).
    Print various stats if printstats is True.
    """
    largest_intra = 0
    largest_inter = 0

    intraaves: List[Tuple[float, int]] = []   #(average, multiplicity)
    interaves: List[Tuple[float, int]] = []   #(average, multiplicity)

    matrix = self.cooler.matrix(sparse=True, balance=False)
    for chrm in self.cooler.chromnames:
      m: coo_matrix = matrix.fetch(chrm)
      largest_intra = max(largest_intra, m.max())
      intraaves.append((m.mean(), m.shape[0]*m.shape[1]))

    for c1, c2 in combinations(self.cooler.chromnames, 2):
      m: coo_matrix = matrix.fetch(c1, c2)
      largest_inter = max(largest_inter, m.max())
      interaves.append((m.mean(), m.shape[0]*m.shape[1]))

    bothsum = 0
    intracount = 0
    if largest_intra:
      sum = 0
      for v, r in intraaves:
        sum += v * r
        intracount += r

      aveintra = sum / intracount
      bothsum += sum
    else:
      aveintra = 0

    intercount = 0
    if largest_inter:
      sum = 0
      for v, r in interaves:
        sum += v * r
        intercount += r

      aveinter = sum / intercount
      bothsum += sum
    else:
      aveinter = 0

    avetotal = 0
    if intracount or intercount:
      avetotal = bothsum / (intracount + intercount)

    if printstats:
      printnow(f'largestintra:{largest_intra}')
      printnow(f'largestinter:{largest_inter}')
      printnow(f'intracount:{intracount}')
      printnow(f'intercount:{intercount}')
      printnow(f'mean intra: {aveintra}')
      if largest_inter:
        printnow(f'mean inter: {aveinter}')

    return avetotal, aveintra, aveinter


  def getHeatmapStats(self, printstats=False, perchm=False):
    """
    Given a Cooler, return the pair of averages (intra, inter).
    Print various stats if printstats is True.
    """
    assert self.cooler.info['bin-type'] == 'fixed'

    largestinter = 0
    largestintra = 0
    chrmTOheat = defdict(list)
    intraheats = []   #For computing the median **inefficiently added**
    interheats = []
    for c1, c2 in product(self.cooler.chromnames, repeat=2):
      submat = self.cooler.matrix(balance=self.usebalanced).fetch(c1, c2)
      for (i,j), heat in np.ndenumerate(submat):
        if(heat is not np.nan):
          if(c1 == c2):                 #If the chromosomes are the same:
            if(largestintra < heat):
              largestintra = heat
            intraheats.append(heat)

            if(perchm):
              chrmTOheat[c1].append(heat)
          else:                               #Otherwise:
            if(largestinter < heat):
              largestinter = heat
            interheats.append(heat)

    if(intraheats):
      aveintra = np.nanmean(intraheats)
    else:
      aveintra = 0
    if(interheats):
      aveinter = np.nanmean(interheats)
    else:
      aveinter = 0

    avetotal = np.mean(intraheats+interheats)

    if(printstats):
      printnow('largestintra:'+str(largestintra))
      printnow('largestinter:'+str(largestinter))
      printnow('intracount:'+str(len(intraheats)))
      printnow('intercount:'+str(len(interheats)))
      printnow('mean/dev intra: {} {}'.format(aveintra, np.nanstd(intraheats)))
      if(interheats):
        printnow('mean/dev inter:{} {}'.format(aveinter, np.nanstd(interheats)))
      printnow('median intra:'+str(np.nanmedian(intraheats)))
      if(interheats):
        printnow('median inter:'+str(np.nanmedian(interheats)))

      if(perchm):
        for c in chrmTOheat.keys():
          printnow(c+": "+str(np.nanmean(chrmTOheat[c])))

    return avetotal, aveintra, aveinter


  def getCoordinate(self, c1, int1: T_Pair, c2, int2: T_Pair,
                    choicef=np.nanargmax) -> Tuple[float, T_HMCoord]:
    """
    Get the heatmap coordinates (chromosome/coordinate pair) for the given
    intervals.
    Return the value and the coordinate dictated by the choicef function.
  
    @param heatmap:    the heatmap
    @param c1:         the first chromosome
    @param int1:       the first interval
    @param c2:         the second chromosome
    @param int2:       the second interval
    @param choicef:    the function used to choose the coordinate
    @param usebalanced:use the weights computed from `cooler balance`
    """
      #Get the submatrix of interest:
    try:
      submat = self.cooler.matrix(balance=self.usebalanced).fetch((c1, *int1),
                                                                  (c2, *int2))
    except ValueError as e:
      message = ''
      if int1[1] == 0 or int2[1] == 0:
        message = (f'Warning: found an interval that ends at 0. This probably'
                   f' means that you created scenarios without giving a'
                   f' heatmap, which gives us the chromosome lengths.\n')
      raise(Exception(f'{message}Error: bad coordinates given:\n'
                      f'{(c1, int1), (c2, int2)}\n{e}'))

    try:
      i, j = np.unravel_index(choicef(submat), submat.shape)
    except ValueError:  #All nans encountered: return the upper left corner:
      i, j = 0, 0
      #if(abs(self.cooler.chromsizes[c1] - int1[1]) < 100000 or
      #   abs(self.cooler.chromsizes[c2] - int2[1]) < 100000):
      #  printerr(f'All nans: {c1, int1, c2, int2} {self.cooler.chromsizes[c1]} '
      #           f'{self.cooler.chromsizes[c2]}')
      #
      #return np.nan, EMPTY_COORD
  
      #Get the corresponding bins:
    bins1 = self.cooler.bins()['start'].fetch((c1, *int1))
    bins2 = self.cooler.bins()['start'].fetch((c2, *int2))
  
    return submat[i, j], ((c1, bins1.iloc[i]), (c2, bins2.iloc[j]))


  def getCoolerStore(self) -> str:
    """
    Return the string used to open the cooler.
    """
    return f'{self.cooler.filename}::{self.cooler.root}'


  floattypes = (np.float128, np.float64, np.float32)
  def iterIntra(self, asfloat=False) -> Iterator[Tuple[str, np.ndarray]]:
    """
    Iterate over the intra-chromosomal heatmaps.
    If asfloat is True, then convert the matrix to a float matrix, if it is not
    already.
    """
    for c in self.cooler.chromnames:
      m = self.getMatrix((c, c))
      if asfloat and m.dtype not in CoolerHeatmap.floattypes:
        m = m.astype(np.float128)

      yield c, m


  def iterInter(self, asfloat=False) -> Iterator[Tuple[Tuple[str, str],
                                                       np.ndarray]]:
    """
    Iterate over the inter-chromosomal heatmaps.
    If asfloat is True, then convert the matrix to a float matrix, if it is not
    already.
    """
    for c1, c2 in combinations(self.cooler.chromnames, 2):
      m = self.getMatrix((c1, c2))
      if asfloat and m.dtype not in CoolerHeatmap.floattypes:
        m = m.astype(np.float64)

      yield (c1, c2), m


  def plotScoreByDistance(self, filename: Path):
    """
    For each intrachromosomal matrix, make a box plot that organizes the
    Hi-C scores by distance. The horizontal axis is distance, and the vertical
    axis is the score.
    """
      #Plot all chromosomes on the same figure, aligned horizontally:
    sns.set(style='whitegrid')
    fig, ax = plt.subplots(ncols=len(self.chromnames()),
                           figsize=(10*len(self.chromnames()), 10),
                           sharey=True, sharex=True)
    for ci, (c, m) in enumerate(self.iterIntra()):
      scores = []
      for i in range(m.shape[0]):
        for j in range(i+1, m.shape[1]):
          scores.append((abs(i-j), m[i,j]))

      df = pd.DataFrame(scores, columns=['distance', 'score'])
      sns.boxplot(x='distance', y='score', data=df, ax=ax[ci])

      ax[ci].set_yscale('asinh')

    plt.savefig(f'{filename}_scoreByDistance.pdf')
    plt.close()


def getCooler(heatmaploc:str, windowsize=0, verbose=True) -> Cooler:
  """
  Open the given Cooler

  @param heatmaploc: read from this heatmap directory
  @param windowsize: size of the heatmap window. if unspecified, then use the
                     smallest size possible

  @return: Cooler containing matrices (see https://cooler.readthedocs.io)
  """
  if cfileops.is_cooler(heatmaploc):
    c = Cooler(heatmaploc)
    if windowsize and c.binsize != windowsize:
      raise(Exception(f'Give window size {windowsize} mismatch with cooler '
                      f'({c.binsize}).'))
    return c

  elif cfileops.is_multires_file(heatmaploc):
    pref = '/resolutions/'
    resolutions = []
    for c in cfileops.list_coolers(heatmaploc):
      if c[:13] != pref:
        raise(Exception(f'Uknown cooler prefix: "{pref}".'))
      else:
        resolutions.append(int(c[13:]))

    if windowsize:
      if windowsize not in resolutions:
        raise(Exception(f'Window size {windowsize} not in cooler ({resolutions}).'))

      return Cooler(f'{heatmaploc}::{pref}{windowsize}')

    if verbose:
      printnow(f'No window/bin resolution specified, using {min(resolutions)}.')
    return Cooler(f'{heatmaploc}::{pref}{min(resolutions)}')

  raise(Exception(f'Input heatmaps "{heatmaploc}" must be in cooler format.'))


def saveMatrixAsCooler(outfile: 'Path|str', matrix: np.ndarray,
                       chromsizes: Series, winsize: int):
  """
  Create a cooler with the given matrix.
  """
  bins = cutil.binnify(chromsizes, winsize)
  pixels = ArrayLoader(bins, matrix, chunksize=10000000)

  dtypes = {}
  if matrix.dtype in CoolerHeatmap.floattypes:
    dtypes = {'count': 'float'}
  create_cooler(str(outfile), bins, pixels, dtypes=dtypes)

  #ctmp = CoolerHeatmap(str(outfile), winsize, False)
  #print(f'shape: {ctmp.shape()}')
  #print(ctmp.getMatrix())

#}   Cooler

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

#{   I/O

def getHeatmap(heatmaploc, usepickle=None, windowsize='',
               cellline='', datastr='') -> T_PairHeatmaps:
  """
  General function to unpickle or read in a heatmap matrix.
  If heatmaploc is given and usepickle is given, then read the heatmaps and
  then pickle them, otherwise read a pickled heatmap.

  @param heatmaploc: read from this heatmap directory if nonempty.
  @param usepickle:  if this is nonempty then pickle the heatmap or read a
                     pickled heatmap.
  @param windowsize: size of the heatmap window (needed if not unpickling).
  @param cellline:   the cell line (used in the filenames only)
  @type cellline:    string
  @param datastr:    the type of data (used in the filenames only)
  @type datastr:     string

  @return: a dict with chromosome pairs as keys and Heatmap objects as values.
  @rtype: dict of pairs of str to Heatmap
  """
  if(heatmaploc):
    printnow('reading HiC data...')
    if(not os.path.exists(heatmaploc)):
      sys.exit("ERROR: heatmap dir '"+heatmaploc+"' not found.")
    sys.stdout.flush()
    heatmap = readHeatmaps(heatmaploc, windowsize, cellline, datastr)
    if(usepickle):
      with open(usepickle, 'wb') as f:
        printnow('pickling HiC data...')
        pickle.dump(heatmap, f, pickle.HIGHEST_PROTOCOL)
  else:
    if(usepickle):
      if(os.path.isfile(usepickle)):
        printnow('unpickling HiC data...')
        sys.stdout.flush()
        with open(usepickle, 'rb') as f:
          heatmap = pickle.load(f)
      else:
        sys.exit("ERROR: pickle file '"+usepickle+"' not found.")
    else:
      sys.exit("ERROR: no input or pickle file given.")

  if(not heatmap):
    raise(Exception('No heatmaps found at "{}".'.format(heatmaploc)))
  return heatmap




def readHeatmaps(dataloc, windowsize='', cellline='', datastr='') -> T_PairHeatmaps:
  """
  Read in the HiC data in the given location.

  @note: if the given directory contains gz files, then it is assumed that we're
         dealing with data from the Ren lab:
         U{http://chromosome.sdsc.edu/mouse/hi-c/download.html}
  @note: look for files starting with "HIC_" and cellline and windowsize info,
         if those don't exist then just take all the .txt files in the dir.

  @param dataloc:    the directory with the data
  @param windowsize: the size of the window
  @param cellline:   the cell line (used in the filenames only)
  @param datastr:    the type of data (used in the filenames only)

  @return: dict with chromosome pairs as keys and Heatmap objects as values.
  @rtype:  dict of pairs of str to Heatmap
  """
  global COMMENT_CHAR

  gzipfiles = list(iglob(dataloc+'/*.gz'))
  if(gzipfiles):                       #If this is Ren's data:
    raise NotImplementedError          #NOTE: readRenHeatmaps returns a strange format
    return readRenHeatmaps(gzipfiles)  #then get the info in that format.

  files = glob(dataloc+'/HIC_'+cellline+'*'+str(windowsize)+'_'+datastr+'*.txt')
  if not files:
    files = glob(dataloc+'/*.txt')

  matrices: T_PairHeatmaps = dict()  # The matrices, indexed by chromosome
  for matfile in files:
    try:
      f = open(matfile, 'r')
    except IOError as e:
      sys.exit("Error opening file {}: {}".format(matfile, e))

            #Get the first line:
    line = f.readline()
    if(line[0] == COMMENT_CHAR):  #Ignore it if it's a comment.
      line = f.readline()

    chrom_col = ''
    chrom_row = ''

    # Get the chromosomes
    fields = line.strip().split('\t')
    chrom_col = fields[1].split('|')[2].split(':')[0]  # Why 1? Why not?
    if 'chr' in chrom_col:
      chrom_col = chrom_col[3:]

    line = f.readline()
    fields = line.strip().split('\t')
    chrom_row = fields[0].split('|')[2].split(':')[0]
    if 'chr' in chrom_row:
      chrom_row = chrom_row[3:]

    # Get the window size
    if not windowsize:
        temp = fields[0].split('|')[2].split(':')[1]
        windowsize = int(temp[2:])
        if temp[-1] == '9':
            windowsize += 1

    matrices[(T_Chrom(chrom_row), T_Chrom(chrom_col))] =\
      Heatmap(chroms=(chrom_row, chrom_col), window_size=windowsize)

    f.close()
    matrices[(T_Chrom(chrom_row), T_Chrom(chrom_col))].load_from_lieb(matfile)

  return matrices



def readRenHeatmaps(files):
  """
  Return a dictionary keys on (chromosome, location) pairs with the heat at
  that location.
  This is for files of the format from the Ren lab.

  @rtype: map pair to pair to heat value
  """
  matrix = defdict(dict)      #The matrix, indexed by (chromosome, index).

  widgets = ['reading data: ', progressbar.Percentage(), ' ',
             progressbar.Bar(marker='-'), ' ',
             progressbar.ETA()]
  pbar = progressbar.ProgressBar(widgets=widgets, maxval=len(files))
  pbar.start()
  winsize = 0
  for i,fname in enumerate(files):
    pbar.update(i)
    with gzip.open(fname) as f:
      chrm = ''
      for line in f:
        entries = line.rstrip().lstrip().split()
        if(not chrm):
          chrm = entries[0].replace('chr', '')
          winsize = int(entries[2])

        beg = int(entries[1])
        end = int(entries[2])
        for col,heat in enumerate(entries[3:]):
          heat = float(heat)
          if(heat):
            assert(winsize)
            if((chrm,col*winsize) in matrix and
               (chrm,beg) in matrix[(chrm,col*winsize)]):
              if(matrix[(chrm,col*winsize)][(chrm,beg)] != heat):
                sys.stderr.write("WARNING: asymmetric HiC values in heatmap.\n")
            else:
              matrix[(chrm, beg)][(chrm, col*winsize)] = heat
              matrix[(chrm, col*winsize)][(chrm, beg)] = heat
  pbar.finish()
  return matrix



def readHeatmapAsMatrix(matfile, nullval=None):
  """
  Read in the given Lieberman-Aiden style heatmap as a numpy matrix.

  @param matfile: the file with the matrix in LA format.
  @param nullval: the value to replace NULL cells with.

  @return: (matrix, rowchrom, colchrom,  header, rownames, colnames)
  @rtype:  (numpy array, string, string, string, list(string), list(string))
  """
  matrix = list(list())
  rownames = []
  with open(matfile, 'r') as mf:
    line1 = mf.readline().rstrip()                  #The header.
    if(line1[0] == COMMENT_CHAR):
      header = line1
      colnames = mf.readline().split()              #The column names.
    else:
      header = ''
      colnames = line1.split()

    for line in mf:
      if(line.isspace()):
        continue
      row = line.lstrip().rstrip().split('\t')
      rownames.append(row[0])
      matrix.append([float(v) if v != NULLV else nullval for v in row[1:]])

  npa = np.array(matrix)

  _,_,rchr,_,_ = parseName(rownames[0])
  _,_,cchr,_,_ = parseName(colnames[0])

  return npa, rchr, cchr, header, rownames, colnames




def writeMatrix(mat, filename, header, columnnames, rowsnames):
  """
  Write the numpy matrix to the given file.

  @param mat:         the numpy matrix
  @param filename:    write to this name
  @param header:      the first line of the file
  @param columnnames: list of column names
  @param rowsnames:   list of row names
  """
  with open(filename, 'w') as outfile:
    outfile.write(header)
    outfile.write('\n')
    outfile.write('\t')
    outfile.write('\t'.join(columnnames))
    outfile.write('\n')
    for i,row in enumerate(mat):
      line = [rowsnames[i]]
      line.extend(NULLV if e is None or np.isnan(e) else str(e) for e in row)
      outfile.write('\t'.join(line))
      outfile.write('\n')


def printHeatmap(mat:np.ndarray, name='heatmap', blur=False, filetype='PDF',
                 mapcolors='hot', axislabels=[], labelstep=10, title='',
                 normalize=False, maskmat=None, maskcols=['cyan','cyan']):
  """
  Make an image of the given heatmap.

  @param mat:        the matrix
  @param name:       the plot name
  @param blur:       blur the squares in the heatmap
  @param filetype:   the output filetype (e.g. 'PDF', 'PNG', 'JPG', etc.)
  @type  filetype:   string
  @param mapcolors:  the color scheme of the heatmap
  @type mapcolors:   color scheme shown at (e.g. jet, hot)
                     U{http://wiki.scipy.org/Cookbook/Matplotlib/Show_colormaps}
  @param axislabels: list of axis labels, one for each row/column
  @param labelstep:  label every labelstep entries
  @param title:      the title of the heatmap
  @param normalize:  normalize the heatmap by cutting the topvalues off
  @param maskmat:    matrix with mask values (the higher the more intense)
  @type maskmat:     matrix with same dimensions as L{mat}.
  @param maskcols:   the colors of the mask
  @type maskcols:    list of strings with valid html color or hex string
  """
  if normalize:
    ceiling = np.nanmedian(mat.flat)+(2*np.nanstd(mat.flat))
    count = 0
    for x in np.nditer(mat, op_flags=['readwrite']):
      if x > ceiling:
        x[...] = ceiling
        count += 1

  if blur:
    plt.imshow(mat, cmap=mapcolors)
    plt.colorbar()
  else:
    plt.imshow(mat, interpolation='none', cmap=mapcolors)
    plt.colorbar()

    if maskmat is not None:
      cols = [colorConverter.to_rgba(mc) for mc in maskcols]
      amap = mpl.colors.LinearSegmentedColormap.from_list('myalpha', cols, 256)
      amap._init()       #Create the _lut array, with rgba values.
      #alphas = np.linspace(0.33, 1, amap.N+3)
      alphas = [1]*len(amap._lut)
      alphas[0] = 0
      amap._lut[:,-1] = alphas
      plt.imshow(maskmat, interpolation='none', cmap=amap)

  axes = plt.gca()
  if title:
    axes.set_title(title)

  if axislabels:
    ticks = list(range(len(axislabels)))
    #plt.tick_params(axis='x', colors='white')
    axes.set_xticklabels(axislabels[::labelstep]+[axislabels[-1]], rotation='vertical')
    axes.set_xticks(ticks[::labelstep]+[ticks[-1]])
    axes.set_yticklabels(axislabels[::labelstep]+[axislabels[-1]], rotation='horizontal')
    axes.set_yticks(ticks[::labelstep]+[ticks[-1]])

  #plt.show()
  plt.savefig(name+'.pdf', format=filetype)
  plt.close()




#}   I/O
#   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .
#{ Matrix Conversion

def makeSupermatrix(chrTOchrTOmat: T_NPHeatmaps) -> np.ndarray:
  """
  Make a supermatrix from the given dictionary of matrices.
  """
  fullmat = None
  for c1 in chrTOchrTOmat:
    matrow = None
    for c2 in chrTOchrTOmat:
      mat = chrTOchrTOmat[c1][c2]
      if matrow is None:
        matrow = mat
      else:
        matrow = np.concatenate((matrow, mat), axis=1)

    if fullmat is None:
      fullmat = matrow
    else:
      fullmat = np.concatenate((fullmat, matrow), axis=0)

  assert fullmat is not None, 'No matrices given.'
  return fullmat


def getInMatrixForm(hm):
  """
  Given a 2d dict keyed on pairs of the form (chromosome, begin index),
  return a 4d dict of the form:  [chrm1][chrm2][ind1][ind2]

  @return: 4d dict of the form:  [chrm1][chrm2][ind1][ind2]
  @rtype:  4d dict
  """
  matrix = defdict(lambda: defdict(lambda: defdict(dict)))
  for (chri,i), idict in hm.items():
    print(f'{chri, i}')
    for (chrj,j), heat in idict.items():
       matrix[chri][chrj][i][j] = heat

  return matrix


def getInDictOfMatricesForm(hms: T_PairHeatmaps) -> T_NPHeatmaps:
  """
  Given a 2D dict keyed on pairs of the form (chromosome, begin index),
  return a 2D dict keyed on chromosomes where each entry is a numpy matrix.

  @note: assume given heatmap is symmetric hms[(h,i)][(g,j)] = hms[(g,j)][(h,i)]
  @note: chromosomes are accessed in lexicographical order (e.g. ret[1][10])
  @note: NULLV values are replaced with numpy.nan (not a number)

  @param hms:     the heatmap

  @return: 2D dict with matrix as entries:  rv[chrm1][chrm2] = numpymatrix
  """
  winsize = getWinSize(hms)
     #Get the dimensions of each matrix:
  chrmsizes = getChromLengths(hms)

     #Find the minimum value in the matrix:
  #minheat = sys.maxint
  #for (chri,i), idict in hms.items():
  #  for (chrj,j), heat in idict.items():
  #    if(heat < minheat):
  #      minheat = heat

     #Build the matrices:
  matrices: T_NPHeatmaps = defdict(dict)

     #Fill the matrices:
  for (chri, chrj), hm in hms.items():
      if(chri < chrj):
        matrices[chri][chrj] = hm.matrix
      else:
        matrices[chrj][chri] = hm.matrix.transpose()

     #Remove empty matrices:
     # Note: There should not be any empty matrix
  for chri in matrices.keys():
    for chrj in matrices[chri].keys():
      if(not np.count_nonzero(matrices[chri][chrj])):
        raise(EmptyMatrixError('empty matrix between chromosomes {chri} and {chrj}'))
        del matrices[chri][chrj]
        if(not matrices[chri].keys()):
          del matrices[chri]

  return matrices


class EmptyMatrixError(Exception):
  pass


def buildHeatmapsFromContacts(datafile, winsize=1000000):
  """
  Read a file with contact pairs and build a heatmap with the given window size.

  @param datafile: the file with contact pairs
  @param winsize:  the size of the windows

  @return: the heatmaps
  @rtype:  dictionary of numpy 2D matrices
  """
  with open(datafile) as df:
    printnow("reading data...")
    sys.stdout.flush()
    garbage = df.readline()  #Read away the first description line.
    data = defdict(lambda: defdict(list))
    maximums = {}            #For each chromosome, keep the maximum value seen.
    matrices = {}
    m = 0
    for line in df:          #Parse the file:
      chr1,chr2,pos1,pos2,str1,str2 = line.rstrip().split(',')
      chr1 = fixChrome(chr1)
      chr2 = fixChrome(chr2)
      p1 = int(pos1)/winsize
      p2 = int(pos2)/winsize

      #data.append((chr1, p1, bool(pos1), chr2, p2, bool(pos2)))
      if(chr1 == chr2):      #Save the contact pair.
        if(p1 < p2):
          data[chr1][chr2].append((p1,p2))
        else:
          data[chr2][chr1].append((p2,p1))
      elif(chr1 < chr2):
        data[chr1][chr2].append((p1,p2))
      else:
        data[chr2][chr1].append((p2,p1))
                             #Save the largest chromosome indices:
      if(chr1 not in maximums or maximums[chr1] < p1):
        maximums[chr1] = p1
      if(chr2 not in maximums or maximums[chr2] < p2):
        maximums[chr2] = p2


                             #Save the largest chromosome indices:
    #np.set_printoptions(suppress=True, threshold='nan')
    rootname = os.path.splitext(os.path.basename(datafile))[0]
    if(os.path.exists(rootname)):
      raise RuntimeError('Directory "'+rootname+'" already exits.\n')
    else:
      os.mkdir(rootname)

    for chr1 in data:
      for chr2 in data[chr1]:
        size1 = maximums[chr1]+1
        size2 = maximums[chr2]+1
        printnow('calculating ('+str(size1)+'x'+str(size2)+') matrix for '+
                 'chromosomes '+chr1+' and '+chr2+'...')
        sys.stdout.flush()

        mat = None
        gc.collect()
        mat = np.zeros(dtype=np.int64, shape=(size1,size2))
        for p1,p2 in data[chr1][chr2]:  #Build the matrix:
          mat[p1][p2] += 1
          if(chr1 == chr2 and p1 != p2):
            mat[p2][p1] += 1

        matfile = rootname+'/'+rootname+'-'+str(winsize)+'-'+chr1+'.'+chr2+'.txt'
        with open(matfile, 'w') as mf:
          commentline = '#generated by Krister using buildheatmaps: '+matfile
          mf.write(commentline+'\n')

          firstrow = ''    #Write the first row:
          for i in range(0, size2):
            firstrow += '\t'+titleStr(chr2, i, i*winsize, winsize)
          mf.write(firstrow+'\n')

          for i,row in enumerate(mat):  #Write the matrix rows:
            mf.write(titleStr(chr1, i, i*winsize, winsize))
            for e in row:
              mf.write('\t'+str(e))
            mf.write('\n')
    printnow('done with '+rootname+'.')



#} Matrix Conversion
#   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .
#{ Matrix Queries


def getMatrixDiagonals(matrices: List[Tuple[str, np.ndarray]])\
  -> List[List[float]]:
  """
  Organize the diagonals of the given matrices into a list of lists, indexed
  by the distance from the main diagonal.
  """
     #Get the matrix sizes:
  maxdim = 0
  for _, mat in matrices:
    if mat.shape[0] > maxdim:
      maxdim = mat.shape[0]

     #Group the heat values by chromosome distance:
  distTOheats = [[] for i in range(maxdim)]   #The total heat at distance i.
  for _, mat in matrices:                     #Get average for each diagonal:
    for i in range(mat.shape[0]):    #Get non-None values on diagonals.
      distTOheats[i] += filter(lambda x: x != None, np.diagonal(mat,i))

  return distTOheats


def printDictStats(matrix):
  "Print statistics about the 2D dictionary."
  #for row in matrix.values():
  #  for v in row.values():
  #    print v
  printnow('max:'+str(functools.reduce(max, [functools.reduce(max, r.values()) for r in matrix.values()])))
  printnow('min:'+str(functools.reduce(min, [functools.reduce(min, r.values()) for r in matrix.values()])))
  printnow('ave:'+str(get2DDictAve(matrix)))




def getHeatmapStats(heatmaps, printstats=False, perchm=False):
  """
  Given a dictionary keyed on pairs of the form (chromosome, begin index),
  return the pair of averages (intra, inter).
  Print various stats if printstats is True.
  """
  largest_intra = 0
  intra_vals = np.asarray([])
  w_ave_intra = []

  largest_inter = 0
  inter_vals = np.asarray([])
  w_ave_inter = []

  for (c1, c2), h in heatmaps.iteritems():
    if c1 == c2:
      if largest_intra < np.nanmax(h.matrix):
        largest_intra = np.nanmax(h.matrix)
      intra_vals = np.append(intra_vals, np.asarray(h.matrix.ravel()))
      w_ave_intra.append(np.nansum(h.matrix) /
                         (h.matrix.size - np.isnan(h.matrix).sum()))
    else:
      if largest_inter < np.nanmax(h.matrix):
        largest_inter = np.nanmax(h.matrix)
      inter_vals = np.append(inter_vals, np.asarray(h.matrix.ravel()))
      w_ave_inter.append(np.nansum(h.matrix) /
                         (h.matrix.size - np.isnan(h.matrix).sum()))

  if largest_intra:
    aveintra = np.nanmean(intra_vals)
  else:
    aveintra = 0

  if largest_inter:
    aveinter = np.nanmean(inter_vals)
  else:
    aveinter = 0

  avetotal = np.nanmean(np.append(np.asarray(intra_vals),
                                  np.asarray(inter_vals)))

  if(printstats):
    printnow('largestintra:'+str(largest_intra))
    printnow('largestinter:'+str(largest_inter))
    printnow('intracount:'+str(intra_vals.size - np.isnan(intra_vals).sum()))
    printnow('intercount:'+str(inter_vals.size - np.isnan(inter_vals).sum()))
    printnow('mean/dev intra: {} {}'.format(aveintra, np.nanstd(intra_vals)))
    if(largest_inter):
      printnow('mean/dev inter: {} {}'.format(aveinter, np.nanstd(inter_vals)))
    printnow('median intra:'+str(np.nanmedian(intra_vals)))
    if(largest_inter):
      printnow('median inter:'+str(np.nanmedian(inter_vals)))

    if(perchm):
      printnow('Warning: perchm not yet implemented.')

  return avetotal, aveintra, aveinter




def getHeatmapStatsMatrix(matrices, printstats=False, perchm=False):
  """
  Given a 2D dictionary keyed on chromosomes, yielding a numpy 2D array,
  return the pair of averages (intra, inter).
  Print various stats if printstats is True.

  @param matrices: 2D dict keyed of chromosomes pointing to a numpy matrix
  """
  largestinter = 0
  largestintra = 0
  chrmTOheat = defdict(list)
  intraheats = []   #For computing the median **inefficiently added**
  interheats = []
  for chr1 in matrices:
    for chr2 in matrices[chr1]:
      for (i,j), heat in np.ndenumerate(matrices[chr1][chr2]):
        if(heat is not np.nan):
          if(chr1 == chr2):                   #If the chromosomes are the same:
            if(largestintra < heat):
              largestintra = heat
            intraheats.append(heat)

            if(perchm):
              chrmTOheat[chr1].append(heat)
          else:                               #Otherwise:
            if(largestinter < heat):
              largestinter = heat
            interheats.append(heat)

  if(intraheats):
    aveintra = np.nanmean(intraheats)
  else:
    aveintra = 0
  if(interheats):
    aveinter = np.nanmean(interheats)
  else:
    aveinter = 0

  avetotal = np.mean(intraheats+interheats)

  if(printstats):
    printnow('largestintra:'+str(largestintra))
    printnow('largestinter:'+str(largestinter))
    printnow('intracount:'+str(len(intraheats)))
    printnow('intercount:'+str(len(interheats)))
    printnow('mean/dev intra: {} {}'.format(aveintra, np.nanstd(intraheats)))
    if(interheats):
      printnow('mean/dev inter:{} {}'.format(aveinter, np.nanstd(interheats)))
    printnow('median intra:'+str(np.nanmedian(intraheats)))
    if(interheats):
      printnow('median inter:'+str(np.nanmedian(interheats)))

    if(perchm):
      for c in chrmTOheat.keys():
        printnow(c+": "+str(np.nanmean(chrmTOheat[c])))

  return avetotal, aveintra, aveinter







def getNumRowsCols(filename):
  """
  Return the number of rows and columns in the given heatmap file.
  """
  rows = 1
  columns = 0
  with open(filename, 'r') as f:
    f.readline()
    tcolumns = len(f.readline().lstrip().rstrip().split('\t'))
    columns = len(f.readline().lstrip().rstrip().split('\t'))-1
    assert(columns == tcolumns)
    for row in f:
      rows += 1
      tcolumns = len(row.lstrip().rstrip().split('\t'))-1
      assert(tcolumns == columns)

  return (rows,columns)




def getChromLengths(hms: T_PairHeatmaps):
  """
  Given a 2D dict keyed on pairs of the form (chromosome, begin index).
  Return a dict mapping chromosome to chromosome length.

  @param hms: heatmaps dict keyed on pairs (chromosome, begin index)
  @return:    dict mapping chromosome to chromosome length
  """
     #Get the dimensions of each matrix:
  chrmlens = defdict(int)
  for (chrm1, chrm2), h in hms.items():
    if chrmlens.get(chrm1) is None:
      chrmlens[chrm1] = h.chrom_len[chrm1]

    if chrmlens.get(chrm2) is None:
      chrmlens[chrm2] = h.chrom_len[chrm2]

  return chrmlens


def getChromLengthsFromHeader(heatmapsdir):
  """
  Extract the chromosome lengths from the given heatmaps directory.

  @param heatmapsdir: directory with heatmaps in it

  @return: dict mapping chromosome to chromosome length
  """
  global COMMENT_CHAR

  files = iglob(heatmapsdir+'/*.txt')

  chrTOlen = {}
  for matfile in files:
    try:
      f = open(matfile, 'r')
    except IOError as e:
      sys.exit("Error opening file "+matfile+': '+e)

            #Get the first line:
    line = f.readline()
    if(line[0] == COMMENT_CHAR):  #Ignore it if it's a comment.
      line = f.readline()

            #Get the line with the column descriptions:
    line = line.lstrip().rstrip()
    indexTOname = []                #The names indexed by position.
            #Save the names:
    ends = []
    chrmsm = ''
    for name in line.split('\t'):
      (n,tech,chrmsm,begin,end) = parseName(name)
      ends.append(begin)

    chrTOlen[chrmsm] = max(ends)

  return chrTOlen


def getWinSize(heatmaps):
  """
  Given a 2d dict keyed on pairs of the form (chromosome, begin index),
  return the size of the window.

  @warn: This function is here for compatibility reason. It will be removed.
  """
  #import random
  #return heatmaps[random.choice(heatmaps.keys())].window_size
  return heatmaps[next(iter(heatmaps.keys()))].window_size


def getNonNullIntervals(matrix, ci, cj):
  """
  Return a list of intervals over which there are non-NULL rows in
  the given matrix for the given chromosomes.
  The list is accessed by: [chrm1][chrm2] where the value is
  the list of intervals over which the rows are non-NULL.

  @param matrix: the heatmap in matrix format matrix[ci][cj][coordi][coordj].
  """
  prev = True
  intervals = []
  for row in sorted(matrix[ci][cj].keys()):
    test = isAllNull(matrix[ci][cj][row].values())
    if(prev and not test):   #Start a new interval.
      first = row
    if(not prev and test):   #End an interval.
      intervals.append((first, row))
    prev = test

  return intervals



def getNonNullInts(m, windowsize):
  """
  Return a list of intervals over which the rows of the matrix are not NULL.
  This should correspond roughly to where the centromere is not.

  @note: returned intervals are pythonic (i.e. the ending index is
         non-inclusive)

  @type m:           numpy matrix
  @param m:          the symmetric matrix for a chromosome
  @param windowsize: the window size for this data
                     (i.e. two entries are this far apart)
  @return: dictionary mapping chromosome to list of interval pairs (pythonic)
  """
  intervals = []
  indices = list(range(len(m)))
  if(not indices):
    print("WARNING: no heatmap given in getNonNullInts.")
    return []

  inint = False
  for nonnull,i in zip((not np.all(np.isnan(m[r])) for r in indices), indices):
    if(nonnull and not inint):
      inint = True
      beg = i * windowsize

    if(not nonnull and inint):
      inint = False
      intervals.append([beg, i * windowsize])
  if(inint):
    intervals.append([beg, indices[-1] * windowsize])

  return intervals




def getNonNullIntsOnDiag(m, windowsize):
  """
  Return a list of intervals over which the diagonal is non-NULL in
  the given matrix.
  This should correspond roughly to where the centromere is not.
  For matrices with a NULLed out diagonal, we have to work from the central
  diagonal until we get one that isn't all NULLs.

  @param m:          the symmetric matrix for chromosome
  @param windowsize: the window size for this data
                     (i.e. two entries are this far apart)
  """
  intervals = []
  if(not m.keys()):
    print("WARNING: no self heatmap for chromosome!")
    return []

         #Get the intervals (using the fact that the diagonal is empty if the
         #diagonal whole row is empty):
  first = cur = min(m.keys())
  maximum = max(m.keys())
  if(cur == 1):
    cur = windowsize

  prev = ((cur in m and cur in m[cur]) and m[cur][cur] != NULLV)
  while(cur <= maximum):
    if(prev and (cur not in m or cur not in m[cur] or m[cur][cur] == NULLV)):
      prev = False                                  #End an interval.
      intervals.append([first, cur])

    if(not prev and (cur in m and cur in m[cur] and m[cur][cur] != NULLV)):
      prev = True                                   #Begin an interval.
      first = cur

    cur += windowsize
  if(prev):
    intervals.append([first, cur])

  return intervals




#} Matrix Queries
#   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .
#{ Scoring

def scoreIntervals(heatmaps: CoolerHeatmap, intervals: T_IntervalPair,
                   choicef=np.nanargmax) -> Tuple[float, T_HMCoord]:
  """
  Get the score for a pair of intervals, and the corresponding coordinate in
  the matrix.  The function choicef must be a numpy function that handles an
  array slice and returns the position of an element
  (e.g. nanargmax, nanargmin).
  """
  ((c1, i1), (c2, i2)) = intervals

  return heatmaps.getCoordinate(c1, i1, c2, i2, choicef)


def roundToNearest2d(heatmap, k1, k2, windowsize, maxdist, fuzzdiag=True):
  """
  Move the coordinate to the nearest defined value directly in the column, row,
  or on the diagonal.
  If there is no heatmap value for the given chromosome, then return
  the original pair.

  @param heatmap:    the heatmap
  @param k1:         row tuple (chrom, startindex) to find a neighbor for
  @param k2:         column tuple (chrom, startindex) to find a neighbor for
  @param windowsize: bin size for the heatmap
  @param maxdist:    the maximum # of windows (on an axis) to search for value
  @param fuzzdiag:   search in a small region off the diagonal
  """
  c1 = k1[0]
  i1 = k1[1]
  c2 = k2[0]
  i2 = k2[1]
  for j in range(maxdist):              #If we're on the heatmap.
    j1 = i1-j*windowsize                #LEFT (origin top-left):
    j2 = i2
    if(hasValue(heatmap, (c1,j1),(c2,j2))):
      return ((c1,j1),(c2,j2))

    j1 = i1+j*windowsize                #RIGHT:
    j2 = i2
    if(hasValue(heatmap, (c1,j1),(c2,j2))):
      return ((c1,j1),(c2,j2))

    j1 = i1                             #UP:
    j2 = i2-j*windowsize
    if(hasValue(heatmap, (c1,j1),(c2,j2))):
      return ((c1,j1),(c2,j2))

    j1 = i1                             #DOWN:
    j2 = i2+j*windowsize
    if(hasValue(heatmap, (c1,j1),(c2,j2))):
      return ((c1,j1),(c2,j2))

    j1 = i1-j*windowsize                #DOWNLEFT:
    j2 = i2+j*windowsize
    if(hasValue(heatmap, (c1,j1),(c2,j2))):
      return ((c1,j1),(c2,j2))

    j1 = i1+j*windowsize                #DOWNRIGHT:
    j2 = i2+j*windowsize
    if(fuzzdiag):
      spot = roundToNearest2d(heatmap, (c1,j1),(c2,j2), windowsize, 9, False)
      if(spot != ((c1,j1),(c2,j2))):
        return spot
    elif(hasValue(heatmap, (c1,j1),(c2,j2))):
      return ((c1,j1),(c2,j2))

    j1 = i1+j*windowsize                #UPRIGHT:
    j2 = i2-j*windowsize
    if(hasValue(heatmap, (c1,j1),(c2,j2))):
      return ((c1,j1),(c2,j2))

    j1 = i1-j*windowsize                #UPLEFT:
    j2 = i2-j*windowsize
    if(fuzzdiag):
      spot = roundToNearest2d(heatmap, (c1,j1),(c2,j2), windowsize, 9, False)
      if(spot != ((c1,j1),(c2,j2))):
        return spot
    elif(hasValue(heatmap, (c1,j1),(c2,j2))):
      return ((c1,j1),(c2,j2))

  return (k1,k2)




def roundToNearest(heatmap, k, windowinc):
  """
  Move the coordinate to the nearest defined value.
  If there is no heatmap value for the given chromosome, then return None.

  This works on one dimension at a time.

  @param heatmap:   the heatmap
  @param k:         the tuple (chrom, startindex) to find a neighbor for
  @param windowinc: bin size for the heatmap
  """
  chrom = k[0]
  coord = k[1]
  counter = 0
  while(k not in heatmap):
    if(coord < 0):            #If we're looking at small values then
      windowinc *= -1         #start incrementing instead of decrementing.
    coord -= windowinc
    k = (chrom,coord)
    counter += 1
    if(counter > 20000):      #If we've been searching too long,
      return None             #then quit.

  return k



def getHeatmapCoordinateRoundDown(heatmap, chrom, interval, windowsize):
  """
  Return the heatmap coordinates (chromosome/coordinate pair) for the given
  interval.
  Round down.
  Unused.
  """
                              #Round down.
  coord = (interval[1]/windowsize)*windowsize
  k = (chrom,coord)
                 #Ensure that k is not off the heatmap:
  windowinc = windowsize
  while(k not in heatmap):
    if(coord < 0):            #If we're looking at small values then
      windowinc *= -1         #start incrementing instead of decrementing.
    coord -= windowinc
    k = (chrom,coord)

  return k



def get2DDictAve(d):
  "Get the average over all floats in the 2D dict."
  return sum([sum(r.values())/len(r) for r in d.values()])/len(d.values())


#} Scoring
#   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .
#{ Randomization


def getRandomLocationWithDist(heatmap: CoolerHeatmap, chrm: T_Chrom,
                              leftsize: int, distance: int, rightsize: int,
                              forcenz=False) -> int:
  """
  Return a random --non NULL-- location in the chromosome given the heatmap
  matrix for that chromosome such that the right-side interval that starts
  -distance- from the location will not have only NULL heatmap values.
  If there is no such location, then return None.
  
  @param heatmap:     the heatmap
  @param otherint:    the first interval
  @param distance:    the distance from the location we would return to the
                      beginning of the right-side interval
  @param rightsize:   the size of the right side interval
  @param forcenz:     only choose a region with non-zero values
  """
  tries = 10000
  matrix = heatmap.cooler.matrix(sparse=True)
  while tries:
    j1 = random.randrange(1, heatmap.chromsizes()[chrm] - rightsize - distance)
    i2 = j1 + distance
    j2 = i2 + rightsize
    i1 = max(0, j1 - leftsize)

    if not forcenz or matrix.fetch((chrm, i1, j1), (chrm, i2, j2)).nnz:
      return j1

    tries -= 1

  raise RandomizedLocationError


def removeForbiddenFromNonnull(intervals, forbidlist, resolution):
  """
  Remove forbidden intervals from the given intervals.

  @note: intervals that are no longer valid will have negative size
         (i.e. i[0] > i[1]).

  @param intervals:  intervals to remove from
  @param forbidlist: list of forbidden intervals
  @param resolution: the size of the resolution when intervals are chopped
  """
    #crossing[i] is a list of forbidden intervals that intersect this one.
  crossing = [[] for _ in range(len(intervals))]   #Forbidden that cross this.
  for i,interval in enumerate(intervals):
    for forbint in forbidlist:
      if(intersects(interval, forbint)):
        crossing[i].append(forbint)

    #Build the list of new intervals:
  newints = []
  for i,interval in enumerate(intervals):
    if(crossing[i]):
      newints += subtractIntsFromInt(interval, crossing[i], resolution)
    else:
      newints.append(interval)

  return newints


def subtractIntsFromInt(interval, intlist, resolution):
  """
  Return a list of intervals that are the parts of interval which are not
  covert by an interval in intlist.
  Round to the nearest resolution.

  @note: assumes that intlist intervals overlap with interval
  """
  leftoverlap = None
  rightoverlap = None
  middle = []
  for i in intlist: 
    if(i[0] < interval[0] and i[1] <= interval[1]):
      leftoverlap = i         #removal interval left of interval
    elif(i[0] <= interval[0] and i[1] >= interval[1]):
      return []               #removal interval completely contains interval
    elif(i[0] <= interval[1] and i[1] > interval[1]):
      rightoverlap = i        #removal interval right of interval
    elif(i[0] >= interval[0] and i[1] <= interval[1]):
      middle.append(i)        #removal interval contained by interval

  leftmost, rightmost = interval
  if(leftoverlap):
    leftmost = leftoverlap[1] #+ resolution
  if(rightoverlap):
    rightmost = rightoverlap[0] #- resolution

  parts = []
  if(middle):
    lastleft = leftmost
    for m in middle:
      parts.append([lastleft, m[0]]) #-resolution])
      lastleft = m[1]
    parts.append([lastleft, rightmost])
  else:
    parts.append([leftmost, rightmost])

  return parts

    



def intersects(int1, int2):
  """
  Return True if the two intervals intersect.
  """
  return ((int1[1] >= int2[0] and int1[1] <= int2[1]) or #right inside of int2
          (int1[0] >= int2[0] and int1[0] <= int2[1]) or #left inside of int2
          (int1[0] <= int2[0] and int1[1] >= int2[1]))   #int1 contains int2

def getRandomLocationIntra(nonnullints, intsize1, intsize2, distance, winsize):
  """
  Return a random --non NULL-- location in the chromosome given the heatmap
  matrix for that chromosome.  This location will be the start of the first
  interval, the second one starting at location+distance.
  
  @param nonnullints: dict of nonnullints for all heatmaps.
  @param intsize1:    the size of the left interval.
  @param intsize2:    the size of the right interval.
  @param distance:    the distance between the two windows.
  @param winsize:     the window size.
  """
  startspots = []
  #for spot in chain(*(range(s, e+winsize, winsize) for s,e in nonnullints)):
  for spot in range(0, nonnullints[-1][1]+winsize, winsize):
    if(intervalOverlapsIntervals(spot, spot+intsize1, nonnullints, winsize) and
       intervalOverlapsIntervals(spot+intsize1+distance,
                                 spot+intsize1+distance+intsize2,
                                 nonnullints, winsize)):
      startspots.append(spot)

  if(not startspots):
    return None
  return random.choice(startspots)    #Return one of the locations.



def checkIntervalOverlapsIntervals(s, e, intlist, windowsize=None):
  """
  Print an error message if the given interval does not ovelap with one of
  the intervals in the given list.

  @param s:          start of the interval
  @param e:          start of the interval (non-pythonic)
  @param intlist:    list of intervals     (pythonic)
  @param windowsize: round to this
  """
  if(not intervalOverlapsIntervals(s, e, intlist, windowsize)):
    print('WARN: {} all NULL:\n\t{}'.format((s,e), intlist))



def intervalOverlapsIntervals(s, e, intlist, windowsize):
  """
  Return True if the given interval overlaps with one of the
  intervals in the given list.

  @param s:          start of the interval
  @param e:          start of the interva  (non-pythonic)
  @param intlist:    list of intervals     (pythonic)
  @param windowsize: round to this
  """
  def overlaps(s1,e1, s2,e2):  #e1 non-pythonic and e2 pythonic
    return ((s1 >= s2 and s1 < e2) or (e1 >= s2 and e1 <= e2) or
            (s1 <= s2 and e1 >= e2))

  s = int(s/windowsize)*windowsize
  e = int(e/windowsize)*windowsize
  return any(map(lambda other: overlaps(s,e, other[0],other[1]), intlist))



def checkIntervalInIntervals(s, e, intlist, windowsize):
  """
  Print an error message if the given interval is not contained in the given
  list of intervals.

  @param s:          start of the interval
  @param e:          start of the interval
  @param intlist:    list of intervals
  @param windowsize: round to this
  """
  s = int(s/windowsize)*windowsize
  e = int(e/windowsize)*windowsize
  if(not intervalInIntervals(s, e, intlist, windowsize)):
    print('WARN: {} in NULL zone:\n\t{}'.format((s,e), intlist))




def intervalInIntervals(s, e, intlist, windowsize=None):
  """
  Return True if the given interval is included completely in one of the
  intervals in the given list.

  @param s:          start of the interval
  @param e:          start of the interval
  @param intlist:    list of intervals
  @param windowsize: round to this
  """
  return any(map(lambda other: s >= other[0] and e <= other[1], intlist))




#} Randomization
#   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .   .
#{  Helper Functions


def fixChrome(chrmsm):
  """
  Return the chromsome number given the wonky numbers in the data file.
  """
  if(chrmsm == '22'):
    return 'X'
  else:
    return str(int(chrmsm)+1)


def titleStr(chrmsm, binnum, start, binsize):
  """
  Return a string with the row or column header.
  """
  return 'HIC_bin'+str(binnum+1)+'|xxxx|chr'+chrmsm+':'+\
                   str(start)+'-'+str(start+binsize-1)




def isAllNull(l):
  """
  Return true if there is only NULL values in this list.
  """
  for e in l:
    if(e != NULLV):
      return False
  return True




nameparse = re.compile(r'([\w-]+)\|([\w\.]+)\|(\w+)\:(\d+)-(\d+)')
def parseName(name):
  """
  Parse a name from the Lieberman-Aiden matrix file.

  @param name: a string starting with a column/row header.
  @return:     tuple (name, assembly, chrmsm, begin, end)
  """
  m = nameparse.match(name.lstrip())
  if(not m):
    raise RuntimeError('No name match for string "'+name+'".\n')
  n = m.group(1)
  assembly = m.group(2)
  chrmsm = m.group(3).replace('chr','')
  begin = int(m.group(4))
  end = int(m.group(5))

  return (n,assembly,chrmsm,begin,end)



def markNulls(matrix):
  """
  Turn zero rows and zero columns into rows and columns with values None.

  @param matrix:  the matrix to work on
  @type  matrix:  a list of lists
  """
  for row in matrix:                #Do the rows:
    if sum(row) == 0:               #(ya, I know)
      row[:] = map(lambda x: None, row)

  for j in range(len(matrix[0])):  #Do the columns:
    allzeros = True
    for i in range(len(matrix)):
      if(matrix[i][j] and matrix[i][j] != 0):
        allzeros = False
    if(allzeros):
      for i in range(len(matrix)):
        matrix[i][j] = None



def markNullsNP(matrix):
  """
  Turn zero rows and zero columns into rows and columns with values nan.

  @param matrix:  the matrix to work on
  @type  matrix:  a 2D numpy array
  """
  for row in matrix:                #Do the rows:
    if(all(map(lambda x: x == 0 or x == None or np.isnan(x), row))):
      for e in np.nditer(row, op_flags=['readwrite'], flags=['refs_ok']):
        e[...] = np.nan

  for col in matrix.transpose():    #Do the columns:
    if(all(map(lambda x: x == 0 or x == None or np.isnan(x), col))):
      for e in np.nditer(col, op_flags=['readwrite'], flags=['refs_ok']):
        e[...] = np.nan




def hasValue(hm, i,j):
  """
  Return true the given heatmap has a value at the given location and is
  non-NULL.

  @param hm: the heatmap
  @param i:  the first coordinate
  @param j:  the second coordinate
  """
  if(i in hm and j in hm[i] and hm[i][j] != NULLV):
    return True
  return False

#}  Helper Functions


#{ Exceptions:
class RandomizedLocationError(Exception):
  pass

#}
