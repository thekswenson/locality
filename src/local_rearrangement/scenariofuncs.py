#!/usr/bin/env python3
# Krister Swenson
"""
Functions for analyzing the scenarios output by "localdist --logscen".
"""
from pathlib import Path
import sys
import os
import re
import pickle
import random
from typing import Dict, List, Optional, Tuple
import numpy as np
import gzip
import bz2
import subprocess
from glob import glob
from numpy import vectorize, isnan, nonzero

from operator import itemgetter
from itertools import chain
from collections import defaultdict as defdict

from .heatmapfuncs import NULLV, CoolerHeatmap, T_Chrom, getWinSize, getRandomLocationIntra
from .heatmapfuncs import getChromLengths, getInMatrixForm
from .heatmapfuncs import getInDictOfMatricesForm, getNonNullInts
from .heatmapfuncs import getRandomLocationWithDist, scoreIntervals
from .types import EMPTY_COORD, T_IntervalPair, T_HMCoord, T_IntervalPairList
from .types import T_HMCoordList, T_SideSpec, T_Move
from pyinclude.usefulfuncs import printerr, printnow



#CONSTANTS:

SCENARIOS_PICKLE_TYPE = 'pgz'   #One of, from fastest to slowest, 'pcl',
                                #'pgz' (20x smaller), or 'pbz2' (40x smaller).

THIS_FILE_DIR = os.path.dirname(os.path.realpath(__file__))
LIFTOVER = THIS_FILE_DIR+'/liftover/liftOver'
LIFTCHAIN = THIS_FILE_DIR+'/liftover/hg18ToHg19.over.chain'

# ____________________________________________________________________________ #
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|    TYPES    |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#


# ____________________________________________________________________________ #
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|  FUNCTIONS  |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#




def readScenarios(scenfiles: List[str], ignoreset=set(), cutoffprop=None,
                  keeploc=False, keepnan=False, commonprop=None):
  """
  Read the scenarios in the file.

  @note: DCJ's with no heatmap values are excluded from the returned lists.

  @param scenfiles:  list of files with scenarios.
  @param ignoreset:  ignore intrachromosome moves from the given list of
                     chromosomes.
  @param cutoffprop: ignore moves that are more than cutoffprop*std away from
                     the mean heat.
  @param keeploc:    if True, then make element pairs with location instead
                     of with the heatmap coordinates
                     (e.g. ret[1][0][0] = (((ci,i),(cj,j)), heat))
  @param keepnan:    keep moves with NaN heat values.
  @param commonprop: ignore moves that occur more than stddev*commonprop from
                     mean number of occurences of a move. keeploc must also
                     be False for this to work.

  @return: Lists of heats classified as intra, inter, or both (in that order).
           For example ret[1] is a list of interchromosomal heats.
           The intrachromosomal heats also have the heatmap indices
           (e.g. ret[0][0][0] = ((0, 1000000), 1.0)).

  @rtype:  tuple of list of lists  (reference by [pairtype][scenario][move])
  """
  assert not (commonprop and keeploc)

  allscens: List[List[Tuple[T_IntervalPair, T_HMCoord, float]]] = []
  for scenfile in orderScenarioFiles(scenfiles):
    both = []
    try:
      intlist, loclist, heatlist, _, _ = readScenario(scenfile)
    except Exception as e:
      print(str(e))
      continue

    for intloc, loc, heat in zip(intlist, loclist, heatlist):
      if keepnan or not np.isnan(heat): #loc != EMPTY_COORD:
        ((ci,i), (cj,j)) = loc
        if ci not in ignoreset and cj not in ignoreset:
          both.append((intloc, loc, heat))

    allscens.append(both)

  sintra = []
  sinter = []
  sboth = []
  for scen in allscens:
    intra = []
    inter = []
    both = []
    for intloc, loc, heat in scen:
      (ci,i), (cj,j) = loc
      if ci == cj:
        if keeploc:
          intra.append((loc, heat))
        else:
          if commonprop:
            intra.append((intloc, ((i,j), heat)))
          else:
            intra.append(((i,j), heat))
      else:
        if keeploc:
          inter.append((loc, heat))
        else:
          if commonprop:
            inter.append((intloc, heat))
          else:
            inter.append(heat)

      if keeploc:
        both.append((loc, heat))
      else:
        if commonprop:
          both.append((intloc, heat))
        else:
          both.append(heat)

    sintra.append(intra)
    sinter.append(inter)
    sboth.append(both)

  if commonprop and not keeploc:
    print('Intra ',)
    sintra = filterExtremeMoves(sintra, commonprop)
    print('Inter ',)
    sinter = filterExtremeMoves(sinter, commonprop)
    print('Both ',)
    sboth = filterExtremeMoves(sboth, commonprop)


  if cutoffprop and not keeploc:
    print('Moves kept after extreme move removal (in %):')
    newsintra,prop = copyWithoutExtremes(sintra, cutoffprop, False)
    print('\tintra: '+str(prop))
    sintra = newsintra
    newsinter,prop = copyWithoutExtremes(sinter, cutoffprop, False)
    print('\tinter: '+str(prop))
    sinter = newsinter
    newsboth,prop = copyWithoutExtremes(sboth, cutoffprop, False)
    print('\tboth:  '+str(prop))
    sboth = newsboth

  return sintra, sinter, sboth


def readScenario(scenfile: str) -> Tuple[T_IntervalPairList,
                                         T_HMCoordList,
                                         List[float],
                                         Optional[List[T_Move]],
                                         Optional[List[T_SideSpec]]]:
  """
  Read a pickled scenario.  If it's an old-style scenario then the fourth
  returned value will be None.

  @return: (intlist, pointlist, heatlist, movelist, specieslist)
           where intlist contains chromosome-interval pairs,
           pointlist is the list of heatmap locations for each interval whose
           corresponding value is in the heatlist
  """
  _,extension = os.path.splitext(scenfile)
  lists = []
  if(extension == '.pcl'):
    lists = pickle.load(scenfile)
  elif(extension == '.pgz'):
    with gzip.GzipFile(scenfile, 'r') as f:
      lists = pickle.load(f)
  elif(extension == '.pbz2'):
    with bz2.BZ2File(scenfile, 'r') as f:
      lists = pickle.load(f)
  else:
    raise(Exception('File with unknown format "{}": {}'.format(extension,
                                                               scenfile)))


  if(len(lists) == 3):
    intlist, pointlist, heatlist = lists
    movelist = None
    splist = None
  elif(len(lists) == 4):
    intlist, pointlist, heatlist, movelist = lists
    splist = None
  elif(len(lists) == 5):
    intlist, pointlist, heatlist, movelist, splist = lists
  else:
    errorstr = "couldn't open file:\n{}\n".format(scenfile)
    errorstr += '\tUnexpected scenario file format.'
    raise(Exception(errorstr))

  return intlist, pointlist, heatlist, movelist, splist


def getScenarioFilesList(scenarios_loc: Path) -> Tuple[List[str], str]:
  """
  Given a filename or directory name, return the list of scenario files
  in the directory, or return the single file after verifying that it exists.
  Also return the extension of the first file in the list.
  """
  extensions = ('.pcl', '.pgz', '.pbz2')
  if scenarios_loc.is_dir():
    scenfiles = []
    for ext in extensions:
      scenfiles.extend(glob(f'{scenarios_loc}/*{ext}'))
    if not scenfiles:
      sys.exit('ERROR: "{}" has no scenarios.'.format(scenarios_loc))

  else:       #check file extension if it's a file and not a directory
    if not scenarios_loc.is_file():
      sys.exit(f'ERROR: non existing file/directory: {scenarios_loc}')
    if not scenarios_loc.suffix in extensions:
      sys.exit(f'ERROR: expected file with extension {extensions}'
               f', got {scenarios_loc}')
    scenfiles = [str(scenarios_loc)]

  return scenfiles, Path(scenfiles[0]).suffix



def writeScenario(fileprefix, physintlist, positionlist, heatlist, movelist,
                  specieslist, filetype=SCENARIOS_PICKLE_TYPE):
  """
  Write a pickled scenario.

  @param fileprefix:   the filename path a prefix.
  @param physintlist:  list of physical intervals for the move.
  @param positionlist: list of the heatmap position chosen for the each heat.
  @param heatlist:     list of the heatmap values for each position.
  @param movelist:     list of the moves in terms of the edges modified.
  @param specieslist:  species (side of the graph) on which the move was done.
  @param filetype:     must be one of 'pcl', 'pgz', 'pbz2' (more compact from
                       left to right, but slower)
  """
  tup = (physintlist, positionlist, heatlist, movelist, specieslist)
  if(filetype == 'pcl'):       #Fastest.
    with open(fileprefix+'.pcl', 'wb') as f:
      pickle.dump(tup, f, pickle.HIGHEST_PROTOCOL)

  elif(filetype == 'pgz'):     #Slower but 20x smaller.
    with gzip.GzipFile(fileprefix+'.pgz', 'w') as f:
      pickle.dump(tup, f, pickle.HIGHEST_PROTOCOL)

  elif(filetype == 'pbz2'):    #Slowest but 40x smaller.
    with bz2.BZ2File(fileprefix+'.pbz2', 'w') as f:
      pickle.dump(tup, f, pickle.HIGHEST_PROTOCOL)

  else:
    raise(Exception('Unknown filetype specified to writeScenario()'))




def filterExtremeMoves(scens, proportion):
  """
  Return the scenarios only keeping moves that occur less than
  proportion*dev the mean number of occurences plus the standard
  """
  locationTOheatlist = defdict(list)
  for scen in scens:
    for (loc, val) in scen:
      if(type(val) != tuple): #If this an scenario inter or a both scenario.
        heat = val
      else:                   #If this is intra.
        _,heat = val

      if(not np.isnan(heat)):
        locationTOheatlist[loc].append(heat)

    #Get the mean number of occurrences for each move:
  movereps = []
  for heatlist in locationTOheatlist.values():
    movereps.append(len(heatlist))
  repsmean = np.average(movereps)
  repsdev = np.std(movereps)
  print('occurences mean: {} dev:{}'.format(repsmean, repsdev))

    #Organize moves by heat:
  bannedmoves = set()
  for move, heatlist in locationTOheatlist.items():
    assert(all(heatlist[0] == heat for heat in heatlist))
    heat = heatlist[0]
    reps = len(heatlist)
    if(reps >= repsmean+(repsdev*proportion)):
      bannedmoves.add(move)

  print('{}/{} moves ignored.'.format(len(bannedmoves), len(locationTOheatlist)))

  newscens = []
  for scen in scens:
    newscen = []
    for (loc, val) in scen:
      if(loc not in bannedmoves):
        newscen.append(val)
    newscens.append(newscen)

  return newscens



def reEvalScenariosHeat(scenfiles, hms, newscens='',
                        printdiff=False, liftindices=False, randomize=False):
  """
  Rescore the scenarios in the given files.

  @param scenfiles:   list of files with scenarios.
  @param hms:         the heatmaps.
  @param newscens:    location to save the new scenarios.
  @param printdiff:   print a message if we find a difference in heat values.
  @param liftindices: use liftOver to map the indices of the intervals.
  @param randomize:   rescore with random locations (controlled for length etc).
  """
  found = False
                   #Map old location to new location (for sanity check).
  locTOloc: Dict[T_IntervalPair, T_HMCoord] = {}
                   #Now rescore each move in each scenario:
  for i, scenfile in enumerate(orderScenarioFiles(scenfiles)):
    try:
      found = reEvalScenarioHeat(scenfile, hms, f'{newscens}/rescore.{i}',
                                 printdiff, liftindices, randomize,
                                 locTOloc)
    except Exception as e:
      print(str(e))
      continue

  if printdiff and not found:
    print("No differences found.")


def reEvalScenarioHeat(scenfile: str, hms: CoolerHeatmap, outfilestem='',
                       printdiff=False, liftindices=False, randomize=False,
                       locTOloc: Dict[T_IntervalPair, T_HMCoord]={}):
  """
  Rescore the scenarios in the given files.

  @param scenfiles:   list of files with scenarios.
  @param hms:         the heatmaps.
  @param outfilestem: the output filename without extension, no file output
                      if empty.
  @param printdiff:   print a message if we find a difference in heat values.
  @param liftindices: use liftOver to map the indices of the intervals.
  @param randomize:   rescore with random locations (controlled for length etc).
  """
  intlist, loclist, heatlist, movelist, speclist = readScenario(scenfile)

  intervals = [] #Lift indices between assemblies:
  if liftindices:
    intervals = liftIntervals(intlist)
  else:
    intervals = intlist

  if randomize:
    newintervals = randomizePhysIntList(hms, intervals, loclist)#, locTOloc)
    intervalsCheck(newintervals, intervals, hms.binsize)
    intervals = newintervals

                 #Rescore the scenario and write it to a file:
  (_,nheatlist,nloclist) = scorePhysIntList(hms, intervals,
                                            outfilestem, movelist, speclist)
  for o,n in zip(intervals, nloclist):
    if o in locTOloc:
      if locTOloc[o] != n:
        sys.exit('bad map {} to {} was {}'.format(o, n, locTOloc[o]))
    else:
      locTOloc[o] = n

  found = False
  if printdiff:     #Check for a difference in heatmap values (sanity check):
    for h1, h2, loc1, loc2 in zip(heatlist, nheatlist, loclist, nloclist):
      if (h1 != h2 and not (np.isnan(h1) and np.isnan(h2))) or loc1 != loc2:
        print((h1 != h2), (np.isnan(h1) and np.isnan(h2)), (loc1 != loc2))
        print("DIFFERENT: {} {} {} {}".format(h1, h2, loc1, loc2))
        found = True

  return found


def liftIntervals(intlist):
  """
  Remap the given interval pairs using liftOver.

  @note: could update this to use tempfile library.

  @return: a list of interval pairs with lifted indices.
  """
  rand = str(random.randrange(sys.maxsize))
  tmpin = '.tmpin'+rand+'.bed'
  tmpout = '.tmpout'+rand+'.bed'
  tmpunmap = '.tmpunmap'+rand+'.bed'

  printnow('lifting intervals...')
  with open(tmpin, 'w') as f:
    for k, ((c1, (i1,j1)),(c2, (i2,j2))) in enumerate(intlist):
      f.write('chr'+c1+'\t'+str(i1)+'\t'+str(j1)+'\t'+str(k)+'.0\n')
      f.write('chr'+c2+'\t'+str(i2)+'\t'+str(j2)+'\t'+str(k)+'.1\n')

  subprocess.call((LIFTOVER, tmpin, LIFTCHAIN, tmpout, tmpunmap))

  with open(tmpout) as f:
    newints = [line.split() for line in f]

  with open(tmpunmap) as f:
    newints += [line.split() for line in f if line[0] == 'c']

  newints.sort(key=lambda l: float(l[3]))

  count = 0
  cap = 2000000
  diffs = []
  for i in range(len(intlist)):
    (c1, (i1,j1)), (c2, (i2,j2)) = intlist[i]
    nc1, ni1, nj1, _ = newints[i*2]
    nc2, ni2, nj2, _ = newints[(i*2)+1]
    di1 = abs(i1-int(ni1))
    dj1 = abs(j1-int(nj1))
    di2 = abs(i2-int(ni2))
    dj2 = abs(j2-int(nj2))
    if(di1 > cap):
      count += 1
    if(dj1 > cap):
      count += 1
    if(di2 > cap):
      count += 1
    if(dj2 > cap):
      count += 1
    diffs += [di1,dj1,di2,dj2]

  print()
  print(str(count)+'/'+str(len(diffs))+' lifts with distance greater than '+str(cap)+' bases.')
  print('average lift distance: '+str(np.average(diffs)))
  print('             lift dev: '+str(np.std(diffs)))

  os.remove(tmpin)
  os.remove(tmpout)
  os.remove(tmpunmap)
  sys.exit()





def randomizeScenarioHeat(scenfile, hms: CoolerHeatmap, newloc: str, reps=1):
  """
  Rescore the scenario with random locations (controlled for length etc).

  @param scenfile:    the file with the scenario.
  @param hms:         the heatmaps.
  @param newloc:      location to save the new scenarios.
  @param reps:        make this many random copies of the given scenario.
  @param fuzz:        evaluate with this much fuzz.
  """
  try:
    intlist, loclist, _, movelist, speclist = readScenario(scenfile)
  except Exception as e:
    print(str(e))
    return

  for i in range(reps):    #Produce the random scenarios:
    newintervals = randomizePhysIntList(hms, intlist, loclist)
    intervalsCheck(newintervals, intlist, hms.binsize)
    intlist = newintervals

                           #Rescore the scenario and write it to a file:
    scorePhysIntList(hms, intlist, f'{newloc}/rescore.{i}',
                     movelist, speclist)


def orderScenarioFiles(scenfiles: List[str]):
  """
  Return the given list of files ordered by the integer before the
  extension (e.g. rescore.2.pcl comes before rescore.11.pcl).

  @note: if there is a file that doesn't match the expected format, then there
         is no guarantee on the order.

  @param scenfiles: list of .pcl/.pgz/.pbz2 files with scenarios in them

  @return: the list of files sorted by integer before the .pcl extension
  """
  extractnumre = re.compile(r'.+\.(\d+)\.(pcl|pgz|pbz2)')
  def indFromName(name):
    m = extractnumre.match(name)
    if(m):
      return int(m.group(1))
    return 0

  return sorted(scenfiles, key=indFromName)


def evalScensByMoves(scenfiles, hms):
  """
  Read the scenarios in the file and reevaluate the moves.

  @param scenfiles:   list of files with scenarios.
  @param hms:         the heatmaps.
  """
  raise(Exception('Function not updated correctly for use with new heatmaps.'))
  found = False

  windowsize = getWinSize(hms)
  if(randomize):   #If we're randomizing then build the datastructures once:
    chromlens = getChromLengths(hms)
    nonnullintsd = {}            #Chromosome to list of nonnull intervals.
    indlist = defdict(dict)      #The index pairs of all nonnull matrix entries.
    alreadyrand = {}             #Map old location to new location.

    matrixform = getInMatrixForm(hms)
    for i in matrixform.keys():
      nonnullintsd[i] = getNonNullInts(matrixform[i][i], windowsize)
    def tTOz(val):               #A helper function to get them.
      return 0 if val else 1
    dofmform = getInDictOfMatricesForm(hms)
    for i in dofmform.keys():
      for j in dofmform[i].keys():
        dofmindices = nonzero(vectorize(tTOz)(isnan(dofmform[i][j])))
        indlist[i][j] = zip(*dofmindices)
        indlist[j][i] = zip(dofmindices[1], dofmindices[0])

  oldints = []
  newlocs = []
  locTOloc = {}
                  #Now rescore each move in each scenario:
  for i, scenfile in enumerate(orderScenarioFiles(scenfiles)):
    try:
      intlist, loclist, heatlist, movelist, speclist = readScenario(scenfile)
    except Exception as e:
      print(str(e))
      continue

    #for ((ck,k),(cl,l)),((ci,i),(cj,j)),heat in zip(intlist,loclist,heatlist):
    #  assert(ck == ci and cl == cj)

    intervals = []
    if(liftindices):
      intervals = liftIntervals(intlist)
    else:
      intervals = intlist

    if(randomize):
      newintervals = randomizePhysIntList(hms, intervals,
                                          loclist) #, alreadyrand)
      intervalsCheck(newintervals, intervals)  #sanity check.
      intervals = newintervals

         #Rescore the scenario and write it to a file:
    (_,nheatlist,nloclist) = scorePhysIntList(hms, intervals,
                                              f'{newscens}/rescore.{i}',
                                              movelist, speclist)
    for o,n in zip(intervals,nloclist):
      if(o in locTOloc):
        if(locTOloc[o] != n):
          sys.exit('bad map {} to {} was {}'.format(o,n,locTOloc[o]))
      else:
        locTOloc[o] = n

    oldints += intervals
    newlocs += nloclist
    if(printdiff):
      for h1, h2, loc1, loc2 in zip(heatlist, nheatlist, loclist, nloclist):
        if(h1 != h2 or loc1 != loc2):
          print("DIFFERENT: {} {} {} {}".format(h1,h2,loc1,loc2))
          found = True

  ##Reuse of locations:
  #print('old ints')
  #print(len(oldints))
  #print(len([loc for loc in oldints if loc[0][0] != loc[1][0]]))
  #print(len(set(loc for loc in oldints if loc[0][0] != loc[1][0])))
  #print('new')
  #print(len(newlocs))
  #print(len([loc for loc in newlocs if loc[0][0] != loc[1][0]]))
  #print(len(set(loc for loc in newlocs if loc[0][0] != loc[1][0])))

  if(printdiff and not found):
    print("No differences found.")




def intervalsCheck(il1, il2, windowsize):
  """
  Compare the intervals to make sure they have the same size, and distance
  between them if it's an intrachromosomal move.

  @param il1: list of intervals ((chrm1,(start1,end1)), (chrm2,(start2,end2)))
  @param il2: list of intervals
  @param chromTOnonnullints: map chromosome to list of nonnull intervals
  @param windowsize:         heatmap window size
  """
  for ((c1,(i1,j1)),(d1,(k1,l1))), ((c2,(i2,j2)),(d2,(k2,l2))) in zip(il1,il2):
    if(c1 != c2 or d1 != d2):
      print('WARNING: chromosome mismatch. '+c1+' != '+c2+' or '+d1+' != '+d2)

    if(c1 == d1 and (i1 != k1 and j1 != l1)):
      if(j1 < k1):
        if(k1-j1 != k2-j2 and k1-j1 > windowsize):
          print('WARN: distance between intervals {}{} | {}{}\n{} != {}'.\
                format((i1,j1),(k1,l1), (i2,j2),(k2,l2), k1-j1, k2-j2))
      else:
        if(i1-l1 != i2-l2 and i1-l1 > windowsize):
          print('WARN: distance between intervals {}{} | {}{}\n{} != {}'.\
                format((i1,j1),(k1,l1), (i2,j2),(k2,l2), i1-l1, i2-l2))

    #if(j1-i1 != j2-i2):
    #  print('WARN: interval size mismatch. {} {}'.format((i1,j1),(i2,j2)))
    #if(l1-k1 != l2-k2):
    #  print('WARN: interval size mismatch. {} {}'.format((k1,l1),(k2,l2)))

         #Check the that we're on a nonnull interval:
    #checkIntervalInIntervals(i1, j1, chromTOnonnullints[c1], windowsize)
    #checkIntervalInIntervals(k1, l1, chromTOnonnullints[c1], windowsize)
    #checkIntervalInIntervals(i2, j2, chromTOnonnullints[c2], windowsize)
    #checkIntervalInIntervals(k2, l2, chromTOnonnullints[c2], windowsize)


def scorePhysIntList(heatmaps: CoolerHeatmap, intervals: List[T_IntervalPair],
                     logtofile='', movelist=None, speclist=None)\
  -> Tuple[Tuple[float, float, float], List[float], List[T_HMCoord]]:
  """
  Return the heat for the given list of physical intervals.

  @param heatmaps:  the heatmaps
  @param intervals: list of interval pairs
  @param logtofile: the filename of the log file
  @param fuzzy:     enlarge the intervals by this many winsizes in
                    each direction
  @param movelist:  list of moves (for logging)
  @param speclist:  list of species each move was done on (for logging)

  @return: tuple of heat averages, list of heat values, and positions taken
  @rtype:  ((all, intra, inter), heatlist, coorinatelist)
  """
  both = []
  intra = []
  inter = []
  heatlist: List[float] = []
  poslist: List[T_HMCoord]  = []

  heatlist, poslist = zip(*(scoreIntervals(heatmaps, i) for i in intervals))

  for i, ((c1, i1), (c2, i2)) in enumerate(intervals):
    if(c1 == c2):
      intra.append(heatlist[i])
    else:
      inter.append(heatlist[i])

  both = heatlist

  if logtofile:
    writeScenario(logtofile, intervals, poslist, heatlist,
                  movelist, speclist)

  bothave = None
  intraave = None
  interave = None
  if len(both):
    bothave = np.nanmean(both)
  if len(intra):
    intraave = np.nanmean(intra)
  if len(inter):
    interave = np.nanmean(inter)

  return (bothave, intraave, interave), list(heatlist), poslist


def isIntraOnly(heatmaps):
  """
  Return true if the interchromosomal heatmap for chromsome 1 with 2 is missing.
  """
  return ('1',0) not in heatmaps or ('2',0) not in heatmaps[('1',0)]




def randomizePhysIntList(hms: CoolerHeatmap, intervals: T_IntervalPairList,
                         loclist: T_HMCoordList,
                         locdict: Optional[Dict[T_HMCoord, T_IntervalPair]]=None)\
  -> T_IntervalPairList:
  """
  Take the given interval pairs and move them to random locations in the
  chromosomes. Keep the interval sizes and distance between intervals constant.

  @param hms:         the heatmaps
  @param intervals:   list of interval pairs representing the intergenic 
                      (breakpoint) regions
  @param loclist:     the locations corresponding to the intervals
                      (each location is contained within the corresponding
                      interval)
  @param locdict:     maps already remapped indices to same location.
                      If this is not None, then store the randomized
                      location for use in a subsequent call.

  @return: a list of interval pairs
  """
  retlist: T_IntervalPairList = []
  assert(len(intervals) == len(loclist))

  for intpair, locpair in zip(intervals, loclist):
    if(locdict != None and locpair in locdict):  #If location already remapped:
      retlist.append(locdict[locpair])
      continue

    if(intpair[0][0] == intpair[1][0]):          #Intrachromosomal pair:
      retlist.append(randomizeIntraPair(intpair, hms, intpair[0][0]))

      #      #Output warnings if the new intervals cover NULL areas:
      #(c1,(i1,j1)), (c2,(i2,j2)) = retlist[-1]
      #checkIntervalOverlapsIntervals(i1, j1, nonnullints[c1], winsize)
      #checkIntervalOverlapsIntervals(i2, j2, nonnullints[c2], winsize)

    else:                                        #Interchromosomal pair:
      retlist.append(randomizeInterPair(intpair, hms))

    if(locdict != None):
      locdict[locpair] = retlist[-1]

  return retlist


def randomizeIntraPair(intpair: T_IntervalPair, heatmap: CoolerHeatmap,
                       chrm: T_Chrom)\
  -> T_IntervalPair:
  """
  Return a pair of intervals that have the same distance between them and
  the same sizes (except for cases at extremities) as the one given.
  If no such pair exists, then return the intpair.

  @param intpair: the intervals to randomize
  """
  (c1,(i1,j1)), (c2,(i2,j2)) = intpair
  switched = False
  if i2 < i1:                #if the second pair is left of the first,
    c1,i1,j1, c2,i2,j2 = c2,i2,j2, c1,i1,j1  #then switch the roles.
    switched = True

           #Check that the intervals do not overlap:
  dist = i2-j1 
  if dist < 0:
    if c1 == c2 and i1 == i2 and j1 == j2:
      dist = 0
    else:
      raise(Exception('Overlaping invervals: '+str(intpair)))

           #Find the location for the right endpoint of the left interval:
  loc = getRandomLocationWithDist(heatmap, chrm, j1-i1, dist, j2-i2)
  if not loc:
    return intpair

  if switched:
    start1 = max(loc-(j1-i1), 0)
    return ((c1,(loc+dist, loc+j2-j1)), (c1,(start1, loc)))
  else:
    start1 = max(loc-(j1-i1), 0)
    return ((c1,(start1, loc)), (c1,(loc+dist, loc+j2-j1)))


def randomizeInterPair(intpair: T_IntervalPair, heatmaps: CoolerHeatmap)\
  -> T_IntervalPair:
  """
  Return an interchromosomal pair of intervals that have the same size as the
  given pair, such that there is a heatmap value defined for the pair.

  @param intpair:   the pairs of intervals to randomize
  @param heatmaps:  the heatmaps
  """
  (c1,(i1,j1)), (c2,(i2,j2)) = intpair

  if heatmaps.nnz((c1, c2)):
    max1 = heatmaps.chromsizes()[c1]-(j1-i1) #If there is interchromosomal data...
    max2 = heatmaps.chromsizes()[c2]-(j2-i2) #Get pairs of indices within range:

    loc1 = random.randrange(max1)
    loc2 = random.randrange(max2)
    while not heatmaps.nnz(((c1, loc1, loc1+j1-i1), (c2, loc2, loc2+j2-i2))):
      loc1 = random.randrange(max1)
      loc2 = random.randrange(max2)

    return (c1, (loc1, loc1+j1-i1)), (c2, (loc2, loc2+j2-i2))

  else:                                #If there is no interchromosomal data...
    return intpair




def randomizePhysIntList_SLOW(nonnullints, indlist, chromlens, winsize, intervals,
                              loclist, locdict=None):
  """
  Take the given interval pairs and move them to random locations in the
  chromosomes. Keep the interval sizes and distance between intervals constant.

  @param nonnullints: dict of nonnull intervals in interchrom heatmaps
  @param indlist:     2D dict of all index pairs for non-null cells
  @param chromlens:   dictionary mapping chromosome to length
  @param winsize:     the size of the windows in the heatmap
  @param intervals:   list of interval pairs
  @param loclist:     the locations corresponding to the intervals
                      (only used if using locdict)
  @param locdict:     maps already remapped indices to same location
  """
  retlist = []
  assert(len(intervals) == len(loclist))

  for intpair,locpair in zip(intervals,loclist):
    if(locdict != None and locpair in locdict):  #If location already remapped:
      retlist.append(locdict[locpair])
      continue

    (c1,(i1,j1)),(c2,(i2,j2)) = intpair
    if(c1 == c2):                                #Intrachromosomal pair:
      switched = False
      if(j2 < i1):     #if the second pair is left of the first,
        c1,i1,j1, c2,i2,j2 = c2,i2,j2, c1,i1,j1  #then switch the roles.
        switched = True

      loc = getRandomLocationIntra(nonnullints[c1], j1-i1, j2-i2, i2-j1,
                                   winsize)
      if(loc is None):
        retlist.append(intpair)
      elif(switched):
        retlist.append(((c1,(loc+i2-i1,loc+j2-i1)), (c1,(loc,loc+j1-i1))))
      else:
        retlist.append(((c1,(loc,loc+j1-i1)), (c1,(loc+i2-i1,loc+j2-i1))))

            #Output warnings if the new intervals cover NULL areas:
      #(c1,(i1,j1)), (c2,(i2,j2)) = retlist[-1]
      #checkIntervalOverlapsIntervals(i1, j1, nonnullints[c1], winsize)
      #checkIntervalOverlapsIntervals(i2, j2, nonnullints[c2], winsize)

    else:                                        #Interchromosomal pair:
      if(c2 in indlist[c1]) and indlist[c1][c2]: #If there is data...
        max1 = chromlens[c1]-(j1-i1)
        max2 = chromlens[c2]-(j2-i2)       #Get pairs of indices within range:
        (loc1,loc2) = random.choice(indlist[c1][c2])       #Keep choosing pairs
        while(loc1*winsize > max1 or loc2*winsize > max2): #until we find one.
          (loc1,loc2) = random.choice(indlist[c1][c2])

        (loc1,loc2) = (loc1*winsize,loc2*winsize)

        retlist.append(((c1,(loc1,loc1+j1-i1)),(c2,(loc2,loc2+j2-i2))))
      else:                          #If there is no interchromosomal data...
        retlist.append(intpair)

    if(locdict != None):
      locdict[locpair] = retlist[-1]


  return retlist



def printExceptionalMoves(scenfiles, proportion, intra=True, inter=False,
                          ignoreset=set(), usefrac=False, useint=False):
  """
  Read the scenarios in the file and print moves that are hotter than
  the given proportion of the standard deviation.

  @param scenfiles:  list of files with scenarios.
  @param proportion: choose those greater than this proportion of the sd.
  @param intra:      include intrachromosomal moves.
  @param inter:      include interchromosomal moves.
  @param ignoreset:  list of chromosomes to ignore intrachrom values for.
  @param usefrac:    given proportion is of # of moves, not of the sd of heat.
  @param useint:     return intervals (x, y) where x = (x1,x2) and y = (y1,y2).
  """
  getExceptionalMoves(scenfiles, proportion, intra, True, ignoreset, usefrac,
                      useint)





def getMovesInOrder(scenfiles):
  """
  Read the scenarios in the file and return a list of moves as they
  occur in the scenarios.  The scenarios are sorted by the integer
  just before the ".pcl" in the filename extension.

  @param scenfiles:  list of files with scenarios.

  @return: list with elements
           (chr1, chr2, (int1,int2), (pos1,pos2), heat, move, species)
           where int1 is an interval and pos1 is a heatmap position,
           move is are the pairs of adjacency edges acted upon,
           and species is the side of the graph acted upon.
  """
  moves = []
  for i, scenfile in enumerate(orderScenarioFiles(scenfiles)):
    try:
      intlist, pointlist, heatlist, movelist, speclist = readScenario(scenfile)
    except Exception as e:
      print(str(e))
      continue

    for intervals, points, heat, move, species in zip(intlist, pointlist,
                                                      heatlist, movelist,
                                                      speclist):
      moves.append((points[0][0], points[1][0],
                    (intervals[0][1], intervals[1][1]),
                    (points[0][1], points[1][1]), heat, move, species))

  return moves




def getExceptionalMoves(scenfiles, proportion, intra=True, inter=False,
                        output=False, ignoreset=set(), usefrac=False,
                        useint=False):
  """
  Read the scenarios in the files and find moves that are hotter than
  the given proportion of the standard deviation, or a certain proportion of
  the hottest moves (if usefrac is True).

  @note: to get all the moves from the scenario, set proportion to 100 and
         usefrac to True.

  @param scenfiles:  list of files with scenarios.
  @param proportion: choose those greater than this proportion of the sd.
  @param intra:      include intrachromosomal moves.
  @param inter:      include interchromosomal moves.
  @param output:     print to stdout.
  @param ignoreset:  list of chromosomes to ignore intrachrom values for.
  @param usefrac:    given proportion is of # of moves, not of the sd of heat.
  @param useint:     return intervals (x, y) where x = (x1,x2) and y = (y1,y2).

  @return: list with elements (length, chr1, chr2, (x,y), heat, count)
           where x (resp. y) is a position in chr1 (resp. chr2) unless
           useint == True, and count is the number of times this move
           occurs.
  """
  allmoves = []
  heats = []
  for i, scenfile in enumerate(orderScenarioFiles(scenfiles)):
    try:
      intlist, pointlist, heatlist, movelist, speclist = readScenario(scenfile)
    except Exception as e:
      print(str(e))
      continue

    loclist = pointlist
    if(useint):
      loclist = intlist
    for locs, heat in zip(loclist, heatlist):#Choose moves that pass the
      if(heat != NULLV and not np.isnan(heat) and heat != 0):                      #chromosome tests.
        if(intra and locs[0][0] == locs[1][0] and
           locs[0][0] not in ignoreset):
          heats.append(heat)
          allmoves.append((locs, heat))
        if(inter and locs[0][0] != locs[1][0] and
           (locs[0][0] not in ignoreset and locs[1][0] not in ignoreset)):
          heats.append(heat)
          allmoves.append((locs, heat))

  ave = np.average(heats)
  dev = np.std(heats)
  if(output):
    print('ave of all moves:'+str(ave))
    print('dev of all moves:'+str(dev))

  locTOcount = defdict(int)
  for ((ci, i), (cj, j)), heat in allmoves:   #Remove duplicate moves:
    if(i > j):
      i,j = j,i
    locTOcount[(((ci, i), (cj, j)), heat)] += 1


  filteredlocTOcount = defdict(int)           #Select hottest moves:
  if(usefrac):
    moves = sorted(locTOcount.keys(), key=itemgetter(1), reverse=True)
    for key in moves[:int(len(moves)*proportion)]:
      filteredlocTOcount[key] = locTOcount[key]
  else:
    for (((ci, i), (cj, j)), heat), count in locTOcount.items():
      if(heat > ave+(proportion*dev)):
        filteredlocTOcount[(((ci, i), (cj, j)), heat)] = count

  retlist = []                                #Build the return value:
  for (((ci, i), (cj, j)), heat), count in filteredlocTOcount.items():
    if(useint):
      distance = abs(i[0]-j[0])/1000
      ik = (i[0]/1000, i[1]/1000)
      jk = (j[0]/1000, j[1]/1000)
    else:
      distance = abs(i-j)/1000
      ik = i/1000
      jk = j/1000
    retlist.append((distance, ci, cj, (ik,jk), heat, count))
    if(output):
      print(str(distance)+'k chr'+ci+', chr'+cj+' ('+str(ik)+','+str(jk)+') '+
            str(heat)+' x '+str(count))

  return retlist



def copyWithoutExtremes(scenarios, percentage, onesided=False):
  """
  Remove the moves that are farther than percentage number of std devs
  from the average heat.

  @param scenarios:  the scenarios to remove moves from.
  @param percentage: remove moves that are bigger than percentage*stdev+ave
                     and smaller than -percentage*stdev+ave.
  @param onesided:   only remove the big ones.

  @return:  the new list of scenarios with the percentage remaining
  """

  if(not scenarios[0]):                     #Empty list:
    return scenarios, np.nan

  elif(type(scenarios[0][0]) is not tuple): #Not intra moves:
    heats = filter(lambda x: x != NULLV and not np.isnan(x), chain(*scenarios))
    cutoff = np.average(heats)+(percentage*np.std(heats))
    #return [[h for h in s if h < cutoff and h > -cutoff] for s in scenarios]
    retval = []
    oldlen = 0
    newlen = 0
    for s in scenarios:
      retval.append([h for h in s if h < cutoff and (onesided or h > -cutoff)])
      oldlen += len(s)
      newlen += len(retval[-1])
    return retval, (newlen/float(oldlen))*100

  else:                                     #Intra moves:
    heats = filter(lambda x: x != NULLV and not np.isnan(x),
                   map(lambda x: x[1], chain(*scenarios)))
    cutoff = np.average(heats)+(percentage*np.std(heats))
    #return [[m for m in s if m[1] < cutoff and m[1] > -cutoff]
    #        for s in scenarios]
    retval = []
    oldlen = 0
    newlen = 0
    for s in scenarios:
      retval.append([m for m in s if m[1] < cutoff and
                                     (onesided or m[1] > -cutoff)])
      #print(set(s)-set(retval[-1]))
      oldlen += len(s)
      newlen += len(retval[-1])

    return retval, (newlen/float(oldlen))*100





def getBottomFrac(l, fraction):
  """
  Return the part of the list that is in the bottom fraction of this list.
  """
  assert(fraction <= 1 and fraction >= 0)
  return sorted(l)[:int(len(l)*fraction)]



def getTopFrac(l, fraction):
  """
  Return the largest fraction of this list.
  """
  assert(fraction <= 1 and fraction >= 0)
  return sorted(l)[int(len(l)*(1-fraction)):]



def getIntersection(movelists, fuzz=0, useint=False):
  """
  Return a list of moves that exist in all move lists.

  @param movelists: list of lists of moves with the form
                    (length, chrom1, chrom2, (x,y), heat, count)
  @param fuzz:      two points are considered equal if they are within this
                    many kilobases for both coordinates
  @param useint:    compare intervals instead of points
  """
                  #Group moves. e.g. movedict[chroms][loc] = equivalent moves.
  movedict = defdict(lambda: defdict(list))
  for gid, movel in enumerate(movelists):
    for rank, m in enumerate(sorted(movel, key=itemgetter(4), reverse=True)):
      chroms = (m[1],m[2])
      loc = m[3]
      if((chroms in movedict and loc in movedict[chroms]) or useint):
        movedict[chroms][loc].append((m,rank,gid))
      else:
        closelist = getCloseIn(movedict, chroms, loc, fuzz)
        if(closelist):
          closelist.append((m,rank,gid))
        else:
          movedict[chroms][loc].append((m,rank,gid))

                  #Choose the moves that exist in all datasets:
  intersection = []
  for c in movedict.keys():
    for (x,y), movel in movedict[c].items():
      if(numUniqueIn(movel) == len(movelists)):
        intersection.append(movel)

  return intersection




def getCloseIn(movedict, chroms, loc, fuzz):
  """
  Return a list that has a point close to this one (within fuzz) if it exists.

  @param movedict: movedict[chrompair][point] gives a list of moves
  @type movedict : dict of form movedict[(ci,cj)][(i,j)] = list()
  @param chroms:   the chromosome pair
  @param loc:      the (x,y) pair
  @param fuzz:     search for a point within this range (in kilobases)
  """
  return next((movedict[chroms][(x,y)] for (x,y) in movedict[chroms].keys()\
               if(abs(loc[0]-x) <= fuzz and abs(loc[1]-y) <= fuzz)), None)

def numUniqueIn(movelist):
  """
  Return the number of unique datasets that exist in the given list.
  """
  return len(set((i for m,r,i in movelist)))



def classifyScenScores(scenarios, mind=None, maxd=None, median=False,
                       output=True):
  """
  Classify and score the scenarios using given thresholds.
  Take a list of scenarios and return lists of averages.

  @param scenarios: tuple of list of lists (ref by [type][scenario][dcjmove])
                    where scenarios[0] holds intra, scenarios[1] holds inter,
                    and scenarios[2] holds both
  @param mind:      record intrachromosomal pairs with distance > threshold.
  @param maxd:      record intrachromosomal pairs with distance < threshold.
  @param median:    use the median of the scenario instead of the average.
  @param output:    True if we should output stats about the scenarios.

  @return: (sintra, sinter, sboth, ignored) where each is a list of values
           summarizing the moves in the scenario (for example average heat).
           ignored is the proportion of all moves that were ignored.
  @rtype:  sintra, sinter, and sboth are lists of floats.  ignored is a float.
  """
  ignoredcount = 0
  nullintra = 0
  intracount = 0
  sintra = []
  for intrascen in scenarios[0]:
    intra = []
    for (i,j), heat in intrascen:  #Get the moves of a scenario:
      intracount += 1
      if heat != NULLV and not np.isnan(heat):    #If it has HiC data.
        if (not mind or abs(i-j) >= mind) and (not maxd or abs(i-j) <= maxd):
          intra.append(heat)
        else:
          ignoredcount += 1
      else:
        nullintra += 1

    if intra:                      #Save the score for the scenario:
      if median:
        sintra.append(np.median(intra))
      else:
        sintra.append(np.average(intra))

  nullinter = 0
  intercount = 0
  sinter = []
  for interscen in scenarios[1]:
    inter = []
    for heat in interscen:
      intercount += 1
      if heat != NULLV and not np.isnan(heat):   #If it has HiC data.
        inter.append(heat)
      else:
        nullinter += 1
    if inter:
      if median:
        sinter.append(np.median(inter))
      else:
        sinter.append(np.average(inter))


  nullboth = 0
  count = 0
  sboth = []
  for scen in scenarios[2]:
    both = []
    for heat in scen:
      count += 1                   #If it was in an un HiCable area.
      if heat == NULLV or np.isnan(heat) or heat == 0:
        nullboth += 1
      else:
        both.append(heat)
    if both:
      if median:
        sboth.append(np.median(both))
      else:
        sboth.append(np.average(both))

        #Output:
  ignored = 1.0
  if output:
    print('MEANS:')
    if sintra:
      print('intra:'+str(np.nanmean(sintra)))
      print('inter:'+str(np.nanmean(sinter) if sinter else 'nan'))
      print('both: '+str(np.nanmean(sboth)))
      print('')
      if intracount:
        print('NULL INTRA:', str(nullintra)+'/'+str(intracount), '=', str(float(nullintra)/intracount))
      else:
        print('NULL INTRA:', str(nullintra)+'/'+str(intracount), '= 0')
      if intercount:
        print('NULL INTER:', str(nullinter)+'/'+str(intercount), '=', str(float(nullinter)/intercount))
      else:
        print('NULL INTER:', str(nullinter)+'/'+str(intercount), '= 0' )
      print('NULL BOTH: ', str(nullboth)+'/'+str(count), '=', str(float(nullboth)/count))
      if intracount:
        ignored = float(ignoredcount)/intracount
      else:
        ignored = 1.0
      print('IGNORED:   ', str(ignoredcount)+'/'+str(intracount), '=', str(ignored))
    else:
      print('skipped due to empty intra list.')

  return (sintra, sinter, sboth, ignored)



def getDistributionDistances(d1: Path, d2: Path, difference=False,
                             absolute=False):
  """
  Return the distance between the means in increments of standard deviations
  of the distributions in the given directory.

  @note: for d1 greater than d2 the value is positive, otherwise negative.

  @param d1:         directory with scenarios
  @param d2:         directory with scenarios
  @param difference: return just the difference, NOT in units of standard
                     deviations
  @param absolute:   use the absolute value

  @return: (intradist, interdist)
  """
  files1 = glob(str(d1)+'/*.pcl')
  files2 = glob(str(d2)+'/*.pcl')
  if(not files1):
    files1 = glob(str(d1)+'/*.pgz')
    files2 = glob(str(d2)+'/*.pgz')
  if(not files1):
    files1 = glob(str(d1)+'/*.pbz2')
    files2 = glob(str(d2)+'/*.pbz2')

  if(len(files2) != len(files1)):
    print('\nWARNING: # of scenarios mismatch between {d1} and {d2}:\n\t'+
          str(len(files1))+' != '+str(len(files2)))

  scens1 = readScenarios(files1)
  intral1,interl1,bothl1,skipped1 = classifyScenScores(scens1, output=False)
  scens2 = readScenarios(files2)
  intral2,interl2,bothl2,skipped2 = classifyScenScores(scens2, output=False)

  intradist = computeDistributionDist(intral1, intral2, difference, absolute)
  if(interl1 and interl2):
    interdist = computeDistributionDist(interl1, interl2, difference, absolute)
  else:
    interdist = np.nan

  return intradist, interdist


def computeDistributionDist(l1, l2, difference=True, absolute=False):
  """
  Return the distance between the means, in terms of multiples of standard
  deviation of values in l2.

  @param difference: return just the difference, NOT in units of standard
                     deviations
  @param absolute:   take the absolute value
  """
  if(not l1 or not l2):
    printerr('WARNING: empty list!\n{}\n\n{}'.format(l1,l2))

  ave1 = np.average(l1)
  #std1 = np.std(l1)
  ave2 = np.average(l2)

  if(difference):
    if(absolute):
      return abs(ave1-ave2)
    else:
      return ave1-ave2

  std2 = np.std(l2)
  if(std2):
    if(absolute):
      return abs((ave1-ave2)/std2)
    else:
      return (ave1-ave2)/std2
  else:
    return 0.0




# ____________________________________________________________________________ #
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|  GRAVEYARD  |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
