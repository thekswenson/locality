"""
This module contains globals used with multiprocessing to avoid copying memory.
In Linux this yields significant memory gains.
"""

#from .heatmap import T_PairHeatmaps

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#Types:

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#Matrices to be shared between threads:

#heatmap: T_PairHeatmaps = {}
#heatmat = {}
