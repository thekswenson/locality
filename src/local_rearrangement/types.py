"""
Types to be used with type hints.
"""
from typing import Tuple, NewType, List, Dict

T_Chrom = NewType('T_Chrom', str)               #chromosome
T_Pair = Tuple[int, int]                        #integer pair
T_Coord = Tuple[T_Chrom, int]                   #genome coordinate
T_Interval = Tuple[T_Chrom, T_Pair]             #genome interval
T_IntervalPair = Tuple[T_Interval, T_Interval]  #pair of intervals
T_IntervalPairList = List[T_IntervalPair]       #list of pairs of intervals

T_HMCoord = Tuple[T_Coord, T_Coord]                   #heatmap coordinate
T_HMCoordHeatList = List[Tuple[T_HMCoord, float]]     #hm coordinates with heat
T_HMCoordList = List[T_HMCoord]                       #list of hm coordinates
T_HMCoordDict = Dict[T_Chrom, Dict[T_Chrom, T_Pair]]  #dict of hm coordinates

EMPTY_COORD = (T_Chrom(''), -1)                 #empty genome coordinate
EMPTY_HMCOORD = (EMPTY_COORD, EMPTY_COORD)      #empty heatmap coordinate

T_SideSpec = NewType('T_SideSpec', int)       #value from set 

T_NodeID = NewType('T_NodeID', int)           #for AGNode
T_GeneEnd = NewType('T_GeneEnd', int)         #for AGNode
T_MoveNode = Tuple[T_NodeID, T_GeneEnd]       #node acted upon by a DCJ
T_MoveEdge = Tuple[T_MoveNode, T_MoveNode]    #edge acted upon by a DCJ
T_Move = Tuple[Tuple[T_MoveEdge, T_MoveEdge],
               Tuple[T_MoveEdge, T_MoveEdge]] #DCJ move
