#!/usr/bin/env python
# Krister Swenson                                                Winter 2012-13
#
"""
Functions to handle repeats given by RepeatMasker files.

rmsk file format at:
U{http://genome.ucsc.edu/cgi-bin/hgTables?hgsid=279999539&hgta_doSchemaDb=hg19&hgta_doSchemaTable=rmsk}
"""

import sys
import os
import pickle
import numpy

from collections import defaultdict

CLASS  = 0
FAMILY = 1
NAME   = 2
BLACK_LIST = {'Simple_repeat'}   #Ignore these repeats.
WHITE_LIST = {}            #Only use these repeats.

def getRepeats(filename, usepickle=False, makepickle=False):
  """
  Get the RepeatMasker info.
  Return a dict (indexed by chromosome), of lists of pairs (t1,t2)
  where t_ is a tuple of the form (chrom, interval).

  Each list is sorted by t1's first index of the interval.

  Pickle it if make_pickle is True.
  """
  if(filename):
    print('reading in repeat data...')
    data = readRepeats(filename)
    if(usepickle):
      print('pickling data...')
      with open(usepickle, 'wb') as f:
        pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)
  else:
    if(usepickle):
      if(os.path.isfile(usepickle)):
        print('unpickling repeat data...')
        with open(usepickle, 'rb') as f:
          data = pickle.load(f)
      else:
        sys.exit(f"ERROR: pickle file '{usepickle}' not found.")
    else:
      sys.exit("ERROR: no input repeats file given.")

  return data





def readRepeats(filename):
  """
  Read in a RepeatMasker file.
  Return a dict (indexed by chromosome), of lists of tuples
  ((start,end),(class,family,name)).

  Each list is sorted by start index.
  """
  global BLACK_LIST, WHITE_LIST

  thelist = defaultdict(list)           #The return value.

  with open(filename) as f:
    for line in f:
      line = line.rstrip().lstrip()
      if(line):
        v = line.split('\t')
        chrom = v[5]
        start = int(v[6])
        end = int(v[7])
        clss = v[11]
        fam = v[12]
        name = v[10]
        #print(chrom,start,end,clss,fam,name)
        if((not WHITE_LIST or
            (clss in WHITE_LIST or fam in WHITE_LIST)) and
           (clss not in BLACK_LIST and fam not in BLACK_LIST)):
          thelist[chrom].append(((start,end),(clss,fam,name)))

        #Sort the lists:
  for v in thelist.values():
    v.sort(key=lambda x: x[0][0])

  return thelist




def printRepeatsStats(selfalign):
  """
  Print information about the given self alignment.
  """
  lengths = []
  for l in selfalign.values():
    for v in l:
      lengths.append(v[0][1]-v[0][0])

  print('ave length:',numpy.average(lengths))
  print('max:',max(lengths))
  print('min:',min(lengths))
