---
################## BASIC CONSTANTS ##############################

SPECIES_A: 'albimanus'
SPECIES_B: 'stephensi'

HICDIR: 'hiC/'        #Root of directories where the Hi-C data are stored.
OUTDIR: 'output/'     #Root of directories where the output will be stored.
SCENDIR: 'scenarios/' #Subdirectory where the scenarios will be stored.

################## DATASET CONSTANTS and FLAGS ###################

  #Basic Parameters:
REPS: 300            #Number of scenarios.
GREEDY_REPS:  0       #Number of greedy scenarios to make.

NO_CLOBBER: True      #Don't recompute scenario if they are there
                      # (i.e. the directory has scenarios in it).

MAX_THREADS: 45       #Maximum number of threads to be used by localdist.
USE_MEDIAN: False     #Scenario's score is the median instead of the mean.

  #Scenarios:
REVERSE: True        #Score reversed scenarios using the other species' heatmap
INTERSECTION: False   #Score combination heatmaps (see COMBINATIONS_* below).
COMBINATIONS_FULL: False #Combination heatmaps (with difference and symdiff).
GREEDY: False         #Build greedy scenarios (VERY slow!).


################## DATASET DEFINITIONS #######################################
#
# The format for a DATASET entry is:
#  (speciesname, scenariodir,
#   syntenyfile,
#   heatmap, hmtype, resolution, intersectid)
#
# Note that the species synteny file should contain species with -speciesname-
# in the first column, and that the heatmap should be from this species.
 
DATASETS:
  #- >-
  #  ('albimanus', 'alb_ste',
  #   'resources/synteny/anopheles/albimanus_stephensi_synteny.tsv',
  #   'anopheles/AalbS2_V4', 'NONE', '.mcool', 100000, 'albste')
  #- >-
  #  ('stephensi', 'ste_alb',
  #   'resources/synteny/anopheles/stephensi_albimanus_synteny.tsv',
  #   'anopheles/AsteI2_V4', 'NONE', '.mcool', 100000, 'stealb')
  - >-
    ('albimanus', 'alb_ste',
     'resources/synteny/anopheles/albimanus_stephensi_synteny.tsv',
     'anopheles/AalbS2_V4', 'LA', '.cool', 100000, 'albste')
  - >-
    ('stephensi', 'ste_alb',
     'resources/synteny/anopheles/stephensi_albimanus_synteny.tsv',
     'anopheles/AsteI2_V4', 'LA', '.cool', 100000, 'stealb')

################## REVERSING SCENARIOS ################################

PARTNER_DATASET:    #map a dataset to its partner (for reversing scenarios)
  'alb_ste': 'ste_alb'
  'ste_alb': 'alb_ste'

################## STATISTICS ##############################################

T_TESTS: True          #Do the paired Student's T tests on actual vs. random.
T_TESTS_PER: True      #Do single-value T-test on actual scenario vs. randoms
                       #(set PERSCEN_RANDREPS and PERSCEN_REPS).
T_TESTS_MIX: False     #Do the paired Student's T tests on actual vs. random
                       #scenarios where Mpaths are mixed before other DCJs.
T_TESTS_SPLIT: False   #Do the paired Student's T tests on actual vs. random
                       #scenarios where Mpaths are split before other DCJs.
PERSCEN_RANDREPS: 20   #Randomize each scenario this many times.
PERSCEN_REPS: 20       #Do this many PERSCEN tests (must be <= REPS).

T_TESTS_SCRAM_V_SCRAM: False #Both on scrambled matrices.
T_TESTS_SCRAM_V_NON: False   #Actual on scrambled vs. random on non-scrambled.
SCRAM_PVAL: False      #Do many scrambles and compute the p-value that the
                       #actual and random have the distance that they do.
SCRAM_PVAL_REPS: 20    #Base the p-value on this many scrambled matrices.


################## PLOTS ##############################################

  #Switches:
PLOT_DISTS: True             #Plot random against actual scenarios.
PLOT_WINDOW: True           #Plot by distance with a sliding window.
PLOT_PERSCEN: True          #Plot random against actual by individual move.
PLOT_WANDER: True           #Plot random wandering scenarios against true.
PLOT_FREQ: True              #Plot the contacts by the frequency of moves.
PLOT_IGNORE: True            #Plot random/actual while ignoring common moves.
PLOT_PERMOVE: True          #Plot random against actual by individual move.
PLOT_SCRAM_V_NON: False      #Plot actual (on scrambled mats) against random.
PLOT_SCRAM_V_SCRAM: False    #Plot actual vs random (both on scrambled mats).

                            #Reversed plots must also have REVERSE set to True:
PLOT_REVERSED: True         #Plot scenario distribution against its reverse.
PLOT_RAND_REVERSED: True    #Plot random distribution against reversed random.
PLOT_REV_REV: True          #Plot random reversed against actual reversed.
PLOT_REVONOTHER: True       #Plot reversed against actual for other species.

PLOT_GREEDYONACTUAL: False   #Plot distribution with the greedy average.
PLOT_GREEDYREVONREV: False   #Plot reversed dist with the rev greedy average.
PLOT_GREEDY_REVERSED: False  #Add line for reversed average of greedy
                             #to the PLOT_GREEDY plot.

  #Plot Colors:
GREEDY_COLOR: green       #Color to make line for greedy plots.
REV_GREEDY_COLOR: orange  #Color to make the reversed greedy line in plots.

################## COMBINE DATASETS ####################################

  #For combining and comparing comparing datasets:
INTERSECDIR: 'intersection/'
SYMDIFFDIR: 'symdiff/'
DIFFDIR: 'difference/'

  #For the creation of intersection matrices (array of comboids):
COMBINATIONS_NAME: 'lanorm'
COMBINATIONS_INTRA: ['gm06690n', 'k562n', 'hesc', 'imr90', 'hela25FA', 'k562M', 'helaM', 'hffM', 'helaNS', 'hffNS']
COMBINATIONS_INTER: ['gm06690n', 'k562n', 'helaALL', 'k562M', 'helaM', 'hffM', 'helaNS', 'hffNS']

#COMBINATIONS_NAME: 'formald'
#COMBINATIONS_INTRA: ['helaALL', 'hela1FA', 'hela25FA'] #Formaldehyde
#COMBINATIONS_INTER: ['helaALL', 'hela1FA', 'hela25FA'] #Formaldehyde
